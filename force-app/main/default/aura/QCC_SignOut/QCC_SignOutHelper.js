({
    closeAllTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getAllTabInfo().then(function(response) {            
            for(var i = 0; i < response.length;i++){
                var tab = response[i];
                console.log('tabId='+tab.tabId);
                if(tab.closeable && !tab.isSubtab){
                    workspaceAPI.closeTab({tabId: tab.tabId});
                }
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    reassignCases : function(component, event, helper) {
        var action = component.get('c.caseAssignedBack');
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('Successfully reassign Cases!');
                window.location.replace(component.get('v.baseUrl')+"/secur/logout.jsp");
            }
        })
        $A.enqueueAction(action);
    }
})