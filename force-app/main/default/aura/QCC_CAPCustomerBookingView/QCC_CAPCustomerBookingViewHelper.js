({
    getLocalList: function(component, pageNumber) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, "slds-hide");
        var recID = component.get("v.recordId");

        var action = component.get("c.fetchCAPBooking");
        action.setParams({
            "recordId": recID,
            "pageNumber": pageNumber
        });
        
        action.setCallback(this, function(response) {
            this.doLayout(response, component, pageNumber);
        });
        
        $A.enqueueAction(action);
    },
    doLayout: function(response, component, pageNumber) {
        var data = response.getReturnValue(); 
        console.log("data#####",data);
        var state = response.getState();
        component.set("v.hideNext", true);
        /*var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");*/
        var lstCurrentVal = [];
        console.log("state###",state);
        if(state === "SUCCESS" && data != null){
            if(data.bookings){
                component.set("v.lstCurrentBook",data.bookings); 
                console.log("ZZZdata.bookings#####",data.bookings);
                console.log("ZZZ resp date: "+data.bookings[0].departureTimeStamp);
                
            }
            console.log("data.nextPageNumber#####",data.nextPageNumber);
            if(data.nextPageNumber != undefined ){
                component.set("v.nextPageNumber", data.nextPageNumber);
                component.set("v.hideNext", false); 
                component.set("v.pageNumber", data.nextPageNumber-1);
            }
            if (data.nextPageNumber == undefined) {
                var pageNum = component.get("v.nextPageNumber");
                component.set("v.pageNumber",pageNum);
            }
        }else if (state === "ERROR") {
            var error = "Error";
            component.set("v.lstCurrentBook",lstCurrentVal);
            component.set("v.hideNext", false);
        } else if (state === "INCOMPLETE") {
            var error = "Error";
            component.set("v.lstCurrentBook",lstCurrentVal);
            component.set("v.hideNext", false);
        }
        component.set("v.serverCalled", true);
    },
    currentpageRecord: function(component, pageNumber){
        
        component.set("v.hidePrev", true);
        component.set("v.hideNext", true);
        
    }
})