({
    closeModal: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
   
    Search: function(component, event, helper) {
            helper.SearchHelper(component, event);
    },
        //Process the selected Frequent Flyers
    handleSelectedFFs: function(component, event, helper) {
        var allFFs = component.get("v.freqFlyerList");
        console.log('AllFFs'+JSON.stringify(allFFs));
        var selectedFFs = [];
        var checkvalue = component.find("checkFreqFlyer");
        console.log('InsidehandleSelectedFFs'+checkvalue); 

        if(!Array.isArray(checkvalue)){
            console.log('IFCONDITION'); 
            if (checkvalue.get("v.value") == true) {
                var index = checkvalue.get("v.text");
                console.log('index'+index);
                var myKey = allFFs[index];
                selectedFFs.push(myKey);
                //selectedFFs.push(checkvalue.get("v.text"));
                console.log('selectedFFs-' + JSON.stringify(selectedFFs));
            }
        }else{
            for (var i = 0; i < checkvalue.length; i++) {
                console.log('For loop checkvalue'); 
                if (checkvalue[i].get("v.value") == true) {                    
                    var index = checkvalue[i].get("v.text");
                    console.log('Index'+index);
                    var myKey = allFFs[index];
                    selectedFFs.push(myKey);
                    //selectedFFs.push(checkvalue.get("v.text"));
                    console.log('selectedFFs-' + JSON.stringify(selectedFFs));
                }
            }
        }
        component.set("v.selectedFlyers",selectedFFs);
        console.log('type-' + typeof selectedFFs);
        
        //console.log('selectedFFs-' + selectedFFs);
        helper.CreateHelper(component, event);
    },
    
    //Select all Frequent Flyers
    handleSelectAllFF: function(component, event, helper) {
        var getID = component.get("v.freqFlyerList");
        var checkvalue = component.find("selectAll").get("v.value");        
        var checkFreqFlyer = component.find("checkFreqFlyer"); 
        if(checkvalue == true){
            for(var i=0; i<checkFreqFlyer.length; i++){
                checkFreqFlyer[i].set("v.value",true);
            }
        }
        else{ 
            for(var i=0; i<checkFreqFlyer.length; i++){
                checkFreqFlyer[i].set("v.value",false);
            }
        }
    },
     
     ReqTypechange : function(component, event, helper) {
         var pickReqTypeval = event.getSource().get("v.value");
         console.log('reqtypevalue-:::::'+pickReqTypeval);
		//var pickReqTypeval = component.find("ReqType").get("v.value")
	},
    
    UpgTierchange : function(component, event, helper) {
         var pickUpgTierval = event.getSource().get("v.value");
   		 console.log('pickUpgTierval-'+pickUpgTierval);
		//var pickUpgTierval = component.find("UpgradeTierPicklist").get("v.value");
        
	}, 
})