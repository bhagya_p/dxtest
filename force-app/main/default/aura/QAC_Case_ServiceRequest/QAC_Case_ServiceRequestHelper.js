({
    refreshBookingOnSuccess : function(component, event) {
        var navigationItemAPI = component.find("navigationItemAPI");
        console.log("inside refresh of Booking");
        navigationItemAPI.refreshNavigationItem().then(function(response) {
            console.log(response);
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    selectRecordType : function(component, event){
        var action = component.get("c.getRecordTypeId");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // set current user information on userInfo attribute
                component.set("v.recordTypeId", storeResponse);
            }
        });
        $A.enqueueAction(action);        
    }    
})