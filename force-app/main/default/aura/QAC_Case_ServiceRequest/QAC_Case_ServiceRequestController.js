({    
    doInit : function(component, event, helper) {
        component.set("v.showSpinner", false);
        
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // set current user information on userInfo attribute
                component.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleApplicationEvent : function(component, event, helper) 
    {
        console.log("QAC_Case_ServiceRequest.handleApplicationEvent: entry");
        var params = event.getParams();
        if(params.pnrInformation != null){
            component.set("v.pnrInformation", params.pnrInformation);
        }        
        console.log("QAC_Case_ServiceRequest.handleApplicationEvent: exit");
    },
    
    handleBack : function(component, event, helper) {
        component.set("v.resetForm", false);
        
        var cmpEvent = component.getEvent("bubblingEvent");
        console.log('cmpEvent: ' + cmpEvent);
        cmpEvent.setParams({"ComponentAction" : 'showPassengers' });
        cmpEvent.fire(); 
    },
    
    handleInsert : function(component, event, helper) {
        var accId = component.find("srAccountId").get("v.value");
        console.log("accId**"+accId);
        component.set("v.showSpinner", true);
        if(accId != null && accId != ''){
            console.log('Insert Case');
            component.find("editFormSR").submit();    
        }
        else{
            component.set("v.showSpinner", false);
            console.log('Show Error');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Information Required',
                message: 'Select Account Information',
                messageTemplate: 'Record {0} created! See it {1}!',
                duration:' 8000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
        }
    },
    handleErrors : function(component, event, helper) {
	    component.set("v.showSpinner", false);
    	window.scrollTo(0,0);
	},
 
	handleSuccess : function(component, event, helper) {
    
	    // component.set("v.showSpinner", true);
        var caseId;
        var flights = component.get("v.pnrInformation.flightDetailsWrapper");
        var passengers = component.get("v.pnrInformation.passengerDetailsWrapper");
        var caseFields = component.get("v.pnrInformation.caseFields");
        console.log('Passengers List ' + passengers);
    
        //Calling the Apex Function
        var action = component.get("c.createCase");
        var payload = event.getParams().response;
        console.log(payload.id);
        component.set('v.recordId', payload.id);
        //Json Encode to send the data to Apex Class
        var Strflights = JSON.stringify(flights);
        var StrPassengers = JSON.stringify(passengers);
        var StrCaseFields = JSON.stringify(caseFields);
        //Setting the Apex Parameter

	    action.setParams({
            "flights" : Strflights,
            "passengers": StrPassengers,
            "caseFields" : StrCaseFields, 
            "caseId" : component.get('v.recordId')
	    });
    
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                console.log( 'check case id ' + a.getReturnValue());
                console.log( 'check case id ' + payload.Id	);
                caseId = a.getReturnValue();
                
                var cmpEvent = component.getEvent("bubblingEvent");
                console.log('cmpEvent: ' + cmpEvent);
                cmpEvent.setParams({"ComponentAction" : 'showSearch' });
                cmpEvent.fire();
                
                helper.refreshBookingOnSuccess(component,event);
                
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": caseId
                });
                navEvt.fire();
                component.set("v.showSpinner", false);
            } 
            else if(state == "ERROR"){
                component.set("v.showSpinner", false);
                console.log('Exception ' + state);
            }
        });
        $A.enqueueAction(action);
	},
        
    onChangeType: function(component, event, helper) {
        component.set("v.refreshFlag", true);
    },
        
    resetCaseFields : function(component,event,helper){
        console.log("inside Reset");
        window.scrollTo(0,0);
        component.set("v.resetForm", true);
        
        helper.selectRecordType(component,event);
        
        var pnrInfo = component.get("v.pnrInformation");
        console.log('Check accountId ' + pnrInfo.caseFields.iataAccount.Id);
        component.find("srAccountId").set("v.value", pnrInfo.caseFields.iataAccount.Id);
    }
})