({
	closeModal : function(component, event, helper) {
        component.set("v.showModal", false);
    }
})