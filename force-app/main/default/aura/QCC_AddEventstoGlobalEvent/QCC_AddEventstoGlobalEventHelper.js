({
    initData: function(component){
        var action = component.get('c.getFieldSets');
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if (state === "SUCCESS")
            {
                component.set('v.listFieldInFieldSet', actionResult.getReturnValue().listFieldSets);
                component.set("v.columns", JSON.parse(actionResult.getReturnValue().columnSets));
            }
        });
        $A.enqueueAction(action);
    },
    getDataEvents: function(component, helper){
        var action = component.get('c.getDataEvents');
        var lstFieldSetMember = component.get("v.listFieldInFieldSet");
        var lstColumns = component.get("v.columns");
        action.setParams({
            lstFieldSetMemberStr : JSON.stringify(lstFieldSetMember),
            lstColumnsStr : JSON.stringify(lstColumns)
        });
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if (state === "SUCCESS")
            {
                component.set('v.data', actionResult.getReturnValue());
                console.log('==actionResult.getReturnValue()==');
                console.log(actionResult.getReturnValue().length);
                
                //sort if column is defined
                var fieldName = component.get("v.sortedBy");
                var sortDirection = component.set("v.sortedDirection");
                if(fieldName != null && sortDirection != null){
                    this.sortData(component, fieldName, sortDirection);
                }
                

                if(actionResult.getReturnValue().length == 0){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "success",
                        "title": "Search Results",
                        "message": "No record found.",
                        duration : 5
                    });
                    resultsToast.fire();
                }
                
            }
        });
        $A.enqueueAction(action);
    },
    doAddEvents: function(component){
        
        var selectedIds = component.get("v.selectedIds");
        var recordId = component.get('v.recordId');

        var action = component.get('c.addEventsToGlobalEvent');
        action.setParams({ 
            lstEventId : selectedIds,
            globalEventId :  recordId           
        });
        
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if (state === "SUCCESS")
            {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "success",
                    "title": "Add Events",
                    "message": "Add events to global event success.",
                    duration : 5
                });
                resultsToast.fire();

                this.getDataEvents(component);
                component.set("v.selectedRows", []);
                var refreshEvent = $A.get("e.force:refreshView");
                if(refreshEvent != null)
                    refreshEvent.fire();
            }else if (state === "ERROR") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "error",
                    "title": "Add Events",
                    "message": "Some errors happen.",
                    duration : 5
                });
                resultsToast.fire();

                var refreshEvent = $A.get("e.force:refreshView");
                if(refreshEvent != null)
                    refreshEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.data", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x.hasOwnProperty(field) ? (typeof x[field] === 'string' ? x[field].toLowerCase() : x[field]) : 'aaa')} :
            function(x) {return x.hasOwnProperty(field) ? (typeof x[field] === 'string' ? x[field].toLowerCase() : x[field]) : 'aaa'};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {            
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
        /*
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
        */
    }
})