({
	init : function(component, event, helper) {
		helper.initData(component);
        console.log(component.get('v.recordData'));
	},
    resetData : function(component, event, helper) {
        console.log('==resetData==');
        var listFieldInFieldSet = component.get('v.listFieldInFieldSet');
        for(var i = 0; i < listFieldInFieldSet.length; i++){
            listFieldInFieldSet[i].fieldValue = '';
        }
        component.set('v.listFieldInFieldSet',listFieldInFieldSet);
        component.set('v.data', []);
    },
    searchData: function(component, event, helper) {
        // check have at least one condition
        var listFieldInFieldSet = component.get('v.listFieldInFieldSet');
        var isNotInputCondition = true;
        for(var i = 0; i < listFieldInFieldSet.length; i++){
            if(listFieldInFieldSet[i].fieldValue){
                isNotInputCondition = false;
                break;
            }
        }
        if(isNotInputCondition){
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "type": "error",
                "title": "Search Events",
                "message": "Please input at least one search condition.",
                duration : 5
            });
            resultsToast.fire();
        } else {
            helper.getDataEvents(component);
        }
    },
    doAddEvents: function(component, event, helper) {
        var listEventSelected = component.get("v.listEventSelected");
        var selectedIds =[];
        for (var i = 0; i < listEventSelected.length; i++){
            selectedIds.push(listEventSelected[i].Id);
        }
        component.set('v.selectedIds', selectedIds);
        var recordData = component.get('v.recordData');
        if(recordData.Status__c == 'Closed'){
            alert('can not add to closed Event');
        }else if(selectedIds.length == 0){
            alert('Please select at least 1 record.');
        }
        else{
            helper.doAddEvents(component, helper);
        }
        
    },
    getSelectedItems: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.listEventSelected',selectedRows);
    },
    // Client-side controller called by the onsort event handler
    updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    }
})