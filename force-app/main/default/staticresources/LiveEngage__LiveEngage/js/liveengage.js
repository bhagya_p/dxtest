var LastSearchTerm = '';
var ChatFlag = '';
var SLDSPath = ''
var ActionsString = '';
var ChatAttachedComplete = '';
var IndicatorString = '';
var IndicatorTypeString = '';
var CurrentView = '';
var CurrentDataView = '';
var SFObjectArray = new Array();
var SDKSkills = '';
var CurrentObjectId = '';
var bubbleClicked = '';
var LoadSDK = '1';
var DisbaleSearchInput = '0';
var DisableOnLoadSearch = '0'
var SDKSearchTerms = new Array();
var SDKDefaultVals = new Array();
var SDKDefaultValsComplete = '0';
var InitialStart = '1';
var accountId = '0';
var sessionId = '0';
var sessionTime = '0';

var SkillName = '';
var chatId = '';
var IconsVisible = false;
var DebugLog = 0;
var SDKSearchCount = 0;
var $relatedParent;
var SaveComplete = 0;
var chatStartTimeErrors = 0;
var crudtype = 'NONE';
var InitComplete = 0;
var LastObjectSearch = 'People';
var SkillSet = 0;
var isMessaging = 0;
var MessagingSet = 0;
var ChatInfoObj = new Object();
var Open


$(document).ready(function() {

    if (queryString('debug') == 1) {
        DebugLog = 1;
        $('#ShowLogLink').show();
    }

    if (DebugLog == 1) {
        addLog('Starting Document Ready');
        var _LTracker = _LTracker || [];

    }

    if (LoadSDK == '0') {
        searchString = LastSearchTerm;

        console.log('search string: ' + searchString);
        //
        if (DebugLog == 1) {
            addLog('SDK Init called');
        }
    }
    if (CurrentDataView != 'ObjectView') {
        console.log('intitializing');
        initialize();
        if (DebugLog == 1)
            addLog('initialize() Line 46');
    }

    if (LoadSDK == '1') {

        //Load the SDK 
        LoadLPSDKFunctions();
        LastSearchTerm = '';
        LoadSDK = '0'
    }

    if (CurrentDataView == 'ObjectView') {
        ShowLinkSuccess();
        LoadActionFunctions();
        //StartChatTranscriptSave();

        if (DebugLog == 1)
            addLog('StartChatTranscriptSave() Line 54');
    }



    if (DisbaleSearchInput == '1')
        $('.token-input').prop('disabled', true);





});

(function($) {
    $.each(['show', 'hide'], function(i, val) {
        var _org = $.fn[val];
        $.fn[val] = function() {
            this.trigger(val);
            _org.apply(this, arguments);
            if ($(this).attr('id') == 'ChatAlreadyLink') {
                var h = $(document).height()
                var s = $(document).scrollTop()
                var newTop = ((h - s) / 2)
                $('.ChatUnlink-Container').css('top', newTop + 'px')
            }

        };
    });
})(jQuery);

function initializeReload() {
    initialize();
    ReloadSeach();
}

function initialize() {



    InitialStart = '1';
    console.log("initialize ready!");

    $('#searchTerm').tokenfield();
    searchTerm = LastSearchTerm;

    console.log("DATAVIEW: " + CurrentDataView);


    if (CurrentDataView == 'Search' && searchTerm.length > 0) {
        console.log("Searching ");
        $('#searchTerm').tokenfield('setTokens', searchTerm);
        //DoSearch();
    }




    $('#ObjectSelectDropdown').append($('<option>', {
        value: "People",
        //text: tmp.label,
        "data-imagesrc": SLDSPath + "/assets/icons/standard/groups_60.png",
        "data-description": "",
    }));

    $('#ObjectSelectDropdown').append($('<option>', {
        value: "All",
        //text: tmp.label,
        "data-imagesrc": SLDSPath + "/assets/icons/standard/topic_60.png",
        "data-description": "",
    }));



    //console.log('ARRAY ; ' + SFObjectArray);
    for (var i = 0; i < SFObjectArray.length; i++) {
        var tmp = JSON.parse(SFObjectArray[i])
        if (tmp.isCustom == 'true') {
            $('#ObjectSelectDropdown').append($('<option>', {
                value: tmp.label,
                "data-src": tmp.name,
                "data-imagesrc": tmp.logoUrl,
                //"data-description": tmp.name
            }));
        } else {
            $('#ObjectSelectDropdown').append($('<option>', {
                value: tmp.label,
                "data-src": tmp.name,
                //text: tmp.label,
                //"data-imagesrc": "{!URLFOR($Resource.SLDS090)}/" + tmp.logoUrl,
                "data-imagesrc": SLDSPath + "/" + tmp.logoUrl,
                //"data-description": tmp.name
            }));
        }

    }


    $('#ObjectSelectDropdown').ddslick({
        showSelectedHTML: true,
        width: 60,
        background: '#F3F5F4',
        onSelected: function(selectedData) {
            ChangeObjectSelectionInit(selectedData.selectedData.value);
        }

    });

    $('.dd-option-value').each(function(index) {
        v = $(this).attr('value');
        $(this).parent().attr('title', v);
    });



    if (CurrentDataView == 'Search') {

        AddIcons();
        //ShowLinkSuccess();
        LoadActionFunctions();
        LoadPopoverData();

        if (ChatFlag == 'true') {
            LoadIndicatorIcons();
        }


    }

    if (CurrentDataView == 'ObjectView' && searchString.length > 0) {
        $('#searchTerm').tokenfield('setTokens', searchString);
    }

    $('#searchTerm-tokenfield').keypress(function(e) {
        var key = e.which;

        if (key == 13) // the enter key code
        {
            $('#searchTerm').tokenfield('createTokensFromInput');
            if (DebugLog == 1)
                addLog('DoSeach');
            DoSearch();
            return false;
        }
    });

    InitialStart = '0';


    if (_ReturnToSearch == 1) {
        $('#ObjectSelectDropdown li a').each(function(idx) {
            //console.log(index + ": " + $(this).attr('title'));
            console.log('TITLE: ' + $(this).attr('title') + ' :: ' + CurrentView);
            if ($(this).attr('title') == CurrentView) {

                $('#ObjectSelectDropdown').ddslick('select', { index: idx });

            }
        });
        _ReturnToSearch = 0;
    }

    //CheckSkillsLoaded();
    //ResetInfoContainer() ;

    SetNoChatPopover();



}

//Prevent Double Button Click
function PreventDoubleClick(thisButton) {
    $(thisButton).attr('disabled', true);
    $(thisButton).unbind('click');
}

function scrollToTop() {
    $(document).scrollTop(0);
}

function SetNoChatPopover() {
    $('[data-toggle="popover_actions_nochat"]').ggpopover();
}

function CheckSkillsLoaded() {
    /*
    if (SkillName.length == 0) {
        $('.pending-skillaccess').show();
        $('.entireBlock').hide();
    } else {
        $('.pending-skillaccess').hide();
        $('.entireBlock').show();
    }
    */
}

var _ReturnToSearch = 0;

function ChangeObjectSelectionInit(val) {
    if (InitialStart == '0') {
        ChangeObjectSelection(val);
    }
}

function CleanUrl() {
    var clean_uri = location.protocol + "//" + location.host + location.pathname;
    window.history.replaceState({}, document.title, clean_uri);
}

function ResetInfoContainer() {

    $('.DataObjectSection').click(function(e) {

        if (bubbleClicked.length > 0 && bubbleClicked != $(this).attr('Id')) {
            $(this).next().hide();
        }

        if (bubbleClicked == $(this).attr('Id'))
            bubbleClicked = '';
        else {
            bubbleClicked = $(this).attr('Id');
        }
    })

    $('.parent-hover-container').mouseleave(function(e) {
        e.stopPropagation();
        if (bubbleClicked.length == 0)
            $(this).find('.hover-container').hide();
    })

    $('body').click(function(e) {
        e.stopPropagation();

        var subject = $('.hover-container');
        if (!subject.has(e.target).length) {

            if (bubbleClicked.length > 0) {
                $('#' + bubbleClicked).ggpopover('hide')
                bubbleClicked = '';
                $('.hover-container').hide();
            }
        }
    })

    $('.parent-hover-container').mouseover(function(e) {

        e.stopPropagation();
        $(this).find('.hover-container').show();
    })



}

function RelatedObjectShow() {
    $('.panel-' + relatedParent).collapse('show');
    window.scrollTo(0, document.body.scrollHeight);
}
var pendingDataLoad = '';
$(document)
    .on('click', '.panel-heading span.clickable', function(e) {

        var $span = $(this).parents('.panel').find('.panel-heading span.clickable');


        if ($span.find('i').hasClass('glyphicon-chevron-up'))
            $span.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        else
            $span.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');

        $(this).parents('.panel').find('.panel-collapse').collapse('toggle');
    })



.on('click', '.info-icon-container', function(e) {

    e.stopPropagation();
    //console.log ( 'info-Icon CLick') ;
    if (bubbleClicked.length > 0 && bubbleClicked != $(this).attr('Id')) {

        $('#' + bubbleClicked).ggpopover('hide');
        $('#' + bubbleClicked).parents('.hover-container').hide();
    }


    if (bubbleClicked != $(this).attr('Id')) {
        //    bubbleClicked = '';
        //else {
        bubbleClicked = $(this).attr('Id');
    }

})

.on('click', '.svgInfoClass', function(e) {
    var popoverId = $(this).parents('a').attr('Id');

    var dataContent = $('#' + popoverId).attr('data-content');
    if (dataContent == null || dataContent.length == 0) {
        $('#InfoLoading').show();
        pendingDataLoad = popoverId;
    }
})

//moved into its own function 
.on('mouseleave', '.parent-hover-container', function(e) {
    e.stopPropagation();
    var childId = $(this).find('.hover-container').attr('Id')
    var childSplt = childId.split('_');
    var bubbleClickedSplt = bubbleClicked.split('_');

    if (childSplt[1] != bubbleClickedSplt[1] || bubbleClicked.length == 0)
        $(this).find('.hover-container').hide();
})

.on('click', 'body', function(e) {
    //e.stopPropagation();
    //console.log(e.target);
    var subject = $('.hover-container');

    var actionsCon = $('#ActionsContainer');

    var objectId = $(e.target).parents('span:first').parent().attr('id');
    if (objectId != null && objectId.indexOf('Info_') == 0) {

        if (objectId != bubbleClicked) {
            $('#' + bubbleClicked).ggpopover('hide')
            bubbleClicked = '';
            $('.hover-container').hide();
        }
    } else {

        if (!subject.has(e.target).length) {

            if (bubbleClicked.length > 0) {
                $('#' + bubbleClicked).ggpopover('hide')
                bubbleClicked = '';
                $('.hover-container').hide();
            }
        }
    }

    if (!actionsCon.has(e.target).length) {
        $('.actions-button').ggpopover('hide');
    }

    var LinkCon = $('#LinkContainer')

    if (!LinkCon.has(e.target).length && IconsVisible == true) {
        ShowLinkedIcons(false);
    }
})

.on('mouseover', '.parent-hover-container', function(e) {

    e.stopPropagation();
    $(this).find('.hover-container').show();
})






function LoadPopoverData() {

    /*
   
      if (CurrentView == 'person') {
         //console.log('person event');

         $('.DataObjectSection').one('mouseover', function() {
             GetContactInfo($(this).attr('id'));
         })

     } else { */
    //console.log('object event');
    $('.DataObjectSection').one('mouseover', function() {
        GetObjectInfo($(this).attr('id'));
    });
    //}

}

function UnlinkChatTranscript(data) {
    console.log('UNLINK CHAT');
    console.log(data);
}

function ToggleSearchBarOff() {
    if (CurrentDataView == 'ObjectView') {
        $('.searchBtnHeaderOff').show();
        $('.searchBtnHeaderOn').hide();
        $('.search').hide()
            //$('#TopSearchBlock').removeClass('TopSearch');
        $('#TopSearchBlock').removeAttr('style');
    }
}

/** Used to set the drop menu to the correct selection.  Performed after search is complete */
function SetDataViewSelection() {

    var selectedIndex = 0;
    for (var i = 0; i < SFObjectArray.length; i++) {
        var tmp = JSON.parse(SFObjectArray[i])
        if (tmp.name.toLowerCase() == CurrentView.toLowerCase()) {
            selectedIndex = i + 2;
            break;
        }

    }

    if (CurrentView == 'All')
        $('#ObjectSelectDropdown').ddslick('select', { index: 1 });
    else if (CurrentView != 'person')
        $('#ObjectSelectDropdown').ddslick('select', { index: selectedIndex });
}

function ToggleSearchBarOn() {
    if (CurrentDataView == 'ObjectView') {

        $('.searchBtnHeaderOff').hide();
        $('.searchBtnHeaderOn').show();
        $('.search').show();
        initialize();
        //$('#TopSearchBlock').css('height','44px');
        $('#TopSearchBlock').css('width', '40px');
        $('#TopSearchBlock').css('background-color', '#ffffff');
    }
}

function LoadActionFunctions() {
    $('[data-toggle="popover_actions"]').attr('data-content', $('<div/>').html(ActionsString).text());

    $('[data-toggle="popover_actions"]').ggpopover();


}

function LoadIndicatorIcons() {

    if (ChatFlag == 'true') {
        $('.indicator-icon-list').html($('<div/>').html(IndicatorString).text());
        $('#IndicatorLogo').attr('src', SLDSPath + '/assets/icons/utility/chatlink.png');
        var NumOfIcons = $('.popover-indicator-icon').length;

        var IconSize = NumOfIcons * 38;
        $('.indicator-icon-horizontal').css('width', IconSize + 'px');
        $('.indicator-icon-horizontal').css('left', '-' + IconSize + 'px');

        //$('.icon-indicator').css('background-color', '#ffffff !important');

        //$('.icon-indicator').child().css('fill' ,'#8F8F8A !important')

        $('.icon-indicator-box').mouseenter(function() {
            //console.log('ENTERING ind box')
            $(this).find('.slds-icon-indicator').css('fill', '#FF9300 !important')
                //console.log($(this).find('.slds-icon-indicator'));
        });
        /*
        $('.icon-indicator').mouseenter(function() {
            console.log (  $(this).find(">:first-child").css('fill') );
            $(this).find(">:first-child").css('fill', '#FFA92C !important');
        });
        */
        $('.icon-indicator-box').mouseleave(function() {
            //$(this).find('.slds-icon-indicator').removeCSS('fill');
        });

        $('[data-toggle^="popover_actions"]').ggpopover();

    } else {

        $('#IndicatorLogo').attr('src', SLDSPath + '/assets/icons/utility/chatunlink.png');
    }

}




function ShowLinkedIcons(toggle) {


    if (toggle == true) {
        $('.unlink-container').hide();
        $('.link-container').show();
        IconsVisible = true;
    } else {
        $('.unlink-container').show();
        $('.link-container').hide();
        IconsVisible = false;
    }

}



function ShowLinkSuccess() {

    if (ChatAttachedComplete == 'true' && crudtype != 'EDITRECORD') {
        $('.ChatLinkedSuccess').show();
        LoadIndicatorIcons();
    }
}

function ShowContactButtons(contactId, type) {

    /*
    if (type == 'enter')
        //$('#buttons_' + contactId).show();
    else {
        //$('#' + bubbleClicked).ggpopover('hide')
        // $('#buttons_' + contactId).hide();
    }
    */
}

function CenterLoadingBox(divClass) {
    try {
        var Mytop = Math.max($(window).height() / 2 - $("." + divClass)[0].offsetHeight / 2, 0);
        var Myleft = Math.max($(window).width() / 2 - $("." + divClass)[0].offsetWidth / 2, 0);
        $("." + divClass).css('top', Mytop + "px");
        $("." + divClass).css('left', Myleft - 50 + "px");
        $("." + divClass).css('position', 'fixed');
        $("." + divClass).css('z-index', '99');
    } catch (err) {}

}



function SetEndTime(ChatId) {

    //Visualforce.remoting.Manager.invokeAction(
    LveEngage.ChatLinkController.setChatEndTime(
        ChatId,
        function(result, event) {
            if (event.status) {

            } else if (event.type === 'exception') {
                //alert(event.message);
            } else {
                //Do Something
            }
        }, {
            escape: true
        }
    );
}

function SetStartTime(startTime, ChatId) {

    //Visualforce.remoting.Manager.invokeAction(
    LiveEngage.ChatLinkController.setChatStartTime(
        startTime, ChatId,
        function(result, event) {
            if (event.status) {

            } else if (event.type === 'exception') {
                //alert(event.message);
            } else {
                //Do Something
            }
        }, {
            escape: true
        }
    );

}

function GetObjectInfo(_data) {
    //var _data = $(this).attr('id');
    var res = _data.split("_");
    console.log('CALLING object info')
        //Need to dynamically get the data .
        //Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.ChatLinkController.GetObjectHtml}',
    LiveEngage.ChatLinkController.GetObjectPopoverData(res[1], SkillName,
        function(result, event) {
            if (event.status) {

                //$('#' + _data).attr('data-content', result);

                /*
                $('#' + _data).ggpopover({
                    html: 'true',
                    arrowcolor: 'Black'
                });
                */


                $('#' + _data).attr('data-content', $("<span />", {
                    html: result
                }).text());
                $('#' + _data).ggpopover();



                $('#InfoLoading').hide();

                var target = $('#' + _data);
                if (target.length) {
                    //event.preventDefault();
                    $('html, body').stop().animate({
                        scrollTop: target.offset().top
                    }, 1000);
                }

                if (pendingDataLoad == _data) {
                    $('#' + _data).ggpopover('show');
                    pendingDataLoad = '';
                }


            } else if (event.type === 'exception') {
                $('#' + _data).ggpopover();
                $('#InfoLoading').hide();



            } else if (event.type === 'exception') {
                $('#' + _data).ggpopover();
            } else {
                //Do Something
            }
        }, {
            escape: true
        }
    );

}

$('[data-toggle="popover1"]').ggpopover();

//    $('[data-toggle="popover"]').ggpopover({placement: "auto bottom",
//        trigger: "hover",
//        html: "true"});

function ShowContactInfo(obj, contactId) {

    $('.contactHeader a').css('border-bottom', '');

    $('.object-data-ppo').each(function(index) {

        var val = $(this).attr('id').toLowerCase();

        if (val.indexOf(obj.toLowerCase()) > -1) {
            $(this).show();
            $('.ID' + obj.toLowerCase()).css('border-bottom', '2px solid orange');
        } else {
            $(this).hide();
        }
    });


}

function CloseToolTip(contactId) {
    $('[data-toggle="popover_' + contactId + '"]').ggpopover('hide');
    $('.hover-container').hide();
    bubbleClicked = '';
}

var searchString;

function ReloadSeach() {
    $('#searchTerm').tokenfield('setTokens', searchString);
    if (DebugLog == 1)
        addLog('ReloadSeach():DoSeach');

    DoSearch();
};

function ReloadSeachAfterUnlink() {
    $('#searchTerm').tokenfield('setTokens', LastSearchTerm);
    if (DebugLog == 1)
        addLog('ReloadSeach():DoSeach');

    PerformSearchUnLink(LastSearchTerm);
};

function DoSearch() {
    console.log('Current View ' + CurrentView);
    if (DebugLog == 1)
        addLog('DoSeach()');

    //$('.svgClass').html('');
    $('#searchTerm').tokenfield('createTokensFromInput');

    if (DisbaleSearchInput == '1')
        $('.token-input').prop('disabled', false);

    //for (var i = 0; i < SFObjectArray.length; i++) {}

    searchString = $('#searchTerm').tokenfield('getTokensList');
    console.log('SEARCHTERM: ' + searchString);
    console.log('CurrentDataView: ' + CurrentDataView);
    if (CurrentDataView == 'ObjectView') {
        if (DebugLog == 1)
            addLog('PerformSearchObjectView(): Line 782 ' + searchString);
        PerformSearchObjectView(searchString)

    } else {
        if (DebugLog == 1)
            addLog('PerformSearch(): Line 786 ' + searchString);
            
            PerformSearch(searchString);
           
       
    }

    if (DisbaleSearchInput == '1') {
        $('.token-input').prop('disabled', true);
        $('.token a').remove();
    }


}

function AddIcons() {
    AddAccountIcons();
    AddContactIcons();
    AddIconsFromList();
    //AddCaseIcons();
    //AddOpportunityIcons();
    AddInfoIcons();

}

function RedirectPage(strLocation) {
    window.location.replace('\\' + strLocation);
}

function AddIconsFromList() {

    for (var i = 0; i < SFObjectArray.length; i++) {
        var tmp = JSON.parse(SFObjectArray[i])

        if (tmp.isCustom == 'false' || tmp.logoUrl.length == 0) {
            $('.svg' + tmp.name + 'Class').html('<svg aria-hidden="true" class="slds-icon--small slds-icon1"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/1452206375000/liveengage__SLDS090/assets/icons/standard-sprite/svg/symbols.svg#' + tmp.name.toLowerCase() + '"></use></svg><span class="slds-assistive-text">' + tmp.name + '</span> ')
        } else {

            $('.svg' + tmp.name + 'Class').html('<img src="' + tmp.logoUrl + '">')
        }
    }

}

function AddAccountIcons() {
    $('.svgAccountClass').html('<svg aria-hidden="true" class="slds-icon"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/1452206375000/liveengage__SLDS090/assets/icons/standard-sprite/svg/symbols.svg#account"></use></svg><span class="slds-assistive-text">Account</span> ')
}

function AddContactIcons() {
    $('.svgContactClass').html('<svg aria-hidden="true" class="slds-icon"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/1452206375000/liveengage__SLDS090/assets/icons/standard-sprite/svg/symbols.svg#contact"></use></svg><span class="slds-assistive-text">Contact</span> ')
}


/*
    function AddOpportunityIcons() {
        $('.svgOpportunityClass').html('<svg aria-hidden="true" class="slds-icon--small"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/1452206375000/liveengage__SLDS090/assets/icons/standard-sprite/svg/symbols.svg#opportunity"></use></svg><span class="slds-assistive-text">Info</span> ')
    }

    function AddCaseIcons() {
        $('.svgCaseClass').html('<svg aria-hidden="true" class="slds-icon--small"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/1452206375000/liveengage__SLDS090/assets/icons/standard-sprite/svg/symbols.svg#case"></use></svg><span class="slds-assistive-text">Info</span> ')
    }
*/
function AddInfoIcons() {
    $('.svgInfoClass').html('<svg aria-hidden="true" class="slds-icon--small  slds-icon1" style="fill:Black"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/1452206375000/liveengage__SLDS090/assets/icons/utility-sprite/svg/symbols.svg#info"></use></svg><span class="slds-assistive-text">Info</span> ')
}

function LinkChatSave(objId, type) {



    if (ChatAttachedComplete == 'true' || crudtype == 'EDITRECORD' || $('.ErrorMsg').children().length > 0)
        return;

    var canSave = 0;
    var mySplitResult = IndicatorTypeString.split(",");

    for (i = 0; i < mySplitResult.length; i++) {
        if (mySplitResult[i] == objId.substring(0, 3)) {
            $('#ChatAlreadyLink').show();
            CurrentObjectId = objId;
            canSave = 1;
            var h = $(document).height()
            var s = $(document).scrollTop()
            var newTop = $('#buttons_' + objId).offset().top;
            $('#ChatAlreadyLink').css('top', newTop + 'px');
        }
    }

    if (canSave == 0) {
        if (type == '0') {

            LinkChatSaveExisting(objId);
        } else {

            LinkChatSaveNew(objId);
            SaveComplete = 1;
        }
    }

}

function LinkChatOverRide() {

    LinkChatSaveExisting(CurrentObjectId);
    $('#ChatAlreadyLink').hide()
}

function ShowChatUnlink() {


    $('#ChatUnLink').show();
}

function HideChatUnLink() {
    $('#ChatUnLink').hide();

}

function CancelLinkOveride() {
    resetLinkFlag();
    $('#ChatAlreadyLink').hide();
    AttachQuickLinkAndActions();
}

function AttachQuickLinkAndActions() {
    LoadIndicatorIcons();
    LoadActionFunctions();
}

function OpenConsoleWindow(objId) {
    window.open("https://na22.salesforce.com/console#/" + objId, "SFConsole")
}




//LivePerson Person SDK Functionality

var name = '';
var visitorLocation = '';
var triedCountry = false;

var notificationHandler = function(data) {
    console.log('Init Complete');
    //lpTag.agentSDK.get('visitorInfo.visitorName', onSuccess, onError);
};




var SkillVar;

var NotifyWhenDone = function(err) {

    console.log('Unbind Complete');

}



var SDKBoundList = new Object();

function GetSDKPersonInfo() {

    //lpTag.agentSDK.unbind('chatInfo', onSuccessChatInfo, onError, errorNotifyWhenDone);
    //Loop through the search terms .  Search terms are generated in the controller.
    if ('SDKSearchTerms' in SDKBoundList == false) {
        SDKBoundList['SDKSearchTerms'] = 'true';
        if (SDKSearchTerms.length > 0) {
            for (i = 0; i < SDKSearchTerms.length; i++) {
                if (DebugLog == 1)
                    addLog('GetSDKPersonInfo() :SDK Bind Line 1161 onSuccessPersonalInfo::  ' + SDKSearchTerms[i].object);

                console.log(SDKSearchTerms[i].object);
                lpTag.agentSDK.bind(SDKSearchTerms[i].object, onSuccessPersonalInfo, onErrorSearchTerms);
                SDKBoundList[SDKSearchTerms[i].object] = 'true';
            }
        }
    }



}





function GetSDKDefaults() {
    console.log('** Calling SDK bind Defaults');
    if ('SDKDefaults' in SDKBoundList == false) {
        if (SDKDefaultVals.length > 0) {
            for (i = 0; i < SDKDefaultVals.length; i++) {
                if (DebugLog == 1)
                    addLog('GetSDKDefaults() :SDK Bind  Line 1010: ' + SDKDefaultVals[i].object);

                lpTag.agentSDK.bind(SDKDefaultVals[i].object, onSuccessDefaultVals, onErrorPersonalInfo);
                SDKBoundList['SDKDefaults'] = 'true';
            }

        }
    }

}





var searchString = '';
var TESTDATA = '11';
var MYTEST;

var onSuccessPersonalInfo2 = function(returnedData) {

    var path = returnedData.key;
    var data = returnedData.newValue;
    console.log(data);
}

var onSuccessPersonalInfo = function(returnedData) {
    console.log('starting onSuccessPersonalInfo ');
    var path = returnedData.key;
    var data = returnedData.newValue;
    console.log(data);

    SDKBoundList[path] = 'Processed';

    var currentSearch = $('#searchTerm').tokenfield('getTokensList');

    if (DebugLog == 1) {
        addLog('onSuccessPersonalInfo:Path: Line 982 ' + path);
    }

    if (SDKSearchTerms.length > 0) {

        if (data.length == null) {

            for (i = 0; i < SDKSearchTerms.length; i++) {

                if (path == SDKSearchTerms[i].object) {
                    SDKSearchCount++;
                    if (DebugLog == 1)
                        addLog('SDKSearchTerms[i].object line 984 ' + SDKSearchTerms[i].object);

                    console.log(SDKSearchTerms[i]);

                    for (c = 0; c < SDKSearchTerms[i].data.length; c++) {

                        var value;

                        console.log('BEFORE VALUE getSDKObjectValue ');


                        value = getSDKObjectValue(data, SDKSearchTerms, i, c);

                        console.log('VALUE::  ' + value);

                        if (value != null) {
                            if (searchString.indexOf(value) == -1) {
                                if (searchString.length == 0)
                                    searchString = value;
                                else
                                    searchString += ',' + value;

                            }

                        }

                        if (DebugLog == 1) {
                            addLog('lpTag.agentSDK.unbind(SDKSearchTerms[i].object, onSuccessPersonalInfo, onErrorPersonalInfo) Line 1003 ' + SDKSearchTerms[i].object);
                            addLog('SDKSearchCount Line 1042 ' + SDKSearchCount);
                        }

                        // lpTag.agentSDK.unbind(SDKSearchTerms[i].object, onSuccessPersonalInfo, onErrorPersonalInfo);

                    }


                }

            }
        }



        if (data.length > 0) {
            console.log('@@@@@@@@@@@@@');
            console.log(path);
            MYTEST = data;
            for (i = 0; i < SDKSearchTerms.length; i++) {

                if (path == SDKSearchTerms[i].object) {

                    SDKSearchCount++;

                    for (c = 0; c < SDKSearchTerms[i].data.length; c++) {



                        console.log('************ NEW function ***************');
                        value = GetSDkArrayValue(data, SDKSearchTerms, i, c);

                        if (DebugLog == 1) {
                            addLog('onSuccessPersonalInfo: VALUE line 1073 : ' + value);
                        }

                        if (value != null && value.length > 0 && searchString.indexOf(value) == -1) {
                            if (searchString.length == 0)
                                searchString = value;
                            else
                                searchString += ',' + value;
                        }
                    }
                }
                //lpTag.agentSDK.unbind(SDKSearchTerms[i].object, onSuccessPersonalInfo, onErrorPersonalInfo);

            }


        }
    }



    if (searchString.length > 0 && SDKSearchCount >= SDKSearchTerms.length) {

        console.log('searchString: ' + searchString);
        $('#searchTerm').tokenfield('setTokens', searchString);
        console.log('SDK Search CurrentDataView: ' + CurrentDataView);
        console.log('SDK Search CurrentView: ' + CurrentView);
        console.log('SDK Search DisableOnLoadSearch: ' + DisableOnLoadSearch);

        if (DisableOnLoadSearch == '0' && CurrentDataView == 'Search') {
            if (DebugLog == 1)
                addLog('onSuccessPersonalInfo : DoSearch Line 1063: ' + searchString);
            console.log('SDK DoSearch ');
            if (CurrentView == 'person')
                DoSearch();
            else {
                var numOfElements = $('#ObjectSelectDropdown').ddslick().data().ddslick.original[0].length

                for (var i = 0; i < numOfElements; i++) {

                    if ($('#ObjectSelectDropdown').ddslick().data().ddslick.original[0][i].getAttribute("data-src") == CurrentView) {

                        var SelectIndex = $('#ObjectSelectDropdown li:has(input[value="' + $('#ObjectSelectDropdown').ddslick().data().ddslick.original[0][i].getAttribute("value") + '"])').index()
                    }
                }
                //$('#ObjectSelectDropdown').ddslick().data().ddslick.original[0][2].getAttribute("data-src")

                if (SelectIndex > 0)
                    $('#ObjectSelectDropdown').ddslick('select', { index: SelectIndex });

                DoSearch();

            }

        }
    }
    console.log('**************** UNBIND: ' + path + '*****************************');
    lpTag.agentSDK.unbind(path, onSuccessPersonalInfo, onErrorPersonalInfo);
};

function GetSDkArrayValue(data, term, i, c) {

    var value;
    console.log('STARTING NEW FUNCTION');
    if (term[i].data[c].useValue == '0') {

        if (term[i].data[c].arrayNeeded.length > 0) {
            value = data[0][term[i].data[c].arrayNeeded][0][term[i].data[c].field];
        } else
            value = data[0][term[i].data[c].field];
    } else if (term[i].data[c].useValue == '1') {

        if (term[i].data[c].arrayNeeded.length > 0) {
            value = data[0][term[i].data[c].arrayNeeded][0][term[i].data[c].field];
        } else
            value = data[0][term[i].data[c].field];

    } else if (term[i].data[c].useValue == '2') {

        var splt = term[i].data[c].filter.split('=');

        for (f = 0; f < data.length; f++) {
            if (data[f][splt[0]].toLowerCase() == splt[1].toLowerCase()) {
                value = data[f][term[i].data[c].field];
            }
        }


    } else if (term[i].data[c].useValue == '3') {



        if (term[i].data[c].SecondaryArray.length > 0) {
            var arrayString = data[term[i].data[c].primaryIndex];

            for (z = 0; z < term[i].data[c].SecondaryArray.length; z++) {
                //console.log (term[i].data[c].SecondaryArray[z]['field'] )
                //console.log( term[i].data[c].SecondaryArray[z]['index'] );
                if (Array.isArray(arrayString[term[i].data[c].SecondaryArray[z]['field']]))
                    arrayString = arrayString[term[i].data[c].SecondaryArray[z]['field']][term[i].data[c].SecondaryArray[z]['index']];
                else
                    arrayString = arrayString[term[i].data[c].SecondaryArray[z]['field']];
                //console.log (arrayString );

            }

            arrayString = arrayString[term[i].data[c].field];
            value = arrayString
        } else {
            value = data[term[i].data[c].primaryIndex][term[i].data[c].field];
        }
    } else if (term[i].data[c].useValue == '4') {
        if (term[i].data[c].arrayNeeded.length > 0) {

            value = data[term[i].data[c].primaryIndex][term[i].data[c].arrayNeeded][term[i].data[c].secondaryIndex];
        } else {
            value = data[term[i].data[c].primaryIndex];
        }

    } else
        value = data[0][term[i].data[c].field];


    return value;

}

function getSDKObjectValue(data, term, i, c) {

    var value = '';
    if (term[i].data[c].useValue == '1') {
        console.log(term[i]);
        value = data[term[i].data[c].field].value;
    } else {
        value = data[term[i].data[c].field];
    }

    return value;
}


var defaultValueOBJ = new Object();
var CStest;

var onSuccessDefaultVals = function(returnedData) {

    var path = returnedData.key;
    var data = returnedData.newValue;

    console.log('***************  onSuccessDefaultVals *******************************');
    console.log(path);
    console.log(data);

    if (DebugLog == 1) {
        addLog('onSuccessDefaultVals:Path: Line 1111 ' + path);
        addLog('onSuccessDefaultVals:Path: Line 1112 ' + data);
    }

    if (SDKDefaultVals.length > 0) {

        if (data.length == null) {
            console.log('defaultvals option 1')
            for (i = 0; i < SDKDefaultVals.length; i++) {



                if (path == SDKDefaultVals[i].object) {

                    if (DebugLog == 1) {
                        addLog('onSuccessDefaultVals :SDKDefaultVals: Line 1317 Has data from SDK: ' + SDKDefaultVals[i].object);
                        addLog('SDKDefaultVals[i].data.length line 1318 : ' + SDKDefaultVals[i].data.length);

                    }


                    for (c = 0; c < SDKDefaultVals[i].data.length; c++) {



                        var value = '';
                        var keyName = path


                        if (DebugLog == 1) {

                            addLog('onSuccessDefaultVals :SDKDefaultVals:data[SDKDefaultVals[i].data[c].field] Line 1332 ' + data[SDKDefaultVals[i].data[c].field]);
                            addLog('onSuccessDefaultVals :KeyName Line 1333 ' + keyName);
                            addLog('onSuccessDefaultVals :Data Line 1334 ' + data);
                            addLog('onSuccessDefaultVals :Data Line 1335 ' + SDKDefaultVals[i].data[c].field);
                            addLog('onSuccessDefaultVals :SDKDefaultVals[i].data[c].useValue Line 1336 ' + SDKDefaultVals[i].data[c].useValue);


                        }

                        if (SDKDefaultVals[i].data[c].useValue == '1') {
                            value = data[SDKDefaultVals[i].data[c].field].value;
                            keyName += '_' + SDKDefaultVals[i].data[c].field;

                        } else if (SDKDefaultVals[i].data[c].useValue == '2') {
                            console.log('filter ');
                            var splt = SDKDefaultVals[i].data[c].filter.split('=');

                            if (data[splt[0]].toLowerCase() == splt[1].toLowerCase()) {
                                keyName = path + '_' + SDKDefaultVals[i].data[c].field;
                                value = data[SDKDefaultVals[i].data[c].field];
                                addLog('onSuccessDefaultVals :KeyName Line 1354 ' + keyName);
                                addLog('onSuccessDefaultVals :Data Line 1355 ' + value);
                            }

                        } else if (SDKDefaultVals[i].data[c].useValue == '3') {
                            keyName += '_' + SDKDefaultVals[i].data[c].field;
                            value = data[SDKDefaultVals[i].data[c].field];

                        } else {
                            value = data[SDKDefaultVals[i].data[c].field];
                            keyName += '_' + SDKDefaultVals[i].data[c].field;
                        }

                        if (value != null && value.length > 0) {
                            //Call remote action and store as a cookie
                            if (DebugLog == 1)
                                addLog('onSuccessDefaultVals :StoreDefaultValue(): Line 1359 PATH: ' + path + '_' + SDKDefaultVals[i].data[c].field + ' Value: ' + value);

                            //if (SDKDefaultVals[i].data[c].useValue == '1')
                            //    StoreDefaultValue(path + '_' + SDKDefaultVals[i].data[c].field + '.value', value);
                            //else

                            if (keyName in defaultValueOBJ == false || defaultValueOBJ[keyName] != value) {
                                //alert( keyName + ' : ' + SDKDefaultVals[i].data[c].lpField ) ;

                                if (SDKDefaultVals[i].data[c].lpField == 'Other') {
                                    if (DebugLog == 1)
                                        addLog('Calling Other StoreDefaultValue ' + SDKDefaultVals[i].data[c].sfField + ' Value: ' + value);
                                    StoreDefaultValue(SDKDefaultVals[i].data[c].sfField, value);
                                } else {
                                    if (DebugLog == 1)
                                        addLog('Calling StoreDefaultValue ' + keyName + ' Value: ' + value);
                                    StoreDefaultValue(keyName, value);
                                }

                                defaultValueOBJ[keyName] = value;
                                console.log('Calling Salesforce Method: ' + keyName + ' - ' + value);
                            }
                        }


                    }


                }
            }
        }

    }
    if (data.length > 0) {
        console.log('defaultvals option 2')
        for (i = 0; i < SDKDefaultVals.length; i++) {

            if (path == SDKDefaultVals[i].object) {

                for (c = 0; c < SDKDefaultVals[i].data.length; c++) {



                    if (DebugLog == 1) {
                        addLog('onSuccessDefaultVals :SDKDefaultVals[i].data.length LINE 1396 ' + SDKDefaultVals[i].data.length);
                        addLog('onSuccessDefaultVals :SDKDefaultVals[i].object LINE 1397 ' + SDKDefaultVals[i].object);
                        addLog('onSuccessDefaultVals :Field Name LINE 1398 ' + SDKDefaultVals[i].data[c].field + ', VALUE: ' + data[0][SDKDefaultVals[i].data[c].field]);
                        addLog('onSuccessDefaultVals : SDKDefaultVals[i].data[c].arrayNeeded LINE 1399 ' + SDKDefaultVals[i].data[c].arrayNeeded.length);

                    }

                    var value = '';
                    var keyName = path;
                    value = GetSDkArrayValue(data, SDKDefaultVals, i, c);
                    keyName = path + '_' + SDKDefaultVals[i].data[c].field;
                    console.log('keyname: ' + keyName);

                    if (value != null && value.length > 0) {

                        if (DebugLog == 1) {
                            addLog('onSuccessDefaultVals :StoreDefaultValue() Line 1412 ');
                        }

                        //StoreDefaultValue(path + '_' + SDKDefaultVals[i].data[c].field, value);

                        if (keyName in defaultValueOBJ == false || defaultValueOBJ[keyName] != value) {
                            if (SDKDefaultVals[i].data[c].lpField == 'Other')
                                StoreDefaultValue(SDKDefaultVals[i].data[c].sfField, value);
                            else
                                StoreDefaultValue(keyName, value);
                            defaultValueOBJ[keyName] = value;
                            console.log('Calling Salesforce Method: ' + keyName + ' - ' + value);
                        }
                    }
                }
            }



            //lpTag.agentSDK.unbind(SDKDefaultVals[i].object, onSuccessDefaultVals, onErrorPersonalInfo);
        }
    }

    lpTag.agentSDK.unbind(path, onSuccessDefaultVals, onErrorPersonalInfo);
};

var skillsProcessed = false;



function StartChatTranscriptSave() {
    //console.log('Calling Chat Transcript');

    if (DebugLog == 1)
        addLog('StartChatTranscriptSave() Line 1227 ');

    lpTag.agentSDK.bind('chatTranscript.lines', onSuccessChatTranscript, onError);
}

var onSuccessChatTranscript = function(returnedData) {
    var path = returnedData.key;
    var data = returnedData.newValue;
    if (data.length > 0) {
        SaveChatTranscript(JSON.stringify(data));
        lpTag.agentSDK.unbind('chatTranscript.lines', onSuccessChatTranscript, onError);
    }
};


var onSuccessVistorState2 = function(returnedData) {
    var path = returnedData.key;
    var data = returnedData.newValue;

}
var onSuccessDaveTest = function(returnedData) {
    var path = returnedData.key;
    var data = returnedData.newValue;
    console.log(data);
};

var onSuccessVistorState = function(returnedData) {

    var path = returnedData.key;
    var data = returnedData.newValue;
    console.log('VistorPath ' + path + ':' + data)

    if (data.length > 0) {
        if (data == 'ended') {

            if (DebugLog == 1)
                addLog('onSuccessVistorState Line 1247 ');

            lpTag.agentSDK.unbind('visitorInfo.chattingVisitorState', onSuccessVistorState, onError);
            lpTag.agentSDK.bind('chatTranscript.lines', onSuccessChatTranscript, onError);

            SetEndTime(String(accountId + sessionId));
            //ChatEndedReset();
            //('#searchTerm').tokenfield('setTokens', []);



        }
        //SaveChatTranscript(JSON.stringify(data));
        //
    }
};



var onError = function(err) {
    // Do something with the error 
    // alert('Error happened');
    console.log('Error happened: ' + err);

    if (DebugLog == 1)
        addLog('onError Line 1271 ' + err);

    if (err) {
        //Nothing to do
    }
};

var onErrorChattingVisitorState = function(err) {
    // Do something with the error 
    // alert('Error happened');
    console.log('Chatting Visitor State  happened: ' + err);

    if (DebugLog == 1)
        addLog('onError Line 1271 ' + err);

    if (err) {
        //Nothing to do
    }
};







var onErrorSearchTerms = function(err) {
    console.log('onErrorSearchTerms ERROR: ' + err);
    SDKSearchCount++;
    if (DebugLog == 1)
        addLog('onErrorPersonalInfo  line 1322: ' + err);
    if (err) {
        //handle the error
    }
};

var onErrorPersonalInfo = function(err) {
    console.log('PersonalInfo ERROR: ' + err);
    SDKSearchCount++;
    if (DebugLog == 1)
        addLog('onErrorPersonalInfo  line 1322: ' + err);
    if (err) {
        //handle the error
    }
};

var transcriptCreated = 0;
var retryChatTime = 0;
var onErrorChatStartTime = function(err) {
    console.log('Chat Start Time ERROR: ' + err);
    if (DebugLog == 1)
        addLog('Chat Start Time ERROR:');
    if (!err) {
        if (DebugLog == 1)
            addLog('Chat Start Time ERROR:');
        //Still create the Chat transcript record
        if (accountId.length > 0 && sessionId.length > 0 && retryChatTime == 0) {
            if (transcriptCreated == 0) {
                CreateChatTranscriptError(accountId + sessionId, null, sessionId);
                transcriptCreated = 1;
                console.log('Chat Transcript Created : ' + transcriptCreated);
                if (DebugLog == 1)
                    addLog('Chat Transcript Created : ' + transcriptCreated);
            }
            chatStartTimeErrors++;
            if (chatStartTimeErrors > 4 && retryChatTime == 0) {
                retryChatTime == 1
                lpTag.agentSDK.unbind('chatInfo.chatStartTime', onSuccessSetStartTime, errorNotifyWhenDone);
                console.log('CHAT START TIME ERRORS Unbind Call: ');
                if (DebugLog == 1)
                    addLog('CHAT START TIME ERRORS Unbind Call: ');
            }

        }
        console.log('CHAT START TIME Retrys: ' + chatStartTimeErrors);
    }

};

var errorNotifyWhenDone = function(err) {
    if (err) {
        console.log('errorNotifyWhenDone ERROR ' + err);
    } else {
        retryChatTime == 1
        console.log('CHAT START TIME Unbind Complete');
    }
};



var retryRtSession = 0

var onErrorRtSession = function(err) {

    console.log(err);
    //console.log('RETRY: ' + retryRtSession ) ;
    if (err) {

    }
};

var onSuccess = function(data) {
    console.log(JSON.stringify(data));
};

function LoadLPSDKFunctions() {

    console.log('Starting LoadLPSDKFunctions');
    if (DebugLog == 1)
        addLog('LoadLPSDKFunctions() , SDk Init Line 1390 ');


    lpTag.agentSDK.init(notificationHandler);

    /*
      lpTag.agentSDK.bind('chatInfo', function(data) {
                console.log(data);
            }, function (err) {
            });
      lpTag.agentSDK.bind('engagementInfo', function(data) {
                console.log(data);
            }, function (err) {
            });
     return;
     */
    console.log('Bind Chat Info');
    lpTag.agentSDK.bind('chatInfo', onSuccessChatInfo, onErrorChatInfo);


    //lpTag.agentSDK.bind('engagementInfo.skill', onSuccessSkill, onErrorSkill);

    if (DebugLog == 1)
        addLog('lpTag.agentSDK.bind(\'visitorInfo.chattingVisitorState\', onSuccessVistorState, onError)');

    //lpTag.agentSDK.bind('visitorInfo.chattingVisitorState', onSuccessVistorState, onErrorChattingVisitorState);

    //lpTag.agentSDK.bind('chattingAgentInfo.agentName', onSuccessAgentInfo, onSuccessAgentInfoError);
    //
    //lpTag.agentSDK.bind('SDE', onSuccessAgentInfo, onSuccessAgentInfoError);

}

var onSuccessAgentInfo = function(data) {
    console.log('SDE');
    console.log(onSuccessAgentInfo);
    console.log(data);
};

var onSuccessAgentInfoError = function(data) {
    console.log('ERROR on onSuccessAgentInfoError');
    console.log(data);
};



var logString = ''

function addLog(LogString) {
    logString += timeStamp() + "  " + LogString + '\r\n';
}


function SaveLog() {
    var blob = new Blob([logString], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "LivePersonJsLog.txt");
    logString = '';
}


function timeStamp() {
    // Create a date object with the current time
    var now = new Date();

    // Create an array with the current month, day and time
    var date = [now.getMonth() + 1, now.getDate(), now.getFullYear()];
    for (var i = 0; i < 2; i++) {
        if (date[i] < 10)
            date[i] = "0" + date[i];
    }

    // Create an array with the current hour, minute and second
    var time = [now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()];

    // Determine AM or PM suffix based on the hour
    var suffix = (time[0] < 12) ? "AM" : "PM";

    // Convert hour from military time
    time[0] = (time[0] < 12) ? time[0] : time[0] - 12;

    // If hour is 0, set it to 12
    time[0] = time[0] || 12;

    // If seconds and minutes are less than 10, add a zero
    for (var i = 0; i < 4; i++) {
        if (time[i] < 10) {
            time[i] = "0" + time[i];
        }
    }

    if (time[3] < 100)
        time[3] = "00" + time[3];




    // Return the formatted string
    return date.join(".") + " " + time.join(":");
}

function queryString(key) {
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]" + key + "=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

//loadjscssfile("myscript.js", "js") //dynamically load and add this .js file
//removejscssfile("somescript.js", "js") //remove all occurences of "somescript.js" on page
function loadjscssfile(filename, filetype) {
    if (filetype == "js") { //if filename is a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", filename)
    } else if (filetype == "css") { //if filename is an external CSS file
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref != "undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

function removejscssfile(filename, filetype) {
    var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none" //determine element type to create nodelist from
    var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none" //determine corresponding attribute to test for
    var allsuspects = document.getElementsByTagName(targetelement)
    for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
        if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1)
            allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}
//SDK Chat Info Functions
//Success for ChatInfo 
var onSuccessChatInfo = function(returnedData) {

    console.log('onSuccessChatInfo started');

    var path = returnedData.key;
    var data = returnedData.newValue;

    ChatInfoObj = data;
    console.log(data);

    if (MessagingSet == 0 && data.hasOwnProperty('isMessaging')) {
        console.log('set messaging');
        MessageStatus(data.isMessaging);
        MessagingSet = 1;
    }

    if (SkillSet == 1) {
        console.log('Handle chat info');
        handleChatInfo(data);
    }


}

var onErrorChatInfo = function(err) {
    // Do something with the error 
    // alert('Error happened');
    console.log('Chat Info Error happened: ' + err);

    if (DebugLog == 1)
        addLog('onError Line 1271 ' + err);

    if (err) {
        //Nothing to do
    }
};

function handleChatInfo(data) {

    if (data == null)
        return;


    if (data.accountId != '' && accountId != data.accountId && ChatInfoObj.isMessaging == false) {
        console.log('calling onSuccessSetAccountId');
        onSuccessSetAccountId(data.accountId);
    }
    if (data.rtSessionId != '' && sessionId != data.rtSessionId) {
        console.log('calling onSuccessSetSessionId');
        onSuccessSetSessionId(data.rtSessionId);

    }

    if (data.chatStartTime != null && sessionTime != data.chatStartTime) {

        console.log('Chat Start Time: ' + data.chatStartTime);
        console.log('calling onSuccessSetStartTime');
        onSuccessSetStartTime(data.chatStartTime);

    }

    if (data.chatStartTime == null && sessionTime == null) {

        console.log('Chat Start Time: ' + data.sessionStartTime);
        console.log('calling onSuccessSetStartTime');
        onSuccessSetStartTime(data.sessionStartTime);

    }
}

var accountIdSet = 0;
//The Success action for the 
var onSuccessSetAccountId = function(data) {

    //var path = returnedData.key;
    //var data = returnedData.;
    console.log('ACCOUNTID: ' + data);

    var isChanged = false
    if (accountId != data) {
        accountId = data;
        isChanged = true
    }

    if (isChanged == true) {


        //if (DebugLog == 1)
        //    addLog('onSuccessSetAccountId:CreateChatTranscript(): ' + accountId + sessionId);
        //if (DebugLog == 1)
        //    addLog(' lpTag.agentSDK.bind(\'chatInfo.rtSessionId\', onSuccessSetSessionId, onErrorRtSession)');
        //Bind to the SDk Session
        //lpTag.agentSDK.bind('chatInfo.rtSessionId', onSuccessSetSessionId, onErrorRtSession);

    }
};

var onSuccessSetSessionId = function(data) {

    //var path = returnedData.key;
    //var data = returnedData.newValue;

    var isChanged = false
    if (sessionId != data) {
        sessionId = data;
        isChanged = true
    }
    if (isChanged == true) {
        sessionId = data;
        $('#entireBlock').show();
        InitComplete = 1;

        if (DebugLog == 1)
            addLog('onSuccessSetSessionId :CreateChatTranscript(): ' + accountId + sessionId);

        //lpTag.agentSDK.unbind('chatInfo.rtSessionId', onSuccessSetSessionId,onErrorSessionId , errorNotifyWhenDone);
        if (DebugLog == 1)
            addLog('unbind chatInfo.rtSessionId');

        if (ChatInfoObj.isMessaging == true) {
            console.log('Creating CreateChatTranscript');

            CreateChatTranscript(sessionId, String(ChatInfoObj.chatStartTime), sessionId, '1');
        } else
            CreateChatTranscript(accountId + sessionId, String(ChatInfoObj.chatStartTime), sessionId, '0');
        console.log(ChatInfoObj.chatStartTime);
        if (ChatInfoObj.chatStartTime != null) {
            sessionTime = data.chatStartTime;
            console.log('Unbind chat info in onSuccessSetSessionId')
            console.log(ChatInfoObj.chatStartTime);
            lpTag.agentSDK.unbind('chatInfo', onSuccessChatInfo, onError, errorNotifyWhenDone);
        }



    }
}

var onSuccessSetStartTime = function(data) {
    //var path = returnedData.chatStartTime.key;
    //var data = returnedData.chatStartTime.newValue;
    var isChanged = false
    if (sessionTime != data) {
        sessionTime = data;
        isChanged = true
    }
    console.log('CHAT START TIME:' + data);
    if (String(data).length > 0) {
        if (accountId.length > 0 && sessionId.length > 0) {

            if (DebugLog == 1)
                addLog('onSuccessSetStartTime :SetStartTime(): ' + accountId + sessionId + ',' + String(data.chatStartTime));

            //Create the chat transaction record
            console.log('Creating Chat Transcript record ');
            if (ChatInfoObj.isMessaging == true)
                CreateChatTranscript(sessionId, String(ChatInfoObj.chatStartTime), sessionId, '1');
            else
                CreateChatTranscript(accountId + sessionId, String(ChatInfoObj.chatStartTime), sessionId, '0');

            console.log('Unbind Chat Start Time on Success');
            lpTag.agentSDK.unbind('chatInfo', onSuccessSetStartTime, errorNotifyWhenDone);

        }
    }
};

//SDK Skill Functions
function BindSkill() {
    console.log('binding to engagementInfo.skill');
    lpTag.agentSDK.bind('engagementInfo.skill', onSuccessSkill, onErrorSkill);
}
var onSuccessSkillUnbind = function(returnedData) {
    console.log('unbind skill');
}
var onSuccessSkill = function(returnedData) {
    var path = returnedData.key;
    var data = returnedData.newValue;
    //SkillVar = returnedData;
    console.log(data);

    if (DebugLog == 1)
        addLog('onSuccessSkill :SKILL DATA Line 1152' + data);
    if (data.length > 0) {

        console.log('call unbind engagementInfo.skill');
        lpTag.agentSDK.unbind('engagementInfo.skill', onSuccessSkill, onErrorSkill, SkillNotifyWhenDone);

        StoreSkillName(data);
        SkillName = data;
        //skillsProcessed = true;


        console.log('Skills Updated ');

        if (DebugLog == 1)
            addLog('onSuccessSkill :CheckSkillsLoaded() ');

        //CheckSkillsLoaded();


        //Remove bind because of an SDK update 05-03-2016
        //lpTag.agentSDK.bind('agentInfo.accountId', onSuccessSetAccountId, onError);
        SkillSet = 1;

        console.log('Current View ' + CurrentView);
        console.log('Handle Chat Info from Skill ');
        handleChatInfo(ChatInfoObj);
        console.log('Current View ' + CurrentView);
    }

};

var onErrorSkill = function(err) {
    // Do something with the error 
    // alert('Error happened');
    console.log('Skill Unbind: ' + err);

    if (DebugLog == 1)
        addLog('onError Line 1271 ' + err);

    if (err) {
        //Nothing to do
    }
};

var SkillNotifyWhenDone = function(err) {
    if (err) {
        console.log('SkillNotifyWhenDone ERROR ' + err);
    } else {
        retryChatTime == 1
        console.log('Skill Unbind Complete');
    }
};


var errorNotifyWhenDone = function(err) {
    if (err) {
        console.log('errorNotifyWhenDone ERROR ' + err);
    } else {
        retryChatTime == 1
        console.log('CHAT START TIME Unbind Complete');
    }
};
