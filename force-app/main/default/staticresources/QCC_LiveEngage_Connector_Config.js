(function (window) {
    window.__env = window.__env || {};
  
    // Base url
    window.__env.salesforceBasePathConfig = "https://qantas.lightning.force.com/one/one.app#/sObject/";

    // Record Type ID
    window.__env.recordTypeIdConfig = "01290000000ukakAAA";
    console.info("CONFIG - Record Type ID ENV Config:", window.__env.recordTypeIdConfig);

}(this));