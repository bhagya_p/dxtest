@isTest
private class CP_ConPaymentCountHandlerTest{ 
    
  @testsetup
   static void createCPTestData(){
     
         // Custom setting creation
         TestUtilityDataClassContractPayment.enableCPcountTriggers();
   }
   static testMethod void CountNumofConPayments() {
         List<Account> accList = new List<Account>();
         List<Opportunity> oppList = new List<Opportunity>();
         List<Proposal__c> propList = new List<Proposal__c>();
         List<Discount_List__c> discList = new List<Discount_List__c>();
         List<Contract_Payments__c> cpList = new  List<Contract_Payments__c>();
       
         
         // Load Accounts
         accList = TestUtilityDataClassContractPayment.getAccounts();
         
         // Load Opportunities
         oppList = TestUtilityDataClassContractPayment.getOpportunities(accList);
         
         // Load Proposals
         propList = TestUtilityDataClassContractPayment.getProposal(oppList);
         
         //Load CP
         cpList = TestUtilityDataClassContractPayment.getCP(propList);
         Test.startTest();               
         //Update CP
         For (Contract_Payments__c c:cpList )
                {
                c.Amount__c=2000;                
                }
       try{
             Database.Update(cpList);
             Database.delete(cpList);
         }catch(Exception e){
             System.debug('Error Occured: '+e.getMessage());
         } 
          Test.stopTest(); 
    }
    }