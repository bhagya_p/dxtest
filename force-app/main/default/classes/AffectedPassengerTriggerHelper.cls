/*----------------------------------------------------------------------------------------------------------------------
Author:        
Company:       Capgemini
Description:   
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
05-May-2018                    

-----------------------------------------------------------------------------------------------------------------------*/
public class AffectedPassengerTriggerHelper {
    
    /*--------------------------------------------------------------------------------------      
Method Name:        isAffectedPassenger
Description:        All Before Trigger Functionalities
Parameter:          new List and old Map records
--------------------------------------------------------------------------------------*/      
    public static void isAffectedPassenger(List<Affected_Passenger__c> lstAffectedPassengers, Map<Id, Affected_Passenger__c> oldAFPMap) {
        try {
            Set<Id> setAllEvnId = new Set<Id>();
            Set<Id> setEvnId = new Set<Id>();
            Map<Id, List<FlightFailure__c>> mapFltFailure = new Map<Id, List<FlightFailure__c>>();
            Map<Id, List<EventCommunication__c>> mapEvtComms = new Map<Id, List<EventCommunication__c>>();
            
            //Check if Pre and Post Flight is changed
            for(Affected_Passenger__c objPas: lstAffectedPassengers){
                Affected_Passenger__c objOldPas = oldAFPMap != Null? oldAFPMap.get(objPas.Id): Null;
                if(objOldPas == Null || objPas.TECH_PreFlight__c != objOldPas.TECH_PreFlight__c || 
                   objPas.TECH_PostFlight__c != objOldPas.TECH_PostFlight__c)
                {
                    setAllEvnId.add(objPas.Event__c);
                    if( String.isBlank(objPas.GlobalEvent__c) ){
                        setEvnId.add(objPas.Event__c);
                    }else{
                        setEvnId.add(objPas.GlobalEvent__c);
                    }
                }
            }
            
            if(setEvnId.size()>0){
                
                //Get All Failures Related to the Event
                mapFltFailure = QCC_EventGenericUtils.queryFlightFailures(setAllEvnId);   
                
                //Get All Events Related to the Event
                mapEvtComms = QCC_EventGenericUtils.queryEvtComms(setEvnId);
                
                for(Affected_Passenger__c objPas: lstAffectedPassengers){
                    if(setEvnId.contains(objPas.Event__c)){
                        List<FlightFailure__c> lstFltFailure = mapFltFailure.containsKey(objPas.Event__c)? mapFltFailure.get(objPas.Event__c): new List<FlightFailure__c>();
                        List<EventCommunication__c> lstEvtComms = mapEvtComms.containsKey(objPas.Event__c)? mapEvtComms.get(objPas.Event__c): new List<EventCommunication__c>();
                        if(lstFltFailure != Null){
                            objPas = QCC_EventGenericUtils.updatePassengerFailureCode(objPas, lstFltFailure, lstEvtComms);
                        }
                    }
                }
                
            }
        } catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Affected Passenger Trigger', 'AffectedPassengerTriggerHelper', 
                                    'isAffectedPassenger', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }
    
    /*--------------------------------------------------------------------------------------      
Method Name:        updateFullEmailBody
Description:        Add the mail content of email in FullEmailBody field
Parameter:          new List and old Map records
--------------------------------------------------------------------------------------*/      
    public static void updateFullEmailBody(List<Affected_Passenger__c> lstAffectedPassengers, Map<Id, Affected_Passenger__c> oldAFPMap){
        set<ID> setPassId = new Set<Id>();
        
        //Check if Pre and Post Flight is changed
        for(Affected_Passenger__c objPas: lstAffectedPassengers){
            Affected_Passenger__c objOldPas = oldAFPMap != Null? oldAFPMap.get(objPas.Id): Null;
            if(objOldPas.SendEmail__C != objPas.SendEmail__c && objPas.SendEmail__c == true)
            {
                setPassId.add(objPas.Id);
                
            }
        }
        if(setPassId.size()>0){
            Map<Id, Id> mapPassengerTemplate = new Map<Id, Id>();
            Set<String> setEmailTemplateName = new Set<String>();
            setEmailTemplateName.add('QCC_PassengerCommsWithOutRecovery');
            setEmailTemplateName.add('QCC_PassengerCommsWithRecoveryFF');
            setEmailTemplateName.add('QCC_PassengerCommsWithRecoveryNFF');
            setEmailTemplateName.add('QCC_GlobalTemplate');
            
            Map<String, Id> mapObj = new Map<String, Id>();
            Contact objCon = new Contact();
            List<Contact> lstCon = [Select Id from Contact where Email = 'noreplyiocevents@qantas.com.au' Limit 1];
            if(lstCon == Null || lstCon.size() == 0){
                objCon = [Select Id from Contact where Email != Null Limit 1];
            }else{
                objCon = lstCon[0];
            }
            for(EmailTemplate objET: [ select Id, Name, Body, Subject, HTMLValue  from EmailTemplate where 
                                      Name IN: setEmailTemplateName])
            {
                mapObj.put(objET.Name, objET.Id);
            }
            
            for(Affected_Passenger__c objPas: lstAffectedPassengers){
                if(setPassId.contains(objPas.Id)){
                    Id ETId;
                    if(String.isNotBlank(objPas.GlobalEvent__c)){
                        ETId =  mapObj.get('QCC_GlobalTemplate');
                    }else if(objPas.TECH_OfferedQantasPoints__c >0 && objPas.TECH_OfferedQantasPoints__c != Null && 
                             objPas.EventCloseAction__c == CustomSettingsUtilities.getConfigDataMap('Event Close Action Recovery And Comms'))
                    {
                        if(String.isNotBlank(objPas.FrequentFlyerNumber__c)){
                            ETId =   mapObj.get('QCC_PassengerCommsWithRecoveryFF');
                        }else{
                            ETId =   mapObj.get('QCC_PassengerCommsWithRecoveryNFF');
                        }
                        
                    }else{
                        ETId =   mapObj.get('QCC_PassengerCommsWithOutRecovery');
                    }
                    mapPassengerTemplate.put(objPas.Id, ETId);
                    //objPas.FullEmailBody__c = 'Email Body will be added';QCC_EmailPreviewController.formEmailConent(objPas.Id, ETId, objCon.Id);
                }
            }
            
            if(mapPassengerTemplate.size() > 0){
                Map<Id, String> mapEmailContent = new Map<Id, String>();
                system.debug('mapEmaixlContent####'+mapPassengerTemplate);
                mapEmailContent = formEmailConent(mapPassengerTemplate, objCon.Id);
                system.debug('mapEmailContent####'+mapEmailContent.keyset());
                for(Affected_Passenger__c objPas: lstAffectedPassengers){
                    system.debug('mapEmailContentinside ####'+mapEmailContent.get(objPas.Id));
                    objPas.FullEmailBody__c = mapEmailContent.get(objPas.Id);
                }
            }
        }
    }
    
    public static Map<Id, String> formEmailConent(Map<Id, Id> mapPassengerTemplate, String conId){
        Map<Id, String> mapEmailContent = new Map<Id, String>();
        List<Id> lstWhatId = new List<Id>();
        List<Messaging.SingleEmailMessage> allMails = new List<Messaging.SingleEmailMessage>();
        for(Id whatId: mapPassengerTemplate.keyset()){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{'praveen.sampath@capgemini.com'};
            mail.setToAddresses(toAddresses);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setWhatId(whatId);
            mail.setTemplateId(mapPassengerTemplate.get(whatId));
            mail.setTargetObjectId(conId);
            allMails.add(mail);
            lstWhatId.add(whatId);
        }
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(allMails);
        Database.rollback(sp);
        
        system.debug('allMails####'+allMails.size());
        system.debug('lstWhatId####'+lstWhatId);
        
        Integer i =0;
        for(Messaging.SingleEmailMessage mail: allMails){
             system.debug('mail####'+mail.getSubject());
            String mailHtmlBody = mail.getPlainTextBody();
            String mailSubject = mail.getSubject();
            String response = 'Subject : '+ mailSubject+' \r\n\r\nEmail Body :'+mailHtmlBody.trim();
            mapEmailContent.put(lstWhatId[i], response);
            i++;
        }
        return mapEmailContent;
    }

    public static void updateAirportName(list<Affected_Passenger__c> lstPassenger){
        Map<String, String> airportName = new Map<String, String>();
        for(Airport_Code__c eachCode : [SELECT Id, Name, Freight_Station_Name__c, Freight_Airport_State_Code__c
                                              FROM Airport_Code__c limit 40000]){
            airportName.put(eachCode.Name, eachCode.Freight_Station_Name__c);                                  
        }

        for(Affected_Passenger__c afp : lstPassenger){
            if(!String.isBlank(afp.ArrivalPort__c)){
                afp.Arrival_Port_Name__c = airportName.get(afp.ArrivalPort__c);
                if(String.isNotBlank(CustomSettingsUtilities.getConfigDataMap(afp.Arrival_Port_Name__c))){
                    afp.Arrival_Port_Name__c = CustomSettingsUtilities.getConfigDataMap(afp.Arrival_Port_Name__c);
                }
            }
            if(!String.isBlank(afp.DeparturePort__c)){
                afp.Departure_Port_Name__c = airportName.get(afp.DeparturePort__c);
                if(String.isNotBlank(CustomSettingsUtilities.getConfigDataMap(afp.Departure_Port_Name__c))){
                    afp.Departure_Port_Name__c = CustomSettingsUtilities.getConfigDataMap(afp.Departure_Port_Name__c);
                }
            }
        }
    } 

    public static void rollupPassengerBasedonTier(List<Affected_Passenger__c> lstAffectedPassengers, Map<Id, Affected_Passenger__c> oldAFPMap){
        Set<Id> setEventId = new Set<Id>();
        List<Event__c> lstEvt = new List<Event__c>();
        
        if(lstAffectedPassengers != Null){
            //Insert and update
            for(Affected_Passenger__c objAffPass: lstAffectedPassengers){
                Affected_Passenger__c oldAffPass = oldAFPMap != Null? oldAFPMap.get(objAffPass.Id): Null;
                if((oldAffPass == Null && objAffPass.Affected__c == true) || 
                   (oldAffPass != Null && objAffPass.Affected__c != oldAffPass.Affected__c))
                {
                    setEventId.add(objAffPass.Event__c);
                }
            }
        }
        
        //Delete Of Passenger
        if(lstAffectedPassengers == Null && oldAFPMap != Null){
            for(Affected_Passenger__c objAffPass: oldAFPMap.values()){
                setEventId.add(objAffPass.Event__c);
            }  
        }
        
        
        if(setEventId.size()>0){
            Map<Id, Event__c> mapEvent = new Map<Id, Event__c>([select Id from Event__c where Id IN: setEventId]);
            AggregateResult[] groupedResults = [SELECT Event__c, FrequentFlyerTier__c, count(Id) FROM 
                                                Affected_Passenger__c where  Event__c IN: setEventId 
                                                GROUP BY Event__c, FrequentFlyerTier__c];
            for (AggregateResult ar : groupedResults)  {
                System.debug('Campaign ID' + ar.get('Event__c'));
                System.debug('Average amount' + ar.get('FrequentFlyerTier__c'));
                System.debug('Average amount' + ar.get('expr0'));
                String EvtId = String.ValueOf(ar.get('Event__c'));
                String Tier = String.ValueOf(ar.get('FrequentFlyerTier__c'));
                String fieldName =  CustomSettingsUtilities.getConfigDataMap(Tier+'fld');
                fieldName = String.isNotBlank(fieldName)?fieldName:CustomSettingsUtilities.getConfigDataMap(Tier+'EventField');
                if(String.isNotBlank(fieldName)){
                    Event__c objEvent = mapEvent.get(EvtId);
                    objEvent.put(fieldName, Integer.valueOf(ar.get('expr0')));
                    mapEvent.put(objEvent.Id, objEvent);
                }
            }
            lstEvt.addAll(mapEvent.values());
            update lstEvt;
        }
    }   
}