@isTest
public class Test_OpportunityTeam {

    public static testMethod void myTest(){
         List<Account> accList = new List<Account>();
         List<Opportunity> oppList = new List<Opportunity>();
         List<User> twoUsers = new List<User>();
         
         // Custom setting creation
         TestUtilityDataClassQantas.enableTriggers();
         
         // Load Accounts
         accList = TestUtilityDataClassQantas.getAccounts();
         
         // Users
         twoUsers = TestUtilityDataClassQantas.getUsers('Qantas Sales', 'Marketing Acquisition & Sales Team', '');
         
         Opportunity opp = new Opportunity(Name = 'Opp'+accList[0].Name , AccountId = accList[0].Id,
                                           Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),
                                           StageName = 'Identified'
                                           );
          try{
              Database.Insert(opp);
          }catch(Exception e){
              System.debug('Exception Occured: '+e.getMessage());
          }
                                  
    }
}