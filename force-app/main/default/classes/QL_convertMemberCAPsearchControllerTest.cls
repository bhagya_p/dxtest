@isTest(SeeAllData = false)
private class QL_convertMemberCAPsearchControllerTest {
    @testSetup
    static void createData() {
        //List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.enableTriggers();
        TestUtilityDataClassQantas.insertQantasConfigData();
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createAccountTriggerSettingasTrue();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createContactTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createCaseTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createRecoveryTriggerSetting());
        insert lsttrgStatus;
        List<Qantas_API__c> lstQantas = TestUtilityDataClassQantas.createQantasAPI();
        insert lstQantas;
        QantasConfigData__c qc = new QantasConfigData__c();
        qc.Name = 'Log Exception Logs Rec Type';
        qc.Config_Value__c = 'Exception Logs';
        qc.Config_Description__c = 'Log Exception Logs Record Type Name';
        insert qc;

    /*    Map<String, Profile> profMap = TestUtilityDataClassQantas.getProfileMap();

         User u = new User(LastName = 'User1', Alias = 'User99', Email = 'sampleuser991@sample.com', 
                           Username = 'sampleuser99username@sample.com', ProfileId = profMap.get('System Administrator').Id,
                           EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                           TimeZoneSidKey = 'Australia/Sydney'
                          );
        insert u;

        User u1 = new User(LastName = 'User1', Alias = 'User', Email = 'sampleuser1@sample.com', 
                           Username = 'sampleuser1username@sample.com', ProfileId = profMap.get('System Administrator').Id,
                           EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                           TimeZoneSidKey = 'Australia/Sydney', ManagerId = u.Id
                          );
        insert u1;*/
        }
      static testMethod void validateTestData() {
      
      
        //List<Account> accounts = new List<Account>();
         List<Account> accList = new List<Account>();
         List<Associated_Person__c> Memberlist = new List<Associated_Person__c>();
         ID recID;
        
       // String type = 'Complaint';
       // String recordType = 'CCC Complaints';
        
        // Load Accounts
          accList = QL_TestUtilityData.getAccounts();
          
           Profile pf = [Select Id from Profile where Name = 'System Administrator'];
            User u = new User();
            u.FirstName = 'Test';
            u.LastName = 'User';
            u.Email = 'testuser@test123456789.com';
            u.CompanyName = 'test.com';
            u.Title = 'Test User';
            u.Username = 'testuser@test123456789.com';
            u.Alias = 'testuser';
            u.CommunityNickname = 'Test User';
            u.TimeZoneSidKey = 'Australia/Sydney';
            u.LocaleSidKey = 'en_AU';
            u.EmailEncodingKey = 'ISO-8859-1';
            u.ProfileId = pf.Id;
            u.LanguageLocaleKey = 'en_US';
            u.CountryCode='AU';
            insert u;
            system.runAs(u) {
        
  
            for(Integer i = 0; i < 5; i++) {
                Associated_Person__c  member = new Associated_Person__c(Account__c=accList[0].Id,Last_Name__c='Sample', First_Name__c='User',Frequent_Flyer_Number__c = '0002750079',Profile_Email__c ='test@abc.com');
               // member.Email = 'praveen@hotmail.com';
               // member.Phone = '0426646801';
                Memberlist.add(member);
            }
            insert Memberlist;
            recID = Memberlist[0].id;
          Test.starttest();
         //   String Body = '{"lastName":"test","qantasFFNo":"0002750079","emailAddr":"QSA@com","customerPerPage" : "100","system_z" : "CAPCRE_CC"}';
       /* String Body = '{"memberId":"1900007871","firstName":"MEMBER","lastName":"LOYTEST","mailingName":"SIR M LOYTEST",'+
                        '"salutation":"Dear Sir Loytest","title":"SIR","dateOfJoining":"2012-05-21","gender":"MALE",'+
                        '"dateOfBirth":"1995-03-20","email":"ibskevinliu@qantas.com.au","emailType":"BUSINESS",'+
                        '"countryOfResidency":"AU","taxResidency":"AU","membershipStatus":"ACTIVE","preferredAddress":"BUSINESS",'+
                        '"pointBalance":1,"company":{"name":"QANTAS LOYALTY IT","positionInCompany":"TEST PROFILE - DPP"},'+
                        '"phoneDetails":[{"type":"BUSINESS","number":"ibs96915525","areaCode":"02","idd":"+61","status":"V"}],'+
                        '"addresses":[{"type":"BUSINESS","lineOne":"LEVEL 7","lineTwo":"440 ELIZABETH STREET","suburb":"MELBOURNE",'+
                        '"postCode":"3000","state":"VIC","countryCode":"AU","status":"V"}],"preferences":{},'+
                        '"programDetails":[{"programCode":"QCASH","programName":"Qantas Cash","enrollmentDate":"2013-08-14",'+
                        '"accountStatus":"ACTIVE","qcachSendMarketing":false,"qcashProgramIdentifier":"AU"},'+
                        '{"programCode":"QFF","programName":"Qantas Frequent Flyer","enrollmentDate":"2012-05-21",'+
                        '"accountStatus":"ACTIVE","tierCode":"FFBR","tierFromDate":"2017-11-22","tierToDate":"2019-05-31",'+
                        '"ffExpireDate":"31-May-2018"}],"webLoginDisabled":false,"statusCredits":{"expiryDate":"2018-05-31",'+
                        '"lifetime":300,"total":0,"loyaltyBonus":0},"tier":{"name":"Frequent Flyer Bronze","code":"FFBR",'+
                        '"expiryDate":"2019-05-31","startDate":"2017-11-22","effectiveTier":"FFBR"}, '+
                        '"airlineCustomerValue":"9879","otherAirlineSchemeCount":0,"qantasFFNo":"0002750079",'+
                        '"qfMemberAirlinePriorityLevel":null,"qfMemberTierClubCode":"FFGD","complaintCount":3,'+
                        '"serviceFailureCount":21,"surpriseDelightCount":1,"mishandledBagCount":39,"flightCancellationCount":26,'+
                        '"flightDelayDomesticCount":13,"flightDelayInternationalCount":6,"flightUpgradeDomesticCount":0,'+
                        '"flightUpgradeInternationalCount":12,"flightDowngradeDomesticCount":22,"flightDowngradeInternationalCount":13,'+
                        '"flightSegmentCount":31,"qantasUniqueID":"900000000013","majorityQCI":"ANZ","majorCorpCountryCode":"AU",'+
                        '"majorCorpBCI":"BCI","majorCorpACI":"ACI","majorCorpOCI":"OCI","majorCorpName":"ANZ Bank",'+
                        '"majorCorpAltName":"Australia New Zealand Banking Corp"}';*/
             String Body = '{"customers":[{"customer":{"qantasUniqueId":"710000000018","customerMatchScore":175,"name":{"firstName":"MARK","middleName":"","lastName":"WAUGH","prefix":"mr","salutation":"Dear Mr.","birthDate":"15/11/1990","genderCode":"M"},"loyaltyDetails":[{"schemeOperatorCode":"QF","loyaltyId":"0006142104","airlineTierCode":"FFBR","startDate":"01/01/1900"}],"phone":[{"typeCode":"B","phoneCountryCode":"61","phoneAreaCode":"08","phoneLineNumber":"98124567","phoneExtension":""},{"typeCode":"H","phoneCountryCode":"61","phoneAreaCode":"","phoneLineNumber":"410678909","phoneExtension":""}],"email":[{"typeCode":"B","emailId":"mark.waugh@mailinator.com"},{"typeCode":"H","emailId":"mark.waugh1@mailinator.com"}],"address":[{"typeCode":"B","line1":"647","line2":"","line3":"london court","line4":"hay st","suburb":"perth","postCode":"6000","state":"wa","country":"AU","countryName":"AUSTRALIA"},{"typeCode":"H","line1":"15","line2":"","line3":"","line4":"hobart rd","suburb":"south launceston","postCode":"7249","state":"tas","country":"AU","countryName":"AUSTRALIA"}],"travelDoc":[{"typeCode":"P","id":"AB123462","issueCountry":"AU"}],"others":{"companyName":"Unilever","comments":"Summary Comment Text18"}}}],"totalMatchedRecords":1}';               
        Map<String, String> mapHeader= new Map<String, String>();
        mapHeader.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new QCC_CAPBookingMock(200, 'Success', Body, mapHeader));
        QL_convertMemberCAPsearchController.invokeWebServiceCAPSearch(recID);
     Test.stoptest();
          }
    }
}