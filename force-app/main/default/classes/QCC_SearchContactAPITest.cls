/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Test Class for QCC_SearchContactAPITest
Inputs:
Test Class:    Not Required
************************************************************************************************
History
************************************************************************************************
03-AUG-2018    Praveen Sampath   InitialDesign
-----------------------------------------------------------------------------------------------------------------------*/
@isTest(seeAllData = false)
private class QCC_SearchContactAPITest {
    @testSetup
    static void createTestData(){ 
        
        TestUtilityDataClassQantas.insertQantasConfigData();
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createAccountTriggerSettingasTrue();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createContactTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createCaseTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createRecoveryTriggerSetting());
        insert lsttrgStatus;
        

        List<Qantas_API__c> lstQantas = TestUtilityDataClassQantas.createQantasAPI();
        insert lstQantas;
        
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Integration Logs Rec Type','Integration Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_PlaceHolderAcc','PlaceHolder Acc 1'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_PlaceHolderAccRectype','Placeholder'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_GenericConDescription','Testtstst jhgdsfkh lsdfh'));
        upsert lstConfigData;

        String recordtypeId = GenericUtils.getObjectRecordTypeId('Account', 'Placeholder');
        Account objAcc = new Account();
        objAcc.RecordtypeId = recordtypeId;
        objAcc.Name = 'PlaceHolder Acc 1';
        insert objAcc;
	}

	
	static TestMethod void searchContactFFPositive(){
		RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        QCC_ContactWrapper conWrap = new QCC_ContactWrapper();
        conWrap.ffNumber = '1234566'; 
        conWrap.lastName = 'Sampath';
        conWrap.firstName = 'Praveen';
        conWrap.ffTier = 'Gold';
        conWrap.capId = '123477888';
        conWrap.isFF = true;
        String msg = Json.serialize(conWrap);
        
        req.requestURI = '/services/apexrest/QCCSearchContact';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(msg);
        
        RestContext.request = req;
        RestContext.response= res;

        
        Test.startTest();
        	QCC_SearchContactAPI.searchContact();
        	system.assert(res.statusCode == 200, 'Response code is not 200');
        Test.StopTest();   
	}

	static TestMethod void searchContactNONFFPositive(){
		RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        QCC_ContactWrapper conWrap = new QCC_ContactWrapper();
        conWrap.ffNumber = ''; 
        conWrap.lastName = '';
        conWrap.firstName = '';
        conWrap.ffTier = '';
        conWrap.capId = '';
        conWrap.isFF = false;
        String msg = Json.serialize(conWrap);
        
        req.requestURI = '/services/apexrest/QCCSearchContact';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(msg);
        
        RestContext.request = req;
        RestContext.response= res;

        
        Test.startTest();
        	QCC_SearchContactAPI.searchContact();
        	system.assert(res.statusCode == 200, 'Response code is not 200');
        Test.StopTest();   
	}

	static TestMethod void searchContactException(){
		RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        String msg = 'hello';
        
        req.requestURI = '/services/apexrest/QCCSearchContact';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(msg);
        
        RestContext.request = req;
        RestContext.response= res;

        
        Test.startTest();
        	QCC_SearchContactAPI.searchContact();
        	system.assert(res.statusCode == 400, 'Response code is not 200');
        Test.StopTest();   
	}

}