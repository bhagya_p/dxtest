@IsTest
public class QAC_AgencySecondaryParsing_Test {
	
    @IsTest
	public static QAC_AgencySecondaryParsing testParse() {
		String json = '{'+
		'  \"primaryAgencyBusiness\": \"Agency Prospect\",'+
		'  \"agencyAddressLine1\": \"street\",'+
		'  \"city\": \"Melbourne\",'+
		'  \"stateCode\": \"NSW\",'+
		'  \"countryCode\": \"AU\",'+
		'  \"postalCode\": \"2135\",'+
		'  \"phoneNumber\": \"+61-02-123456789\",'+
		'  \"employees\": \"6000\",'+
		'  \"accountEmail\": \"test@example.com\"'+
		'}';
        
		QAC_AgencySecondaryParsing obj = QAC_AgencySecondaryParsing.parse(json);
		System.assert(obj != null);
        return obj;
	}
}