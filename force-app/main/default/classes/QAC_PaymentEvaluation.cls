global class QAC_PaymentEvaluation implements Database.Batchable<Service_Payment__c> {
    
    global Service_Payment__c[] start(Database.BatchableContext BC) {
        return [SELECT Id, Name, Work_Order__c, Payment_Type__c, Case__c, Payment_Status__c, Payment_Due_Date__c FROM Service_Payment__c WHERE Payment_Status__c = 'Pending'];
    }
    
    global void execute(Database.BatchableContext BC, List<Service_Payment__c> servicePaymentList) {
    	list<Case> toUpdateCase = new list<Case>();
        list<WorkOrder> toUpdateWorkOrder = new list<WorkOrder>();
        
        for(Service_Payment__c eachPayment : servicePaymentList){
            if(eachPayment.Payment_Due_Date__c != NULL)
            {
                if(eachPayment.Payment_Due_Date__c < Date.today()){
                    if(eachPayment.Case__c != NULL )
                    {
                        //Changes added JIRA:TS-3240
                        toUpdateCase.add(new Case(Id = eachPayment.Case__c, Status = 'Auto Closed', Approve__c='No', Condition_Met__c='No'));    
                        //Changes ends JIRA:TS-3240
                    }
                    if(eachPayment.Work_Order__c != NULL )
                    {
                        toUpdateWorkOrder.add(new WorkOrder(Id= eachPayment.Work_Order__c, Status = 'Canceled', EndDate = Date.today()));
                    }
                    eachPayment.Payment_Status__c = 'Cancelled';
                }    
            }
                        
        }
        
        list<Database.SaveResult> spResults = Database.update(servicePaymentList, false);
        list<Database.SaveResult>  caseResults = Database.update(toUpdateCase, false);
        list<Database.SaveResult>  woResults = Database.update(toUpdateWorkOrder, false);
        
        
    }
    
    public void finish(Database.BatchableContext BC) {
        
    }
}