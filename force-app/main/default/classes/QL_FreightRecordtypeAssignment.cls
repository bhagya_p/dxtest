/***********************************************************************************************************************************

Description: Lightning component handler for the FreightRecordtypeAssignment

History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Karpagam               CRM-3068    LEx-FreightRecordtypeAssignment                T01

Created Date    : 25/05/17 (DD/MM/YYYY)

**********************************************************************************************************************************/

public class QL_FreightRecordtypeAssignment {
    @AuraEnabled public Boolean isSuccess {get;set;}
    @AuraEnabled public String msg {get;set;}
    @AuraEnabled public String recID {get;set;}
    @AuraEnabled
    public static List<String> getlistrecType(){
        List<String> listOption = new list<String>();
        try{
            List<Freight_case_Recordtype__mdt> listtype = [Select Type__c from  Freight_case_Recordtype__mdt WHERE   Type__c!=null Order By Type__c ASC];
            
            
            for(Freight_case_Recordtype__mdt c : listtype ){
                listOption.add(c.Type__c);
            }                
            
            
        }catch(exception ex){
        }
        return listOption;
    }
    @AuraEnabled
    public static QL_FreightRecordtypeAssignment reassignToRecordType(String caseId, String recTypeName){
        QL_FreightRecordtypeAssignment obj = new QL_FreightRecordtypeAssignment();
        try{
            
            Boolean valueChanged = false;
            List<Case> Updatecase = new List<Case>();
            Map<String,String> Metadatavalues= new Map<String,String>();
            Map<String,String> Caserecordtype = new Map<String,String>();
            List<Case> objCase = [Select id, ownerId,Type,RecordTypeId,Status FROM Case where Id=:caseId];
            if(!objCase.isEmpty()){
                for(Case eachCase : objCase ){
                    if(eachCase.Status == 'Closed'){
                        obj.isSuccess = false;
                        obj.msg = 'You cannot change the RecordType for Closed case!';
                        return obj;
                    }
                }    
            } 
            
            
            system.debug('recTypeName**************'+recTypeName); 
            system.debug('objCase **************'+objCase); 
            for(RecordType listofRType : [select Id, DeveloperName from RecordType where sObjectType = 'case' and DeveloperName like '%Freight%'])
                Caserecordtype.put(listofRType.DeveloperName, listofRType.Id);
            
            for(Freight_case_Recordtype__mdt c: [select Type__c,Recordtype_developer_Name__c from Freight_case_Recordtype__mdt])   
                Metadatavalues.put(c.Type__c,c.Recordtype_developer_Name__c);       
            
            for(Case c : objCase )
            {
                if(Metadatavalues.containsKey(recTypeName))
                {
                    system.debug('Caserecordtype.get(Metadatavalues.get(recTypeName))********'+Caserecordtype.get(Metadatavalues.get(recTypeName)));
                    c.Type=recTypeName;
                    c.RecordTypeId = Caserecordtype.get(Metadatavalues.get(recTypeName));
                    Updatecase.add(c);
                }
            }
            if(Updatecase!=null)  
                Update Updatecase;  
            obj.isSuccess = true;
            obj.msg = 'The case RecordType has been updated!';
            
        }catch(Exception e){
            system.debug('Error occured: '+e.getMessage());
        }
        return obj;
    }}