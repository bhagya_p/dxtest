/***********************************************************************************************************************************

Description: Apex class for retrieving the account Information using IATA Code. 

History:
======================================================================================================================
Name                          Description                                           Tag
======================================================================================================================
Yuvaraj MV                    Account record retrieval class                        T01
Ajay                          Edited Class based on Swagger changes                 T02

Created Date    : 09/05/18 (DD/MM/YYYY)

TestClass       :QAC_AcctRetrievalUsingIATA_Test
**********************************************************************************************************************************/

@RestResource(urlmapping='/QACAcctInformationRetrieval/*')
global class QAC_AcctRetrievalUsingIATA 
{
    @HttpGet
    global static void doAcctRetrieval()
    {
        
        QAC_AgencyProfileResponses.QACAgencyPrimaryDetailsResponse myAgPrimResp;
        QAC_AgencyProfileResponses.QACAgencySecondaryDetailsResponse myAgSecResp;
        
        QAC_AgencyAccountRetrieval accret;
        QAC_AgencyAccountRetrieval.AgencyPrimaryDetails primary;
        QAC_AgencyAccountRetrieval.AgencySecondaryDetails secondary;
        
        QAC_AgencyProfileResponses.exceptionResponse mapErrorResponse;
        List<QAC_AgencyProfileResponses.FieldErrors> listFieldErrorResp = new List<QAC_AgencyProfileResponses.FieldErrors>();
        
        string finalResponse;
        
        map<String,Account> existingAccountmap = new map<String,Account>();
        String iataCode;
        String requst;
        
        // Reading Request parameters, JSON extration from Body of request is primary
        
        try
        {
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;
            iataCode = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);

            if(string.isNotEmpty(iataCode))
            {
                for(Account eachAccount : [Select Id,Name,ABN_Tax_Reference__c, Qantas_Industry_Centre_ID__c,Agency_Sub_Chain__c,
                                           QAC_Request_Pending__c,Agency_Sales_Region__r.Name,Active__c,Business_Type__c,
                                           Agency_Sales_Region__r.Hide_TMCTA__c,Agency_Sales_Region__c,BillingStreet,Email__c,
                                           BillingCity,BillingCountryCode,BillingPostalCode,Phone_Number__c,Phone_Country_Code__c,
                                           Phone_Area__c,phone,ATAS__c,BillingState,NumberOfEmployees,Website 
                                           from Account where Qantas_Industry_Centre_ID__c =: iataCode])
                {
                    if(eachAccount != null)
                    {
                        existingAccountmap.put(eachAccount.Qantas_Industry_Centre_ID__c,eachAccount);
                    }
                }
            }
            
             system.debug('existing ***'+existingAccountmap);
            
            if(!existingAccountmap.isEmpty())
            {
                response.statusCode = 200;
               
                    if(existingAccountmap.containsKey(iataCode))
                    {
                        accret = new QAC_AgencyAccountRetrieval();
                        
                        primary =  new QAC_AgencyAccountRetrieval.AgencyPrimaryDetails(existingAccountmap.get(iataCode));
                        secondary = new QAC_AgencyAccountRetrieval.AgencySecondaryDetails(existingAccountmap.get(iataCode));
                        accret.agencyPrimaryDetails = primary;
                        accret.agencySecondaryDetails = secondary;
                        
                        finalResponse = JSON.serializePretty(accret,true);
                        response.responseBody = Blob.valueOf(finalResponse);
                    }
            }
            else if(string.isEmpty(iataCode))
            {
                response.statusCode = 400;
                listFieldErrorResp.add(new QAC_AgencyProfileResponses.FieldErrors('Id','IATA is Blank, Please enter valid IATA code' ));
                mapErrorResponse = new QAC_AgencyProfileResponses.exceptionResponse('400', 'Bad Request', listFieldErrorResp);
                response.responseBody = Blob.valueOf(JSON.serializePretty(mapErrorResponse,true));
            }
            else
            {
                response.statusCode         = 404;
                listFieldErrorResp.add(new QAC_AgencyProfileResponses.FieldErrors('Id','IATA doesnt exits' ));
                mapErrorResponse = new QAC_AgencyProfileResponses.exceptionResponse('404', 'Bad Request', listFieldErrorResp);
                response.responseBody = Blob.valueOf(JSON.serializePretty(mapErrorResponse,true));
            }
        }
        catch(Exception ex)
        {
            
            RestRequest request = RestContext.request;
            RestResponse response = RestContext.response;

            system.debug('Exception###' + ex);
            
            /**CreateLogs.insertLogRec('Log Exception Logs Rec Type', 'QAC - Create Retrieval API', 'QAC_AccountRetrieval',
                                    'create',(finalResponse.length() > 32768) ? finalResponse.substring(0,32767) : finalResponse,
                                    '', true, '', ex);**/
			
            response.statusCode = 500;
            listFieldErrorResp.add(new QAC_AgencyProfileResponses.FieldErrors(ex.getStackTraceString(), ex.getMessage() ));
            mapErrorResponse = new QAC_AgencyProfileResponses.ExceptionResponse('500', 'Salesforce Internal Error', listFieldErrorResp);
            response.responseBody = Blob.valueOf(JSON.serializePretty(mapErrorResponse,true));
        }
    }
}