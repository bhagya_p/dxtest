/**********************************************************************************************
Author:        Pushkar Purushothaman
Company:       TCS
Description:   Fetch case disruption data from the CAP Disruption API 
   
************************************************************************************************
History
************************************************************************************************
24-Oct-2018      Pushkar Purushothaman               Initial Design
**********************************************************************************************/

public without sharing class QCC_FlightDisruptions {
    @AuraEnabled
    public static Object getFlightDisruptionDetails(String ffNum){
        String token = QCC_GenericUtils.getCAPValidToken('QCC_CAPToken');
        if(String.isBlank(token)) return null;
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_FlightDisruptionsAPI');
        DateTime currentDateTime = DateTime.now();
        Long fromTimeStamp = currentDateTime.addMonths(-12).getTime();
        Long toTimeStamp = currentDateTime.getTime();
        System.debug('Endpoint'+qantasAPI.EndPoint__c);
        String endpoint = qantasAPI.EndPoint__c+ffNum+'/flights/disrupted?fromTimeStamp='+fromTimeStamp+'&toTimeStamp='+toTimeStamp+'&includeQFOperatedOnly=true&returnCountOnly=false';
        HTTP http = new HTTP();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setMethod(qantasAPI.Method__c);
        httpRequest.setEndpoint(endpoint);
        httpRequest.setHeader('Authorization', token);
        httpRequest.setHeader('system','SALESFORCE');
        HttpResponse httpResponse = http.send(httpRequest);
        QCC_GenericUtils.updateToken(token, 'QCC_CAPToken');
        if(httpResponse.getStatusCode() == 200){
            Object myObj = JSON.deserializeUntyped(httpResponse.getBody());
            System.debug(httpResponse.getBody());
            return myObj;
        }else{
            System.debug('response: '+httpResponse.getBody());
        }
        return null;
    }
    
}