/* 
 * Created By : Ajay Bharathan | TCS | Cloud Developer 
 * Main Class : QAC_ChildTriggerHandler
*/
@isTest
public class QAC_ChildTriggerHandler_Test {

    @isTest
    public static void testdeleteFlight() {
        
        String agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        String flightRTId = Schema.SObjectType.Flight__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createCaseTriggerSetting();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createAffectedPassengerTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        
        //Inserting Account
        Account myAccount = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Agency Account', 
                                        RecordTypeId = agencyRecordTypeId, Migrated__c = true, ABN_Tax_Reference__c  ='34563565365',
                                        Estimated_Total_Air_Travel_Spend__c = 0,IATA_Number__c = '4364557');
        insert myAccount;

        // inserting Case
        Case newCase = new Case(External_System_Reference_Id__c = 'C-002002002003',Origin = 'QAC Website',RecordTypeId = caseRTId,
                                Type = 'Operational Waiver',Problem_Type__c = 'Schedule Change',Waiver_Sub_Type__c = 'Ticket Refund',Status = 'Open',
                                Subject = 'to be decided',Description = 'some desc',Justification__c = 'must give',Authority_Status__c = 'Approved',
                                AccountId = myAccount.Id,Consultants_Name__c = 'abcd',Consultant_Email__c = 'test@test.com',Consultant_Phone__c = '28213812',
                                PNR_number__c = '454546',Old_PNR_Number__c = '1021512',Passenger_Name__c = 'TestUser',Travel_Type__c = 'Domestic');   
        insert newCase;

        list<Flight__c> flightlist = new list<Flight__c>();
        for(Integer i=0;i<4;i++){
            Flight__c newTA = new Flight__c();
            newTA.RecordTypeId = flightRTId;
            newTA.Case__c = newCase.Id;
            flightList.add(newTA);            
        }
        insert flightList;
        
        Test.startTest();
            delete flightList[0];
            delete flightList[1];
        Test.stopTest();
    }
    
    @isTest
    public static void testdeleteSP() {
        
        String agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createCaseTriggerSetting();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createAffectedPassengerTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        
        //Inserting Account
        Account myAccount = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Agency Account', 
                                        RecordTypeId = agencyRecordTypeId, Migrated__c = true, ABN_Tax_Reference__c  ='34563565365',
                                        Estimated_Total_Air_Travel_Spend__c = 0,IATA_Number__c = '4364557');
        insert myAccount;

        // inserting Case
        Case newCase = new Case(External_System_Reference_Id__c = 'C-002002002003',Origin = 'QAC Website',RecordTypeId = caseRTId,
                                Type = 'Operational Waiver',Problem_Type__c = 'Schedule Change',Waiver_Sub_Type__c = 'Ticket Refund',Status = 'Open',
                                Subject = 'to be decided',Description = 'some desc',Justification__c = 'must give',Authority_Status__c = 'Approved',
                                AccountId = myAccount.Id,Consultants_Name__c = 'abcd',Consultant_Email__c = 'test@test.com',Consultant_Phone__c = '28213812',
                                PNR_number__c = '454547',Old_PNR_Number__c = '1021512',Passenger_Name__c = 'TestUser',Travel_Type__c = 'Domestic');   
        insert newCase;

        list<Service_Payment__c> paylist = new list<Service_Payment__c>();
        for(Integer i=0;i<4;i++){
            Service_Payment__c newSP = new Service_Payment__c();
            newSP.Case__c = newCase.Id;
            paylist.add(newSP);            
        }
        insert paylist;
        
        Test.startTest();
            delete paylist[0];
            delete paylist[1];
        Test.stopTest();
    }

    @isTest
    public static void testdeleteWO() {
        
        String agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createCaseTriggerSetting();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createAffectedPassengerTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        
        //Inserting Account
        Account myAccount = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Agency Account', 
                                        RecordTypeId = agencyRecordTypeId, Migrated__c = true, ABN_Tax_Reference__c  ='34563565365',
                                        Estimated_Total_Air_Travel_Spend__c = 0,IATA_Number__c = '4364557');
        insert myAccount;

        // inserting Case
        Case newCase2 = new Case(External_System_Reference_Id__c = 'C-002002002004',Origin = 'QAC Website',RecordTypeId = caseRTId,
                                Type = 'Operational Waiver',Problem_Type__c = 'Schedule Change',Waiver_Sub_Type__c = 'Ticket Reissue',Status = 'Open',
                                Subject = 'to be decided',Description = 'some desc',Justification__c = 'must give',Authority_Status__c = 'Approved',
                                AccountId = myAccount.Id,Consultants_Name__c = 'abcd',Consultant_Email__c = 'test@test.com',Consultant_Phone__c = '28213812',
                                PNR_number__c = '454548',Old_PNR_Number__c = '1021512',Passenger_Name__c = 'TestUser',Travel_Type__c = 'Domestic');   
        insert newCase2;

        list<WorkOrder> woList = new list<WorkOrder>();
        for(Integer i=0;i<4;i++){
            WorkOrder newWO = new WorkOrder();
            newWO.CaseId = newCase2.Id;
            woList.add(newWO);            
        }
        insert woList;
        
        Test.startTest();
            delete woList[0];
            delete woList[1];
        Test.stopTest();
    }

    @isTest
    public static void testdeletePAX() {
        
        String agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        String passengerRTId = Schema.SObjectType.Affected_Passenger__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createCaseTriggerSetting();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createAffectedPassengerTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        
        //Inserting Account
        Account myAccount = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Agency Account', 
                                        RecordTypeId = agencyRecordTypeId, Migrated__c = true, ABN_Tax_Reference__c  ='34563565365',
                                        Estimated_Total_Air_Travel_Spend__c = 0,IATA_Number__c = '4364557');
        insert myAccount;

        // inserting Case
        Case newCase = new Case(External_System_Reference_Id__c = 'C-002002002003',Origin = 'QAC Website',RecordTypeId = caseRTId,
                                Type = 'Operational Waiver',Problem_Type__c = 'Schedule Change',Waiver_Sub_Type__c = 'Ticket Refund',Status = 'Open',
                                Subject = 'to be decided',Description = 'some desc',Justification__c = 'must give',Authority_Status__c = 'Approved',
                                AccountId = myAccount.Id,Consultants_Name__c = 'abcd',Consultant_Email__c = 'test@test.com',Consultant_Phone__c = '28213812',
                                PNR_number__c = '454549',Old_PNR_Number__c = '1021512',Passenger_Name__c = 'TestUser',Travel_Type__c = 'Domestic');   
        insert newCase;

        list<Affected_Passenger__c> pslist = new list<Affected_Passenger__c>();
        for(Integer i=0;i<4;i++){
            Affected_Passenger__c newPassenger = new Affected_Passenger__c();
            newPassenger.RecordTypeId = passengerRTId;
            newPassenger.Case__c = newCase.Id;
            pslist.add(newPassenger);
        }
        insert pslist;

        Test.startTest();
            delete pslist[0];
            delete pslist[1];
        Test.stopTest();
    }

    @isTest
    public static void testdeleteCaseComment() {
        
        String agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createCaseTriggerSetting();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createAffectedPassengerTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        
        //Inserting Account
        Account myAccount = new Account(Name = 'Sample', Active__c = true, Aquire__c = true, Type = 'Agency Account', 
                                        RecordTypeId = agencyRecordTypeId, Migrated__c = true, ABN_Tax_Reference__c  ='34563565365',
                                        Estimated_Total_Air_Travel_Spend__c = 0,IATA_Number__c = '4364557');
        insert myAccount;

        // inserting Case
        Case newCase = new Case(External_System_Reference_Id__c = 'C-002002002003',Origin = 'QAC Website',RecordTypeId = caseRTId,
                                Type = 'Operational Waiver',Problem_Type__c = 'Schedule Change',Waiver_Sub_Type__c = 'Ticket Refund',Status = 'Open',
                                Subject = 'to be decided',Description = 'some desc',Justification__c = 'must give',Authority_Status__c = 'Approved',
                                AccountId = myAccount.Id,Consultants_Name__c = 'abcd',Consultant_Email__c = 'test@test.com',Consultant_Phone__c = '28213812',
                                PNR_number__c = '454542',Old_PNR_Number__c = '1021512',Passenger_Name__c = 'TestUser',Travel_Type__c = 'Domestic');   
        insert newCase;

        list<CaseComment> cclist = new list<CaseComment>();
        for(Integer i=0;i<4;i++){
            CaseComment newCC = new CaseComment();
            newCC.ParentId = newCase.Id;
            cclist.add(newCC);
        }
        insert cclist;

        Test.startTest();
            delete cclist[0];
            delete cclist[1];
        Test.stopTest();
    }
    
}