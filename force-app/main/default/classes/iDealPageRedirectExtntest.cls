/***********************************************************************************************************************************

Description: Test class for the apex class iDealPageRedirectExtn

History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Bharathkumar Narayanan  CRM-2828    Test class for the apex class iDealPageRedirectExtn                  T01
                                    
Created Date    : 11/06/17 (DD/MM/YYYY)

**********************************************************************************************************************************/

@isTest
public class iDealPageRedirectExtntest {

    @isTest
    public static  void TestIDealPageL(){
        
        test.startTest();
        PageReference pageRef = Page.iDealPageRedirect;

        Account testAccount = new Account(Name='Test Company Name123');
       insert testAccount;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',testAccount.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
        integer count=0;
        iDealPageRedirectExtn testExtn = new iDealPageRedirectExtn(sc);
        
        test.stopTest();
        
        
        
    }
}