/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   QCC Generic Utility Class 
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
24-Oct-2017      Praveen Sampath               Initial Design
31-Oct-2017      Praveen Sampath               getCAPValidToken
18-May-2018      Cuong Ly                      fetchSalesforceEmailTemplate
-----------------------------------------------------------------------------------------------------------------------*/
public without sharing class QCC_GenericUtils {

    public static List<QantasConfigData__c> listDelayCode{get;set;}
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        getCAPValidToken
    Description:        get CAP ValidToken from CAP system while invoking the Service
    Parameter:          
    --------------------------------------------------------------------------------------*/  
    public static String getCAPValidToken(String tokenType){
        return getAPIGatewayValidToken(tokenType);
        /**
        * Deprecated - API Gateway migrated to WSO2
        System.debug('getCAPValidToken ' + tokenType);
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI(tokenType);
        
        String tokenVal = '';
        if(qantasAPI.LastModifiedDate < System.now().addHours(-45) || qantasAPI.Token__c == Null ){
        QCC_CAPTokenWrapper token;
        try {
        token = QCC_InvokeCAPAPI.invokeTokenCapAPI(qantasAPI);  
        } catch(Exception e) {
        System.debug(e.getMessage());
        }
        
        if(token == null) return null;
        
        tokenVal = token.token_type+' '+token.access_token;
        tokenVal = tokenVal.capitalize();
        //QCC_GenericUtils.updateToken(tokenVal, tokenType);
        }
        else{
        tokenVal = qantasAPI.Token__c;
        }
        return tokenVal;*/
    } 
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        getAPIGatewayValidToken
    Description:        get Valid Token from API Gateway based on WSO2 system while invoking the Service
    Parameter:          
    --------------------------------------------------------------------------------------*/  
    public static String getAPIGatewayValidToken(String tokenType){
        System.debug('getCAPValidToken ' + tokenType);
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI(tokenType);
        
        String tokenVal = '';
        if(qantasAPI.LastModifiedDate < System.now().addHours(-45) || qantasAPI.Token__c == Null ){
            QCC_CAPTokenWrapper token;
            try {
                token = QCC_InvokeCAPAPI.invokeTokenAPIGateway(qantasAPI);  
            } catch(Exception e) {
                System.debug(e.getMessage());
            }
            
            if(token == null) return null;
            
            tokenVal = token.token_type+' '+token.access_token;
            tokenVal = tokenVal.capitalize();
            //QCC_GenericUtils.updateToken(tokenVal, tokenType);
        }
        else{
            tokenVal = qantasAPI.Token__c;
        }
        return tokenVal;
    } 
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        fetchGenericContact
    Description:        get Contact that has a genereic email 
    Parameter:          
    --------------------------------------------------------------------------------------*/  
    public static String fetchGenericContact(){
        String email = CustomSettingsUtilities.getConfigDataMap('QCC_ContactEmail');
        String lastName = CustomSettingsUtilities.getConfigDataMap('QCC_NoReplyContactLastName');
        Contact objCon = [Select Id, Email from Contact where LastName =: lastName and Email =: email limit 1];
        return objCOn.Id;
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        updateToken
    Description:        Update token if not matching with the current value in custom setting
    Parameter:          
    --------------------------------------------------------------------------------------*/
    /*Need to comment as the token will be same for all services that are having same Client Id, Client Secret, Endpoint and Scope
    public static void updateToken(String token, string settingName){
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI(settingName);
        
        if(qantasAPI.Token__c != token)
        {
            qantasAPI.Token__c = token;
            update qantasAPI;
        }
        
    }*/
    // Need to uncomment when the above method is commented
    public static void updateToken(String token, string settingName){
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI(settingName);
        
        if(qantasAPI.Token__c != token && String.isNotBlank(token))
        {
            List<Qantas_API__c> apisWithSameCredentialsToUpdate = new List<Qantas_API__c>();
            for(Qantas_API__c api : retrieveAPIsWithSameCredentials(qantasAPI)) {
                api.Token__c = token;
                apisWithSameCredentialsToUpdate.add(api);
            }
            if(apisWithSameCredentialsToUpdate != null && apisWithSameCredentialsToUpdate.size() > 0) {
                update apisWithSameCredentialsToUpdate;
            }            
        }
    }
    
    private static List<Qantas_API__c> retrieveAPIsWithSameCredentials(Qantas_API__c qantasAPI) {
        List<Qantas_API__c> allQantasAPIs = new List<Qantas_API__c>();
        allQantasAPIs = [SELECT Id, Name FROM Qantas_API__c WHERE ClientId__c = :qantasAPI.ClientId__c AND ClientSecret__c = :qantasAPI.ClientSecret__c AND EndPoint__c = :qantasAPI.EndPoint__c AND Scope__c = :qantasAPI.Scope__c];
        return allQantasAPIs;
    }
    /*--------------------------------------------------------------------------------------      
Method Name:        fetchContact
Description:        Fetch Contact Details
Parameter:          CaseWrapper
--------------------------------------------------------------------------------------*/ 
    // public static List<Contact> fetchContact(QCCCaseWrapper qccCaseWrap){
    //   List<Contact> lstCon = new List<Contact>();
    //   if(!String.isBlank(qccCaseWrap.capId)){
    //    lstCon = [Select Id, AccountId from Contact where CAP_ID__c	=: qccCaseWrap.capId];
    //    }else if(!String.isBlank(qccCaseWrap.ffNumber)){
    //        lstCon = [Select Id, AccountId from Contact where Frequent_Flyer_Number__c =: qccCaseWrap.ffNumber];
    //    }
    //    return lstCon;
    //}
    
    
    /*--------------------------------------------------------------------------------------      
Method Name:        mogrifyJSON
Description:        Json for Invoke CAP system
Parameter:          String,Map
--------------------------------------------------------------------------------------*/ 
    public static String mogrifyJSON(String data, Map<String, String> replacements) {
        // Regex to match the start of the line and the key
        // surrounded by quotes and ending with a colon
        String regexFormat = '(?m)^\\s*"{0}"\\s*:';
        
        // Replacement format of the new key surrounded by
        // quotes and ending with a colon
        String replacementFormat = '"{0}" :';
        
        // Since our JSON can come in formatted however the
        // endpoint wants, we need to format it to a standard
        // we know and can handle
        String formattedJSON = JSON.serializePretty(JSON.deserializeUntyped(data));
        
        // Iterate over all the keys we want to replace
        for (String key : replacements.keySet()) {
            // Generate our regex based on the key
            String regex = String.format(
                regexFormat,
                new List<String> {key}
            );
            
            // Generate our replacement
            String replacement = String.format(
                replacementFormat,
                new List<String> {replacements.get(key)}
            );
            
            // Find all and replace
            formattedJSON = formattedJSON.replaceAll(regex, replacement);
        }
        
        return formattedJSON;
    }
    
    public static void postToCaseWithMention(Case caseIn , String content){
        try {
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            mentionSegmentInput.id = caseIn.OwnerId;
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            textSegmentInput.text = content;
            messageBodyInput.messageSegments.add(textSegmentInput);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = caseIn.Id;
            
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, feedItemInput);
        } catch(Exception ex) {
            system.debug('ex###############'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - Case Trigger', 'QCC_GenericUtils', 
                                    'postToCaseWithMention', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
        
    }
    
    public static Datetime convertStringtoDateTime(String dateValue,String timeValue){
        String[] actualDepDateStr = String.isNotBlank(dateValue) ? (dateValue).replace('-', ' ').replace('/', ' ').split(' ') : null;
        String[] actualDepTimeStr = String.isNotBlank(timeValue) ? (timeValue).replace(':', ' ').split(' ') : null;
        Datetime dt = null;
        if(actualDepDateStr != null && actualDepTimeStr != null) {
            Integer dd = Integer.valueOf(actualDepDateStr[0]);
            Integer mm = Integer.valueOf(actualDepDateStr[1]);
            Integer yy = Integer.valueOf(actualDepDateStr[2]);
            Integer hh = Integer.valueOf(actualDepTimeStr[0]);
            Integer mi = Integer.valueOf(actualDepTimeStr[1]);
            Integer ss = Integer.valueOf(actualDepTimeStr[2]);
            dt = Datetime.newInstance(yy, mm, dd, hh, mi, ss);
        }
        return dt;
    }
    
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        fetchSalesforceEmailTemplate
    Description:        get Id of Salesforce EmailTemplate 
    Parameter:          email template name
    --------------------------------------------------------------------------------------*/  
    public static String fetchSalesforceEmailTemplate(String emailTemplateName){
        List<EmailTemplate> emailTemplates = [Select Id from EmailTemplate where Name =: emailTemplateName limit 1];
        if(emailTemplates.size() > 0){
            return emailTemplates.get(0).Id;
        }else{
            return '';
        }
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        formatFlightNumber
    Description:        add '0' prefix to flight number to follow format xxxx 
    Parameter:          flight number
    --------------------------------------------------------------------------------------*/ 
    public static String formatFlightNumber(String flightNumber){
        Integer len = flightNumber.length();
        String formatString = '0000' + flightNumber;
        return formatString.right(4);
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        fetchContactForNonFFCase
    Description:        Fetch the latest contact or create a new contact in case if the latest 
    Case Holds Morethan 10k Cases
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static Contact fetchContactForNonFFCase(){
        String recordtypeId = GenericUtils.getObjectRecordTypeId('Contact', CustomSettingsUtilities.getConfigDataMap('QCC_PlaceHolderAccRectype'));
        List<Contact> lstCon = [Select Id, AccountId, LastName from Contact where RecordTypeId =: recordtypeId Order By CreatedDate DESC limit 1]; 
        
        Integer caseCount = 0;
        Contact objCon = Null;
        if(lstCon != Null && lstCon.size()>0){
            objCon = lstCon[0];
            caseCount = [Select count() from Case where ContactId =: objCon.Id];
        }
        
        if(caseCount >= 10000 || lstCon == Null || lstCon.size() ==0){
            Integer count = (objCon != Null && objCon.LastName.remove('Null').isNumeric())?Integer.valueOf(objCon.LastName.remove('Null'))+1: 1;
            
            Contact objNewCon = new Contact();
            //objNewCon.AccountId = objAcc.Id;
            objNewCon.LastName = 'Null'+count;
            objNewCon.Description = CustomSettingsUtilities.getConfigDataMap('QCC_GenericConDescription');
            objNewCon.RecordTypeId = GenericUtils.getObjectRecordTypeId('Contact', CustomSettingsUtilities.getConfigDataMap('QCC_PlaceHolderAccRectype'));
            insert objNewCon;
            return objNewCon;
        }
        return objCon;
    }
    
	/*--------------------------------------------------------------------------------------      
    Method Name:        fetchContactForNonFFCases
    Description:        Fetch the latest contact or create a new contact if the  
    					contact Holds Morethan 10k Cases. Also, returns the case count.
    Parameter:          
    --------------------------------------------------------------------------------------*/
    public static ContactWrap fetchContactForNonFFCases(){
        String recordtypeId = GenericUtils.getObjectRecordTypeId('Contact', CustomSettingsUtilities.getConfigDataMap('QCC_PlaceHolderAccRectype'));
        List<Contact> lstCon = [Select Id, AccountId, LastName from Contact where RecordTypeId =: recordtypeId Order By CreatedDate DESC limit 1]; 
        ContactWrap conWrap = new ContactWrap();
        Integer caseCount = 0;
        Contact objCon = Null;
        if(lstCon != Null && lstCon.size()>0){
            objCon = lstCon[0];
            caseCount = [Select count() from Case where ContactId =: objCon.Id];
            conWrap.objCon = objCon;
            conWrap.count = caseCount;
        }
        
        if(caseCount >= 10000 || lstCon == Null || lstCon.size() ==0){
            Integer count = (objCon != Null && objCon.LastName.remove('Null').isNumeric())?Integer.valueOf(objCon.LastName.remove('Null'))+1: 1;
            
            Contact objNewCon = new Contact();
            //objNewCon.AccountId = objAcc.Id;
            objNewCon.LastName = 'Null'+count;
            objNewCon.Description = CustomSettingsUtilities.getConfigDataMap('QCC_GenericConDescription');
            objNewCon.RecordTypeId = GenericUtils.getObjectRecordTypeId('Contact', CustomSettingsUtilities.getConfigDataMap('QCC_PlaceHolderAccRectype'));
            insert objNewCon;
            conWrap.objCon = objNewCon;
            conWrap.count = 0;
        }
        
        return conWrap;
    }
    
    public class ContactWrap{
        public Contact objCon;
        public Integer count;
    }

    public static Integer stringToInteger(string val){
        if(String.isBlank(val))
            return 0;
        while(val.startsWith('0')){
            val = val.removeStart('0');
        }
        return Integer.valueOf(val);
    }
    
    public Static Contact fetchContactForFFCase(QCC_ContactWrapper contactWrap){
        Contact objCon = new Contact();
        String ffNumber = contactWrap.ffNumber;
        String ffNumWithOutLeadingZeros = ffNumber.replaceFirst('^0+','');
        System.debug('ffNumWithOutLeadingZeros#### '+ffNumWithOutLeadingZeros);
        String ffNumWithLeadingZeros = '0%'+ffNumWithOutLeadingZeros;
        System.debug('ffNumWithOutLeadingZeros#### '+ffNumWithLeadingZeros);
        List<Contact> lstCon = [Select Id, Frequent_Flyer_Number__c, LastName, FirstName, Frequent_Flyer_Tier__c, CAP_ID__c, AccountId
                               from Contact where Frequent_Flyer_Number__c =: ffNumWithOutLeadingZeros or Frequent_Flyer_Number__c like :ffNumWithLeadingZeros ORDER BY CAP_ID__c NULLS LAST Limit 1];
        if(lstCon != Null && lstCon.size() == 1){
            objCon = lstCon[0];
            objCon.Frequent_Flyer_Tier__c = CustomSettingsUtilities.getConfigDataMap(contactWrap.ffTier);
            objCon.CAP_ID__c = contactWrap.capId;
        }else{
            objCon.Frequent_Flyer_Tier__c = CustomSettingsUtilities.getConfigDataMap(contactWrap.ffTier);
            objCon.CAP_ID__c = contactWrap.capId;
            objCon.FirstName = contactWrap.firstName;
            objCon.LastName = contactWrap.lastName;
            objCon.Frequent_Flyer_Number__c = ffNumWithOutLeadingZeros;//contactWrap.ffNumber;
        }
        upsert objCon;
        return objCon;
    }
    
    public static String doTrim(String str) {
        return String.isNotBlank(str) ? str.trim() : str;
    }

    public static String getDelayReason(String delayCode, String shortDes){
        if(listDelayCode == null || listDelayCode.size()== 0 ){
            listDelayCode = [select name, Config_Description__c, Config_Value__c from QantasConfigData__c where Config_Description__c = 'DelayCode'];
        }
        for(QantasConfigData__c qcd : listDelayCode){
            if(qcd.name.contains(delayCode+'_'+shortDes)){
                System.debug(qcd.Config_Value__c);
                return qcd.Config_Value__c.split(' - ')[0];
            }
        }
        return '';
    }
    
    public static String getIntegerFromString(String str){
        system.debug('tring.isNotBlank(str)###'+str);
        if(String.isNotBlank(str)){
            str = str.remove(':');
        }
           
        if(String.isNotBlank(str) && (str.isAlphanumeric() || str.isAlphanumericSpace())){
            Pattern ptr = Pattern.compile('[^0-9]');
            String intValue = ptr.matcher(str).replaceAll('');
             system.debug('intValue###'+intValue);
            return intValue;
        }
        return null;
    }
}