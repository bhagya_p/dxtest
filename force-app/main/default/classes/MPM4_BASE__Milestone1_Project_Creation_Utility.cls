/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Milestone1_Project_Creation_Utility {
    global Milestone1_Project_Creation_Utility() {

    }
    global void CreateChildProject(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, Map<String,Object> params) {

    }
}
