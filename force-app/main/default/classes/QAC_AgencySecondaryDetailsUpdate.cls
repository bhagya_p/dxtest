/*
 * Created By : Ajay Bharathan | TCS | Cloud Developer 
 * Purpose : to update secondary details for My Agency Profile
 * REST API : QAC_AgencySecondaryParsing
*/

@RestResource(urlmapping='/QACAgencySecondaryInfo/*')
global class QAC_AgencySecondaryDetailsUpdate
{
    public static String responseJSON = '';

    @HttpPatch
    global static void doSecondaryUpdate()
    {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        //QAC_AgencyProfileResponses qacAgResp = new QAC_AgencyProfileResponses();
        QAC_AgencyProfileResponses.QACAgencySecondaryDetailsResponse myAgSecResp;
        QAC_AgencyProfileResponses.ExceptionResponse myExcepResp;
        list<QAC_AgencyProfileResponses.FieldErrors> myFieldErrorRespList = new list<QAC_AgencyProfileResponses.FieldErrors>();
        
        try
        {
            String iataNumber = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
            //QAC_AgencySecondaryParsing receivedRequest = new QAC_AgencySecondaryParsing(); 
            QAC_AgencySecondaryParsing receivedRequest = QAC_AgencySecondaryParsing.parse(request.requestBody.toString());
            system.debug('request @@'+receivedRequest);

            Account existingAccount;
            
            if(String.isNotBlank(iataNumber))
            {
                for(Account eachAccount : [Select Id, BillingState,
                                           Business_Type__c,BillingStreet,BillingCity,BillingCountryCode,BillingPostalCode,
                                           Phone_Number__c,Phone_Country_Code__c,NumberOfEmployees,Email__c,ATAS__c,Website 
                                           from Account where Qantas_industry_centre_id__c =: iataNumber])
                {
                    existingAccount = eachAccount;
                }
                system.debug('existingAccount:'+existingAccount);
                if(existingAccount != null)
                {
                    if(string.isNotBlank(receivedRequest.primaryAgencyBusiness))
                    existingAccount.Business_Type__c = receivedRequest.primaryAgencyBusiness;
                    
                    if(string.isNotBlank(receivedRequest.agencyAddressLine1))
                    existingAccount.BillingStreet = receivedRequest.agencyAddressLine1;
                    
                    if(string.isNotBlank(receivedRequest.city))
                    existingAccount.BillingCity = receivedRequest.city;
                    
                    if(string.isNotBlank(receivedRequest.stateCode))
                    existingAccount.BillingState = receivedRequest.stateCode;
                    
                    if(string.isNotBlank(receivedRequest.countryCode))
                    existingAccount.BillingCountryCode = receivedRequest.countryCode;
                    
                    if(string.isNotBlank(receivedRequest.postalCode))
                    existingAccount.BillingPostalCode = receivedRequest.postalCode;
                    
                    // to replace special Characters in the String
                    String regExp = '[\\-\\+\\.\\^:\\(\\)\\" ",]';
                    String regExpCountry = '[\\(\\)\\" ",]';
                    String replaceExp = '';
                   
                    // to Store Phone Area Code & Country Code
                    String countryCode = '';
                    String areaCode = '';
                    String phoneCode = '';
                    String myPhoneNumber = '';
                    String dialingCode = '';
                    List<string> listPhone;
                    
                    /**if(receivedRequest.PhoneNumber != null && receivedRequest.PhoneNumber.contains('-'))
                    {
                        myPhoneNumber = receivedRequest.PhoneNumber.replaceAll(regExp,replaceExp);
                    	listPhone = receivedRequest.PhoneNumber.split('-');
                        if(listPhone.size() == 3)
                        {
                            countryCode = listPhone[0];
                            areaCode = listPhone[1];
                            phoneCode = listPhone[2];
                            system.debug('countryCode:'+countryCode+';areaCode:'+areaCode+';phoneCode:'+phoneCode);
                   		 } 
                        existingAccount.Phone = myPhoneNumber;
                        existingAccount.Phone_Country_Code__c = countryCode;       
                        existingAccount.Phone_Area__c = areaCode;       
                        existingAccount.Phone_Number__c = phoneCode;
                    }**/
                    if(receivedRequest.PhoneNumber != null)
                    {
                        myPhoneNumber = receivedRequest.PhoneNumber.replaceAll(regExp,replaceExp);
                        myPhoneNumber = myPhoneNumber.replaceAll(' ', '');
                        existingAccount.Phone_Number__c = myPhoneNumber;
                    }
                    //Comment this code inorder to revert
                    if(receivedRequest.dialCode != null)
                    {
                        dialingCode = receivedRequest.dialCode.replaceAll(regExp,replaceExp);
                        dialingCode = dialingCode.replaceAll(' ', '');
                        existingAccount.Phone_Country_Code__c = dialingCode;
                    }
                                        
                    if(string.isNotBlank(receivedRequest.employees))
                    //existingAccount.NumberOfEmployees = (String.isNotBlank(receivedRequest.employees)) ? Integer.valueOf(receivedRequest.employees) : null;
                    existingAccount.NumberOfEmployees = Integer.valueOf(receivedRequest.employees);
                    
                    if(string.isNotBlank(receivedRequest.accountEmail))
                    existingAccount.Email__c = receivedRequest.accountEmail;
                    
                    if(string.isNotBlank(receivedRequest.agencyWebSite))
                    existingAccount.Website = receivedRequest.agencyWebSite;
                    
                    if(string.isNotBlank(receivedRequest.atasNumber))
                        existingAccount.ATAS__c = receivedRequest.atasNumber;
                    
                    update existingAccount;
                    
                    if(existingAccount.Id != null){
                        response.statusCode = 200;
                        myAgSecResp = new QAC_AgencyProfileResponses.QACAgencySecondaryDetailsResponse(existingAccount);
                        responseJson += JSON.serializePretty(myAgSecResp,true);
                        //qacAgResp.qacAgencySecondaryDetailsResponse = myAgSecResp;
                    }
                }
                else 
                {
                    response.statusCode = 404;
                    myFieldErrorRespList.add(new QAC_AgencyProfileResponses.FieldErrors('AccountId','Query Returned NULL in Salesforce'));
                    myExcepResp = new QAC_AgencyProfileResponses.ExceptionResponse('404','Agency does not exist in Salesforce',myFieldErrorRespList);
                    responseJson += JSON.serializePretty(myExcepResp);
                    //qacAgResp.exceptionResponse = myExcepResp;
                }
            }
            else
            {
                response.statusCode = 400;
                myFieldErrorRespList.add(new QAC_AgencyProfileResponses.FieldErrors('IATA_Number__c','Invalid Request'));
                myExcepResp = new QAC_AgencyProfileResponses.ExceptionResponse('400','Invalid IATA Number',myFieldErrorRespList);
                responseJson += JSON.serializePretty(myExcepResp);
                //qacAgResp.exceptionResponse = myExcepResp;
            }
            //responseJson += JSON.serializePretty(qacAgResp,true);
            response.responseBody = Blob.valueOf(responseJson);
        }
        catch(Exception ex)
        {
            system.debug(' @@ ' + ex.getMessage());
            response.statusCode = 500;
            string strException = ex.getStackTraceString();
            strException = strException.replace('QAC_AgencySecondaryDetailsUpdate', '');
            myFieldErrorRespList.add(new QAC_AgencyProfileResponses.FieldErrors('Exception',strException));
            myExcepResp = new QAC_AgencyProfileResponses.ExceptionResponse('500','Salesforce Internal Issue',myFieldErrorRespList);
            responseJson += JSON.serializePretty(myExcepResp);
            //qacAgResp.exceptionResponse = myExcepResp;
            response.responseBody = Blob.valueOf(responseJson);
        }
        finally{
           // CreateLogs.insertLogRec('Log Integration Logs Rec Type', 'QAC Tradesite - QAC_AgencySecondaryDetailsUpdate', 'QAC_AgencySecondaryDetailsUpdate',
             //                       'QAC_AgencySecondaryDetailsUpdate',(request.requestBody.toString().length() > 32768) ? request.requestBody.toString().substring(0,32767) : request.requestBody.toString(), String.valueOf(''), true,'', null);
        }
        
    }
}