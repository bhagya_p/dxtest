/*----------------------------------------------------------------------------------------------------------------------
Author:        Ajay 
Company:       TCS
Description:   Class used for Account Contact Relation tile for lightning component
Test Class:    FreightAllTileControllerTest     
*********************************************************************************************************************************
History
1-Aug-2017    Ajay               Initial Design
        
       
********************************************************************************************************************************/
public class FreightACRTileController {
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        getACRRecord
    Description:        Method for retrieving Account Contact Relation
    Parameter:          Case Id 
    --------------------------------------------------------------------------------------*/
    
    @AuraEnabled
    public static AccountContactRelation getACRRecord(Id caseId) {
        system.debug('Case ID ***'+caseId);
        Case currentCase = [Select Id,AccountId,ContactId,CaseNumber from Case where Id =: caseId];
        Id aId = currentCase.AccountId;
        Id cId = currentCase.ContactId;
        AccountContactRelation currentACR = [Select Id,IsDirect,Freight_City__c,Related_Phone__c,Related_Email__c,Freight_Persona__c,AccountId,Account.Name,Contact.FirstName,Contact.LastName,Job_Title__c,ContactId 
                                             from AccountContactRelation where AccountId =: aId and ContactId =: cId];
        if(currentACR == null)
            return null;
        else
            return currentACR;
    }
    
}