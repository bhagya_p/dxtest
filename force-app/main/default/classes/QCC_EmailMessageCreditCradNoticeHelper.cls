/*----------------------------------------------------------------------------------------------------------------------
Author:        Bharathkumar
Company:       Tata Consultancy services
Description:   QCC EmailMessageCreditCardNoticeHelper
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
20-August-2018      Bharathkumar               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/

public with sharing  class QCC_EmailMessageCreditCradNoticeHelper {

    
    //for masking
    public static void maskCreditCard(List<EmailMessage> EmailMessageList){
        Boolean hasCardInfo;      
       // EmailMessage c = [select id,HasCreditCard__c,Description,Origin from EmailMessage where id = :EmailMessageId Limit 1];
        Boolean isUpdate = false;
        String emailHtmlBody;
        String emailTextBody;
        for(EmailMessage em :EmailMessageList){
                        System.debug('em#####'+em);
        if(em != null && em.subject.contains('Qantas Customer Care')){
          if(em.TextBody != null){
               String str = em.TextBody;
              emailTextBody = em.TextBody;
              str= str.replace('\n',' ');
              str= str.replace(';',' ');
                List<String> spiltStr = str.split(' ');
                for(String s : spiltStr){
                    String s1 = s;
                    System.debug('s####'+s);
                    s= s.replace('\n','');
                    System.debug('s####'+s);
                    s=s.replace('\r','');
                    System.debug('s####'+s);
                    s =  s.trim(); 
                    System.debug('s####'+s);
                    Boolean isAusPhoneNumberWithplus = s.startsWith('+61')||s.startsWith('0')||s.startsWith('61')||s.startsWith('+');                    
                    Boolean isPhoneNumber;
                    System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                    if(isAusPhoneNumberWithplus){
                        System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                        isPhoneNumber =  true;
                        continue;
                    } 
                    System.debug('isAusPhoneNumberWithplus"""""""::::::"""""""'+isAusPhoneNumberWithplus);                    
                    s = s.replaceAll('\\D','');
                    Pattern p=Pattern.compile('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35*\\d{3})*\\d{11})$');
                    Matcher m = p.matcher(s);
                    System.debug('m.matches():'+m.matches());
                    if(m.matches()){                        
                        
                        //String mcard1='5555555555554444';
                        System.debug('mcard1'+s);
                        System.debug('mcard1 length()'+s.length());
                        integer i =s.length();
                        String s2 = s.substring(2, i);
                        System.debug('ss'+s2);
                        String s3 = s.replace(s2, '-XXXX-XXXX-XXXX');
                        System.debug('ss2'+s3);
                        String strChanged = str.replace(s1, s3);
                        str = str.replace(s1, s3);
                       // emailTextBody= strChanged;
                        //em.TextBody = strChanged;
                        em.TextBody = str;
                        System.debug('c.TextBody###'+em.TextBody);
                                              
                    }                    
                }               
               // hasCardInfo = c.HasCreditCard__c;

            }   
            if(em.HtmlBody != null){
               String str = em.HtmlBody;
                str = str.replaceAll('\\<.*?\\>', ' ');
        System.debug(str);
                emailHtmlBody = em.HtmlBody;
              str= str.replace('\n',' ');
                str= str.replace('&nbsp;',' ');
                List<String> spiltStr = str.split(' ');
                for(String s : spiltStr){
                    String s1 = s;
                    System.debug('s####'+s);
                    s= s.replace('\n','');                    
                    System.debug('s####'+s);
                    s=s.replace('\r','');
                    System.debug('s####'+s);
                    s =  s.trim(); 
                    System.debug('s####'+s);
                    Boolean isAusPhoneNumberWithplus = s.startsWith('+61')||s.startsWith('0')||s.startsWith('61')||s.startsWith('+');                    
                    Boolean isPhoneNumber;
                    System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                    if(isAusPhoneNumberWithplus){
                        System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                        isPhoneNumber =  true;
                        continue;
                    } 
                    System.debug('isAusPhoneNumberWithplus"""""""::::::"""""""'+isAusPhoneNumberWithplus);                    
                    s = s.replaceAll('\\D','');
                    Pattern p=Pattern.compile('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35*\\d{3})*\\d{11})$');
                    Matcher m = p.matcher(s);
                    System.debug('m.matches():'+m.matches());
                    if(m.matches()){                        
                        
                        //String mcard1='5555555555554444';
                        System.debug('mcard1'+s);
                        System.debug('mcard1 length()'+s.length());
                        integer i =s.length();
                        //if(i>21){
                        //    i=17;
                       // }
                        String s2 = s.substring(2, i);
                        System.debug('ss'+s2);
                        String s3 = s.replace(s2, '-XXXX-XXXX-XXXX');
                        System.debug('ss2'+s3);
                        String strChanged = str.replace(s1, s3);
                       str =  str.replace(s1, s3);
                        //emailHtmlBody = strChanged;
                        //em.HtmlBody = strChanged;
                        //em.HtmlBody = str;
                        em.HtmlBody = em.HtmlBody.replace(s1, s3);
                        System.debug('c.HtmlBody###'+em.HtmlBody);
                                              
                    }                    
                }               
               // hasCardInfo = c.HasCreditCard__c;

            } 
           // em.TextBody = emailTextBody;
          ////  em.HtmlBody = emailHtmlBody;
        } 
        
        }
               
    }
}