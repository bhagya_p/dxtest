/*-----------------------------------------------------------------------------------------------------------------------------
Author:        Jayaraman Jayaraj
Company:       TCS
Description:   This is FreightCustomerRankingController class to display customer Ranking flag in Account lightning page.
Test Class:    FreightAllTileControllerTest
*********************************************************************************************************************************
*********************************************************************************************************************************
History

08-Aug-2017    Jay         Initial Design
01-Nov-2017    Ajay        Modified - for multiple record types
********************************************************************************************************************************/
public class FreightCustomerRankingController{
    
    @AuraEnabled
    public String recTypeName {get;set;}
    
    @AuraEnabled
    public Account acc {get;set;}
        
    @AuraEnabled
    public static FreightCustomerRankingController getAccRecord(Id caseId) {
        system.debug('Case ID ***'+caseId);
        
        FreightCustomerRankingController fcr = new FreightCustomerRankingController();
        
        Case currentCase = [Select Id,AccountId,ContactId,CaseNumber,RecordType.DeveloperName from Case where Id =: caseId];
        Id accId = currentCase.AccountId;
        
        fcr.recTypeName = (currentCase.RecordType.DeveloperName.contains('Freight')) ? 'Freight' : 'Others';
            
        system.debug('rec T Name**'+fcr.recTypeName);
        
        if(String.isNotBlank(accId)){
            Account currentACC = [Select Id, Name, Qantas_Industry_Centre_ID__c, BillingCountry,BillingCountryCode, Customer_Rank__c, Customer_Account_Tier_f__c, Customer_Ranking__c, Sentiment__c, Sentiment_Colour__c, Sentiment2__c, Sentiment_Indicator_Reason__c from Account where Id =: accId];
            fcr.acc = currentAcc;
        }
        return fcr;
            
    }
}