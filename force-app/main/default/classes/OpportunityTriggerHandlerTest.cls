/**
 * This class contains unit tests for validating the behavior of OpportunityTriggerHandler class.
 */
@isTest
public class OpportunityTriggerHandlerTest {
    
    public static testMethod void createQuoteTest() {
        Id LoyaltyRecordTypeId= GenericUtils.getObjectRecordTypeId('Quote', 'Loyalty Commercial');
                
        List<Opportunity> opp1 = [Select Id, Name, StageName FROM Opportunity];        
        List<Quote> q = [Select Id, RecordTypeId, OpportunityId, Price_Per_Point__c, Active__c FROM Quote];
        
        for(Integer j = 0; j < opp1.size(); j++)
        {
            opp1[j].StageName = 'Negotiate';
        }
        
        update opp1;
        
        
        List<Quote> qts = [Select Id, RecordTypeId, OpportunityId, Price_Per_Point__c, Active__c FROM Quote WHERE OpportunityId = :opp1 and RecordTypeId=:LoyaltyRecordTypeId ORDER BY QuoteNumber];
        
        for(integer i = 0; i < qts.size(); i++) {
            System.assertEquals( LoyaltyRecordTypeId, qts[i].RecordTypeId);
            for(integer j = 0; j < q.size(); j++)
            if(qts[i].id == q[j].Id) {
                System.assertEquals(True, qts[i].Active__c);
            }
        }
    }
    
    
    @testSetup static void insertData() {
        Id LoyaltyRecordTypeId= GenericUtils.getObjectRecordTypeId('Quote', 'Loyalty Commercial');
        Id oppRecordTypeId= GenericUtils.getObjectRecordTypeId('Opportunity', 'Loyalty Commercial');
        List<Account> accList = new List<Account>{};
        List<Opportunity> oppList = new List<Opportunity>{};
        List<Quote> quoteList = new List<Quote>{};
        for(integer i = 0; i < 5; i++)
        {
            Account acc = new Account(Name = 'Test1');
            accList.add(acc);
        }
        insert accList;
        for(integer i = 0; i < 5; i++)
        {
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = oppRecordTypeId;
            opp.AccountId = accList[i].Id;
            opp.Name = 'Opp'+i;
            opp.Customer_Base__c = 1000;
            opp.Points_Per__c = 1.45;
            opp.Price_Per_Point__c = 10;
            opp.Assumed_Tag_Rate_at_Maturity__c = 1.45;
            opp.Average_Spend_Per_Customer__c = 1000;
            opp.StageName = 'Prospect';
            opp.CloseDate = System.today();
            oppList.add(opp);
        }
        
        insert oppList;
        for(integer i = 0; i < 5; i++) {
            Quote q = new Quote(RecordTypeId=LoyaltyRecordTypeId,Name='Opp Quote'+i, OpportunityId = oppList[i].Id, Active__c = true);
            quoteList.add(q);
        }
        insert quoteList;
    }
}