@isTest

private class QCC_CustomerSearchTest{

    @testSetup
    static void createData(){
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createAccountTriggerSettingasTrue();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createContactTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createCaseTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createRecoveryTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        List<Qantas_API__c> lstQantas = TestUtilityDataClassQantas.createQantasAPI();
        insert lstQantas;

        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Integration Logs Rec Type','Integration Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('RecoveryStatusApproved','Approved'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('RecoveryType','Qantas Points'));
        
        insert lstConfigData;
        //Create Account
        Account objAcc = TestUtilityDataClassQantas.createAccount();
        objAcc.Airline_Level__c = '1';
        objAcc.Active__c = true;
        insert objAcc;
        system.assert(objAcc != Null, 'Account is not inserted');

        //Create Contact
        Contact objContact = TestUtilityDataClassQantas.createContact(objAcc.Id);
        objContact.LastName='ContTest2';
        objContact.CAP_ID__c='710000000017';
        insert objContact;
        system.assert(objContact.Id != Null, 'Contact is not inserted');

        //Create Case
        List<Contact> lstContact = new List<Contact>();
        lstContact.add(objContact);
        String recordType = 'CCC Compliment';
        String type = 'Compliment';
        List<Case> lstCase = TestUtilityDataClassQantas.insertCases(lstContact, recordType, type, 'Qantas.com','','','');
        system.assert(lstCase.size()>0, 'Case List is Zero');
        system.assert(lstCase[0].Id != Null, 'Case is not inserted');

        //Create Recovery
        List<Recovery__c> lstRecovery = TestUtilityDataClassQantas.insertRecoveries(lstCase);
        system.assert(lstRecovery.size()>0, 'Recovery List is Zero');
        system.assert(lstRecovery[0].Id != Null, 'Recovery is not inserted');
        Map<String, Profile> profMap = TestUtilityDataClassQantas.getProfileMap();
        User u1 = new User(LastName = 'User1', Alias = 'User', Email = 'sampleuser1@sample.com', 
         Username = 'sampleuser1username@sample.com', ProfileId = profMap.get('Qantas CC Consultant').Id,
         EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
         TimeZoneSidKey = 'Australia/Sydney'
         );

         insert u1;
    }

    static testMethod void test_searchCAP() {

        
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
         User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
         QCC_CustomerSearchHandler.ContactWrapper result2;
         QCC_CustomerSearchHandler.ContactWrapper result1;
         System.runAs(u1){
            //Invalid search
            Test.startTest();
                result1 = QCC_CustomerSearchController.executeSearch(null,null,false);
                Contact contact1 = new Contact(Firstname='gold',Lastname='Frequentlfyer' , Email='gold.frequentflyer1@mailinator.com' , Phone_Line_Number__c = '0423535353' , Frequent_Flyer_Number__c = '34095545435', Phone_Extension__c='34' , Phone_Area_Code__c='234', Phone_Country_Code__c = '09');
                result2 = QCC_CustomerSearchController.executeSearch(contact1,'YSTD',false);
            Test.stopTest();

            System.assert(result1 == null);
            System.assert(result2.contacts.size() > 0);
            for(Contact con : result2.contacts){
                System.assert(con.Profile_Homeport__c != null);
                System.assert(con.Source__c == 'CAP');
            }
        }
        
    }


    static testMethod void test_searchCRM() {

        
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
         User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
         System.runAs(u1){
            //search existing contact
            Contact contact = new Contact(Firstname='Triton',Lastname='Challenger' , email = 'testwe@email.com');
            Test.startTest();
            insert contact;
            QCC_CustomerSearchHandler.ContactWrapper result3 = QCC_CustomerSearchController.executeSearch(contact,'',true);
            Test.stopTest();
            System.assert(result3.contacts.size() > 0);
            for(Contact con : result3.contacts){
                System.assert(con.Source__c == 'SFDC');
            }
        }
        
    }

    static testMethod void test_searchCRMNotFound() {

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
         User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
         System.runAs(u1){
            //search existing contact
            Contact contact = new Contact(Firstname='GOLD',Lastname='Challengererefe' , Email = 'gold.frequentflyer1@mailinator.com', Frequent_Flyer_Number__c='45',Phone_Extension__c='44',Phone_Area_Code__c='45',Phone_Line_Number__c='45',Phone_Country_Code__c='45');
            QCC_CustomerSearchHandler.ContactWrapper result3 = QCC_CustomerSearchController.executeSearch(contact,'',true);
            System.assert(result3.contacts.size() == 0);
        }
        Test.stopTest();
    }

    //static testMethod void test_updateExisting() {

        
    //    Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
    //     User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
    //     QCC_CustomerSearchHandler.ContactWrapper result2;
    //     QCC_CustomerSearchHandler.ContactWrapper result1;
    //     System.runAs(u1){
    //        //Invalid search
    //        Test.startTest();
    //            result1 = QCC_CustomerSearchController.executeSearch(null,null,false);
    //            Contact contact1 = new Contact(Firstname ='gold',Lastname='Frequentlfyer' , Email='gold.frequentflyer1@mailinator.com' , Phone_Line_Number__c = '0423535353' , Frequent_Flyer_Number__c = '34095545435', Phone_Extension__c='34' , Phone_Area_Code__c='234', Phone_Country_Code__c = '09' , CAP_ID__c = '700000000037');
    //            insert contact1;
    //            result2 = QCC_CustomerSearchController.executeSearch(contact1,'YSTD',false);
    //            Contact newContact = QCC_CustomerSearchController.saveIt(result2.contacts[0]);
    //            System.assert(newContact.CAP_ID__c == '710000000025');
    //        Test.stopTest();
    //    }
        
    //}

    static testmethod void testAssignToCase(){
        //loadData();
        
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        QCC_CustomerSearchHandler.ContactWrapper result;
        Case cs;
        System.runAs(u1){
            Contact contact1 = new Contact(Firstname ='gold',Lastname='Frequentlfyer' , Email='gold.frequentflyer1@mailinator.com' , Phone_Line_Number__c = '0423535353' , Frequent_Flyer_Number__c = '34095545435', Phone_Extension__c='34' , Phone_Area_Code__c='234', Phone_Country_Code__c = '09' , CAP_ID__c = '700000000037');
            Test.startTest();
                result = QCC_CustomerSearchController.executeSearch(contact1,'',false);
                System.assert(result  != null);
                System.assert(result.contacts[0] != null);
                System.assert(result.contacts[0].Source__c == 'CAP');
                cs = TestUtilityDataClassQantas.createCaseWithCodingValues('CCC Further Action Required', 'Further Action Required', 'In-Flight', 'Food and Beverage', 'Catering Quality/Selection', 'Presentation/quality of selection');
                insert cs;
            
            Case assigned = QCC_CustomerSearchController.AssignToContact(result.contacts[0] , cs.Id);
            Test.stopTest();
            System.assert(result.contacts[0] != null);
            System.assert(result.contacts[0].Source__c == 'CAP');

            System.assert(assigned.ContactId != cs.Id);
        }
    }

      static testmethod void testCreateContact(){
        //loadData();
        
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        QCC_CustomerSearchHandler.ContactWrapper result;
        Case cs;
        System.runAs(u1){
            Contact contact1 = new Contact(Firstname ='gold',Lastname='Frequentlfyer' , Email='gold.frequentflyer1@mailinator.com' , Phone_Line_Number__c = '0423535353' , Frequent_Flyer_Number__c = '34095545435', Phone_Extension__c='34' , Phone_Area_Code__c='234', Phone_Country_Code__c = '09' , CAP_ID__c = '700000000037');
            Test.startTest();
                result = QCC_CustomerSearchController.executeSearch(contact1,'',false);
                System.assert(result  != null);
                System.assert(result.contacts[0] != null);
                System.assert(result.contacts[0].Source__c == 'CAP');
                cs = TestUtilityDataClassQantas.createCaseWithCodingValues('CCC Further Action Required', 'Further Action Required', 'In-Flight', 'Food and Beverage', 'Catering Quality/Selection', 'Presentation/quality of selection');
                insert cs;
            
            Contact created = QCC_CustomerSearchController.saveIt(result.contacts[0]);
            Test.stopTest();
            System.assert(created != null);
            System.assert(created.Id != null);
        }
    }
      static testmethod void testCreateNewCRMContact(){
        //loadData();
        
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        QCC_CustomerSearchHandler.ContactWrapper result;
        System.runAs(u1){
            Contact contact1 = new Contact(Firstname ='gold',Lastname='Frequentlfyer' , Email='gold.frequentflyer11@mailinator.com' , Phone_Line_Number__c = '042353151353' , Frequent_Flyer_Number__c = '34095545434156' , Phone_Area_Code__c='2314', Phone_Country_Code__c = '019');
            Test.startTest();
                result = QCC_CustomerSearchController.executeSearch(contact1,'',false);
                //System.assert(result  == null);
                Contact created = QCC_CustomerSearchController.saveIt(contact1);
                System.assert(created != null);
                System.assert(created.Id != null);
                //update exiting CRM contact
                created.LastName = 'Testing';
                Contact updated = QCC_CustomerSearchController.saveIt(contact1);
                System.assert(created.LastName == 'Testing');
            Test.stopTest();

        }
    }

      static testmethod void test_updateExistingContact(){
        //loadData();
        
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        QCC_CustomerSearchHandler.ContactWrapper result;
        Case cs;
        System.runAs(u1){
            Contact contact1 = new Contact(Firstname ='gold',Lastname='Frequentlfyer' , Email='gold.frequentflyer1@mailinator.com' , Phone_Line_Number__c = '0423535353' , Frequent_Flyer_Number__c = '34095545435', Phone_Extension__c='34' , Phone_Area_Code__c='234', Phone_Country_Code__c = '09' , CAP_ID__c = '700000000037');
            Test.startTest();
                result = QCC_CustomerSearchController.executeSearch(contact1,'',false);
                System.assert(result  != null);
                System.assert(result.contacts[0] != null);
                System.assert(result.contacts[0].Source__c == 'CAP');
                cs = TestUtilityDataClassQantas.createCaseWithCodingValues('CCC Further Action Required', 'Further Action Required', 'In-Flight', 'Food and Beverage', 'Catering Quality/Selection', 'Presentation/quality of selection');
                insert cs;
            //create contact
            Contact created = QCC_CustomerSearchController.saveIt(result.contacts[0]);
            String capId = created.CAP_ID__c;
            created.CAP_ID__c = 'XXXXXXXXXXXXXX';
            update created;
            //get contact from cap again
            result = QCC_CustomerSearchController.executeSearch(contact1,'',false);
            //System.assert(result  != null);
            //Contact updated = QCC_CustomerSearchController.saveIt(result.contacts[0]);
            //System.assert(updated.CAP_ID__c == capId);

            //Test.stopTest();
            //System.assert(updated != null);
            //System.assert(updated.Id != null);
        }
    }
  
 	static testMethod void test_searchCRM2() {
        Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
         User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
         System.runAs(u1){
            //search existing contact
            Contact contact = new Contact(Firstname='Triton',Lastname='Challenger' , email = 'testwe@email.com',Phone_Line_Number__c='123456',
                                         Alternate_Phone__c = '123456');
            Test.startTest();
            insert contact;
            QCC_CustomerSearchHandler.ContactWrapper result3 = QCC_CustomerSearchController.executeSearch(contact,'',true);
            Test.stopTest();
            System.assert(result3.contacts.size() > 0);
            for(Contact con : result3.contacts){
                System.assert(con.Source__c == 'SFDC');
            }
        }
    }
    
    static testMethod void testForceSaveContact() {
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
         System.runAs(u1){
             //insert existing contact
             Contact contact = new Contact(Lastname='ContTest2' , email = 'testwe@email.com',Phone_Line_Number__c='123456',
                                           Alternate_Phone__c = '123456',MailingStateCode='NSW');
             Test.startTest();
             QCC_CustomerSearchHandler.forceSaveContact(contact, true);
             Test.stopTest();
         }
    }
	
    static testMethod void testForceSaveContact2() {
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
         System.runAs(u1){
             //insert existing contact
             Contact contact = new Contact(Lastname='ContTest2' , email = 'testwe@email.com',Phone_Line_Number__c='123456',
                                           Alternate_Phone__c = '123456',MailingStateCode='NSW');
             Test.startTest();
             QCC_CustomerSearchHandler.forceSaveContact(contact, false);
             Test.stopTest();
         }
    }
    
    static testMethod void test_searchCAPException() {
        //Test.setMock(HttpCalloutMock.class, new QCC_CAPServicesMock());
        User u1 = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        QCC_CustomerSearchHandler.ContactWrapper result2;
        System.runAs(u1){
            //Invalid search
            Test.startTest();
            Contact contact1 = new Contact(Firstname='Triton',Lastname='CHAIRMANSLOUNGE', Phone_Line_Number__c = '0412345678', email='test@test.com',Frequent_Flyer_Number__c=
                                           '0123456789',Phone_Extension__c='04000',Phone_Country_Code__c='091');
            result2 = QCC_CustomerSearchController.executeSearch(contact1,'K5F5ZE',false);
            Test.stopTest();
        }
    }
    
    static testMethod void testProcessResponseException() {
        Test.startTest();
        QCC_CustomerSearchHandler.processResponse(null, 100, false);
        Test.stopTest();
        List<Log__c> logs = [SELECT Id FROM Log__c Where Class_Name__c = 'QCC_CustomerSearchHandler'];
        System.assert(logs.size() > 0, 'Log for QCC_CustomerSearchHandler not found');
    }
    
    static testMethod void testGetExistingContactException() {
        Test.startTest();
        QCC_CustomerSearchHandler.getExistingContact(null);
        Test.stopTest();
        List<Log__c> logs = [SELECT Id FROM Log__c Where Class_Name__c = 'QCC_CustomerSearchHandler'];
        System.assert(logs.size() > 0, 'Log for QCC_CustomerSearchHandler not found');
    }
    
    static testMethod void testSearchCRMContactsException() {
        Test.startTest();
        QCC_CustomerSearchHandler.searchCRMContacts(null);
        Test.stopTest();
        List<Log__c> logs = [SELECT Id FROM Log__c Where Class_Name__c = 'QCC_CustomerSearchHandler'];
        System.assert(logs.size() > 0, 'Log for QCC_CustomerSearchHandler not found');
    }
}