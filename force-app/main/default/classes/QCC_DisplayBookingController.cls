/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath 
Company:       Capgemini
Description:   QCC Display Booking Controlller
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
31-Oct-2017      Praveen Sampath               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public class QCC_DisplayBookingController {
     
    /*--------------------------------------------------------------------------------------      
    Method Name:        fetchCAPBooking
    Description:        To get List of booking information from CAP system
    Parameter:          RecordId,Integer
    --------------------------------------------------------------------------------------*/ 
    @AuraEnabled
    public static QCC_BookingSummaryWrapper fetchCAPBooking(String recordId, Integer pageNumber){
        try{
            Contact objCon = contactSelect(recordId);
            //String capID = '710000000017';
            String capID = objCon.CAP_ID__c;
            String bookingPerPage ='10';
            QCC_BookingSummaryWrapper booking = QCC_InvokeCAPAPI.invokeBookingCAPAPI(capID, bookingPerPage, pageNumber);
            return booking;
        }catch(Exception ex){
            system.debug('Exception###'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - CAP Booking', 'QCC_DisplayBookingController', 
                                    'fetchCAPBooking', '', '', false,'', ex);
        }
        return null;
    }
    

    

    /*--------------------------------------------------------------------------------------      
    Method Name:        contactSelect
    Description:        Fetch the Contact Information
    Parameter:          ID
    --------------------------------------------------------------------------------------*/ 
    private static Contact contactSelect (Id recordId) {
        String recId = recordId;
        Contact objCon;
        if(recId.startsWith('003')){
            objCon = [SELECT Id, CAP_ID__c, FirstName, LastName, Frequent_Flyer_Number__c FROM Contact 
                      WHERE Id =: recordId];
        }
        else if(recId.startsWith('500')){
            Case objCase = [Select Id, ContactId, Contact.CAP_ID__c, Contact.Frequent_Flyer_Number__c, Contact.FirstName,
                            Contact.LastName from Case where Id =: recordId];
            objCon = objCase.Contact;

        }
        return objCon;
    }

}