@isTest
public class EmailMessageTriggerHelperTest {
    @testSetup 
    static void setup() {
        //List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.enableTriggers();
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createAccountTriggerSettingasTrue();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createContactTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createCaseTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createEmailMessageTriggerSetting());
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        QantasConfigData__c qc = new QantasConfigData__c();
        qc.Name = 'Log Exception Logs Rec Type';
        qc.Config_Value__c = 'Exception Logs';
        qc.Config_Description__c = 'Log Exception Logs Record Type Name';
        insert qc;
        //TestUtilityDataClassQantas.insertQantasConfigData();
        Map<String, Profile> profMap = TestUtilityDataClassQantas.getProfileMap();

        List<User> testUsers = new List<User>();
        User u1 = new User(LastName = 'User1', Alias = 'User', Email = 'sampleuser1@sample.com', 
                               Username = 'sampleuser1username@sample.com', ProfileId = profMap.get('Qantas CC Consultant').Id,
                               EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',Location__c='Hobart',
                               TimeZoneSidKey = 'Australia/Sydney'
                              );
        testUsers.add(u1);
        User u2 = new User(LastName = 'User2', Alias = 'User', Email = 'sampleuser2@sample.com', 
                               Username = 'sampleuser2username@sample.com', ProfileId = profMap.get('Data Administrator').Id,
                               EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',Location__c='Hobart',
                               TimeZoneSidKey = 'Australia/Sydney'
                              );
        testUsers.add(u2);
        User u3 = new User(LastName = 'User3', Alias = 'User', Email = 'sampleuser3@sample.com', 
                               Username = 'sampleuser3username@sample.com', ProfileId = profMap.get('Qantas CC Team Lead').Id,
                               EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',Location__c='Hobart',
                               TimeZoneSidKey = 'Australia/Sydney'
                              );
        testUsers.add(u3);
        User u4 = new User(LastName = 'User4', Alias = 'User', Email = 'sampleuser4@sample.com', 
                               Username = 'sampleuser4username@sample.com', ProfileId = profMap.get('Qantas CC Operational Manager').Id,
                               EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',Location__c='Hobart',
                               TimeZoneSidKey = 'Australia/Sydney'
                              );
        testUsers.add(u4);
        insert testUsers;

        System.runAs(u1){
            Account acc        = TestUtilityDataClassQantas.createAccountOfQantasCorporateEmailAccount();
            acc.ABN_Tax_Reference__c = '11111111111';
            insert acc;
            System.debug('ZZZ in EmailMessageTriggerHelperTest testsetup acc: '+acc);
            Contact con        = TestUtilityDataClassQantas.createContact(acc.Id);
            con.Business_Types__c = 'Agency';
            con.Job_Role__c       = 'Other';
            con.Function__c       = 'Other';
            con.Frequent_Flyer_Tier__c = 'Bronze';
            insert con;
            con.AccountId         = acc.id;
            update con; 
            System.debug('ZZZ in EmailMessageTriggerHelperTest testsetup con: '+con);
            System.debug('ZZZ in EmailMessageTriggerHelperTest testsetup con Acc Name: '+con.Account.Name);
            Case newcase1      = TestUtilityDataClassQantas.createCaseWithCodingValues('CCC Complaints','Complaint','Departure Airport','Announcements','General','');
            newcase1.ContactId = con.id;
            newcase1.Origin    = 'Phone';
            insert newcase1;
        }
    }  

    @isTest 
    static void PreventdeleteEmailMessage1(){
        User u1 = [Select Username from User where Username = 'sampleuser1username@sample.com'];
        User u2 = [Select Username from User where Username = 'sampleuser2username@sample.com'];
        Account acct = [SELECT Id,Name FROM Account WHERE Name='QantasCorporateEmailAccount' LIMIT 1];
        Contact cont = [Select Id,Name,Email,AccountId From Contact Where Account.Name ='QantasCorporateEmailAccount' Limit 1];
        Case ncase   = [Select Id,CaseNumber,ContactId,Record_Type__c , Type From Case where ContactId=: cont.id Limit 1];
            
        Test.startTest();
        ncase.Type   = 'Baggage';
        EmailMessage em   = new EmailMessage();
        System.runAs(u2){
            //Sending email         
            em.Incoming       = false;
            em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            em.ToAddress      = cont.Email;
            em.Subject        = 'Qantas Customer Care';
            em.HtmlBody       = '<br/><h1>Hello!!</h1>';
            em.Status         = '3';
            em.ParentId       = ncase.id;
            insert em;
        }
        
        System.runAs(u1){
            try {
                EmailMessage msg = [SELECT Id FROM EmailMessage WHERE Id =: em.Id];
                delete msg;
            } catch(Exception e) {
                System.debug(e.getMessage());
                System.assert(e.getMessage().contains('You do not have access to delete the EmailMessage'));
            }
        }
        Test.stopTest();
    }
    
    @isTest 
    static void PreventdeleteEmailMessage2(){
        User u3 = [Select Id, Username from User where Username = 'sampleuser3username@sample.com'];
        User u2 = [Select Id, Username from User where Username = 'sampleuser2username@sample.com'];
        Account acct = [SELECT Id,Name FROM Account WHERE Name='QantasCorporateEmailAccount' LIMIT 1];
        Contact cont = [Select Id,Name,Email,AccountId From Contact Where Account.Name ='QantasCorporateEmailAccount' Limit 1];
        Case ncase   = [Select Id, OwnerId From Case where ContactId=: cont.id Limit 1];
            
        Test.startTest();
        ncase.OwnerId   = u3.Id;
        update ncase;
        EmailMessage em   = new EmailMessage();
        System.runAs(u2){
            //Sending email         
            em.Incoming       = false;
            em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            em.ToAddress      = cont.Email;
            em.Subject        = 'Qantas Customer Care';
            em.HtmlBody       = '<br/><h1>Hello!!</h1>';
            em.Status         = '3';
            em.ParentId       = ncase.id;
            insert em;
        }
        
        System.runAs(u3){
            try {
                EmailMessage msg = [SELECT Id FROM EmailMessage WHERE Id =: em.Id];
                 delete msg;
            } catch(Exception e) {
                System.debug(e.getMessage());
                System.assert(e.getMessage().contains('You do not have access to delete the EmailMessage'));
            }
        }
        Test.stopTest();
    }
    
    @isTest 
    static void PreventdeleteEmailMessage3(){
        User u4 = [Select Id, Username from User where Username = 'sampleuser4username@sample.com'];
        User u2 = [Select Id, Username from User where Username = 'sampleuser2username@sample.com'];
        Account acct = [SELECT Id,Name FROM Account WHERE Name='QantasCorporateEmailAccount' LIMIT 1];
        Contact cont = [Select Id,Name,Email,AccountId From Contact Where Account.Name ='QantasCorporateEmailAccount' Limit 1];
        Case ncase   = [Select Id, OwnerId From Case where ContactId=: cont.id Limit 1];
            
        Test.startTest();
        ncase.OwnerId   = u4.Id;
        update ncase;
        EmailMessage em   = new EmailMessage();
        System.runAs(u2){
            //Sending email         
            em.Incoming       = false;
            em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            em.ToAddress      = cont.Email;
            em.Subject        = 'Qantas Customer Care';
            em.HtmlBody       = '<br/><h1>Hello!!</h1>';
            em.Status         = '3';
            em.ParentId       = ncase.id;
            insert em;
        }
        
        System.runAs(u4){
            try {
                EmailMessage msg = [SELECT Id FROM EmailMessage WHERE Id =: em.Id];
                 delete msg;
            } catch(Exception e) {
                System.debug(e.getMessage());
                System.assert(e.getMessage().contains('You do not have access to delete the EmailMessage'));
            }
        }
        Test.stopTest();
    }

    @isTest 
    static void caseClosedAndReopened() {
        User u2 = [Select Username from User where Username = 'sampleuser2username@sample.com'];
        Account acct = [SELECT Id,Name FROM Account WHERE Name='QantasCorporateEmailAccount' LIMIT 1];
        Contact cont = [Select Id,Name,Email,AccountId From Contact Where Account.Name ='QantasCorporateEmailAccount' Limit 1];
        Case ncase   = [Select Id,CaseNumber,ContactId,Record_Type__c , Type From Case where ContactId=: cont.id Limit 1];
        ncase.Type   = 'Baggage';

        
        //List<Task> tt = [Select Subject,Status,Case_Status__c,ActivityDate,WhatId,WhoId,Email__c,Task_From__c From Task Where WhatId=: em.ParentId];
        ncase.Auto_Close_on_Email_Sent__c = true;
        update ncase;

        Test.startTest();
        EmailMessage em   = new EmailMessage();
        system.runAs(u2) {
            //Sending email
            em.Incoming       = false;
            em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            em.ToAddress      = cont.Email;
            em.Subject        = 'Qantas Customer Care';
            em.HtmlBody       = '<br/><h1>Hello!!</h1>';
            em.Status         = '3';
            em.ParentId       = ncase.id;
            insert em;
        }

        Case closedCase     = [select Id , Status FROM Case where Id =: ncase.id];
        System.assert(closedCase.Status == 'Closed');


        //Customer's response
        EmailMessage emIn   = new EmailMessage();
        system.runAs(u2) {
            emIn.Incoming       = true;
            emIn.FromAddress    = cont.Email;
            emIn.ToAddress      = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            emIn.Subject        = 'Qantas Customer Care';
            emIn.HtmlBody       = '<br/><h1>Hello!!</h1>';
            emIn.Status         = '3';
            emIn.ParentId       = ncase.id;
            insert emIn;
        }
        Case reopenedCase   = [select Id , Status FROM Case where Id =: ncase.id];
        System.debug('reopenedCase####'+reopenedCase);
        System.assert(reopenedCase.Status == 'Reopened');
        Test.stopTest();

    }

    static testMethod void caseClosedAndReopened2() {
        User u1 = [Select Id, Username from User where Username = 'sampleuser1username@sample.com'];
        User u2 = [Select Id, Username from User where Username = 'sampleuser2username@sample.com'];
        System.debug('userid = '+u1.Id);
        Account acct = [SELECT Id,Name FROM Account WHERE Name='QantasCorporateEmailAccount' LIMIT 1];
        Contact cont = [Select Id,Name,Email,AccountId From Contact Where Account.Name ='QantasCorporateEmailAccount' Limit 1];
        Case ncase   = [Select Id,CaseNumber,ContactId, RecordTypeId, Type, Group__c, Cabin_Class__c, OwnerId From Case where ContactId=: cont.id Limit 1];
        ncase.Type   = 'Baggage';
        ncase.Group__c = 'Delayed Baggage';
        ncase.Cabin_Class__c = 'Economy';
        ncase.RecordTypeId = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case Baggage RecType'));
        System.debug('case owner = '+ncase.OwnerId);
        ncase.OwnerId = u1.Id;
        //List<Task> tt = [Select Subject,Status,Case_Status__c,ActivityDate,WhatId,WhoId,Email__c,Task_From__c From Task Where WhatId=: em.ParentId];
        ncase.Auto_Close_on_Email_Sent__c = true;
        update ncase;

        Test.startTest();
        EmailMessage em   = new EmailMessage();
        system.runAs(u2) {
            //Sending email
            em.Incoming       = false;
            em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            em.ToAddress      = cont.Email;
            em.Subject        = 'Qantas Customer Care';
            em.HtmlBody       = '<br/><h1>Hello!!</h1>';
            em.Status         = '3';
            em.ParentId       = ncase.id;
            insert em;
        }

        Case closedCase     = [select Id , Status FROM Case where Id =: ncase.id];
        System.assert(closedCase.Status == 'Closed');


        //Customer's response
        EmailMessage emIn   = new EmailMessage();
        //system.runAs(u2) {
            emIn.Incoming       = true;
            emIn.FromAddress    = cont.Email;
            emIn.ToAddress      = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            emIn.Subject        = 'Qantas Customer Care';
            emIn.HtmlBody       = '<br/><h1>Hello!!</h1>';
            emIn.Status         = '3';
            emIn.ParentId       = ncase.id;
            insert emIn;
        //}
        Case reopenedCase   = [select Id , Status FROM Case where Id =: ncase.id];
        System.debug('reopenedCase####'+reopenedCase);
        System.assert(reopenedCase.Status == 'Reopened');
        Test.stopTest();

    }
    
    static testMethod void caseClosedAndReopened3() {
        User u1 = [Select Id, Username from User where Username = 'sampleuser1username@sample.com'];
        User u2 = [Select Id, Username from User where Username = 'sampleuser2username@sample.com'];
        System.debug('userid = '+u1.Id);
        Account acct = [SELECT Id,Name FROM Account WHERE Name='QantasCorporateEmailAccount' LIMIT 1];
        Contact cont = [Select Id,Name,Email,AccountId From Contact Where Account.Name ='QantasCorporateEmailAccount' Limit 1];
        Case ncase   = [Select Id,CaseNumber,ContactId, RecordTypeId, Type, Group__c, Cabin_Class__c, OwnerId From Case where ContactId=: cont.id Limit 1];
        ncase.Type   = 'Baggage';
        ncase.Group__c = 'Damaged Baggage';
        ncase.Cabin_Class__c = 'Economy';
        ncase.RecordTypeId = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case Baggage RecType'));
        System.debug('case owner = '+ncase.OwnerId);
        ncase.OwnerId = u1.Id;
        //List<Task> tt = [Select Subject,Status,Case_Status__c,ActivityDate,WhatId,WhoId,Email__c,Task_From__c From Task Where WhatId=: em.ParentId];
        ncase.Auto_Close_on_Email_Sent__c = true;
        update ncase;

        Test.startTest();
        EmailMessage em   = new EmailMessage();
        system.runAs(u2) {
            //Sending email
            em.Incoming       = false;
            em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            em.ToAddress      = cont.Email;
            em.Subject        = 'Qantas Customer Care';
            em.HtmlBody       = '<br/><h1>Hello!!</h1>';
            em.Status         = '3';
            em.ParentId       = ncase.id;
            insert em;
        }

        Case closedCase     = [select Id , Status FROM Case where Id =: ncase.id];
        System.assert(closedCase.Status == 'Closed');


        //Customer's response
        EmailMessage emIn   = new EmailMessage();
        //system.runAs(u2) {
            emIn.Incoming       = true;
            emIn.FromAddress    = cont.Email;
            emIn.ToAddress      = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
            emIn.Subject        = 'Qantas Customer Care';
            emIn.HtmlBody       = '<br/><h1>Hello!!</h1>';
            emIn.Status         = '3';
            emIn.ParentId       = ncase.id;
            insert emIn;
        //}
        Case reopenedCase   = [select Id , Status FROM Case where Id =: ncase.id];
        System.debug('reopenedCase####'+reopenedCase);
        System.assert(reopenedCase.Status == 'Reopened');
        Test.stopTest();

    }
    
    static testMethod void testEmailMessageTrigger1() {
        EmailMessage em   = new EmailMessage();
        em.Incoming       = false;
        em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
        em.To_Address__c  = 'onlinesalesqueries@qantas.com.au';
        em.Subject        = 'Qantas Customer Care';
        em.HtmlBody       = '<br/><h1>Hello!!</h1>';
        em.Status         = '3';
        Test.startTest();
        insert em;
        Test.stopTest();
        EmailMessage msg = [SELECT Id, ToAddress FROM EmailMessage WHERE Id = :em.Id];
        System.assertEquals(em.To_Address__c, msg.ToAddress);
    }
    
    static testMethod void testEmailMessageTrigger2() {
        EmailMessage em   = new EmailMessage();
        em.Incoming       = false;
        em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
        em.To_Addresss__c  = 'test@test.com';
        em.Subject        = 'Qantas Customer Care';
        em.HtmlBody       = '<br/><h1>Hello!!</h1>';
        em.Status         = '3';
        Test.startTest();
        insert em;
        Test.stopTest();
        EmailMessage msg = [SELECT Id, ToAddress FROM EmailMessage WHERE Id = :em.Id];
        System.assertEquals(em.To_Addresss__c, msg.ToAddress);
    }
    
    static testMethod void testEmailMessageTriggerException() {
        Trigger_Status__c ts = [SELECT Id FROM Trigger_Status__c WHERE Name = 'EmailMessageHelper'];
        delete ts;
        EmailMessage em   = new EmailMessage();
        em.Incoming       = false;
        em.FromAddress    = CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email');
        em.To_Addresss__c  = 'test@test.com';
        em.Subject        = 'Qantas Customer Care';
        em.HtmlBody       = '<br/><h1>Hello!!</h1>';
        em.Status         = '3';
        Test.startTest();
        insert em;
        Test.stopTest();
        List<EmailMessage> msg = [SELECT Id, ToAddress FROM EmailMessage];
        System.assertNotEquals(em.To_Addresss__c, msg[0].ToAddress);
    }
}