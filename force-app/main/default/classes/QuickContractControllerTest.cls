/**
 * This class contains unit tests for validating the behavior of QuickContractController class.
 */
@isTest
public class QuickContractControllerTest {
    public static testMethod void test() {
        Id LoyaltyRecordTypeId= GenericUtils.getObjectRecordTypeId('Quote', 'Loyalty Commercial');
        Id oppRecordTypeId= GenericUtils.getObjectRecordTypeId('Opportunity', 'Loyalty Commercial');
        List<Account> accList = new List<Account>{};
        List<Opportunity> oppList = new List<Opportunity>{};
        List<Quote> quoteList = new List<Quote>{};
        for(integer i = 0; i < 5; i++)
        {
            Account acc = new Account(Name = 'Test1');
            accList.add(acc);
        }
        insert accList;
        
        for(integer i = 0; i < 5; i++)
        {
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = oppRecordTypeId;
            opp.AccountId = accList[i].Id;
            opp.Name = 'Opp'+i;
            opp.Customer_Base__c = 1000;
            opp.Points_Per__c = 10;
            opp.Price_Per_Point__c = 0.0180;
            opp.Assumed_Tag_Rate_at_Maturity__c = 1.45;
            opp.Average_Spend_Per_Customer__c = 1000;
            opp.StageName = 'Prospect';
            opp.CloseDate = System.today();
            if(Math.mod(i,2) == 0) {
                opp.Type = 'Qantas Business Rewards';
            } else {
                opp.Type = 'Qantas Frequent Flyer';
            }
            
            oppList.add(opp);
        }
        insert oppList;
        
        for(integer i = 0; i < 5; i++) {
            Quote q = new Quote(RecordTypeId=LoyaltyRecordTypeId,Name='Opp Quote'+i, OpportunityId = oppList[i].Id, Active__c = true,
                                Price_Per_Point__c = oppList[i].Price_Per_Point__c);
            quoteList.add(q);
        }
        insert quoteList;
        
        Quote qt = QuickContractController.getQuote(quoteList[0].Id);
        System.assertEquals('Opp Quote0', qt.Name);
        System.assertEquals(0.0180, qt.Price_Per_Point__c);
        
        Contract__c con = new Contract__c(Name='Con');
        con = QuickContractController.saveContractWithQuote(con, qt);
        
        System.assertEquals(qt.Id, con.Quote__c);
        System.assertEquals(0.0180, con.Price_Per_Point__c);
        System.assertEquals(qt.AccountId, con.Account__c);
        System.assertEquals(qt.OwnerId, con.Contract_Owner__c);
        
        Quote qt2 = QuickContractController.getQuote(quoteList[1].Id);
        System.assertEquals('Opp Quote1', qt2.Name);
        System.assertEquals(0.0180, qt2.Price_Per_Point__c);
        
        Contract__c con2 = new Contract__c(Name='Con');
        con2 = QuickContractController.saveContractWithQuote(con2, qt2);
        
        System.assertEquals(qt2.Id, con2.Quote__c);
        System.assertEquals(0.0180, con2.Price_Per_Point__c);
        System.assertEquals(qt2.AccountId, con2.Account__c);
        System.assertEquals(qt2.OwnerId, con2.Contract_Owner__c);
    }
}