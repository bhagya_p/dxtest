/***********************************************************************************************************************************

Description: Test class for contract trigger handler 

History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Bharathkumar Narayanan  CRM-2698    Contracts & Agreements: technical redesign                  T01
                                    
Created Date    : 15/07/17 (DD/MM/YYYY)

**********************************************************************************************************************************/
@isTest
public class ContractTriggerHandlerTest{
    
    /**
      *@purpose : Test method for AccountContractUpdater Class
      *@param   :
      *@return  : -
     */
    public static testMethod void accountUpdaterTest(){
        
        List<Account> accountList = AccountContractUpdaterTestUtil.createAccounts();
        List<Opportunity> opportunityList = AccountContractUpdaterTestUtil.createOpportunity(accountList);
        List<Proposal__c> proposalList = AccountContractUpdaterTestUtil.createProposals(opportunityList);
        List<Contract__c> contractList = AccountContractUpdaterTestUtil.createContracts(proposalList, opportunityList);
        
        //CRM-3381-Loyalty: Superceeded Contract Status
        List<Opportunity> opportunityLisToUpd = AccountContractUpdaterTestUtil.createOpportunity(accountList);
        
       	opportunityLisToUpd[0].Type = 'Qantas Business Rewards';
		opportunityLisToUpd[1].Type = 'Qantas Frequent Flyer';		
        update opportunityLisToUpd;
        
        List<Proposal__c> proposalListLoyalty = AccountContractUpdaterTestUtil.createProposals(opportunityLisToUpd);
        List<Contract__c> contractListLoyalty = AccountContractUpdaterTestUtil.createContracts(proposalListLoyalty, opportunityLisToUpd);
                
        Id LoyaltyRecordTypeId= GenericUtils.getObjectRecordTypeId('Contract__c', 'Loyalty Commercial');
        
        contractListLoyalty[0].RecordTypeId = LoyaltyRecordTypeId;
        contractListLoyalty[0].Status__c = 'Current';
        contractListLoyalty[1].RecordTypeId = LoyaltyRecordTypeId;
        contractListLoyalty[1].Status__c = 'Current';
        
        Test.startTest(); 
        update contractListLoyalty;
        
        //Insert assert check
        AccountContractUpdaterTestUtil.doAssertCheckForActiveContract(accountList,contractList);
        
        //Update Contract & assert equal
        contractList = AccountContractUpdaterTestUtil.updateContractStartEndDate(contractList);
        AccountContractUpdaterTestUtil.doAssertCheckForActiveContract(accountList,contractList);
        
        
        //Create inactive Contract & assert equal
        List<Contract__c> inActiveContractList = AccountContractUpdaterTestUtil.createInactiveContract(proposalList, 
                                                                                                       opportunityList);
                                                                                                       
        AccountContractUpdaterTestUtil.doAssertCheckForInActiveContract(accountList, contractList, inActiveContractList);
        
        //Delete active Contract & assert equal
        DELETE contractList;
        AccountContractUpdaterTestUtil.doAssertCheckOnDelete(accountList, inActiveContractList);
        
        UNDELETE contractList;
        
        Test.stopTest();

    }
    

    
}