/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Wrapper Class for Case Details Display
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
24-OCT-2017    Praveen Sampath               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public class QCCCaseWrapper {
	public String capId;
	public String engagementId;
	public String ffNumber;
	public string subject;
	public String description;
	public String caseType;
	public String pnr;
	public String lastName;
	public String firstName;
	public String phone;
	public String email;
	public Boolean isFreqFlyer;
	public String location;
	public String flightNo;
	public Date departureDate;
}