/*----------------------------------------------------------------------------------------------------------------------
Author:        Ajay Sadhanand B
Company:       TCS
Description:   Class used for Open All Cases tile for lightning component
Test Class:    FreightAllTileControllerTest     
*********************************************************************************************************************************
History
04-Aug-2017    Ajay               Initial Design
        
       
********************************************************************************************************************************/
public class FreightOpenCasesController {

    @AuraEnabled
    public static list<Case> getAllOpenCases(String caseId) {
        
        String[] caseStatusval = new String[]{'Open','On Hold','Assigned','Acknowledged'};
        
        list<Case> openCaseList = new list<Case>();
        system.debug('Case ID ***'+caseId);

        if(String.isNotBlank(caseId)){
            Case currentCase = [Select Id,AccountId,Account.Name from Case where Id =: caseId];
            for(Case c : [Select CaseNumber,Subject,Type,Sub_Type_1__c,Sub_Type_2__c,Status,Freight_Case_Status_Flag__c from Case where AccountId =: currentCase.AccountId AND Status in: caseStatusval]){
                c.Subject = c.Subject.length() > 120 ? c.Subject.subString(0,120) + '...' : c.Subject;
                openCaseList.add(c);
            }
        }
        
        if(!openCaseList.isEmpty())        
            return openCaseList;
        else
            return null;
    }

}