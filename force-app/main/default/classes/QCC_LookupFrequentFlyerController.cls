/*----------------------------------------------------------------------------------------------------------------------
Author:        Purushotham B
Company:       Capgemini
Description:   Lookup Frequent Flyer Controller
Inputs:
Test Class:    QCC_LookupFrequentFlyerControllerTest 
************************************************************************************************
History
************************************************************************************************
31-July-2018             Purushotham B          Initial Design 
-----------------------------------------------------------------------------------------------------------------------*/
public class QCC_LookupFrequentFlyerController {
    private static String ffTier;
    private static String ffTierSF;
    private static String ffNum;
    private static Map<String, String> countryCodeMap = new Map<String, String>();
    private static Boolean isNameMismatch = false;
    
    /*----------------------------------------------------------------------------------------------      
    Method Name:    lookupCustomer
    Description:    Search for the Customer in CAP CRE
    Parameter:      String, String    
    Return:         String    
    ----------------------------------------------------------------------------------------------*/
	@AuraEnabled
    public static String lookupCustomer(String ffNumber, String lastName, String capId){
        try {
            ffNumber = QCC_GenericUtils.doTrim(ffNumber);
            lastName = QCC_GenericUtils.doTrim(lastName);
            Contact con = new Contact();
            con.Frequent_Flyer_Number__c = ffNumber;
            con.LastName = lastName;
            con.CAP_ID__c = capId;
            QCC_CustomerDetailsRequest requestBody = QCC_CustomerSearchHandler.searchHelper(con,'');
            
            QCC_CustomerDetailsResponse.Response response = QCC_InvokeCAPAPI.invokeCustomerDetailCAPAPI(requestBody);
            if(response == null || response.customers == null || response.customers.size() == 0) 
            {
                return null;
            }
            system.debug('response======'+response);
            QCC_CustomerDetailsResponse.Customer customer;
            if(String.isBlank(capId)) {
                system.debug('ffNumber======'+ffNumber);
                system.debug('lastname======'+lastname);
                system.debug('response======'+response);
                customer = getValidCustomer(ffNumber, lastname, response);
                system.debug('system======'+customer);
                system.debug('isNameMismatch======'+isNameMismatch);

                if(customer == null && !isNameMismatch) {
                    return null;
                }
            } else {
                customer = response.customers[0].customer;
            }
            
            if(isNameMismatch) {
                return 'NameMismatch';
            }
			
            return JSON.serialize(getCustomerInfo(ffNumber, customer)).replaceALL('type_x', 'type');
        } catch(Exception ex) {
            system.debug('Exception###'+ex); 
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - Lookup Frequent Flyer', 'QCC_LookupFrequentFlyerController', 
                                    'lookupCustomer', '', '', false,'', ex);
        }
        return null;
    }
    
    /*----------------------------------------------------------------------------------------------      
    Method Name:    getValidCustomer
    Description:    Checks and returns the valid customer
    Parameter:      String, String, QCC_CustomerDetailsResponse.Response    
    Return:         QCC_CustomerDetailsResponse.Customer    
    ----------------------------------------------------------------------------------------------*/
    private static QCC_CustomerDetailsResponse.Customer getValidCustomer(String ffNumber, String lastName,QCC_CustomerDetailsResponse.Response response) {
        QCC_CustomerDetailsResponse.Customer customer;
        Boolean isValid = false;
        isNameMismatch = false;
        for(QCC_CustomerDetailsResponse.Customers cust: response.customers) {
            customer = cust.customer;
            if(customer != null) {
                if(customer.loyaltyDetails != null && customer.loyaltyDetails.size() > 0) {
                    Map<String, QCC_CustomerDetailsResponse.LoyaltyDetails> loyaltyMap = new Map<String, QCC_CustomerDetailsResponse.LoyaltyDetails>();
                    for(QCC_CustomerDetailsResponse.LoyaltyDetails loyalty: customer.loyaltyDetails) {
                        loyaltyMap.put(loyalty.loyaltyId.replaceFirst('^0+',''), loyalty);
                        if(String.isNotBlank(loyalty.schemeOperatorCode)){
                            if(loyalty.schemeOperatorCode.equalsIgnoreCase('QF')){
                                ffNum = loyalty.loyaltyId;
                                if(String.isNotBlank(loyalty.airlineTierCode)){
                                    ffTier = loyalty.airlineTierCode;
                                    ffTierSF = CustomSettingsUtilities.getConfigDataMap(loyalty.airlineTierCode);
                                    System.debug('FF Tier == '+ffTier);
                                }
                            }
                        }
                    }
                    system.debug('loyaltyMap###'+loyaltyMap);
                    system.debug('ffNumber####'+ffNumber);
                    isValid = loyaltyMap.containsKey(ffNumber.replaceFirst('^0+',''));
                    system.debug('isValid####'+isValid);
                }
                if(isValid && String.isNotBlank(lastName) && customer.Name != null && String.isNotBlank(customer.Name.lastName)) {
                    if(lastName.toUpperCase() != customer.Name.lastName.toUpperCase()) {
                        isNameMismatch = true;
                        isValid = false;
                    }
                }
            }
            
            if(isValid) {
                break;
            }
        }
        system.debug('customer####'+customer);
         system.debug('isValid####'+isValid);
        customer = isValid ? customer : null;
        return customer;
    }
    
    /*----------------------------------------------------------------------------------------------      
    Method Name:    getCustomerInfo
    Description:    Retrieves the required customer information
    Parameter:      String, QCC_CustomerDetailsResponse.Customer   
    Return:         CustomerWrapper    
    ----------------------------------------------------------------------------------------------*/
    private static CustomerWrapper getCustomerInfo(String ffNumber,QCC_CustomerDetailsResponse.Customer customer) {
        system.debug('customer$$$$$'+customer);
        CustomerWrapper cw = new CustomerWrapper();
        if(customer.Name != null) {
            cw.firstName = customer.Name.firstName;
            cw.lastName = customer.Name.lastName;
        }
        cw.capId = customer.qantasUniqueId;
        cw.ffNumber = ffNum;
        cw.ffTier = ffTier;
        cw.ffTierSF = ffTierSF;
        system.debug('customer.Phone$$$$$'+customer.Phone);
        if(customer.Phone != null) {
            cw.displayPhone = getPhoneDetails(customer.Phone);
        }else{
            cw.displayPhone = getPhoneDetails(new List<QCC_CustomerDetailsResponse.Phone>());
        }
        
        system.debug('customer.Address$$$$$'+customer.Address);
        if(customer.Address != null) {
            for(QCC_CAPCountryCodeMap__mdt codeData: [SELECT Label, Country_Code__c, Country_Name__c FROM QCC_CAPCountryCodeMap__mdt]) {
                countryCodeMap.put(codeData.Country_Code__c, codeData.Country_Name__c);
            }
            
            for(QCC_CustomerDetailsResponse.Address thisAddress : customer.Address){
                String street = thisAddress.line1 + ' ' + thisAddress.line2 + ' ' + thisAddress.line3 + ' ' + thisAddress.line4;
                String suburb = thisAddress.suburb.trim();
                String state = thisAddress.state.trim();
                String postCode = thisAddress.postCode.trim();
                String countryCode = thisAddress.country.trim();
                String country = countryCodeMap.containsKey(countryCode) ? countryCodeMap.get(countryCode) : null;
                
                if(String.isNotBlank(thisAddress.typeCode))
                {
                    if(thisAddress.typeCode.equalsIgnoreCase('H')){
                        cw.address = new Address(street, suburb, postCode, state, country);
                        break;
                    }
                    else if(thisAddress.typeCode.equalsIgnoreCase('B')){
                        cw.address = new Address(street, suburb, postCode, state, country);
                    }
                }
            }
        }
        
        if(customer.email != null){
            for(QCC_CustomerDetailsResponse.Email email : customer.email) {
                if(String.isNotBlank(email.emailId)) {
                    cw.email = email.emailId;
                    break;
                }
            }
        }
        
        List<displayColumn> listColumn = new List<displayColumn>();
        listColumn.add(new displayColumn('Phone', 'phoneNumberWithType', 'text'));
        cw.displayColumn = listColumn;
        System.debug('CustomerWrapper = '+cw);
        return cw;
    }
    
    /*----------------------------------------------------------------------------------------------      
    Method Name:    getPhoneDetails
    Description:    Retrieves the phone details of the customer 
    Parameter:      List<QCC_CustomerDetailsResponse.Phone>   
    Return:         List<Phone>    
    ----------------------------------------------------------------------------------------------*/
    private static List<Phone> getPhoneDetails(List<QCC_CustomerDetailsResponse.Phone> phonesRes) {
        List<Phone> phones = new List<Phone>();
        //Integer i = 0;
        for(QCC_CustomerDetailsResponse.Phone thisPhone : phonesRes) {
            Phone ph = new Phone();
            //ph.phoneId = String.valueOf(i++);
            String phoneNumber;
            if(String.isNotBlank(thisPhone.phoneCountryCode)){
                phoneNumber = thisPhone.phoneCountryCode;
            }
            if(String.isNotBlank(thisPhone.phoneAreaCode)){
                phoneNumber = String.isBlank(phoneNumber) ? thisPhone.phoneAreaCode : phoneNumber + thisPhone.phoneAreaCode;
            }
            if(String.isNotBlank(thisPhone.phoneLineNumber)){
                phoneNumber = String.isBlank(phoneNumber) ? thisPhone.phoneLineNumber : phoneNumber + thisPhone.phoneLineNumber;
            }
            
            // phoneIds have been given to sort the list in desired order. The order is Home - Business - Other - Additional Phone Number
            if(String.isNotBlank(thisPhone.typeCode) && String.isNotBlank(phoneNumber)){
                ph.phoneNumber = phoneNumber.trim();
                if (thisPhone.typeCode.equalsIgnoreCase('B')) {
                    ph.phoneType = 'Business';
                    ph.phoneId = 1;
                }else if (thisPhone.typeCode.equalsIgnoreCase('H')){
                    ph.phoneType = 'Home';
                    ph.phoneId = 0;
                }else{
                    ph.phoneType = 'Other';
                    ph.phoneId = 2;
                }
                ph.phoneNumberWithType = ph.phoneType + ': ' + ph.phoneNumber;
                ph.label = ph.phoneNumberWithType;
                ph.value = ph.phoneNumberWithType;//ph.phoneNumber;
                phones.add(ph);
            }
        }
        // This is to show an additional option to the User for entering a Phone number manually
        //phones.add(new Phone('','','',CustomSettingsUtilities.getConfigDataMap('AdditionalPhoneLabel'),'additional'));
        phones.add(new Phone('','','',Label.QCC_AdditionalPhoneLabel,'additional'));
        System.debug('Before Sort '+phones);
        phones.sort();
        System.debug('After sort '+phones);
        return phones;
    }
    
    // Wrapper class used to display the columns on the lightning component
    public class displayColumn{
        public String label{get;set;}
        public String fieldName{get;set;}
        public String type_x{get;set;}

        public displayColumn(String label, String fieldName, String inputType){
            this.label = label;
            this.fieldName = fieldName;
            this.type_x = inputType;
        }
    }
    
    public class Phone implements Comparable{
        public Integer phoneId;
        public String phoneType;
        public String phoneNumber;
        public String phoneNumberWithType;
        public String label;
        public String value;
        
        public Phone() {
            
        }
        
        public Phone(String phoneType, String phoneNumber, String phoneNumberWithType, String label, String value) {
            this.phoneType = phoneType;
            this.phoneNumber = phoneNumber;
            this.phoneNumberWithType = phoneNumberWithType;
            this.label = label;
            this.value = value;
            if(value == 'additional') {
                this.phoneId = 3;
            }
        }
        
        // Sorts the custom list based on phoneId field
        public Integer compareTo(Object compareTo) {
            Phone compareToPhone = (Phone)compareTo;
            if (phoneId == compareToPhone.phoneId) return 0;
            if (phoneId > compareToPhone.phoneId) return 1;
            return -1;        
        }
    }
    
    public class Address {
        String street;
        String suburb;
        String postCode;
        String state;
        String country;
        
        public Address(String street, String suburb, String postCode, String state, String country) {
            this.street = street;
            this.suburb = suburb;
            this.postCode = postCode;
            this.state = state;
            this.country = country;
        }
    }
    
    public class CustomerWrapper {
        public List<displayColumn> displayColumn{get;set;}
        public String firstName{get;set;}
        public String lastName{get;set;}
        public String ffNumber{get;set;}
        public String email{get;set;}
        public String capId{get;set;}
        public String ffTier{get;set;}
        public List<Phone> displayPhone{get;set;}
        public Address address{get;set;}
        public String ffTierSF{get;set;}
    }
}