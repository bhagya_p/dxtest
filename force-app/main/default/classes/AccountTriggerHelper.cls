/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Class to be invoked from Account Trigger
Inputs:
Test Class:    AccountTriggerHelperTest. 
************************************************************************************************
History
************************************************************************************************
06-Mar-2017    Benazir S A               Initial Design
07-Mar-2017    Praveen Sampath          Added createDelAccountLogTracker Method
21-Apr-2017    Praveen/Benazir         Added createCaseOnMultipleActiveQBR Method
-----------------------------------------------------------------------------------------------------------------------*/
public class AccountTriggerHelper {
    public Static Set<Id> setRecTypeId = allNonFreightAccountRecordType();

    /*--------------------------------------------------------------------------------------      
    Method Name:        updateAccountDetail
    Description:        Method to Assign Amex Response Code
    Parameter:          Account New List & Old Map 
    --------------------------------------------------------------------------------------*/    
    public static void updateAccountDetail(List<Account> newlstAccount, Map<Id,Account> oldMapAccount){
        try{
            Set<Id> setAccId = new Set<Id>();
            Map<String, String> mapAmexResCode = new Map<String, String>();
            Map<Id, String> mapAccCustomer = new Map<Id, String>(); 


            for(Account acc: newlstAccount){
                Account oldAcc = oldMapAccount.get(acc.Id);
                if(acc.AMEX__c == true && acc.AMEX_response_Code__c != 'D' && setRecTypeId.contains(acc.RecordTypeId) &&
                    ( acc.Agency__c != oldAcc.Agency__c || acc.Aquire_Override__c != oldAcc.Aquire_Override__c || 
                      acc.Dealing_Flag__c != oldAcc.Dealing_Flag__c))
                {
                    setAccId.add(acc.Id);
                }
            }
            
            if(setAccId.size() > 0){
                Id amexRecId = GenericUtils.getObjectRecordTypeId('Product_Registration__c',  CustomSettingsUtilities.getConfigDataMap('Prd Reg AEQCC Registration Rec Type'));
            
                for(Product_Registration__c prodReg: [select Id, Customer_s_Requested_Choice__c, Account__c from Product_Registration__c
                                                      where Account__c IN: setAccId AND RecordTypeId =: amexRecId AND Active__c = true ])
                {
                    mapAccCustomer.put(prodReg.Account__c, prodReg.Customer_s_Requested_Choice__c);
                }
                
                if(mapAccCustomer.size() > 0){
                    mapAmexResCode = CustomSettingsUtilities.getAllAmexResponseCode();
                }
            }

            for(Account acc: newlstAccount){
                if(setAccId.contains(acc.Id)){
                    acc.AMEX_response_Code__c = CustomSettingsUtilities.getAmexResponseCodeForAccount( mapAmexResCode, 
                                                                        acc.Agency__c, acc.Dealing_Flag__c, acc.Aquire_Override__c,
                                                                        mapAccCustomer.get(acc.Id)); 
                }
            }
        }
        catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Account Team - Account Trigger', 'AccountTriggerHelper', 
                                     'updateAccountDetail', String.valueOf(''), String.valueOf(''),false,'', ex);
        }

    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        updateAccountTeam
    Description:        Method to Assign & create Account team members based on user department
    Parameter:          Account New Map & Old Map 
    --------------------------------------------------------------------------------------*/    
    public static void updateAccountTeam(Map<Id,Account> newMapAccount, Map<Id,Account> oldMapAccount){
        try{
            //AccountTeamHandler accHandler = new AccountTeamHandler(); // Commented on Feb 23 as this AccountTeamHandler class is no longer used
            List<Account> lstNewAccount = newMapAccount.values();
           // accHandler.setRevenueManualUpdate(lstNewAccount, oldMapAccount);
          //  accHandler.createAccountTeamMembers(lstNewAccount, oldMapAccount);
        }
        catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Account Team - Account Trigger', 'AccountTriggerHelper', 
                                     'updateAccountTeam', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        createAccountEligiblityLogs
    Description:        Method to Create AccountIneligibilty Logs and Update
    Parameter:          Account New Map & Old Map 
    --------------------------------------------------------------------------------------*/    
    public static void createAccountEligiblityLogs(Map<Id,Account> newMapAccount, Map<Id,Account> oldMapAccount){
        try{
            AccountEligibilityLogHandler.createAccountEligiblityLogs(newMapAccount, oldMapAccount);
        }
        catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Account Team - Account Trigger', 'AccountTriggerHelper', 
                                     'createAccountEligiblityLogs', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        calculateDiscountCode
    Description:        Method to DiscountCode Calculations on Products
    Parameter:          Account New Map & Old Map 
    --------------------------------------------------------------------------------------*/    
    public static void calculateDiscountCode(Map<Id,Account> newMapAccount, Map<Id,Account> oldMapAccount){
        try{
            AccountAirlineLevelHandler.updateDiscountCodeOnProducts(newMapAccount, oldMapAccount);
        }
        catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Account Team - Account Trigger', 'AccountTriggerHelper', 
                                     'calculateDiscountCode', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        createDelAccountLogTracker
    Description:        Method to Create Delete Tracker Logs records when any Account is Deleted
    Parameter:          Account Old Map 
    --------------------------------------------------------------------------------------*/    
    public static void createDelAccountLogTracker(Map<Id,Account> oldMapAccount){
        try{
            String recTypeName = CustomSettingsUtilities.getConfigDataMap('Del Log SME Account Rec Type');
            List<CreateLogs.DeleteLogWrapper> lstDelLogWrapper = new List<CreateLogs.DeleteLogWrapper>();
            for(Account acc: oldMapAccount.values()){
                String directvalue = CustomSettingsUtilities.getConfigDataMap('AccListner Type Direct Stream');
                if(acc.Listner_Type__c == directvalue){
                    CreateLogs.DeleteLogWrapper delLogWrapper = new CreateLogs.DeleteLogWrapper();
                    delLogWrapper.recType = recTypeName;
                    delLogWrapper.recName =  acc.Name;
                    delLogWrapper.abnNumber = acc.ABN_Tax_Reference__c;
                    delLogWrapper.sobjectId = acc.Id;
                    delLogWrapper.sobjectName = 'Account';
                    lstDelLogWrapper.add(delLogWrapper);
                }
            }
            List<Delete_Tracker_Log__c> lstDelTrackerLog = CreateLogs.createDeleteLog(lstDelLogWrapper);
            insert lstDelTrackerLog;
        }catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Account Team - Account Trigger', 'AccountTriggerHelper', 
                                     'createDelAccountLogTracker', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }

    //Private Method to add Recortype
    private static Set<Id> allNonFreightAccountRecordType(){
        String allNonFreightAccRecType = String.isBlank(CustomSettingsUtilities.getConfigDataMap('Account RecType1'))?'':CustomSettingsUtilities.getConfigDataMap('Account RecType1');
        allNonFreightAccRecType += String.isBlank(CustomSettingsUtilities.getConfigDataMap('Account RecType2'))?'':CustomSettingsUtilities.getConfigDataMap('Account RecType2');
        allNonFreightAccRecType += String.isBlank(CustomSettingsUtilities.getConfigDataMap('Account RecType3'))?'':CustomSettingsUtilities.getConfigDataMap('Account RecType3');
        List<String> setAllRectypeName = allNonFreightAccRecType.split(';');
        
        Set<Id> setRecId = new Set<Id>();
        for(String recTypeName: setAllRectypeName){
            setRecId.add(GenericUtils.getObjectRecordTypeId('Account', recTypeName)); 
        }
        return setRecId;
    }
}