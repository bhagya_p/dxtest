public class FeedItemTriggerHelper {
    
    public static void preventChatterDeletion(List<FeedItem> lstFeedItem){
        //try{
            Boolean isPreventDelete = false;
            
            
            if(EnableValidationRule__c.getInstance(userInfo.getUserId()) != Null ){
                isPreventDelete = EnableValidationRule__c.getInstance(userInfo.getUserId()).PreventChatterDeletion__c;
            }
            
            if(isPreventDelete){
                for(FeedItem objFeed: lstFeedItem){
                    objFeed.addError(CustomSettingsUtilities.getConfigDataMap('QCC_PreventChatterDelete'));
                }
            }
        //}
        /*catch(Exception ex){
            system.debug('ex###############'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - FeedItem Trigger', 'FeedItemTriggerHelper', 'preventChatterDeletion', String.valueOf(''), String.valueOf(''),false,'', ex);
        }*/
    }
}