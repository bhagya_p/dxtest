public class QCC_LoyaltyResponseWrapper {

    public String memberId; 
    public String firstName; 
    public String lastName; 
    public String mailingName; 
    public String salutation; 
    public String title; 
    public String dateOfJoining; 
    public String gender; 
    public String dateOfBirth; 
    public String email; 
    public String emailType; 
    public String countryOfResidency; 
    public String taxResidency; 
    public String membershipStatus; 
    public String preferredAddress; 
    public Integer pointBalance; 
    public List<ProgramDetails> programDetails; 
    public Boolean webLoginDisabled; 
    public Company company; 
    public List<PhoneDetails> phoneDetails; 
    public List<Addresses> addresses;
    public Preferences preferences; 
    public StatusCredits statusCredits;
    
    public class Preferences {}
    public class ProgramDetails {
        public String programCode; 
        public String programName; 
        public String enrollmentDate; 
        public String accountStatus; 
        public Boolean qcachSendMarketing; 
        public String qcashProgramIdentifier; 
        public String tierCode; 
        public String tierFromDate;
        public String tierToDate;
        public String ffExpireDate; 
        public String tierName; 
 
    }
    

    public class PhoneDetails {
        public String type_Z; // in json: type
        public String number_Z; // in json: number
        public String areaCode; 
        public String idd; 
        public String status; 
    }
    
    public class Addresses {
        public String type_Z; // in json: type
        public String lineOne; 
        public String lineTwo; 
        public String suburb;
        public String postCode; 
        public String state; 
        public String countryCode; 
        public String status; 
    }
    
    public class Company {
        public String name; 
        public String positionInCompany; 
    }
    
    
    public class Tier {
        public String name; 
        public String code; 
        public String expiryDate; 
        public String startDate; 
        public String effectiveTier; 
    }
    
    public class StatusCredits {
        public String expiryDate; 
        public Integer lifetime; 
        public Integer total; 
        public Integer loyaltyBonus; 

    }
    

}