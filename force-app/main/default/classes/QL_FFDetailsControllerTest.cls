/*==============================================================================================================================
Author:         Priyadharshini V
Company:        TCS
Purpose:        Test class for QL_FFDetailsController
Date:           27/04/2018          
History:
JIRA :          CRM 3092  
==============================================================================================================================*/
@isTest(SeeAllData = false)
private class QL_FFDetailsControllerTest {
    @testSetup
    static void createData() {
        //List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.enableTriggers();
        TestUtilityDataClassQantas.insertQantasConfigData();
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createAccountTriggerSettingasTrue();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createContactTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createCaseTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createRecoveryTriggerSetting());
        insert lsttrgStatus;
        List<Qantas_API__c> lstQantas = TestUtilityDataClassQantas.createQantasAPI();
        insert lstQantas;
        QantasConfigData__c qc = new QantasConfigData__c();
        qc.Name = 'Log Exception Logs Rec Type';
        qc.Config_Value__c = 'Exception Logs';
        qc.Config_Description__c = 'Log Exception Logs Record Type Name';
        insert qc;

        Map<String, Profile> profMap = TestUtilityDataClassQantas.getProfileMap();

         User u = new User(LastName = 'User1', Alias = 'User99', Email = 'sampleuser991@sample.com', 
                           Username = 'sampleuser99username@sample.com', ProfileId = profMap.get('Qantas CC Consultant').Id,
                           EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                           TimeZoneSidKey = 'Australia/Sydney'
                          );
        insert u;

        User u1 = new User(LastName = 'User1', Alias = 'User', Email = 'sampleuser1@sample.com', 
                           Username = 'sampleuser1username@sample.com', ProfileId = profMap.get('Qantas CC Consultant').Id,
                           EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                           TimeZoneSidKey = 'Australia/Sydney', ManagerId = u.Id
                          );
        insert u1;
        
        //List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        
        String type = 'Complaint';
       String recordType = 'CCC Complaints';
        
        
        System.runAs(u1) {
            for(Integer i = 0; i < 5; i++) {
                Contact con = new Contact(LastName='Sample'+i, FirstName='User', Business_Types__c='Agency', Job_Role__c='Agency Manager', Function__c='Advisory Services');
                con.Frequent_Flyer_Number__c = '12456787'+''+i;
                con.Email = 'praveen@hotmail.com';
                con.Phone = '0426646801';
                contacts.add(con);
            }
            insert contacts;
            
            List<Case> cases = TestUtilityDataClassQantas.insertCases(contacts, recordType, type, 'Qantas.com','Website Functionality','','');
        }
        
     }
    static testMethod void testSaveRecovery() {
        User usr = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        System.runAs(usr) {
            List<Case> cases = [SELECT Id FROM Case];
            List<String> recoveryIds = new List<String>();
            Test.startTest();
            for(Integer i = 0; i < cases.size(); i++) {
                String recoveryId = QL_FFDetailsController.saveRecovery(cases[i].id);
                recoveryIds.add(recoveryId);
            }
            Test.stopTest();
            List<Recovery__c> recoveries = [Select Id,Case_Number__c FROM Recovery__c where RecordType.DeveloperName='Customer_Connect' AND Id = :recoveryIds];
            System.assertEquals(cases.size(), recoveries.size());
        }
    }
    
    static testMethod void testIsConsultant() {
        Map<String, Profile> profMap = TestUtilityDataClassQantas.getProfileMap();
        List<User> users = TestUtilityDataClassQantas.getUsers('Qantas CC Consultant', '', 'QF-Customer Care');
        Test.startTest();
        for(Integer i = 0; i < users.size(); i++) {
            System.assertEquals(true, QL_FFDetailsController.isConsultant(users[i].id));
        }
        
        User u = new User(LastName = 'User', Alias = 'User' , 
                              Email = 'sampleuser@sample.com', Username = 'sampleuserusername@sample.com', 
                              ProfileId = profMap.get('Qantas CC Team Lead').Id, 
                              EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                              TimeZoneSidKey = 'Australia/Sydney'
                             );
        insert u;
        Test.stopTest();
        System.assertEquals(false, QL_FFDetailsController.isConsultant(u.id));
    }
    
    static testMethod void testIsCaseClosed() {
        User usr = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        System.runAs(usr) {
            List<Case> cases = [SELECT Id, status FROM Case];
            Test.startTest();
            for(Integer i = 0; i < cases.size(); i++) {
                System.assertEquals(false, QL_FFDetailsController.isCaseClosed(cases[i].id));
                cases[i].status = 'Closed';
            }
            update cases;
            
            List<Case> updatedCases = [Select Id, Status FROM Case Where Id = :cases];
            for(Integer i = 0; i < updatedCases.size(); i++) {
                System.assertEquals(true, QL_FFDetailsController.isCaseClosed(updatedCases[i].id));
            }
            Test.stopTest();
        }
    }

    static TestMethod void fetchCustomerDetailPostiveTest(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCCFF_SC-Bronze','999'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('FFBR','Bronze'));
        insert lstConfigData;
        Contact objCon = [select Id from Contact limit 1];

        Test.startTest();
        String Body = '{"memberId":"1900007871","firstName":"MEMBER","lastName":"LOYTEST","mailingName":"SIR M LOYTEST",'+
                        '"salutation":"Dear Sir Loytest","title":"SIR","dateOfJoining":"2012-05-21","gender":"MALE",'+
                        '"dateOfBirth":"1995-03-20","email":"ibskevinliu@qantas.com.au","emailType":"BUSINESS",'+
                        '"countryOfResidency":"AU","taxResidency":"AU","membershipStatus":"ACTIVE","preferredAddress":"BUSINESS",'+
                        '"pointBalance":1,"company":{"name":"QANTAS LOYALTY IT","positionInCompany":"TEST PROFILE - DPP"},'+
                        '"phoneDetails":[{"type":"BUSINESS","number":"ibs96915525","areaCode":"02","idd":"+61","status":"V"}],'+
                        '"addresses":[{"type":"BUSINESS","lineOne":"LEVEL 7","lineTwo":"440 ELIZABETH STREET","suburb":"MELBOURNE",'+
                        '"postCode":"3000","state":"VIC","countryCode":"AU","status":"V"}],"preferences":{},'+
                        '"programDetails":[{"programCode":"QCASH","programName":"Qantas Cash","enrollmentDate":"2013-08-14",'+
                        '"accountStatus":"ACTIVE","qcachSendMarketing":false,"qcashProgramIdentifier":"AU"},'+
                        '{"programCode":"QFF","programName":"Qantas Frequent Flyer","enrollmentDate":"2012-05-21",'+
                        '"accountStatus":"ACTIVE","tierCode":"FFBR","tierFromDate":"2017-11-22","tierToDate":"2019-05-31",'+
                        '"ffExpireDate":"31-May-2018"}],"webLoginDisabled":false,"statusCredits":{"expiryDate":"2018-05-31",'+
                        '"lifetime":300,"total":0,"loyaltyBonus":0},"tier":{"name":"Frequent Flyer Bronze","code":"FFBR",'+
                        '"expiryDate":"2019-05-31","startDate":"2017-11-22","effectiveTier":"FFBR"}, '+
                        '"airlineCustomerValue":"9879","otherAirlineSchemeCount":0,"qantasFFNo":"900000000000013",'+
                        '"qfMemberAirlinePriorityLevel":null,"qfMemberTierClubCode":"FFGD","complaintCount":3,'+
                        '"serviceFailureCount":21,"surpriseDelightCount":1,"mishandledBagCount":39,"flightCancellationCount":26,'+
                        '"flightDelayDomesticCount":13,"flightDelayInternationalCount":6,"flightUpgradeDomesticCount":0,'+
                        '"flightUpgradeInternationalCount":12,"flightDowngradeDomesticCount":22,"flightDowngradeInternationalCount":13,'+
                        '"flightSegmentCount":31,"qantasUniqueID":"900000000013","majorityQCI":"ANZ","majorCorpCountryCode":"AU",'+
                        '"majorCorpBCI":"BCI","majorCorpACI":"ACI","majorCorpOCI":"OCI","majorCorpName":"ANZ Bank",'+
                        '"majorCorpAltName":"Australia New Zealand Banking Corp"}';
        Map<String, String> mapHeader= new Map<String, String>();
        mapHeader.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new QCC_CAPBookingMock(200, 'Success', Body, mapHeader));
        QL_FFDetailsController.fetchCustomerDetail(objCon.Id, true);
        Test.stopTest();
    }

     static TestMethod void fetchCustomerDetailNegativeTest(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCCFF_SC-Bronze','999'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('FFBR','Bronze'));
        insert lstConfigData;
        Contact objCon = [select Id from Contact limit 1];

        Test.startTest();
        Boolean isException = false;
        String Body = '';
        Map<String, String> mapHeader= new Map<String, String>();
        mapHeader.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new QCC_CAPBookingMock(200, 'Success', Body, mapHeader));
        try{
            QL_FFDetailsController.fetchCustomerDetail(objCon.Id, true);
        }catch(Exception ex){
            isException = true;
        }
        system.assert(isException = true, 'No Exception');
        Test.stopTest();
    }

    static TestMethod void fetchCaseSummaryPostiveTest(){
        Contact objCon = [select Id from Contact limit 1];
        Case objCase = [select Id from Case limit 1];
        String Body = '{"airlineCustomerValue":"9879","otherAirlineSchemeCount":0,"qantasFFNo":"900000000000013",'+
                        '"qfMemberAirlinePriorityLevel":null,"qfMemberTierClubCode":"FFGD","complaintCount":3,'+
                        '"serviceFailureCount":21,"surpriseDelightCount":1,"mishandledBagCount":39,"flightCancellationCount":26,'+
                        '"flightDelayDomesticCount":13,"flightDelayInternationalCount":6,"flightUpgradeDomesticCount":0,'+
                        '"flightUpgradeInternationalCount":12,"flightDowngradeDomesticCount":22,"flightDowngradeInternationalCount":13,'+
                        '"flightSegmentCount":31,"qantasUniqueID":"900000000013","majorityQCI":"ANZ","majorCorpCountryCode":"AU",'+
                        '"majorCorpBCI":"BCI","majorCorpACI":"ACI","majorCorpOCI":"OCI","majorCorpName":"ANZ Bank",'+
                        '"majorCorpAltName":"Australia New Zealand Banking Corp"}';
        Map<String, String> mapHeader= new Map<String, String>();
        mapHeader.put('Content-Type', 'application/json');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new QCC_CAPBookingMock(200, 'Success', Body, mapHeader));
        QL_FFDetailsController.BannerWrapper bannerWrap = QL_FFDetailsController.fetchCaseSummary(objCon.Id);
        QL_FFDetailsController.BannerWrapper bannerWrap1 = QL_FFDetailsController.fetchCaseSummary(objCase.Id);
        Test.stopTest();
    }

    static TestMethod void fetchCaseSummaryNegativeTest(){
        Contact objCon = [select Id from Contact limit 1];
        String Body = '';
        Map<String, String> mapHeader= new Map<String, String>();
        mapHeader.put('Content-Type', 'application/json');
        Test.startTest();
        Boolean isException = false;
        Test.setMock(HttpCalloutMock.class, new QCC_CAPBookingMock(200, 'Success', Body, mapHeader));
        try{
            QL_FFDetailsController.BannerWrapper bannerWrap = QL_FFDetailsController.fetchCaseSummary(objCon.Id);
        }catch(Exception ex){
            isException = true;
        }
        system.assert(isException = true, 'No Exception');
        Test.stopTest();
    }

     static TestMethod void fetchRecordTypeValuesPostiveTest(){
        Test.startTest();
        List<RecordType> lstRectype = QL_FFDetailsController.fetchRecordTypeValues();
        system.assert(lstRectype.size()>0, ' No recordype on case');
        Test.stopTest();
    } 

    static TestMethod void isCaseInQueuePostiveTest(){
        Case objCase = [select Id from Case limit 1];
        Test.startTest();
        Boolean isinQueue = QL_FFDetailsController.isCaseInQueue(objCase.Id);
        system.assert(isinQueue != true, ' No recordype on case');
        Test.stopTest();
    }

    static TestMethod void isaveRecoveryNegativeTest(){
        Contact objCon = [select Id from Contact limit 1];
        Test.startTest();
        Boolean isinQueue = false;
        try{
            QL_FFDetailsController.saveRecovery(objCon.Id);
        }catch(Exception ex){
            isinQueue = true;
        }
        
        system.assert(isinQueue = true, ' No recordype on case');
        Test.stopTest();
    }

    static TestMethod void isConsultantNegativeTest(){
        Contact objCon = [select Id from Contact limit 1];
        Test.startTest();
        Boolean isinQueue = false;
        try{
            QL_FFDetailsController.isConsultant(objCon.Id);
        }catch(Exception ex){
            isinQueue = true;
        }
        
        system.assert(isinQueue = true, ' No recordype on case');
        Test.stopTest();
    }

    static TestMethod void isCaseClosedNegativeTest(){
        Contact objCon = [select Id from Contact limit 1];
        Test.startTest();
        Boolean isinQueue = false;
        try{
            QL_FFDetailsController.isCaseClosed(objCon.Id);
        }catch(Exception ex){
            isinQueue = true;
        }
        
        system.assert(isinQueue = true, ' No recordype on case');
        Test.stopTest();
    }

    static TestMethod void isCaseInQueueNegativeTest(){
        Test.startTest();
        Boolean isinQueue = false;
        try{
             QL_FFDetailsController.isCaseInQueue(null);
        }catch(Exception ex){
            isinQueue = true;
        }
        
        system.assert(isinQueue = true, ' No recordype on case');

        Test.stopTest();
    }

    static TestMethod void escalatingCasePositiveTest(){
        Test.startTest();
        User usr = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        Case objCase = [select Id from Case limit 1];
        objCase.OwnerId = usr.Id;
        update objCase;
        System.runAs(usr) {
            String esclaste =  QL_FFDetailsController.escalatingCase(objCase.Id);
            system.assert(esclaste != '', ' No recordype on case');
            Test.stopTest();
        }
    }

    static TestMethod void escalatingCaseNegativeTest(){
        Test.startTest();
        User usr = [SELECT Id FROM User WHERE Username = 'sampleuser1username@sample.com'];
        Case objCase = [select Id from Case limit 1];
        objCase.OwnerId = usr.Id;
        update objCase;
        System.runAs(usr) {
            String esclaste =  QL_FFDetailsController.escalatingCase(null);
            system.assert(esclaste != '', ' No recordype on case');
            Test.stopTest();
        }
    } 
}