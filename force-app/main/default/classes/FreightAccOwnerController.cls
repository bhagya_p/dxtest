/*----------------------------------------------------------------------------------------------------------------------
Author:        Ajay 
Company:       TCS
Description:   Class used for Account Owner tile for lightning component
Test Class:    FreightAllTileControllerTest     
*********************************************************************************************************************************
History
7-Aug-2017    Ajay               Initial Design
        
       
********************************************************************************************************************************/
public class FreightAccOwnerController {

    /*--------------------------------------------------------------------------------------      
    Method Name:        getACRRecord
    Description:        Method for retrieving Account Owner
    Parameter:          Case Id 
    --------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static User getAccOwner(String caseId) {

        system.debug('Case ID ***'+caseId);
        
        User CurrentOwner;

        if(String.isNotBlank(caseId)){
            Case currentCase = [Select Id,Account.OwnerId from Case where Id =: caseId];
            CurrentOwner = [Select Id,IsActive,Phone,Email,Name from User where Id =: currentCase.Account.OwnerId AND IsActive = true];
        }
        
        if(currentOwner <> null)        
            return CurrentOwner;
        else
            return null;
    }
}