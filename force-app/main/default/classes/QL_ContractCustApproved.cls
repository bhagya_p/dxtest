/***********************************************************************************************************************************

Description: Lightning component handler for the customer contract approval 

History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Karpagam     CRM-2797   LEx-Contract Approval                  T01
                                    
Created Date    : 25/09/17 (DD/MM/YYYY)

**********************************************************************************************************************************/

public with sharing class QL_ContractCustApproved {
    
    @AuraEnabled public Boolean isSuccess {get;set;}
    @AuraEnabled public String msg {get;set;}
    
    @AuraEnabled
    public static QL_ContractCustApproved getProposal(Id propId)
    {
        
        QL_ContractCustApproved obj = new QL_ContractCustApproved();
        
        try{
        List<Contract__c> conList = [SELECT Id,Status__c FROM Contract__c where Id =: propId];
        List<Contract__c> conToUpdate = new List<Contract__c>();
            system.debug('conListt***'+conList);
            // Check the valid contract exist
             if(!conList.isEmpty()){
             for(Contract__c c:conList)
              {
                // Check for the Status as "Approval Required – Legal"
                 if(c.Status__c == 'Approval Required – Legal')
                  {
                  obj.isSuccess =false;
                  obj.msg='Legal Approval Required!';
                  
                  }
                 //For other Status 
                 else
                 {
                 c.status__c = 'Signed by Customer'; 
                 conToUpdate.add(c);
                 }
              }
             
             }
            // If Contract not exist or in case of any errors
             else
             {
              obj.isSuccess =false;
              obj.msg='Some Error Occured';
             }
           // Update the Contract
             if(conToUpdate.size()>0)
              update conToUpdate;
              
            obj.isSuccess = true;
             return obj;  
             }
             
      catch(Exception ex)
       {
         throw new AuraHandledException(ex.getMessage());    
        }       
     }  
     }