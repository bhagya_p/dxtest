/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Webservice Class to Invoke CAP System
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
27-Oct-2017      Praveen Sampath               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public without sharing class QCC_InvokeCAPAPI {

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeTokenCapAPI
    Description:        Invoke authorization Token from CAP system
    Parameter:          Custom Setting
    --------------------------------------------------------------------------------------*/ 
    public static QCC_CAPTokenWrapper invokeTokenCapAPI( Qantas_API__c qantasAPI ){
        HTTPRequest req=new HTTPRequest();
        req.setEndpoint(qantasAPI.EndPoint__c);
        req.setHeader('client_id',qantasAPI.ClientId__c);
        req.setHeader('client_secret',qantasAPI.ClientSecret__c);
        req.setHeader('grant_type',qantasAPI.GrantType__c);
        req.setMethod(qantasAPI.Method__c); 
        req.setTimeout(120000);
        
        Http http = new Http();
        HttpResponse response = http.send(req);
        QCC_CAPTokenWrapper token;
        if(response.getStatusCode() == 200){
           token = (QCC_CAPTokenWrapper) System.JSON.deserialize(response.getBody(), QCC_CAPTokenWrapper.class);
        }        
        return token;        
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeTokenAPIGateway
    Description:        Invoke authorization Token for API Gateway(New Encrption logic)
    Parameter:          Custom Setting
    --------------------------------------------------------------------------------------*/ 
    public static QCC_CAPTokenWrapper invokeTokenAPIGateway( Qantas_API__c qantasAPI ){
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint(qantasAPI.EndPoint__c);
        Blob auth = Blob.valueOf(qantasAPI.ClientId__c+':'+qantasAPI.ClientSecret__c);
        String authEncode =  EncodingUtil.base64Encode(auth);
        req.setHeader('Authorization','Basic '+authEncode);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setMethod('POST'); 
        req.setTimeout(120000);
        String body = 'grant_type='+qantasAPI.GrantType__c;
        // Check Scope
        if(!String.isBlank(qantasAPI.Scope__c)){
            body += '&scope=' + qantasAPI.Scope__c;
        }
        body += '&validity_period=172800';
        req.setBody(body);

        Http http = new Http();
        HttpResponse response = http.send(req);
        System.debug('response.getBody(): '+response.getBody());
        QCC_CAPTokenWrapper token;
        if(response.getStatusCode() == 200){
           token = (QCC_CAPTokenWrapper) System.JSON.deserialize(response.getBody(), QCC_CAPTokenWrapper.class);
        }     
        return token;        
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeTokenLoyaltyAPI
    Description:        Invoke authorization Token from CAP system
    Parameter:          Custom Setting
    --------------------------------------------------------------------------------------*/ 
    public static QCC_CAPTokenWrapper invokeTokenLoyaltyAPI( Qantas_API__c qantasAPI ){
        HTTPRequest req=new HTTPRequest();
        req.setEndpoint(qantasAPI.EndPoint__c);
        req.setHeader('client_id',qantasAPI.ClientId__c);
        req.setHeader('client_secret',qantasAPI.ClientSecret__c);
        req.setHeader('grant_type',qantasAPI.GrantType__c);
        req.setMethod(qantasAPI.Method__c); 
        
        Http http = new Http();
        HttpResponse response = http.send(req);
        
        QCC_CAPTokenWrapper token = (QCC_CAPTokenWrapper) System.JSON.deserialize(response.getBody(), QCC_CAPTokenWrapper.class);
        
        return token;        
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeCustomerDetailCAPAPI
    Description:        Fetch Customer address Information from CAP system
    Parameter:          QCC_CustomerDetailsRequest
    --------------------------------------------------------------------------------------*/ 
	public static QCC_CustomerDetailsResponse.Response invokeCustomerDetailCAPAPI(QCC_CustomerDetailsRequest capReqBody) {
        String token = QCC_GenericUtils.getCAPValidToken('QCC_CAPToken');
        if(String.isBlank(token)) return null;
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPDetailsAPI');
        Http callOut = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(qantasAPI.EndPoint__c);
        request.setMethod(qantasAPI.Method__c); 
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', token);
        
        // A map of existing key to replacement key
        Map<String, String> replacementsReq = new Map<String, String> {
            'system_z' => 'system'
                };
                    
        String reqStr = QCC_GenericUtils.mogrifyJSON(JSON.serialize(capReqBody, true), replacementsReq);
        request.setBody(reqStr);
        system.debug('request########'+request);
        system.debug('requestBody########'+request.getBody());
        HttpResponse response =  callOut.send(request);
        system.debug('response$$$$$$$'+response);
        system.debug('Response Boy######'+response.getBody());
        
        QCC_GenericUtils.updateToken(token, 'QCC_CAPToken');
        if(response.getStatusCode() == 200){
            QCC_CustomerDetailsResponse.Response custResp = (QCC_CustomerDetailsResponse.Response) System.JSON.deserialize(response.getBody(), QCC_CustomerDetailsResponse.Response.class);
            return custResp;
        } 
        return null;
    }


    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeTransactionCAPAPI
    Description:        Customer Transaction Summary from CAP system
    Parameter:          CustomerTransactionRequest
    --------------------------------------------------------------------------------------*/ 
    public static  QCC_CAPTransactionWrapper.CAPTransactionResponse invokeTransactionCAPAPI(QCC_CAPTransactionWrapper.CAPTransactionRequest qccCapTransactionreq) {
        String token = QCC_GenericUtils.getCAPValidToken('QCC_CAPToken');
        if(String.isBlank(token)) return null;
        String body = JSON.serialize(qccCapTransactionreq, true);
        body = body.replace('system_x', 'system');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPCustomerTransAPI');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(qantasAPI.EndPoint__c);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', token);
        req.setBody(body);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        system.debug('response#########'+response);
        system.debug('response Body#########'+response.getBody());
        system.debug('req#########'+req);
        system.debug('req body#########'+req.getBody());
        QCC_GenericUtils.updateToken(token, 'QCC_CAPToken');

        if(response.getStatusCode() == 200){
            QCC_CAPTransactionWrapper.CAPTransactionResponse responseWrap = (QCC_CAPTransactionWrapper.CAPTransactionResponse)JSON.deserialize(response.getBody(), QCC_CAPTransactionWrapper.CAPTransactionResponse.class);
            return responseWrap; 
        } 
        return null;      
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeLoyaltyAPI
    Description:        Fetch Booking Summary Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static  QCC_LoyaltyResponseWrapper invokeLoyaltyAPI(String ffNumber) {
        String token = QCC_GenericUtils.getCAPValidToken('QCC_LoyaltyToken');
        if(String.isBlank(token)) return null;
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_LoyaltyAPI');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+ffNumber+'/profile';
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        system.debug('response#########'+response);
        system.debug('response Body#########'+response.getBody());
        system.debug('req#########'+req);
        system.debug('req body#########'+req.getBody());
        QCC_GenericUtils.updateToken(token, 'QCC_LoyaltyToken');

        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_LoyaltyResponseWrapper responseWrap = (QCC_LoyaltyResponseWrapper)JSON.deserialize(response.getBody(), QCC_LoyaltyResponseWrapper.class);
            system.debug('responseWrap Body#########'+responseWrap);
            return responseWrap;
        } 
        return null;      
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeLoyaltyPostAPI
    Description:        Fetch Booking Summary Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static  Boolean invokeLoyaltyPostAPI(String ffNumber, LoyaltyRequestWrapper loyaltyReq) {
        String token = QCC_GenericUtils.getCAPValidToken('QCC_LoyaltyToken');
        if(String.isBlank(token)) return null;
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_LoyaltyAPI');
        String body = JSON.serialize(loyaltyReq, true);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+ffNumber+'/accrual';
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', token);
        req.setBody(body);
        req.setTimeout(120000);
        System.debug('Request :' +  req);
        HTTPResponse response = http.send(req);
        system.debug('response#########'+response);
        system.debug('response Body#########'+response.getBody());
        system.debug('req#########'+req);
        system.debug('req body#########'+req.getBody());
        QCC_GenericUtils.updateToken(token, 'QCC_LoyaltyToken');

        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            return true;
        } 
        return false; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRFlightInfoCAPAPI
    Description:        Fetch Booking Flight Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRFlightInfoWrapper invokePNRFlightInfoCAPAPI(String pnr, string creationDate, String token, Boolean isHistory, String archivalData){
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+pnr+'/flights?creationDate='+creationDate+'&archivalData='+archivalData;
        if(isHistory){
            dynamicURL = '/'+pnr+'/flights?creationDate='+creationDate+'&deleted=true&archivalData=true';
        }
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRFlightInfoWrapper pnrBooking = (QCC_PNRFlightInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRFlightInfoWrapper.class);
            system.debug('QCC_PNRFlightInfoWrapper#########'+pnrBooking);
            return pnrBooking;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRFlightInfoCAPAPI
    Description:        Fetch Booking Flight Information from CAP system
    Parameter:          PNR, Lastname
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRFlightInfoWrapper invokePNRFlightInfoCAPAPI(String pnr, string lastName, String token, String archivalData){
        if(String.isBlank(pnr) || String.isBlank(lastName)){
            return null;
        }
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = (qantasAPI.EndPoint__c.endsWith('/')?'': '/')+pnr+'/flights?lastName='+lastName.toUpperCase()+'&archivalData='+archivalData;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        system.debug('response Body#########'+response);
        system.debug(response);
		QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRFlightInfoWrapper pnrBooking = (QCC_PNRFlightInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRFlightInfoWrapper.class);
            system.debug('QCC_PNRFlightInfoWrapper#########'+pnrBooking);
            return pnrBooking;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRPassengerInfoCAPAPI
    Description:        Fetch Booking Passenger Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRPassengerInfoWrapper invokePNRPassengerInfoCAPAPI(String pnr, string creationDate, String token, String archivalData){
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+pnr+'/passengers?creationDate='+creationDate+'&archivalData='+archivalData;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRPassengerInfoWrapper pnrPassenger = (QCC_PNRPassengerInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRPassengerInfoWrapper.class);
            system.debug('QCC_PNRBookingRetrieveCAPWrapper#########'+pnrPassenger);
            return pnrPassenger;
        }
        return null; 
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRPassengerInfoCAPAPI
    Description:        Fetch Booking Passenger Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRPassengerInfoWrapper invokePNRPassengerInfoCAPAPI(String pnr,  String token, String lastName){
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+pnr+'/passengers?lastName='+lastName;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRPassengerInfoWrapper pnrPassenger = (QCC_PNRPassengerInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRPassengerInfoWrapper.class);
            system.debug('QCC_PNRBookingRetrieveCAPWrapper#########'+pnrPassenger);
            return pnrPassenger;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRContactInfoCAPAPI
    Description:        Fetch Booking Contact Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRContactInfoWrapper invokePNRContactInfoCAPAPI(String pnr, string creationDate, String token, String archivalData){
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+pnr+'/contacts?creationDate='+creationDate+'&archivalData='+archivalData;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRContactInfoWrapper pnrContact = (QCC_PNRContactInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRContactInfoWrapper.class);
            system.debug('QCC_PNRContactInfoWrapper#########'+pnrContact);
            return pnrContact;
        }
        return null; 
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRContactInfoCAPAPI
    Description:        Fetch Booking Contact Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRContactInfoWrapper invokePNRContactInfoCAPAPI(String pnr, String token, String lastName){
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+pnr+'/contacts?lastName='+lastName;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRContactInfoWrapper pnrContact = (QCC_PNRContactInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRContactInfoWrapper.class);
            system.debug('QCC_PNRContactInfoWrapper#########'+pnrContact);
            return pnrContact;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRAncillaryInfoCAPAPI
    Description:        Fetch Booking Ancillary Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRAncillaryInfoWrapper invokePNRAncillaryInfoCAPAPI(String pnr, string creationDate, String token, String archivalData){
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+pnr+'/ancillaries?creationDate='+creationDate+'&archivalData='+archivalData;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRAncillaryInfoWrapper pnrAncillary = (QCC_PNRAncillaryInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRAncillaryInfoWrapper.class);
            system.debug('QCC_PNRAncillaryInfoWrapper#########'+pnrAncillary);
            return pnrAncillary;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRLegStatusInfoCAPAPI
    Description:        Fetch Passer Leg Info from CAP Booking Servivce
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRLegStatusInfoWrapper invokePNRLegStatusInfoCAPAPI(String pnr, string creationDate, String passengerId, 
                                                                           String segmentId, String token, Boolean isHistory, String archivalData)
    {
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = pnr+'/legstatus?creationDate='+creationDate+ (String.isNotBlank(segmentId) ? '&segmentId='+segmentId : '') + (String.isNotBlank(passengerId) ? '&passengerId='+passengerId : '') +'&archivalData='+archivalData;
        
        if(isHistory){
             dynamicURL = pnr+'/legstatus?creationDate='+creationDate+'&segmentTattoo='+segmentId+'&passengerTattoo='+passengerId+'&archivalData=true';
        }
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRLegStatusInfoWrapper pnrLegStatus = (QCC_PNRLegStatusInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRLegStatusInfoWrapper.class);
            system.debug('pnrLegStatus#########'+pnrLegStatus);
            return pnrLegStatus;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRSSRInfoCAPAPI
    Description:        Fetch Passer SSr Info from CAP Booking Servivce
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_PNRSSRInfoWrapper invokePNRSSRInfoCAPAPI(String pnr, string creationDate, String passengerId, 
                                                               String segmentId, String token, Boolean isHistory, String archivalData)
    {
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_BookingToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPNRRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        String dynamicURL = pnr+'/ssr?creationDate='+creationDate+'&segmentId='+segmentId+'&passengerId='+passengerId+'&archivalData='+archivalData;

        if(isHistory){
            dynamicURL = pnr+'/ssr?creationDate='+creationDate+'&segmentTattoo='+segmentId+'&passengerTattoo='+passengerId+'&archivalData=true';
        }
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        //QCC_GenericUtils.updateToken(token, 'QCC_BookingToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_PNRSSRInfoWrapper pnrSSR = (QCC_PNRSSRInfoWrapper)JSON.deserialize(response.getBody(), QCC_PNRSSRInfoWrapper.class);
            system.debug('QCC_PNRSSRInfoWrapper#########'+pnrSSR);
            return pnrSSR;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeBookingCAPAPI
    Description:        Fetch Booking Summary Information from CAP system
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_BookingSummaryWrapper invokeBookingCAPAPI(String capID, String bookingPerPage, Integer pageNumber){
        String token = QCC_GenericUtils.getCAPValidToken('QCC_CAPToken');
        if(String.isBlank(token)) return null;
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_BookingSummaryAPI');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = '/'+capID+'/bookings/summary?bookingPerPage='+bookingPerPage+'&pageNumber='+pageNumber;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        QCC_GenericUtils.updateToken(token, 'QCC_CAPToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_BookingSummaryWrapper bookingSummary = (QCC_BookingSummaryWrapper)JSON.deserialize(response.getBody(), QCC_BookingSummaryWrapper.class);
            system.debug('QCC_BookingSummaryWrapper#########'+bookingSummary);
            return bookingSummary;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokePNRSSRInfoCAPAPI
    Description:        Fetch Passer SSr Info from CAP Booking Servivce
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_FlightPassengersResponse invokeFlightPassengerAPI(String flightNumber , Date scheduleDepDate , String departurePort , String arrivalPort, Integer pagNo)
    {
        String token = QCC_GenericUtils.getCAPValidToken('QCC_FlightToken');
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPPassengersRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        if(String.isBlank(flightNumber)) return null;
        System.debug('$$$scheduleDepDate ' + scheduleDepDate);
        System.debug('$$$flightNumber length ' + flightNumber.length());
        String operatingCarrier = flightNumber.substring(0,2);
        System.debug('$$$operatingCarrier ' + operatingCarrier);
        String operatingFlightNumber = flightNumber.substring(2);
        System.debug('$$$operatingFlightNumber ' + operatingFlightNumber);
        System.debug('$$$operatingCarrier ' + operatingCarrier);

        //if(scheduleDepDate == null) return null;
        String dateVal = GenericUtils.parseDatetoString(scheduleDepDate);
        //String dynamicURL = 'QF/0516/2018-02-03/SYD/passengers?passengersPerPage=50&frequentFlyersOnly=false&includeLoyaltyDetails=true&includeConnectingFlights=true&includeCustomerContactDetails=true';
        //String endpointURL = 'http://qccare.getsandbox.com/passengers';
        String dynamicURL = operatingCarrier + '/' + operatingFlightNumber + '/' + dateVal + '/' + departurePort +'/passengers?passengersPerPage=40&frequentFlyersOnly=false&includeLoyaltyDetails=true&includeConnectingFlights=true&includeContactDetails=true&includePaxFromPreviousLegs=true';
        dynamicURL = pagNo != Null? dynamicURL+'&pageNo='+pagNo: dynamicURL;
        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(120000);
        HTTPResponse response = http.send(req);
        QCC_GenericUtils.updateToken(token, 'QCC_FlightToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_FlightPassengersResponse passengersResp = (QCC_FlightPassengersResponse)JSON.deserialize(response.getBody(), QCC_FlightPassengersResponse.class);
            return passengersResp;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeFlightInfoAPI
    Description:        Fetch Flight info from CAP
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_FlightInfoResponse invokeFlightInfoAPI(String departureDate , String departurePort , String flightNo, String arrivalPort)
    {
        return invokeFlightInfoAPI(departureDate, departurePort, flightNo, arrivalPort, '');
        //String token = QCC_GenericUtils.getCAPValidToken('QCC_FlightToken');
        //if(String.isBlank(token)) {
        //    return null;
        //}
        //Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPFlightInfoRetrieve');
        
        //Http http = new Http();
        //HttpRequest req = new HttpRequest();
        //String dynamicURL = departureDate + '/' + departurePort + '/schedules?flightNo=' + flightNo + '&arrivalPort=' + arrivalPort;

        //String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        //req.setEndpoint(endpointURL);
        //req.setMethod(qantasAPI.Method__c); 
        //req.setHeader('System', 'CAPCRE_CC');
        //req.setHeader('Authorization', token);
        //req.setTimeout(120000);
        //HTTPResponse response = http.send(req);
        //if(response.getStatusCode() == 200){
        //    system.debug('response Body#########'+response.getBody());
        //    QCC_FlightInfoResponse flightInfoResp = (QCC_FlightInfoResponse)JSON.deserialize(response.getBody(), QCC_FlightInfoResponse.class);
        //    return flightInfoResp;
        //}else{
        //    system.debug('response: '+response);
        //}
        //return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeFlightInfoAPI
    Description:        Fetch Flight info from CAP
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_FlightInfoResponse invokeFlightInfoAPI(String departureDate , String departurePort , String flightNo, String arrivalPort, String carrier)
    {
        String token = QCC_GenericUtils.getCAPValidToken('QCC_FlightToken');
        if(String.isBlank(token)) {
            return null;
        }
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPFlightInfoRetrieve');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = departureDate + '/' + departurePort + '/schedules?flightNo=' + flightNo + (!String.isBlank(arrivalPort) ? ('&arrivalPort=' + arrivalPort) : '') + (!String.isBlank(carrier) ? ('&carrier=' + carrier) : '');

        String endpointURL = qantasAPI.EndPoint__c+''+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod(qantasAPI.Method__c); 
        req.setHeader('System', 'CAPCRE_CC');
        req.setHeader('Authorization', token);
        req.setTimeout(2000);
        HTTPResponse response = http.send(req);
        QCC_GenericUtils.updateToken(token, 'QCC_FlightToken');
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());
            QCC_FlightInfoResponse flightInfoResp = (QCC_FlightInfoResponse)JSON.deserialize(response.getBody(), QCC_FlightInfoResponse.class);
            return flightInfoResp;
        }else{
            system.debug('response: '+response);
            QCC_FlightInfoResponse flightInfoResp = new QCC_FlightInfoResponse();
            flightInfoResp.errorMessage = '' + response.getStatusCode() + '-=-' + response.getStatus();
            return flightInfoResp;
        }
        return null; 
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        invokeFlightInfoAPI
    Description:        Fetch Flight info from CAP
    Parameter:          
    --------------------------------------------------------------------------------------*/ 
    public static QCC_FlightStatusAPIResponse invokeFlightStatusAPI(String departureFrom , String departureTo , String flightNo)
    {
        //String token = QCC_GenericUtils.getAPIGatewayValidToken('QCC_FlightStatusToken');
        //if(String.isBlank(token)) {
        //    return null;
        //}
        //Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_FlightStatusAPI');
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        String dynamicURL = flightNo + '?departureFrom=' + departureFrom +'&departureTo=' + departureTo;

        String endpointURL = 'callout:QantasFlightStatus/'+dynamicURL;
        req.setEndpoint(endpointURL);
        req.setMethod('GET'); 
        //req.setHeader('System', 'CAPCRE_CC');
        //req.setHeader('Authorization', token);
        req.setTimeout(2000);
        HTTPResponse response = http.send(req);
        if(response.getStatusCode() == 200){
            system.debug('response Body#########'+response.getBody());   
            String jsonRes = response.getBody().replace('"from":', '"from_x":');
            QCC_FlightStatusAPIResponse flightInfoResp = (QCC_FlightStatusAPIResponse)JSON.deserialize(jsonRes, QCC_FlightStatusAPIResponse.class);
            return flightInfoResp;
        }else{
            system.debug('response: '+response);
        }
        return null; 
    }
   
}