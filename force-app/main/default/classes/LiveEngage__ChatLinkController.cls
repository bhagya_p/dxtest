/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ChatLinkController {
    @RemoteAction
    global static String GetObjectPopoverData(String objectId, String SkillName) {
        return null;
    }
    @RemoteAction
    global static void setChatEndTime(String ChatId) {

    }
    @RemoteAction
    global static void setChatStartTime(String chatStartTime, String ChatId) {

    }
}
