public with sharing class ContentDocumentLinkTriggerHelper {
	public static void avoidingUploadFileToEvent(List<ContentDocumentLink> lstCDL){
		try{
			Boolean isPreventUpload = false;
			Set<Id> setContDocID = new Set<Id>();
			String prefix = Schema.getGlobalDescribe().get('Event__c').getDescribe().getKeyPrefix();
			User curUser = [select id, profileId, profile.Name from user where id = :userInfo.getUserId() limit 1];

			for(ContentDocumentLink cdl : lstCDL){
				setContDocID.add(cdl.ContentDocumentId);
                //Notes can be edited by All CJM's and check if the content is liked to flight Event
                if(cdl.LinkedEntityId != Null && 
                   (cdl.LinkedEntityId).getSObjectType().getDescribe().getName()== 'Event__c')
                {
                    cdl.ShareType = 'I';
                }
			}
			
			Map<ID, ContentDocument> mapCD = new Map<ID, ContentDocument>([select id, OwnerId from ContentDocument where Id IN :setContDocID]);

		    isPreventUpload = (curUser.profile.Name == 'Qantas CC Consultant');
		    if(isPreventUpload){
		    	for(ContentDocumentLink objContLink: lstCDL)
				{
					if(String.valueOf(objContLink.LinkedEntityId).startsWith(prefix)){
						objContLink.addError('Consultant can\'t upload file to Flight Event');
						mapCD.get(objContLink.ContentDocumentId).addError('Consultant can\'t upload file to Flight Event');
					}
				}
		    }

		}catch(Exception ex){
			System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
	        CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - Content Doc Link Trigger', 'ContentDocumentLinkTriggerHelper', 
	                                'avoidingUploadFileToEvent', String.valueOf(''), String.valueOf(''),false,'', ex);
	    }
		
	}
}