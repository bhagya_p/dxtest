/*----------------------------------------------------------------------------------------------------------------------
Author:        Abhijeet P
Company:       TCS
Description:   This test class will cover the test coverage for all Tile created as part of Freight functionality
    
************************************************************************************************
History
************************************************************************************************
10-Aug-2017    Abhijeet P              Initial Design

-----------------------------------------------------------------------------------------------------------------------*/


@isTest
private class FreightAllTileControllerTest{
    
    //Added by QCC Project
    @testSetup
    static void createTestData(){
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createCaseTriggerSetting();
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();

        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Integration Logs Rec Type','Integration Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('RecoveryStatusApproved','Approved'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('RecoveryType','Qantas Points'));
        
        insert lstConfigData;
    }

    @isTest 
    static void ACRTileRecordTest() {
        
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Freight Claims').getRecordTypeId();
        String accRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Account').getRecordTypeId();
        
        TestUtilityDataClassQantas.enableTriggers();
        TestUtilityDataClassQantas.createEnableValidationRuleSetting();

        // disable ContactCeateAccount to not create person account
        List<Trigger_Status__c> contactTriggerStatus = [SELECT Id, Name, Active__c FROM Trigger_Status__c WHERE Name = 'ContactCeateAccount'];
        List<Trigger_Status__c> updateTriggerStatus = new List<Trigger_Status__c>();
        for(Trigger_Status__c triggerStatus: contactTriggerStatus) {
          triggerStatus.Active__c = false;
          updateTriggerStatus.add(triggerStatus);
        }
        update updateTriggerStatus;

        
        Integration_User_Tradesite__c testSetting = new Integration_User_Tradesite__c();
        testSetting.name = 'tradeSiteUser';
        testSetting.Username__c = 'sf1';
        testSetting.Password__c = 'pas';
        testSetting.Endpoint__c = 'endpo';
        testSetting.NameChangeStartNumber__c = 1;
        testSetting.NameChangeEndNumber__c = 9;
        testSetting.NameChangeLastNumber__c = 1;
        testSetting.ApprovedText__c = 'Appr';
        testSetting.RejectedText__c = 'Reje';
        insert testSetting;
        
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        insert lstConfigData;
        
        TestUtilityDataClassQantas.createQantasConfigDataAccRecType();
        
        Profile p = [Select Id, Name from Profile where Name = 'Qantas Freight Service'];
        
        Trigger_Status__c ts = new Trigger_Status__c(Name = 'trgTradeSiteResponses', Active__c = true);
        insert ts;
        
        List<Freight_RecordTypeSeggregation__c> lstfreCusSetting = Freight_TriggerRecordTypeUtility.createAccTrigTestCustomSetting();
        insert lstfreCusSetting;
        
        Airport_Code__c aCode = new Airport_Code__c(Name = 'SYD', QF_Freight_Presence__c =TRUE, Freight_Station_Name__c = 'Sydney', Freight_Airport_State_Code__c  ='NSW');
        insert aCode;
        
        /*User u = new User(LastName = 'TestUser', Alias = 'testu',Email = 'testuser@sample.com',Username = 'testusername@sample.com', 
                          CommunityNickname = 'testu', Phone = '1232141246', ProfileId = p.Id, 
                          EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_AU',
                          TimeZoneSidKey = 'Australia/Sydney',IsActive = true );
        insert u;*/
        
        Account testacc = new Account(Name='Test Tcs Account',
                                      Active__c = true,
                                      Type = 'Freight-Country',
                                      Customer_Ranking__c = '5',
                                      Sentiment__c = 'Positive',
                                      RecordTypeId = accRTId);
        insert testacc;
        
        Contact testcon = new Contact(FirstName='Bob',
                                      LastName='Test',
                                      AccountId=testacc.id,
                                      Function__c = 'IT',
                                      Business_Types__c = 'Freight',
                                      Email = 'abc@test.com');
        insert testcon;
        
        Case testcase = new Case(AccountId = testacc.id,
                                 ContactId = testcon.id,
                                 RecordTypeId = caseRTId,
                                 Subject = 'the case',
                                 Description = 'test desc',
                                 Status = 'Open',
                                 Priority = 'Medium',
                                 Origin = 'Phone',
                                 Type = 'Customer Query',
                                 Customer_Advice_Method__c = 'Email',
                                 Freight_Port__c = aCode.Id
                                );
        insert testcase;
        
        // testing ACR Tile , Acc Owner Details and Account Ranking Tile
        Test.StartTest();
        FreightACRTileController.getACRRecord(testcase.Id);
        FreightCustomerRankingController.getAccRecord(testcase.Id);
        FreightAccOwnerController.getAccOwner(testcase.Id);
        Test.StopTest();    
        
    }
    
    @isTest 
    static void OpenCasesTileTest() {
        
        List<Freight_RecordTypeSeggregation__c> lstfreCusSetting = Freight_TriggerRecordTypeUtility.createAccTrigTestCustomSetting();
        insert lstfreCusSetting;
        
        Airport_Code__c aCode = new Airport_Code__c(Name = 'SYD', QF_Freight_Presence__c =TRUE, Freight_Station_Name__c = 'Sydney', Freight_Airport_State_Code__c  ='NSW');
        insert aCode;
        
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Freight Claims').getRecordTypeId();
        String accRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Freight Account').getRecordTypeId();
        
        Trigger_Status__c ts = new Trigger_Status__c(Name = 'trgTradeSiteResponses', Active__c = true);
        insert ts;
        
        Account testacc = new Account(Name='Test Tcs Account',
                                      Active__c = true,
                                      Type = 'Freight-Country',
                                      RecordTypeId = accRTId);
        insert testacc;
        
        Contact testcon = new Contact(FirstName='Bob',
                                      LastName='Test',
                                      AccountId=testacc.id,
                                      Function__c = 'IT',
                                      Business_Types__c = 'Freight',
                                      Email = 'abc@test.com');
        insert testcon;
        
        Case testcase = new Case(AccountId = testacc.id,
                                 ContactId = testcon.id,
                                 RecordTypeId = caseRTId,
                                 Subject = 'the case',
                                 Description = 'test desc',
                                 Status = 'Open',
                                 Priority = 'Medium',
                                 Origin = 'Phone',
                                 Type = 'Customer Query',
                                 Customer_Advice_Method__c = 'Email',
                                 Freight_Resolve_Reason__c = 'Other',
                                 Freight_Resolve_Comments__c = 'Resolved',
                                 Freight_Recovery_Solution__c = 'Other',
                                 Freight_Recovery_Cost__c = 100
                                );
        insert testcase;
        
        
        list<Case> openCasesList = new list<Case>();
        //list<Case> returnCaselist = new list<Case>();
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query',Customer_Advice_Method__c = 'Email'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe caseAssignedAssignedAssignedAssigned',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query',Customer_Advice_Method__c = 'Email'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query',Customer_Advice_Method__c = 'Email'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query',Customer_Advice_Method__c = 'Email'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query',Customer_Advice_Method__c = 'Email'));
        
        if(!openCasesList.isEmpty())
            insert openCasesList;
        
        Test.StartTest();
        FreightOpenCasesController.getAllOpenCases(testcase.Id);
        
        testcase.Status = 'Assigned';
        update testcase;
        testcase.Status = 'Resolved';
        testcase.Freight_Resolve_Reason__c='Other';
        testcase.Resolved_Reasons_Sub_Type__c='Other';
        update testcase;
        testcase.Status = 'Reopened';
        update testcase;
        testcase.Status = 'Closed';
        update testcase;
        Test.StopTest();    
        
    }

  /*--------------------------------------------------------------------------------------      
    Method Name:        OpenCasesTileTestservicerequest
    Description:        This test Method will increase the test coverage for'assignEntitlement1' method 
                        in Freight_CaseTriggerHandler class 
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/
    
     @isTest 
    static void OpenCasesTileTestservicerequest() {
        
        List<Freight_RecordTypeSeggregation__c> lstfreCusSetting = Freight_TriggerRecordTypeUtility.createAccTrigTestCustomSetting();
        insert lstfreCusSetting;

        
        
        String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        String accRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();

        Trigger_Status__c ts = new Trigger_Status__c(Name = 'trgTradeSiteResponses', Active__c = true);
        insert ts;
        
        Trigger_Status__c ts1 = new Trigger_Status__c(Name = 'AccountTeam', Active__c = true);
        insert ts1;
        
        Trigger_Status__c ts2 = new Trigger_Status__c(Name = 'AccountEligibilityLogTrigger', Active__c = true);
        insert ts2;
        
        Trigger_Status__c ts3 = new Trigger_Status__c(Name = 'AccountAirlineLeveltrigger', Active__c = true);
        insert ts3;
        
        Trigger_Status__c ts4 = new Trigger_Status__c(Name = 'AccountDeleteTrigger', Active__c = true);
        insert ts4;
        
        Trigger_Status__c ts5 = new Trigger_Status__c(Name = 'AccountUpdateAccountDetail', Active__c = true);
        insert ts5;
        
        
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Account RecType1','Agency Account;Charter Account;Customer Account;Global Account;Other Account;Prospect Account;Regional Account;'));
        
        insert lstConfigData;
        
        Account testacc = new Account(Name='Test Tcs Account',
                                      Active__c = true,
                                      RecordTypeId=accRTId
                                      );
        insert testacc;
        
        
        
        Contact testcon = new Contact(FirstName='Bob',
                                      LastName='Test',
                                      AccountId=testacc.id,
                                      Function__c = 'IT',
                                      Business_Types__c = 'Freight',
                                      Email = 'abc@test.com');
        insert testcon;
        
        Case testcase = new Case(AccountId = testacc.id,
                                 ContactId = testcon.id,
                                 RecordTypeId = caseRTId,
                                 Subject = 'the case',
                                 Description = 'test desc',
                                 Status = 'Open',
                                 Priority = 'Medium',
                                 Origin = 'Phone',
                                 Type = 'Customer Query',
                                // Customer_Advice_Method__c = 'Email',
                               //  Freight_Resolve_Reason__c = 'Other',
                                 Freight_Resolve_Comments__c = 'Resolved',
                                // Freight_Recovery_Solution__c = 'Other',
                                 Freight_Recovery_Cost__c = 100
                                );
        insert testcase;
        
        
        list<Case> openCasesList = new list<Case>();
        //list<Case> returnCaselist = new list<Case>();
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe casethe caseAssignedAssignedAssignedAssigned',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query'));
        openCasesList.add(new Case(AccountId = testacc.id,ContactId = testcon.id,RecordTypeId = caseRTId,Subject = 'the case',Description = 'test desc',Status = 'Open',Priority = 'Medium',Origin = 'Phone',Type = 'Customer Query'));
        
        if(!openCasesList.isEmpty())
            insert openCasesList;
        
        Test.StartTest();
        
            testcase.Status = 'In Progress';
            update testcase;
            testcase.Status = 'Resolved';
            update testcase;
            testcase.Status = 'Reopened';
            update testcase;
            testcase.Status = 'Closed';
            update testcase;
        Test.StopTest();    
        
    }
    
}