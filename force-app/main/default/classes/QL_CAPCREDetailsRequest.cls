public class QL_CAPCREDetailsRequest {
  @AuraEnabled public String system_z;// in json: system
  @AuraEnabled public String firstName ;
  @AuraEnabled public String middleName ; 
  @AuraEnabled public String lastName ;
  @AuraEnabled public String prefix ;
  @AuraEnabled public String salutation ; 
  @AuraEnabled public String qantasUniqueId;
  @AuraEnabled public String qantasFFNo ;
  @AuraEnabled public String birthDate ;
  @AuraEnabled public String genderCode;
  @AuraEnabled public String phoneCountryCode;
  @AuraEnabled public String phoneAreaCode;
  @AuraEnabled public String phoneLineNumber;
  @AuraEnabled public String phoneExtension;
  @AuraEnabled public String emailAddr;
  @AuraEnabled public String travelDocTypeCode;
  @AuraEnabled public String travelDocId ;
  @AuraEnabled public String travelDocIssueCountry ;
  @AuraEnabled public String pnr ;
  @AuraEnabled public String customerPerPage ;
  @AuraEnabled public String pageNumber;
}