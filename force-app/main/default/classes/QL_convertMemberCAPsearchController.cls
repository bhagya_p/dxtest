public class QL_convertMemberCAPsearchController
{
 
    @AuraEnabled public Boolean isSuccess {get;set;}
    @AuraEnabled public String msg {get;set;}
    @AuraEnabled public String recID {get;set;}
    
    @AuraEnabled
 public static QL_convertMemberCAPsearchController invokeWebServiceCAPSearch(Id memID)
 
  {     
        QL_convertMemberCAPsearchController objq = new QL_convertMemberCAPsearchController();
        List<Contact> lstcon = new List<Contact>();
        List<Contact> existingcon = new List<Contact>();
        String CAPID;
        try
        {  
        Associated_Person__c  member= [SELECT Id,Account__c,First_Name__c,Frequent_Flyer_Number__c,Last_Name__c,Profile_Email__c FROM Associated_Person__c  WHERE ID =:memID ];
        existingcon =[SELECT Id,Frequent_Flyer_Number__c FROM CONTACT WHERE Frequent_Flyer_Number__c=: member.Frequent_Flyer_Number__c];
        system.debug('existingcon ********'+existingcon);
         if(!existingcon.isEmpty()){
         objq.isSuccess = false;
         objq.msg = 'Contact exist for this Member';
         return objq;
         }
        
        QL_CAPCREDetailsRequest requestBody = new QL_CAPCREDetailsRequest();
        requestBody.qantasFFNo = member.Frequent_Flyer_Number__c.trim();
        requestBody.lastName =member.Last_Name__c;
        requestBody.emailAddr = member.Profile_Email__c;
        requestBody.customerPerPage='100';
        requestBody.system_z='CAPCRE_CC';
        system.debug('requestBody^^^^^^^'+requestBody);
        String token = QL_GenericUtils.getCAPValidToken('QCC_CAPToken');
       
        system.debug('token$$$$$$$$$$$$$'+token);
        Qantas_API__c qantasAPI = CustomSettingsUtilities.getQantasCAPAPI('QCC_CAPDetailsAPI');
        system.debug('qantasAPIYYYYY'+qantasAPI);
        Http callOut = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(qantasAPI.EndPoint__c);
        request.setMethod(qantasAPI.Method__c); 
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', token);
        
        // A map of existing key to replacement key
        Map<String, String> replacementsReq = new Map<String, String> {
            'system_z' => 'system'
                };
        
        String reqStr = QL_GenericUtils.mogrifyJSON(JSON.serialize(requestBody, true), replacementsReq);
        request.setBody(reqStr);
        system.debug('request########'+request);
        system.debug('requestBody########'+request.getBody());
        HttpResponse response =  callOut.send(request);
        system.debug('response$$$$$$$'+response);
        system.debug('Response Boy######'+response.getBody());
      
        
        QL_GenericUtils.updateToken(token, 'QCC_CAPToken');
        if(response.getStatusCode() == 200){
            QL_CAPCREDetailsResponse.Response custResp = (QL_CAPCREDetailsResponse.Response) System.JSON.deserialize(response.getBody(), QL_CAPCREDetailsResponse.Response.class);
           
            for(QL_CAPCREDetailsResponse.Customers obj: custResp.customers){
            
             if(obj != null && obj.Customer != null && String.isNotBlank( obj.Customer.qantasUniqueId)){          
           
                system.debug('obj.Customer.qantasUniqueId*********'+obj.Customer.qantasUniqueId);
                Contact cont = new Contact();
                cont.firstname = member.First_Name__c;
                cont.lastname = member.Last_Name__c;
                cont.Frequent_Flyer_Number__c= member.Frequent_Flyer_Number__c;                
                cont.CAP_ID__c =  obj.Customer.qantasUniqueId;
                CAPID = obj.Customer.qantasUniqueId;
                cont.Email=member.Profile_Email__c;
                cont.Function__c='Other';
                cont.Business_Types__c='Agency';
                cont.Job_Role__c='Other';
                cont.AccountId=member.Account__c;
                lstcon.add(cont);               
               
            }
            }
            
            
             if(lstcon.size()>0)
               insert lstcon;
             objq.recID=lstcon[0].Id;
             system.debug('=*****'+objq.recID);
             objq.isSuccess = true;
             objq.msg = 'Contact has been created sucessfully!';

        } 
                          return objq;
      }
       catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());    
        }

   }
   
}