/***********************************************************************************************************************************

Description: Apex class for retrieving the account information and TMC Information. 

History:
======================================================================================================================
Name                          Description                                                 Tag
======================================================================================================================
Abhijeet P				      Account and TMC  record retrieval class                     T01

Created Date    : 03/05/18 (DD/MM/YYYY)

**********************************************************************************************************************************/

global class QAC_TMCInformationParsing {

    /*
     * Variable Declarations
     */
    global String gdsCode 			{get; set;}
    global String pseudoCityCode	{get; set;}
    global Boolean active			{get; set;}
    global String agencyProductFare	{get; set;}
    global String name				{get; set;}
    global DateTime lastInactiveDate {get; set;}
    global Id tmcId					{get;set;} 
    global Id accountId				{get;set;}
    
    /*
     * Constructor
     */
   public QAC_TMCInformationParsing(Agency_Info__c tmcInfo){
        this.tmcId	 			= tmcInfo.Id;
        this.name 				= tmcInfo.Name;  
        this.gdsCode 			= tmcInfo.Code__c;
        this.pseudoCityCode 	= tmcInfo.PCC__c;    
        this.agencyProductFare 	= tmcInfo.Agency_Product_Fares__c;
        this.active 			= tmcInfo.Active__c;  
        this.lastInactiveDate 	= tmcInfo.Last_Inactive_Date__c;
    }

    /*
     * To Parse deserialise data
     */    
    public static List<QAC_TMCInformationParsing> parse(String json) {
		return (List<QAC_TMCInformationParsing>) System.JSON.deserialize(json, List<QAC_TMCInformationParsing>.class);
	}
	
}