@IsTest
public class QCC_AddEventstoGlobalEventControllerTest {
	
	private static Event__c globalEvent;
	private static List<Event__c> listEvent;

	static {

		TestUtilityDataClassQantas.insertQantasConfigData();
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createGlobalEventTriggerSetting();
        insert lsttrgStatus;
        List<Trigger_Status__c> lsttrgStatusEvent = TestUtilityDataClassQantas.createEventTriggerSetting();
        for(Trigger_Status__c ts : lsttrgStatus){
            ts.Active__c = false;
        }
        insert lsttrgStatusEvent;

		globalEvent = new Event__c();
    	listEvent = new List<Event__c>();

    	//String airportEvents = 'Snowfall;High Winds;Volcano';
    	//Event__c globalEvent = new Event__c();
        Id recTypeId = Schema.SObjectType.Event__c.getRecordTypeInfosByName().get('Global Event').getRecordTypeId();
        //globalEvent.Name = 'test global event';
        //globalEvent.Airport_Event_Type__c = airportEvents;
        globalEvent.RecordTypeId = recTypeId;
    	insert globalEvent;

    	for(Integer i = 0; i < 5; i++){
    		Event__c itemEvent = new Event__c();
            itemEvent.ScheduledDepDate__c = Date.newInstance(2018, 10, 10);
            itemEvent.Scheduled_Depature_UTC__c = DateTime.newInstanceGmt(2018, 10, 10, 0, 0, 0);
    		itemEvent.GlobalEvent__c = globalEvent.Id;
    		listEvent.add(itemEvent);
    	}
    	insert listEvent;
	}

    
    static testMethod void testMethodGetFieldSets() {
        Test.startTest();
        QCC_AddEventstoGlobalEventController.DataInitWrapper dataInitWrapper = QCC_AddEventstoGlobalEventController.getFieldSets();
        QCC_AddEventstoGlobalEventController.parseStringToGMT('2018-10-01T00:00:00.000Z');
        Test.stopTest();
        
        System.assertEquals(true, dataInitWrapper.listFieldSets.size() > 0);
        System.assertEquals(true, String.isNotBlank(dataInitWrapper.columnSets));
    }

    static testMethod void testMethodGetDataEvents() {
    	QCC_AddEventstoGlobalEventController.DataInitWrapper dataInitWrapper = QCC_AddEventstoGlobalEventController.getFieldSets();
        
        for(Integer i = 0; i < dataInitWrapper.listFieldSets.size(); i++){
            if(dataInitWrapper.listFieldSets[i].fieldLabel == 'Scheduled Depature UTC From'){
                dataInitWrapper.listFieldSets[i].fieldValue = '2018-10-01T00:00:00.000Z';
            } else if(dataInitWrapper.listFieldSets[i].fieldLabel == 'Scheduled Depature UTC To'){
                dataInitWrapper.listFieldSets[i].fieldValue = '2018-10-20T00:00:00.000Z';
            } else if(dataInitWrapper.listFieldSets[i].fieldLabel == 'Scheduled Departure Date From'){
                dataInitWrapper.listFieldSets[i].fieldValue = '2018-10-01';
            } else if(dataInitWrapper.listFieldSets[i].fieldLabel == 'Scheduled Departure Date To'){
                dataInitWrapper.listFieldSets[i].fieldValue = '2018-10-20';
            }
        }
        
        System.debug('==dataInitWrapper.listFieldSets==');
        System.debug(dataInitWrapper.listFieldSets);
        
        Test.startTest();
        List<Event__c> lstEvents = QCC_AddEventstoGlobalEventController.getDataEvents(JSON.serialize(dataInitWrapper.listFieldSets), dataInitWrapper.columnSets);
        Test.stopTest();
        
        System.assertEquals(true, dataInitWrapper.listFieldSets.size() > 0);
        System.assertEquals(true, String.isNotBlank(dataInitWrapper.columnSets));
    }

    static testMethod void testMethodAddEventsToGlobalEvent() {
    	QCC_AddEventstoGlobalEventController.DataInitWrapper dataInitWrapper = QCC_AddEventstoGlobalEventController.getFieldSets();
        System.debug(dataInitWrapper.listFieldSets);
        List<String> lstEventId = new List<String>();

        for(Event__c item : listEvent){
        	lstEventId.add(item.Id);
        }

        Test.startTest();
        QCC_AddEventstoGlobalEventController.addEventsToGlobalEvent(lstEventId, globalEvent.Id);
        Test.stopTest();
        
        List<Event__c> lstEvent = [
            SELECT Id
            	,GlobalEvent__c
            FROM Event__c
            WHERE Id IN :lstEventId
        ];

        System.assertEquals(globalEvent.Id, lstEvent[0].GlobalEvent__c);
    }
}