/*******************************************************************************************************************************
Description: This apex is invoked by a process builder which will convert lead into Account and Contact in 
case of no duplicates exist. Update account type accordingly.
Created Date: 10-04-2017
JIRA: CRM-2545

Modified Date: 15-06-2017
Modified By : Freight Team as part of Freight release 2 changes.

JIRA: CRM-3040

Modified Date: 05-May-2018
Modified By : DPP Lead changes, auto opportunity creation


******************************************************************************************************************************** */

Public class AutoConvertLeads {@InvocableMethod
    public static void LeadAssign(List < Id > LeadIds) {
        try {
            system.debug('insidetry');
            Map < List<String>,
                Contact > conMap = new Map < List<String>,
                Contact > ();
            Contact[] consToUpdate = new List < Contact > ();
            String msg = '';
            String ErrorMessageRecipient = '';
            Set < String > emailSet = new Set < String > ();
            Set < String > lName = new Set < String > ();
            List < Lead > newList = new List < Lead > ();
            //Freight Start
            Map <Id,Id> LeadWithParAccMap = new Map<Id,Id> ();
            Map <Id,Id> newAccIdWithParAccMap = new Map<Id,Id> ();
            Map<ID,AccountContactRelation> parAccIdWithAcrObjMap = new Map<ID,AccountContactRelation> ();
            //Freight End
            Id accountId;
            Id contactId;
            Id leadId;
            system.debug(LeadIds);
            newList = [Select Email,Parent_Account__c,LastName,Auto_Create_Opportunity__c,Opportunity_Type__c from Lead where Id IN: LeadIds];
            system.debug(newList);
            //Get email addresses.
            for (Lead l: newList) {
                system.debug(l);
                emailSet.add(l.Email);
                //Freight Start
                if (l.Parent_Account__c!=null){ 
                    LeadWithParAccMap.put(l.Id,l.Parent_Account__c); 
                }
                //Freight End
            }
            List<string> str = new List<string>();
            //Get existing contacts.        
            for (Contact con: [select Email,LastName from Contact where email in :emailSet AND LastName IN:lName]) {
                str.add(con.Email +','+con.LastName);
                conMap.put(str, con);
                system.debug('Existcontact'+con);
            }
            LeadStatus CLeadStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true Limit 1];
            List < Database.LeadConvert > MassLeadconvert = new List < Database.LeadConvert > ();
            List < String > convertMsgs = new List < String > (); // List of Strings containing status messages about each conversion operation
            for (Lead l: newList) {
                Database.LeadConvert Leadconvert = new Database.LeadConvert();
                Leadconvert.setLeadId(l.id);
                Leadconvert.setConvertedStatus(CLeadStatus.MasterLabel);
                if(l.Auto_Create_Opportunity__c== TRUE)
                    Leadconvert.setDoNotCreateOpportunity(False); //Remove this line if you want to create an opportunity from Lead Conversion 
                else
                    Leadconvert.setDoNotCreateOpportunity(TRUE);
                Leadconvert.setSendNotificationEmail(TRUE);
                system.debug('Existcontactmap'+conMap);
                List<string> EmailLname = new List<string>();
                EmailLname.add(l.Email+','+l.LastName);
                if (conMap.keySet().contains(EmailLname)) {
                    Leadconvert.setContactId(conMap.get(EmailLname).Id);
                }
                MassLeadconvert.add(Leadconvert);
            }
            
            if (!MassLeadconvert.isEmpty()) {
                List < Database.LeadConvertResult > lcr = Database.convertLead(MassLeadconvert, false);
                
                for (Database.LeadConvertResult lcrList: lcr) {
                    system.debug('LeadConvertResult**********' + lcrList);
                    //Capture failed conversions for the email message.
                    //Freight Start
                    newAccIdWithParAccMap.put(lcrList.getAccountId(), LeadWithParAccMap.get(lcrList.getLeadId()));
                    //Freight End
                    Lead le = [SELECT Id, Name, OwnerId,Auto_Lead_Convert__c,Auto_Create_Opportunity__c,Opportunity_Type__c from Lead WHERE ID = :lcrList.getLeadId()];
                    
                    system.debug('leadconvertBoolean**********' + lcrList.isSuccess());
                    if (lcrList.isSuccess()) {
                        accountId = lcrList.getAccountId();
                        contactId = lcrList.getContactId();
                        leadId = lcrList.getLeadId();
                        Account acc = [SELECT Name From Account WHERE ID = :accountId];
                        Contact con = [SELECT Name From Contact WHERE ID = :contactId];
                        system.debug('leadId**********' + leadId);
                        system.debug('accountId**********' + accountId);
                        //String convertMsg='CONVERSION SUCCEEDED: Converted Lead: ' + le.Name + ' into Account: ' + acc.Name + ' and Contact: ' + con.Name + '\n';
                        String convertMsg = 'Account:' + ' <' + acc.Name + '>' + ' and Contact:' + ' <' + con.Name + '>' + ' created from your Lead. Thank you.';
                        
                        convertMsgs.add(convertMsg);
                        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                        
                        messageBodyInput.messageSegments = new List < ConnectApi.MessageSegmentInput > ();
                        
                        mentionSegmentInput.id = le.OwnerId;
                        messageBodyInput.messageSegments.add(mentionSegmentInput);
                        
                        textSegmentInput.text = ' ' + convertMsg;
                        messageBodyInput.messageSegments.add(textSegmentInput);
                        
                        feedItemInput.body = messageBodyInput;
                        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                        feedItemInput.subjectId = accountId;
                        
                        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                    }
                    else {
                        system.debug('inside lead error part*************');
                        le.Auto_Lead_Convert__c=False;
                        update le;
                        Database.Error[] errors = lcrlist.getErrors();
                        String convertMsg = 'Case has been created for the System Administrator to action. Thank you.';
                        for (Database.Error err: errors) {
                            //  convertMsg += '\tERROR: ' + err.getMessage() + '\n';
                            system.debug('convertMsg ############' + convertMsg);
                            convertMsgs.add(convertMsg);
                            //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), le.Id, ConnectApi.FeedElementType.FeedItem, 'Hi'+convertMsgs);               
                            
                            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                            
                            messageBodyInput.messageSegments = new List < ConnectApi.MessageSegmentInput > ();
                            
                            mentionSegmentInput.id = le.OwnerId;
                            messageBodyInput.messageSegments.add(mentionSegmentInput);
                            
                            textSegmentInput.text = ' ' + convertMsg;
                            messageBodyInput.messageSegments.add(textSegmentInput);
                            
                            feedItemInput.body = messageBodyInput;
                            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                            feedItemInput.subjectId = le.Id;
                            
                            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                        }
                        
                    }
                    if (accountId != null) {
                        system.debug('accountId************' + accountId);
                        List < account > accToUpdate = new List < account > ();
                        Map < String,
                            ID > RecordTypeMAP = new Map < String,
                            Id > ();
                        for (RecordType RecType: [Select Id, DeveloperName from RecordType where sObjectType = 'account']) {
                            RecordTypeMAP.put(RecType.DeveloperName, RecType.id);
                        }
                        
                        
                        List < Account > aclist = [SELECT Id, Name, New_Account_Type__c, Type, IATA_Number__c, RecordTypeID, BillingCountry, BillingCountryCode, BillingState, Parentid, Freight_Corporate_Identifier__c, Freight_Station_Code__r.Name, Freight_Unique_Identifier__c, Freight_Customer_Code__c FROM Account WHERE Id = :accountId];
                        For(account accRec: aclist) {
                            system.debug('accRec.BillingCountry########' + accRec.BillingCountry);
                            system.debug('accRec.BillingCountryCode########' + accRec.BillingCountryCode);
                            system.debug('accRec.BillingState########' + accRec.BillingState);
                            if (accRec.New_Account_Type__c == 'Agency') {
                                system.debug('accRec*************' + accRec);
                                accRec.Type = 'Agency Account';
                                accRec.Qantas_Industry_Centre_ID__c = accRec.IATA_Number__c;
                                accRec.RecordTypeID = RecordTypeMAP.get('Agency_Account');
                                accRec.Industry = 'Travel';
                                accRec.ParentId = newAccIdWithParAccMap.get(accRec.Id);
                                if (accRec.BillingCountry == 'AU') {
                                    system.debug('accRec.BillingState**********' + accRec.BillingState);
                                    String myString1 = accRec.Name;
                                    system.debug('myString1%%%%%%' + myString1);
                                    String myString2 = accRec.BillingState;
                                    system.debug('myString2 $$$$$$$' + myString2);
                                    Boolean result = myString1.contains(myString2);
                                    system.debug('result ^^^^^^^^^' + result);
                                    if (result == false) accRec.Name = accRec.Name + ' [' + accRec.BillingState + ']';
                                }
                                else {
                                    system.debug('accRec.BillingCountryCode**********' + accRec.BillingCountryCode);
                                    String myString1 = accRec.Name;
                                    system.debug('myString1%%%%%%' + myString1);
                                    String myString2 = accRec.BillingCountryCode;
                                    system.debug('myString2 $$$$$$$' + myString2);
                                    Boolean result = myString1.contains(myString2);
                                    system.debug('result ^^^^^^^^^' + result);
                                    if (result == false) accRec.Name = accRec.Name + ' [' + accRec.BillingCountryCode + ']';
                                }
                                accToUpdate.add(accRec);
                            }
                            else if (accRec.New_Account_Type__c == 'Prospect') {
                                accRec.Type = 'Prospect Account';
                                accRec.RecordTypeid = RecordTypeMAP.get('Prospect_Account');
                                accRec.ParentId = newAccIdWithParAccMap.get(accRec.Id);
                                accToUpdate.add(accRec);
                            }
                            
                            else if (accRec.New_Account_Type__c == 'Customer') {
                                accRec.Type = 'Customer Account';
                                accRec.RecordTypeid = RecordTypeMAP.get('Customer_Account');
                                accRec.ParentId = newAccIdWithParAccMap.get(accRec.Id);
                                accToUpdate.add(accRec);
                            }
                            
                            else if (accRec.New_Account_Type__c == 'Charter') {
                                accRec.Type = 'Charter Account';
                                accRec.RecordTypeid = RecordTypeMAP.get('Charter_Account');
                                accRec.ParentId = newAccIdWithParAccMap.get(accRec.Id);
                                accToUpdate.add(accRec);
                            }
                            else if (accRec.New_Account_Type__c == 'Other') {
                                accRec.Type = 'Other Account';
                                accRec.RecordTypeid = RecordTypeMAP.get('Other_Account');
                                accRec.ParentId = newAccIdWithParAccMap.get(accRec.Id);
                                accToUpdate.add(accRec);
                            }
                            //Freight Start
                            else if (accRec.New_Account_Type__c == 'Freight-Branch') {
                                if(accRec.ParentId == null){
                                    accRec.ParentId = newAccIdWithParAccMap.get(accRec.Id);    
                                }   
                                accRec.Type = accRec.New_Account_Type__c;
                                accRec.RecordTypeid = RecordTypeMAP.get('Freight_Account');
                                if(accRec.Freight_Customer_Code__c != ''){
                                    accRec.Freight_Unique_Identifier__c = accRec.Freight_Customer_Code__c;
                                }
                                
                                accRec.Name += ' '+'[FREIGHT-'+accRec.Freight_Corporate_Identifier__c+'-'+accRec.BillingCountrycode+'-'+accRec.Freight_Station_Code__r.Name+']';
                                
                                accToUpdate.add(accRec);    
                                
                            }
                            //Freight Start
                            
                        }
                        if (accToUpdate.size() > 0) update accToUpdate;
                    }
                    if(lcrList.getOpportunityId()!= null){
                        List<Opportunity> optyToUpd = new List<Opportunity>();
                        Map < String,ID > OptyRecordTypeMAP = new Map < String,Id > ();
                        for (RecordType RecType: [Select Id, DeveloperName from RecordType where sObjectType = 'opportunity']) {
                            OptyRecordTypeMAP.put(RecType.DeveloperName, RecType.id);
                        }
                        Opportunity opp = [SELECT Name,RecordTypeId From Opportunity WHERE ID = :lcrList.getOpportunityId()];
                        if(le.Opportunity_Type__c=='Agency Partnerships')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('Agency_Partnerships');
                            optyToUpd . add(opp);
                        }
                        else if(le.Opportunity_Type__c=='Business and Government')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('Business_and_Government');
                            optyToUpd . add(opp);
                        }
                        else if(le.Opportunity_Type__c=='Charters')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('Scheduled_Charters');
                            optyToUpd . add(opp);
                        }
                        else if(le.Opportunity_Type__c=='GSA')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('GSA');
                            optyToUpd . add(opp);
                        }
                        else if(le.Opportunity_Type__c=='SME Product')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('SME_Product');
                            optyToUpd . add(opp);
                        }
                        else if(le.Opportunity_Type__c=='Sales Development')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('Sales_Development');
                            optyToUpd . add(opp);
                        }
                        else if(le.Opportunity_Type__c=='DPP Onboarding')
                        {
                            opp.RecordTypeid = OptyRecordTypeMAP.get('DPP_Onboarding_Partners');
                            optyToUpd . add(opp);
                        }
                        if(optyToUpd.size()>0)
                            update optyToUpd;
                    }
                    //Freight Start
                    if (contactId != null) {
                        List < Contact > conToUpdate = new List < Contact > ();
                        
                        List < Contact > conlist = [SELECT Id, Name, Business_Types__c, Accountid FROM Contact WHERE Id = :contactId AND Business_Types__c = 'Freight' ];
                        For(Contact conRec: conlist) {
                            if(conRec.Business_Types__c == 'Freight')
                            {
                                if(conRec.Accountid != null){
                                    conRec.Accountid = newAccIdWithParAccMap.get(conRec.Accountid);
                                    conToUpdate.add(conRec);
                                }
                            }
                            
                        }
                        if (conToUpdate.size() > 0) update conToUpdate;
                    }  
                    //Freight End                  
                }
                //Freight ACR Part Start
                if (accountId != null && contactId != null)
                {
                    
                    List < AccountContactRelation > acrToupdateList = new List < AccountContactRelation > ();
                    
                    List < AccountContactRelation > acrlist = [SELECT Id, Accountid, Contactid, Job_Role__c, Business_Type__c, Related_Phone__c, Related_Email__c, Job_Title__c, Function__c, Freight_City__c FROM AccountContactRelation WHERE Contactid = :contactId AND Accountid = : accountId  AND Business_Type__c = 'Freight' ];
                    For(AccountContactRelation acrRec: acrlist) {            
                        
                        parAccIdWithAcrObjMap.put(newAccIdWithParAccMap.get(acrRec.Accountid), acrRec);                    
                    } 
                    
                    
                    List < AccountContactRelation > acrupdatelist = [SELECT Id, Accountid, Contactid, Job_Role__c, Business_Type__c, Related_Phone__c, Related_Email__c, Job_Title__c, Function__c, Freight_City__c FROM AccountContactRelation WHERE Contactid = :contactId AND Accountid = : newAccIdWithParAccMap.get(accountId)  ];
                    For(AccountContactRelation acrupdateRec: acrupdatelist) {            
                        
                        
                        acrupdateRec.Job_Role__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Job_Role__c;
                        acrupdateRec.Related_Email__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Related_Email__c;
                        acrupdateRec.Job_Title__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Job_Title__c;
                        acrupdateRec.Function__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Function__c; 
                        if (parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Business_Type__c != null){
                            acrupdateRec.Business_Type__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Business_Type__c;
                        }
                        if (parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Related_Phone__c != null){
                            acrupdateRec.Related_Phone__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Related_Phone__c;
                        }
                        if (parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Freight_City__c != null){
                            acrupdateRec.Freight_City__c = parAccIdWithAcrObjMap.get(acrupdateRec.Accountid).Freight_City__c;
                        }
                        
                        
                        acrToupdateList.add(acrupdateRec); 
                    } 
                    if (acrToupdateList.size() > 0) update acrToupdateList; 
                    
                }
                //Freight ACR Part End
            }
            //Send email with failed conversions.
            /*  if(msg != '') {
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[]{ErrorMessageRecipient,'karpagam.swaminathan@tcs.com'};

mail.setToAddresses(toAddresses);
mail.setSubject('Lead conversion errors');
mail.setPlainTextBody(msg);
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });           
}*/
            // Send notification email
            String msgBody = 'Automatic Lead conversion results:\n';
            for (String convertMsg: convertMsgs) {
                msgBody += convertMsg;
            }
            // sendEmail(UserInfo.getUserId(), UserInfo.getName(), 'Automatic Lead Conversion', msgBody, null);
        }
        catch(Exception ex) {
            // sendEmail(UserInfo.getUserId(), UserInfo.getName(), 'Automatic Lead Conversion Failed', ex.getMessage(), null);
            throw ex;
            system.debug('insidecatch');
            system.debug('insidecatch'+ ex);
        }
        
    }
    /* public static Boolean sendEmail(Id targetId, String senderDisplayName, 
String subject, String textBody, Messaging.EmailFileAttachment[] attachments) {

// Create and send a single email message to the targetId      
Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

// Use the specified template
email.setTargetObjectId(targetId);
email.setSubject(subject);
email.setPlainTextBody(textBody);
email.setSenderDisplayName(senderDisplayName);
email.setSaveAsActivity(false);
if (attachments != null) {
email.setFileAttachments(attachments);
}
Messaging.SendEmailResult[] results = 
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
return results[0].isSuccess();        
} */
}