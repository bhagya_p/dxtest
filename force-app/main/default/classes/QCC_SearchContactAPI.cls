/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Rest API Request Apex to create update Passenger and contact
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
11-June-2018      Praveen Sampath               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
@RestResource(urlMapping='/QCCSearchContact')
global class QCC_SearchContactAPI {
    /*----------------------------------------------------------------------------------------------      
    Method Name:        searchContact
    Description:        Create Contact or Get the Correct contact
    Parameter:          FF Number, Last Name and isFF
    Return:             Contact ID
    ----------------------------------------------------------------------------------------------*/
    @HttpPost
    global static void searchContact() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        try{
            String resBody;
            String reqBody = req.requestBody.toString();
            QCC_ContactWrapper conWrap =  (QCC_ContactWrapper) Json.deserialize(reqBody, QCC_ContactWrapper.class);
            Contact objCon = new Contact();
            if(conWrap != Null){
                if(conWrap.isFF){
                    objCon = QCC_GenericUtils.fetchContactForFFCase(conWrap);
                }else{
                    objCon = QCC_GenericUtils.fetchContactForNonFFCase();
                }
                
            }
            // Inspect publishing result 
            if (objCon != Null) {
                res.statusCode = 200;
                ContactInfo con = new ContactInfo();
                con.conId = objCon.Id;
                con.msg = 'Success';
                resBody = Json.serialize(con);
            } 
            res.responseBody = Blob.valueOf(resBody); 
        }catch(Exception ex){
            res.statusCode = 400;
            ContactInfo con = new ContactInfo();
            con.msg = 'Error returned: '+ ex.getMessage();
            String resBody = Json.serialize(con);
            res.responseBody = Blob.valueOf(resBody); 
            system.debug('Exception###'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - Customer Search', 'QCC_SearchContactAPI', 
                                    'searchContact', String.valueOf(req), String.valueOf(res), true,'', ex);
        }
    }

    public class ContactInfo{
        public String conId;
        public String msg;
    }
}