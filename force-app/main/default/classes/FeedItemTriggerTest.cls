@isTest
private class FeedItemTriggerTest {
    @testSetup
    static void createData() {

        //List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.enableTriggers();
        List<Trigger_Status__c> lsttrgStatus = TestUtilityDataClassQantas.createAccountTriggerSettingasTrue();
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createContactTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createCaseTriggerSetting());
        lsttrgStatus.addAll(TestUtilityDataClassQantas.createRecoveryTriggerSetting());
        lsttrgStatus.add(new Trigger_Status__c(Name= 'FeedItemTrgPreventChatterDeletion', Active__c= true));
        insert lsttrgStatus;
        TestUtilityDataClassQantas.insertQantasConfigData();
        List<Qantas_API__c> lstQantas = TestUtilityDataClassQantas.createQantasAPI();
        insert lstQantas;

        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Integration Logs Rec Type','Integration Logs'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('RecoveryStatusApproved','Approved'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_FligjtCode Domestic','Domestic'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_EmergencyOutcomeDomestic','Emergency Expense - Domestic Stage'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_FligjtCode International','International'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_EmergencyOutcomeInternational','   Emergency Expense - International Stage'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('QCC_EmergencyOutcomeNotEligible','Not Eligible for Emergency Expenses'));
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('CaseStatusClosed','Closed'));
        	
        insert lstConfigData;

        //Create Account
        Account objAcc = TestUtilityDataClassQantas.createAccount();
        objAcc.Airline_Level__c = '1';
        objAcc.Active__c = true;
        insert objAcc;
        system.assert(objAcc != Null, 'Account is not inserted');

        //Create Contact
        Contact objContact = TestUtilityDataClassQantas.createContact(objAcc.Id);
        objContact.LastName='ContTest2';
        objContact.CAP_ID__c='710000000017';
        insert objContact;
        system.assert(objContact.Id != Null, 'Contact is not inserted');

        //Create Case
        List<Contact> lstContact = new List<Contact>();
        lstContact.add(objContact);
        String recordType = 'CCC Complaints';
        String type = 'Complaint';
        List<Case> lstCase = TestUtilityDataClassQantas.insertCases(lstContact, recordType, type, 'Flight Disruptions','','','');
        system.assert(lstCase.size()>0, 'Case List is Zero');
        system.assert(lstCase[0].Id != Null, 'Case is not inserted');
        
        FeedItem fi = new FeedItem();
        fi.ParentId = lstCase[0].id;
        fi.Body = 'Testing';
        insert fi;
        system.assert(fi.Id != Null, 'FeedItem is not inserted');
    }
    
    static testMethod void testPreventChatterDeletionPositive() {
        List<EnableValidationRule__c> lstEnableValidation = new List<EnableValidationRule__c>();
        lstEnableValidation.add(TestUtilityDataClassQantas.createEnableValidationRuleSettingQCC(userInfo.getprofileID()));
        insert lstEnableValidation;

		List<FeedItem> lstFeedItems = [select Id from FeedItem Limit 1 ];
        Id feedItemId = lstFeedItems[0].Id;
        
		Test.startTest();
        try {
            delete lstFeedItems;
        } catch (Exception ex) {}
		
		List<FeedItem> lstFIAftDel =  [select Id from FeedItem Where Id =: feedItemId ];
		system.assert(lstFIAftDel.size() == 1, 'FeedItem is not deleted');
		Test.stopTest();
    }
    
    static testMethod void testPreventChatterDeletionNegative() {
        EnableValidationRule__c enableVal = TestUtilityDataClassQantas.createEnableValidationRuleSettingQCC(userInfo.getUserID());
        enableVal.PreventChatterDeletion__c = false;
        insert enableVal;
        
        List<FeedItem> lstFeedItems = [select Id from FeedItem Limit 1 ];
        Id feedItemId = lstFeedItems[0].Id;
        
		Test.startTest();
        
        delete lstFeedItems;
        
		List<FeedItem> lstFIAftDel =  [select Id from FeedItem Where Id =: feedItemId ];
		system.assert(lstFIAftDel.size() == 0, 'FeedItem is deleted');
		Test.stopTest();
    }
}