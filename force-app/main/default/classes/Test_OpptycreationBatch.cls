/**
 * This class contains unit tests for validating the behavior of Opptycreation_Batch  calss
 * 
 */
@isTest
private class Test_OpptycreationBatch {

    //Added By Praveen For test class failure fix
    @testsetup
    static void createData(){
        List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
        lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Log Exception Logs Rec Type','Exception Logs'));
        insert lstConfigData;

        TestUtilityDataClassQantas.createQantasConfigDataAccRecType();
    }

    static testMethod void testBatchProcess() {
       
        TestUtilityDataClassQantas.enableTriggers();
        
        List<Opportunity_main__c> Opptymaintenance = new List<Opportunity_main__c>();
        
        Date myDate = Date.newInstance(2016, 9, 15);
        Date newDate = mydate.addDays(2);

        Opptymaintenance.add(new Opportunity_main__c(Name = 'Corporate Airfares', Active__c = TRUE, Category__c = 'Corporate Airfares', Create_an_Opportunity_before_in_months__c = 3, Close_Date__c = newDate));
        
        Opptymaintenance.add(new Opportunity_main__c(Name = 'Qantas Business Savings', Active__c = TRUE, Category__c = 'Qantas Business Savings', Create_an_Opportunity_before_in_months__c = 6, Close_Date__c = newDate));
        
        insert Opptymaintenance;
        
        List<Account> accList = new List<Account>();
        
        for(Integer i=0; i<4; i++){
        
                Account acc = (new Account(Name = 'Sample+i', Active__c = true, 
                                  RecordTypeId = TestUtilityDataClassQantas.customerRecordTypeId 
                                  ));
                                  
               accList.add(acc);
        }
         Trigger_Status__c cs = new Trigger_Status__c();
         cs.Name='AccountTeam';
         cs.Active__c = true;
            //cs.Other fiels values
         insert cs;
         insert accList;
         
         List<Opportunity> oppList = new List<Opportunity>();
         
         oppList.add(new Opportunity(Name = 'Opp1', AccountId = accList[0].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 500000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),StageName = 'Qualify',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
         //oppList.add(new Opportunity(Name = 'Opp2', AccountId = accList[1].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 600000, Category__c = 'Corporate Airfares', CloseDate = Date.Today(),StageName = 'Closed - Not Accepted',Closed_Reasons__c = 'Network',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));
         //oppList.add(new Opportunity(Name = 'Opp3', AccountId = accList[2].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 700000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Qualify',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Non-Contractual Other'));
         //oppList.add(new Opportunity(Name = 'Opp4', AccountId = accList[3].id, RecordTypeId = TestUtilityDataClassQantas.businessGovRTypeId,  Amount = 800000, Category__c = 'Qantas Business Savings', CloseDate = Date.Today(),StageName = 'Closed - Accepted',Closed_Reasons__c = 'Network',Sub_Type_Level_1__c = 'Currently In Contract', Sub_Type_Level_2__c = 'Renewal'));
         
         insert oppList;
         
             List<Proposal__c> propList = new List<Proposal__c>();
         
         propList.add(new Proposal__c(Name = 'Pro1', Account__c = oppList[0].AccountId, Opportunity__c = oppList[0].Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false));
         /*propList.add(new Proposal__c(Name = 'Pro2', Account__c = oppList[1].AccountId, Opportunity__c = oppList[1].Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 600000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false));
         propList.add(new Proposal__c(Name = 'Pro3', Account__c = oppList[2].AccountId, Opportunity__c = oppList[2].Id,
                                              Active__c = true, Type__c = 'Qantas Business Savings', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 700000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false));
         propList.add(new Proposal__c(Name = 'Pro4', Account__c = oppList[3].AccountId, Opportunity__c = oppList[3].Id,
                                              Active__c = true, Type__c = 'Qantas Business Savings', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No',
                                              MCA_Routes_Annual_Share__c = 70, Valid_From_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 800000, Qantas_Club_Discount__c = 'No', Status__c = 'Draft',
                                              NAM_Approved__c = false, NAM_Rejected__c = false, Pricing_Approved__c = false, Pricing_Rejected__c = false,
                                              DO_Approved__c = false, DO_Rejected__c = false, TeamLead_Approved__c = false, TeamLead_Rejected__c = false,
                                              Approval_Required_Others__c = false));*/
         insert propList;

           List<Contract__c> contrList = new List<Contract__c>();
           
           
           
             contrList.add(new Contract__c(Name = 'Con1' , Account__c = propList[0].Account__c, Opportunity__c = propList[0].Opportunity__c, Proposal__c = propList[0].Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 500000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'                                              
                                              ));
                                              
                /* contrList.add(new Contract__c(Name = 'Con1' , Account__c = propList[1].Account__c, Opportunity__c = propList[1].Opportunity__c, Proposal__c = propList[1].Id,
                                              Active__c = true, Type__c = 'Corporate Airfares', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 600000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'                                              
                                              ));
                                              
                 contrList.add(new Contract__c(Name = 'Con1' , Account__c = propList[2].Account__c, Opportunity__c = propList[2].Opportunity__c, Proposal__c = propList[2].Id,
                                              Active__c = true, Type__c = 'Qantas Business Savings', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                             Qantas_Annual_Expenditure__c = 700000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'                                              
                                              ));
                                              
                     contrList.add(new Contract__c(Name = 'Con1' , Account__c = propList[3].Account__c, Opportunity__c = propList[3].Opportunity__c, Proposal__c = propList[3].Id,
                                              Active__c = true, Type__c = 'Qantas Business Savings', International_Annual_Share__c = 70,
                                              Domestic_Annual_Share__c = 90, Frequent_Flyer_Status_Upgrade__c = 'No', Contracted__c = TRUE,
                                              MCA_Routes_Annual_Share__c = 70, Contract_Start_Date__c = Date.Today(), Contract_End_Date__c = Date.Today(),
                                              Qantas_Annual_Expenditure__c = 800000, Qantas_Club_Discount__c = 'No', Status__c = 'Signed by Customer'                                              
                                              ));*/
                                              
                                              insert contrList;
                                              
          Test.startTest();
            Opptycreation_Batch batchObj = new Opptycreation_Batch();
            Database.executeBatch(batchObj);
        Test.stopTest(); 
    }
    }