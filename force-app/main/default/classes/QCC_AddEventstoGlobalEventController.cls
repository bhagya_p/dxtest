public class QCC_AddEventstoGlobalEventController {
    
    @AuraEnabled
    public static DataInitWrapper getFieldSets(){
        
        DataInitWrapper dataInitWrapper = new DataInitWrapper();
        dataInitWrapper.listFieldSets = getFieldSetMember('Event__c','QCC_SearchableFields');
        dataInitWrapper.columnSets = getColumns('QCC_AddEventstoGlobalEvent');
        
        System.debug('==dataInitWrapper.listFieldSets==' + dataInitWrapper.listFieldSets);
        System.debug('==dataInitWrapper.columnSets==' + dataInitWrapper.columnSets);
        
        return dataInitWrapper;
    }
    
	@AuraEnabled
    public static List<Event__c> getDataEvents(String lstFieldSetMemberStr, String lstColumnsStr){
        try{
			
			List<FieldSetMemberWrapperClass> lstFieldSetMember = (List<FieldSetMemberWrapperClass>)JSON.deserialize(lstFieldSetMemberStr,List<FieldSetMemberWrapperClass>.class);
            List<DisplayColumn> lstColumns = (List<DisplayColumn>)JSON.deserialize(lstColumnsStr,List<DisplayColumn>.class);
            
            String sqlStr = 'SELECT Id ';
            for(displayColumn item : lstColumns){
                sqlStr += ',' + item.fieldName;
            }
            sqlStr += ' FROM Event__c ';
            String sqlConditions = '';
            for(Integer i = 0; i < lstFieldSetMember.size(); i++){
                FieldSetMemberWrapperClass item = lstFieldSetMember[i];
                if(String.isNotBlank(item.fieldValue)){
                    if(item.fieldType.toUpperCase() == 'DATETIME'){
                        if(item.isFromDateTime){
                            DateTime gmtDateTimeFrom = parseStringToGMT(item.fieldValue);
                            sqlConditions += ' AND ' + item.fieldAPIName + ' >= :gmtDateTimeFrom';
                        }
                        if(item.isToDateTime){
                            DateTime gmtDateTimeTo = parseStringToGMT(item.fieldValue);
                            sqlConditions += ' AND ' + item.fieldAPIName + ' <= :gmtDateTimeTo';
                        }
                    } 
                    else if(item.fieldType.toUpperCase() == 'DATE'){
                        if(item.isFromDateTime){
                            sqlConditions += ' AND ' + item.fieldAPIName + ' >= ' + item.fieldValue;
                        }
                        if(item.isToDateTime){
                            sqlConditions += ' AND ' + item.fieldAPIName + ' <= ' + + item.fieldValue;
                        }
                    }
                    else {
                        sqlConditions += ' AND ' + item.fieldAPIName + ' = ' + '\'' + item.fieldValue + '\' ';
                    }
                }
            }
            //adding Not Closed Condition + not mapping to Event Condition
            sqlConditions += ' AND Status__c != \'Closed\'';
            sqlConditions += ' AND GlobalEvent__c = null';
            if(String.isNotBlank(sqlConditions)){
                sqlConditions = sqlConditions.replaceFirst('AND', 'WHERE');
            	sqlStr += sqlConditions;
            }
            System.debug('==sqlStr==' + sqlStr);
            List<Event__c> lstGlobalEvent = Database.query(sqlStr);
            return lstGlobalEvent;
        }catch(exception ex){
            System.debug(ex.getMessage() + '-'+ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage() + '-'+ex.getStackTraceString());
        }
    }
    
    @AuraEnabled
    public static void addEventsToGlobalEvent(List<String> lstEventId, String globalEventId){
        try{
			
			List<Event__c> lstEvent = [
                SELECT Id
                	,GlobalEvent__c
                FROM Event__c
                WHERE Id IN :lstEventId
            ];
            
            for(Event__c item : lstEvent){
                item.GlobalEvent__c = globalEventId;
            }
            
            // Update Global Event for list Event
            if(!lstEvent.isEmpty()){
                update lstEvent;
            }
        } catch(exception ex) {
            System.debug(ex.getMessage() + '-'+ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage() + '-'+ex.getStackTraceString());
        }
        
    }
    
    // 2018-10-25T04:54:00.000Z
    @TestVisible
    private static DateTime parseStringToGMT(String dateTimeStr){
        try{
            dateTimeStr = dateTimeStr.left(19);
            Integer year = integer.valueOf((dateTimeStr.split('T')[0]).split('-')[0]);
            Integer month = integer.valueOf((dateTimeStr.split('T')[0]).split('-')[1]);
            Integer day = integer.valueOf((dateTimeStr.split('T')[0]).split('-')[2]);
            Integer hour = integer.valueOf((dateTimeStr.split('T')[1]).split(':')[0]);
            Integer minute = integer.valueOf((dateTimeStr.split('T')[1]).split(':')[1]);
            Integer second = integer.valueOf((dateTimeStr.split('T')[1]).split(':')[2]);
            DateTime output = DateTime.newInstanceGmt(year, month, day, hour, minute, second);
            return output;
        }catch(exception ex){
            System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
        }
        return null;
    }
    
    public static List<FieldSetMemberWrapperClass> getFieldSetMember(String objectName , String fieldSetName){
        
        List<FieldSetMemberWrapperClass> wrapperList = new List<FieldSetMemberWrapperClass>();
        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(objectName);
        try{
            for(Schema.FieldSetMember fieldMember : sObj.getDescribe().fieldSets.getMap().get(fieldSetName).getFields()){
                Boolean isDateField = false;
                FieldSetMemberWrapperClass wrapper = new FieldSetMemberWrapperClass();
                wrapper.fieldType = String.valueOf(fieldMember.getType());
                wrapper.fieldLabel = fieldMember.getLabel();
                wrapper.fieldAPIName = fieldMember.getFieldPath();
                // change date field to datetime field
                String currFieldType = String.valueOf(fieldMember.getType());
                
                if(currFieldType == 'date' || currFieldType == 'datetime'){
                    isDateField = true;
                    wrapper.fieldLabel = fieldMember.getLabel() + ' From';
                    wrapper.isFromDateTime = true;
                }
                wrapperList.add(wrapper);
                // add datetime To
                if(isDateField){
                    FieldSetMemberWrapperClass wrapperTo = new FieldSetMemberWrapperClass();
                    wrapperTo.fieldType = currFieldType;
                    wrapperTo.fieldAPIName = fieldMember.getFieldPath();
                	wrapperTo.fieldLabel = fieldMember.getLabel() + ' To';
                    wrapperTo.isToDateTime = true;
                    wrapperList.add(wrapperTo);
                }
            }
            System.debug('#### wrapperList ' + wrapperList);
        }catch(exception ex){
            System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
        }
        return wrapperList;
    }
    
    public class DataInitWrapper {
        @AuraEnabled
        public String columnSets {get;set;}
        @AuraEnabled
        public List<FieldSetMemberWrapperClass> listFieldSets {get;set;}
    }
    
	public class FieldSetMemberWrapperClass {
        @AuraEnabled
        public String fieldType                          {get;set;}
        @AuraEnabled
        public String fieldLabel                         {get;set;}
        @AuraEnabled
        public String fieldAPIName                       {get;set;}
        @AuraEnabled
        public String fieldValue                         {get;set;}
        @AuraEnabled
        public Boolean isFromDateTime {get; set;} // use when field type is datetime
        @AuraEnabled
        public Boolean isToDateTime {get; set;} // use when field type is datetime
        
        public FieldSetMemberWrapperClass(){
            this.fieldType = '';
            this.fieldLabel = '';
            this.fieldAPIName = '';
            this.fieldValue = '';
            this.isFromDateTime = false;
            this.isToDateTime = false;
        }
    }
    
    public static String getColumns(String componentName) {
        try{
            List<displayColumn> listColumn = new List<displayColumn>();
            for(QCC_Data_Table__mdt dataTable: [SELECT ComponentName__c
                                                	,Order__c
                                                	,FieldAPI__c
                                                	,FieldLabel__c
                                                	,Sortable__c
                                                	,Type__c 
                                                FROM QCC_Data_Table__mdt
                                                WHERE ComponentName__c  = :componentName 
                                                Order By Order__c]) {
    			String fieldLabel = dataTable.FieldLabel__c;
                String fieldAPI = dataTable.FieldAPI__c;
                String fieldDataType = dataTable.Type__c;
                Boolean sortable = dataTable.Sortable__c;
                TypeAttributes typeAttribute = (TypeAttributes)System.JSON.deserialize('{ "label": { "fieldName": "'+fieldAPI+'" }, "variant": "base", "title": { "fieldName": "'+fieldAPI+'"}}', TypeAttributes.Class);
    			
    			listColumn.add(new displayColumn(fieldLabel, fieldAPI, fieldDataType, sortable, typeAttribute));
            }
            return JSON.serialize(listColumn).replaceALL('type_x', 'type');
        }catch(exception ex){
            System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
        }
        return '';
    }
    
    // Wrapper class to hold the structure of columns attribute of lightning:datatable tag
    public class DisplayColumn {
        public String label{get;set;}
        public String fieldName{get;set;}
        public String type_x{get;set;}
        public Boolean sortable;
        public TypeAttributes typeAttributes;

        public DisplayColumn (String label, String fieldName, String inputType, Boolean sortable,TypeAttributes typeAttributes){
            this.label = label;
            this.fieldName = fieldName;
            this.type_x = inputType;
            this.sortable = sortable;
            this.typeAttributes = typeAttributes;
        }
    }
    
    public class TypeAttributes {
        public Label label;
        public String variant;
        public String year;
        public String month;
        public String day;
        public String hour;
        public String minute;
        public Label title;
    }
    
    public class Label {
        public String fieldName;
    }
    
    
}