/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Class to be invoked from Recovery Trigger
Inputs:
Test Class:    
************************************************************************************************
History
************************************************************************************************
27-Nov-2017    Praveen Sampath                Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public class LoyaltyAPIHelper { 
    /*--------------------------------------------------------------------------------------      
     Method Name:        invokeLoyaltyAPI
     Description:        Method to Invoke Loyalty API Service
     Parameter:          FF Number, Points,Last Name,Case Number, Recovery Id
    --------------------------------------------------------------------------------------*/    
    @future(Callout = true)
    public static void invokeLoyaltyAPI(String ffNumber, Integer pointsAdded, String lastName, String caseNumber, String recoveryId){
        try{
            Recovery__c objRecovery = [Select Id, Name, Status__c , FulfilledBy__c, 
                                       Fulfilled_By_profile__c , Recovery_Locked__c, API_Attempts__c, Case_Number__c
                                       from Recovery__c where RecordType.DeveloperName='Customer_Connect' AND Id =: recoveryId];

            LoyaltyRequestWrapper loyaltyReq = new LoyaltyRequestWrapper();
            String todayString = String.valueOf(system.today());
            List<String> lsttoday = todayString.split('-');
            String activityDate = lsttoday[2]+''+lsttoday[1]+''+lsttoday[0];
            loyaltyReq.activityDate = activityDate;
            loyaltyReq.validationLevel = CustomSettingsUtilities.getConfigDataMap('LoyaltyAPIValidationLevel');
            loyaltyReq.partnerID = CustomSettingsUtilities.getConfigDataMap('LoyaltyAPIPartnerID');
            
            LoyaltyRequestWrapper.Member member = new LoyaltyRequestWrapper.Member();
            member.surname = lastName;
            
            LoyaltyRequestWrapper.Payment payment = new LoyaltyRequestWrapper.Payment();
            
            payment.transactionReference = objRecovery.Name;
            payment.basePointsSign = '+';
            payment.numberOfBasePoints = pointsAdded;
            payment.statementText='COMPLIMENTS OF CUSTOMER CONTACT';
            
            loyaltyReq.payment = payment;
            loyaltyReq.member = member;
            Boolean isSuccess = false;
            String additionalComment;
            String comments;

            if(ffNumber != null) {
                isSuccess = QCC_InvokeCAPAPI.invokeLoyaltyPostAPI(ffNumber,loyaltyReq);
            } else {
                additionalComment = 'Frequent Flyer Number not available';
            }

            String userProfile = [SELECT Name FROM Profile where id = :Userinfo.getProfileId()].Name;
            
            if(isSuccess){
                objRecovery.Status__c = CustomSettingsUtilities.getConfigDataMap('RecoveryStatusFinalised');
                objRecovery.FulfilledBy__c = UserInfo.getUserId();
                objRecovery.Fulfilled_By_profile__c = userProfile;
                objRecovery.Fulfilled_Date__c = System.now();
                comments = 'Loyalty API call successful after Attempt #'+ (Integer.valueOf(objRecovery.API_Attempts__c) + 1);
            }else{
                if(objRecovery.Status__c != CustomSettingsUtilities.getConfigDataMap('RecoveryStatusFinalised')){
                    objRecovery.Status__c = CustomSettingsUtilities.getConfigDataMap('RecoveryStatusFinalisation Declined');
                    objRecovery.API_Attempts__c = objRecovery.API_Attempts__c == null ? 0 : objRecovery.API_Attempts__c;
                    objRecovery.API_Attempts__c++;
                    //objRecovery.Status__c = CustomSettingsUtilities.getConfigDataMap('SubmitForFinalisation');
                    comments = additionalComment == null ? 'Loyalty API call unsuccessful Attempt #'+ Integer.valueOf(objRecovery.API_Attempts__c) : 'Loyalty API call unsuccessful Attempt #'+ Integer.valueOf(objRecovery.API_Attempts__c) + '\n' + additionalComment;
                }
                //'Finalisation Declined';//QantasConfigData - RecoveryStatusFinalisation Declined
            }
            if(!Test.isRunningTest()) {
                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, objRecovery.Case_Number__c, ConnectApi.FeedElementType.FeedItem, comments);
            }
            update objRecovery;
        }
        catch(Exception ex){
            system.debug('ex##########'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Invoked from RecoveryTriggerHelper', 'LoyaltyAPIHelper', 
                                    'invokeLoyaltyAPI', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }
}