/**
 * This class contains unit tests for validating the behavior of VariationTriggerHelper class.
 */
@isTest
public class VariationTriggerHelperTest {
    public static testMethod void createVariationTest() {
        Id LoyaltyRecordTypeId1= GenericUtils.getObjectRecordTypeId('Variation__c', 'Amendment Only');
        Id LoyaltyRecordTypeId2= GenericUtils.getObjectRecordTypeId('Variation__c', 'Extension');
        Id LoyaltyRecordTypeIdOpp= GenericUtils.getObjectRecordTypeId('Opportunity', 'Loyalty Commercial');
        Id LoyaltyRecordTypeIdContract= GenericUtils.getObjectRecordTypeId('Contract__C', 'Loyalty Commercial');
        List<variation__c> variations = new List<variation__c>();
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.ABN_Tax_Reference__c ='1234';
        insert acc;
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = 'Prospect';
        opp.CloseDate = system.today();
        opp.RecordTypeId =LoyaltyRecordTypeIdOpp;
        opp.Amount = 500000;
        opp.Price_Per_Point__c = 0.0180;
        insert opp;
        Contract__c contract =new Contract__c();
        contract.Name = 'TestContract';
        contract.Account__c = acc.Id;
        contract.Opportunity__c =opp.Id;
        contract.RecordTypeId = LoyaltyRecordTypeIdContract;
        insert contract;
        Variation__c variation = new Variation__c();
        variation.Contract_Agreement__c = contract.Id;
        variation.Contract_Variation_Reason__c = 'Extension';
        variation.Active__c = true;
        variation.RecordTypeId=LoyaltyRecordTypeId2;
        variation.Contract_End_Date__c = System.today();
        variations.add(variation);
        Boolean failedAtStatusDraft=false;
        try {        
        	insert variations;
        } catch(Exception e){
            failedAtStatusDraft=true;
        }
        System.assertEquals(true, failedAtStatusDraft, 'Should not able to create Variation for Contract & Agreement at Draft Stage');
        contract.Status__c='Signed by Customer';
    }
}