/***********************************************************************************************************************************

Description: Test Class for Apex class for retrieving the account Information using IATA Code. 

History:
======================================================================================================================
Name                          Description                                       	Tag
======================================================================================================================
Yuvaraj MV         			Account record retrieval class                     		T01

Created Date    : 			23/06/18 (DD/MM/YYYY)

TestClass		:			QAC_AcctRetrievalUsingIATA_Test
Main Class Name	: 			QAC_AcctRetrievalUsingIATA
**********************************************************************************************************************************/
@isTest
public class QAC_AcctRetrievalUsingIATA_Test 
{
    
    @testsetup
    public static void dataSetup()
    {
        String agencyAcctRtId 	= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
        
        QantasConfigData__c qcd = new QantasConfigData__c(Name = 'Log Exception Logs Rec Type',Config_Value__c = 'Exception Logs');
        insert qcd;
        
        Account agencyAccount1 	= new Account(Name = 'Test Account111', Active__c = true, Aquire__c = true, Type = 'Agency Account', 
                                              RecordTypeId = agencyAcctRtId, Migrated__c = true, ABN_Tax_Reference__c  ='98987566453',
                                              Estimated_Total_Air_Travel_Spend__c = 0,IATA_Number__c = '9999999',Phone_Country_Code__c = '+61',
                                              Phone_Area__c = '02', Phone_Number__c = '469017222');
        insert agencyAccount1;
    }
    
    /* All Exception handled in this excepTestJson Method */
    public static testMethod void validIATA()
    {
        //IATA is Valid
        QAC_AcctRetrievalUsingIATA_Test.requestMethod('9999999');
        Test.startTest();
        QAC_AcctRetrievalUsingIATA.doAcctRetrieval();
        Test.stopTest();
    }
    
    public static testMethod void emptyIATA()
    {
        //IATA is empty
        QAC_AcctRetrievalUsingIATA_Test.requestMethod('');
        Test.startTest();
        QAC_AcctRetrievalUsingIATA.doAcctRetrieval();
        Test.stopTest();
    }
    
    public static testMethod void invalidIATA()
    {
        //IATA doesn't exists
        QAC_AcctRetrievalUsingIATA_Test.requestMethod('99999');
        Test.startTest();
        QAC_AcctRetrievalUsingIATA.doAcctRetrieval();
        Test.stopTest();
    }
    
    
    @isTest
    public static void testwithExcepJSON()
    {
        try{
            system.debug('Inside JSON Excep');
            
            Test.startTest();
            QAC_AcctRetrievalUsingIATA.doAcctRetrieval();
            Test.stopTest();
        }
        catch(Exception myException){
            system.debug('inside exception test'+myException.getTypeName());
        }
    }
    
    public static void requestMethod(String agencyCode)
    {
        system.debug('@@@@insideRequest method');
        RestRequest req = new RestRequest();
        RestResponse res =  new RestResponse();
        
        req.requestURI = '/services/apexrest/QACAcctInformationRetrieval/'+agencyCode;
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    }
   }