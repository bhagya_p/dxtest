/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Wrapper for getting CAP System Token
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
31-Oct-2017      Praveen Sampath               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public class QCC_CAPTokenWrapper {
    public String token_type;   
    public Integer expires_in;  
    public String access_token; 
    
}