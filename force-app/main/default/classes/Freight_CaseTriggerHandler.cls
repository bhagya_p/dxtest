/*----------------------------------------------------------------------------------------------------------------------
Author:        Abhijeet P
Company:       TCS
Description:   Class to be invoked from Case Trigger, all Logics has been restricted for Freight RecordType
Test Class:    FreightAllTileControllerTest 
************************************************************************************************
History
************************************************************************************************
10-Aug-2017    Abhijeet P              Initial Design

-----------------------------------------------------------------------------------------------------------------------*/

public class Freight_CaseTriggerHandler {

    // To retrieve RecordtypeId of Freight Cases
    Freight_TriggerRecordTypeUtility recordTypeUtility = new Freight_TriggerRecordTypeUtility();
  map<string,string> mapRecordTypeVsCategory = recordTypeUtility.returnRecordTypeSettingMap('Freight', 'Case');    



  /*--------------------------------------------------------------------------------------      
    Method Name:        assignEntitlement for Service Request
    Description:        Method for creation or assignment Entitlement for Account
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/
    public void assignEntitlement1(list<Case> caseNewList, map<Id, Case> caseOldMap)
    {
        system.debug('inside entitlement process');
        // set of accountIds to determing Entitlement Assignment for Account
        Set<Id> accountIds = new Set<Id>();
        map<Id,Account> accountMap = new Map<Id,Account>();
        List<Entitlement> entitlementsToInsert = new List<Entitlement>();
        Map<Id, Entitlement> accEntitlementMap = new Map<Id,Entitlement>();
         Id caseRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();        
        
        // Iterating Trigger.New to retieve distinct AccountIds and distinct Port Ids
        for (Case eachCase : caseNewList) {
            if(eachCase.RecordTypeId == caseRtId && eachCase.AccountId != null){
                accountIds.add(eachCase.AccountId);
                system.debug('acc ids**'+accountIds);
            }
        }
        
        if(!accountIds.isEmpty()) 
        {
            for (Entitlement ent : [SELECT AccountId,Name FROM Entitlement
                                                  WHERE AccountId in :accountIds]) 
            {
                accEntitlementMap.put(ent.AccountId, ent);
                system.debug('ent ids**'+accEntitlementMap);
            }
      
            string slaLabel = Label.Qantas_Agency_Connect_Entitlement_Process;
            // Querying Entitlemnet Process
            SlaProcess process = [SELECT Id,Name FROM SlaProcess WHERE Name =: slaLabel]; 
            Id accountRtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency Account').getRecordTypeId();
    
            for(Account acc : [Select Id, Name, BillingCountry, BillingCountryCode,RecordTypeId from Account where Id in: accountIds]){
                if(acc != null && acc.BillingCountryCode == 'AU' && acc.RecordTypeId==accountRtId)
                {
                    if(!accEntitlementMap.containsKey(acc.Id))
                    {
                        Entitlement myEnt = new Entitlement();
                        myEnt.Name = acc.Name + ' - Entitlement';   
                        myEnt.SlaProcessId   = process.Id;
                        myEnt.StartDate     = Date.today().addDays(-1);
                        myEnt.Enddate     = Date.newInstance(2099, 12, 31);
                        myEnt.AccountId     = acc.id;
                        entitlementsToInsert.add(myEnt);
                    }
                }
                accountMap.put(acc.Id, acc);
                system.debug('acc map'+accountMap);
            }
            
            if (!entitlementsToInsert.isEmpty()) {
                insert entitlementsToInsert;
            
                for(Entitlement ent : entitlementsToInsert) {
                    accEntitlementMap.put(ent.AccountId, ent);
                }
                    
                system.debug('ent ids**'+accEntitlementMap);
            }
                
            // Assignement of ENtitlement to Case to associate required Milestone with Case
            for(Case eachCase : caseNewList) 
            {
                if(eachCase.RecordTypeId == caseRtId )
                {
                    if(eachCase.AccountId != null && (accountMap != null && !accountMap.isEmpty() && accountMap.get(eachCase.AccountId).BillingCountryCode == 'AU'&& accountMap.get(eachCase.AccountId).RecordTypeId == accountRtId )) 
                    {
                        eachCase.EntitlementId = accEntitlementMap.get(eachCase.AccountId).Id;
                        system.debug('case id**'+eachCase);
                    }
                }  
            }
            
        }
    }
  
   /*--------------------------------------------------------------------------------------      
    Method Name:        assignEntitlement
    Description:        Method for creation or assignment Entitlement for Account
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/
    public void assignEntitlement(list<Case> caseNewList, map<Id, Case> caseOldMap){
        
    // set of accountIds to determing Entitlement Assignment for Account
    Set<Id> accountIds     = new Set<Id>();
        
        // set of Port Ids to determing Entitlement Assignment for Account
    Set<Id> distinctPortIds = new Set<Id>();
    
        // Iterating Trigger.New to retieve distinct AccountIds and distinct Port Ids
    for (Case eachCase : caseNewList) {
      if(mapRecordTypeVsCategory.keySet().contains(eachCase.RecordTypeId)){
        accountIds.add(eachCase.AccountId);
                distinctPortIds.add(eachCase.Freight_Port__c);
            }
    }
    
        // Removing null values from sets, in case relevant fields are not populated for Case
    accountIds.remove(null);
        distinctPortIds.remove(null);
    
        // To Identify whether Freight Account is associated with any Entitlement or not
        // If Entitlement exist for Account, relevant entitlement will be associated with Account
        // Else respective Entitlement record created for Account
    if (!accountIds.isEmpty()) {
      Map<Id, Entitlement> entitlementMap = new Map<Id, Entitlement>();

      for (Entitlement eachEntitlement : [SELECT AccountId,Name FROM Entitlement
                  WHERE AccountId in :accountIds]) {
        entitlementMap.put(eachEntitlement.AccountId, eachEntitlement);
      }
      
            string slaLabel = Label.Freight_SLA_Process;
            // Querying Entitlemnet Process
      SlaProcess process = [SELECT Name FROM SlaProcess
                    WHERE Name =: slaLabel]; 
      
      List<Entitlement> entitlementsToInsert = new List<Entitlement>();
            
      for (Account eachAccount : [SELECT Id, Name FROM Account WHERE Id IN: accountIds]) {

                if (entitlementMap.containsKey(eachAccount.Id)) {
          continue;
        }
        
                // New Entitlement records creation data
                entitlementsToInsert.add(new Entitlement(
          // Entitlement is based on Account Name + Entitlement
          Name       = eachAccount.Name + ' - Entitlement',        
          SlaProcessId   = process.Id,
          StartDate     = Date.today().addDays(-1),   
          Enddate     = Date.newInstance(2099, 12, 31),   
          AccountId     = eachAccount.id
        ));
      }
      
            // Insertion of required Entitlements
      if (!entitlementsToInsert.isEmpty()) {
        insert entitlementsToInsert;
        
        for (Entitlement eachEntitlement : entitlementsToInsert) {
          entitlementMap.put(eachEntitlement.AccountId, eachEntitlement);
        }
      }
            
            // To hold PortId vs State code map
            map<Id, string> portIdVsStateCodeMap = new map<Id, string>();
            
            // Retrieving States for relevant Stations or Port Code
            for(Airport_Code__c eachCode : [SELECT Id, Name, Freight_Station_Name__c, Freight_Airport_State_Code__c
                                              FROM Airport_Code__c
                                               WHERE Id IN: distinctPortIds ]){
                portIdVsStateCodeMap.put(eachCode.Id, eachCode.Freight_Airport_State_Code__c);                                  
            }
            
      // Assignement of ENtitlement to Case to associate required Milestone with Case
      for (Case eachCase : caseNewList) {
        if(mapRecordTypeVsCategory.keySet().contains(eachCase.RecordTypeId)){
          
                    // Assigning Entitlment to the Case
                    if (eachCase.AccountId == null) {
            continue;
          }
          eachCase.EntitlementId = entitlementMap.get(eachCase.AccountId).Id;
                    
                    // Assigning State Code to Case based on Port selected
                    if(eachCase.Freight_Port__c != null){
                        if(portIdVsStateCodeMap.get(eachCase.Freight_Port__c) != null)
                        eachCase.Freight_Port_State_Code__c = portIdVsStateCodeMap.get(eachCase.Freight_Port__c);
                    }
                    else{
                        eachCase.Freight_Port_State_Code__c = '';
                    }
        }  
      }
    }
    }
  
  
  
  /*--------------------------------------------------------------------------------------      
    Method Name:        autoChangeStatus
    Description:        Auto Changing status of Case while updation of Case
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/  
  public void autoChangeStatus(list<Case> caseNewList, map<Id, Case> caseOldMap){

    for (Case eachCase : caseNewList) {
      // Freight Recordtype verification 
            if(mapRecordTypeVsCategory.keySet().contains(eachCase.RecordTypeId)){
        
                // to auto change status of Cases based on auto Assignment
        if(eachCase.Status == 'Open' ){
                    if(caseOldMap.get(eachCase.Id) != null ){
                        if(eachCase.OwnerId != caseOldMap.get(eachCase.Id).OwnerId){
              eachCase.Status = 'Assigned';
            }
                    }  
        }
                
                // Updation Reopening Time stamp based on Case Status change
                if(eachCase.Status == 'Reopened' && caseOldMap.get(eachCase.Id).Status != 'Reopened'){
                  eachCase.Reopened_Date__c = system.now();
                }
      }
    }
  }
  
  
   /*--------------------------------------------------------------------------------------      
    Method Name:        autoChangeStatus
    Description:        Auto Changing status of Case while updation of Case
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/  
  public void autoChangeStatus1(list<Case> caseNewList, map<Id, Case> caseOldMap){
   
    Id caseRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
  
    for (Case eachCase : caseNewList) {
      // service request Recordtype verification 
            if(eachCase.RecordTypeId == caseRtId){
                // Updation Reopening Time stamp based on Case Status change
                if(eachCase.Status == 'Reopened' && caseOldMap.get(eachCase.Id).Status != 'Reopened'){
                  eachCase.Reopened_Date__c = system.now();
                }
      }
    }
  }
  
    /*--------------------------------------------------------------------------------------      
    Method Name:        assignRecordTypeWebToCase
    Description:        Update Recordtypes coming 
    Parameter:          Case New List
    --------------------------------------------------------------------------------------*/  
  public void assignRecordTypeWebToCase(list<Case> caseNewList){
    
        for(Case eachCase : caseNewList){
            if(eachCase.Freight_WebToCaseType__c != '' && eachCase.Origin == 'Web'){
                
                if(eachCase.Freight_WebToCaseType__c == 'Freight - General Enquiries'){  
                    eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Freight Query').getRecordTypeId();
                  eachCase.Priority = 'Low';
                  eachCase.Status = 'Open';
                }
                else if(eachCase.Freight_WebToCaseType__c == 'Freight - Track and trace query'){  
                    eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Freight Shipment').getRecordTypeId();
                  eachCase.Priority = 'Low';
                  eachCase.Status = 'Open';
                }
                else if(eachCase.Freight_WebToCaseType__c == 'Freight - Give us feedback'){
                    eachCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Freight Feedback').getRecordTypeId();
                  eachCase.Priority = 'Low';
                  eachCase.Status = 'Open';
                }
            }
            
        }
        
    }
    
    
  /*--------------------------------------------------------------------------------------      
    Method Name:        autoMilestonCompletion
    Description:        Auto completion of Milestoins based on status change of Case
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/  
  public void autoMilestonCompletion(list<Case> caseNewList, map<Id, Case> caseOldMap){
    
    // Record Type Filtering Logic to be introduced
    
    DateTime completionDate     = System.now();
    set<Id> ownerAssignIdSet     = new set<Id>();
    set<Id> caseClosureIdSet    = new set<Id>();
    set<Id> caseResolutionIdSet    = new set<Id>();
        set<Id> caseReopenIdSet      = new set<Id>();
    
    for (Case eachCase : caseNewList) {
    
            // Freight Recordtype verification 
            if(mapRecordTypeVsCategory.keySet().contains(eachCase.RecordTypeId)){
        system.debug(' @@ Status New @@ ' + eachCase.Status);
                system.debug(' @@ Status Old @@ ' + caseOldMap.get(eachCase.Id).Status);
                if(eachCase.Status == 'Assigned' && caseOldMap.get(eachCase.Id).Status != 'Assigned'){
                    ownerAssignIdSet.add(eachCase.Id);
                }
                else if(eachCase.Status == 'Closed' && caseOldMap.get(eachCase.Id).Status != 'Closed'){
                    caseClosureIdSet.add(eachCase.Id);
                }
                else if(eachCase.Status == 'Resolved' && caseOldMap.get(eachCase.Id).Status != 'Resolved'){
                    caseResolutionIdSet.add(eachCase.Id);
                }
                
                else if(eachCase.Status == 'Reopened' && caseOldMap.get(eachCase.Id).Status != 'Reopened'){
                    caseReopenIdSet.add(eachCase.Id);
                }
            }    
    }
    
    if(ownerAssignIdSet.size() > 0){
      Freight_MilestoneUtils.completeMilestone(ownerAssignIdSet, 'Ownership Assignment', completionDate);
    }
    
    if(caseClosureIdSet.size() > 0){
      Freight_MilestoneUtils.completeMilestone(caseClosureIdSet, 'Case Closure', completionDate);
    }
    
    if(caseResolutionIdSet.size() > 0){
      Freight_MilestoneUtils.completeMilestone(caseResolutionIdSet, 'Case Resolution', completionDate);
    }
        
        if(caseReopenIdSet.size() > 0){
            completionDate   = null;
      Freight_MilestoneUtils.completeMilestone(caseReopenIdSet, 'Case Resolution', completionDate);
    }
        
  }
  
  /*--------------------------------------------------------------------------------------      
    Method Name:        autoMilestonCompletion
    Description:        Auto completion of Milestoins based on status change of Case
    Parameter:          Case New List & Old Map 
    --------------------------------------------------------------------------------------*/  
  public void autoMilestonCompletionforservicerequest(list<Case> caseNewList, map<Id, Case> caseOldMap){
    
    // Record Type Filtering Logic to be introduced
    
    DateTime completionDate     = System.now();
    set<Id> ownerAssignIdSet     = new set<Id>();
    set<Id> caseClosureIdSet    = new set<Id>();
    set<Id> caseReopenIdSet      = new set<Id>();
    set<Id> caseClosureBothIdSet = new set<Id>();
  Id caseRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
    
    for (Case eachCase : caseNewList) {
    
            // Service RequestRecordtype verification 
            //if(mapRecordTypeVsCategory.keySet().contains(eachCase.RecordTypeId)){
        system.debug(' @@ Status New @@ ' + eachCase.Status);
                system.debug(' @@ Status Old @@ ' + caseOldMap.get(eachCase.Id).Status);
                if(eachCase.Status == 'In Progress' && caseOldMap.get(eachCase.Id).Status != 'In Progress' && eachCase.RecordTypeId ==caseRtId){
                    ownerAssignIdSet.add(eachCase.Id);
                    
                }
                else if(eachCase.Status == 'Closed - Approved' && caseOldMap.get(eachCase.Id).Status != 'Closed - Approved' && eachCase.RecordTypeId ==caseRtId){
                    caseClosureIdSet.add(eachCase.Id);
                }
  
                else if(eachCase.Status == 'Closed - Rejected' && caseOldMap.get(eachCase.Id).Status != 'Closed - Rejected' && eachCase.RecordTypeId ==caseRtId){
                    caseClosureIdSet.add(eachCase.Id);
                }
                else if(eachCase.Status == 'Closed' && caseOldMap.get(eachCase.Id).Status != 'Closed' && eachCase.RecordTypeId ==caseRtId){
                    caseClosureIdSet.add(eachCase.Id);
                }
                
               else if(eachCase.Status == 'Auto Closed' && caseOldMap.get(eachCase.Id).Status != 'Auto Closed' && eachCase.RecordTypeId ==caseRtId){
                    //ownerAssignIdSet.add(eachCase.Id);
                    caseClosureBothIdSet.add(eachCase.Id);
                    
                }    
                      
                else if(eachCase.Status == 'Reopened' && caseOldMap.get(eachCase.Id).Status != 'Reopened'&& eachCase.RecordTypeId ==caseRtId){
                    caseReopenIdSet.add(eachCase.Id);
                }
                
                
           // }    
    }
    
    
    
    
    if(ownerAssignIdSet.size() > 0){
      Freight_MilestoneUtils.completeMilestone(ownerAssignIdSet, 'Ownership Assignment', completionDate);
    }
    
    if(caseClosureIdSet.size() > 0){
      Freight_MilestoneUtils.completeMilestone(caseClosureIdSet, 'Case Closure', completionDate);
    }
    
    if(caseClosureBothIdSet.size() > 0){
      Freight_MilestoneUtils.completeMilestoneAutoClosed(caseClosureBothIdSet,  completionDate);
    }
    
    
     if(caseReopenIdSet.size() > 0){
            completionDate = null;
        system.debug('reopen size' + caseReopenIdSet.size());
        system.debug('Print reopen caseids'+caseReopenIdSet);
       Freight_MilestoneUtils.completeMilestoneForReopenCase(caseReopenIdSet, 'Case Closure',completionDate);
    }
    
   
        
  }
  

  
}