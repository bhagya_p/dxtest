public class QCC_SignOutController {
	@AuraEnabled
    public static String getBasicInfo(){
        string urler = URL.getSalesforceBaseUrl().toExternalForm();
        User u = [Select id, Name, City,location__c, profileId, profile.Name from User Where id=:UserInfo.getUserId()];
        
        infoWrapper iw = new infoWrapper();
        iw.baseURL = urler;
        iw.currentUser = u;
        
        return (JSON.serialize(iw));
    }
    
    @AuraEnabled
    public static void caseAssignedBack(){
        Try{
            caseQueueInfo();
            caseHotWords();
            User u = [Select id, Name, City, location__c, profileId, profile.Name from User Where id=:UserInfo.getUserId()];

            if( (u.location__c == null || (u.location__c != null && !u.location__c.equalsIgnoreCase('Sydney'))) 
                    &&  u.profile.Name.equalsIgnoreCase('Qantas CC Consultant'))
            {
                List<Case> listCase = [Select id, OwnerId, Last_Queue_Owner__c,
                                       Returned_to_Queue__c,Assign_using_active_assignment_rules__c , 
                                       QCC_enableAssignmentRules__c, RecordTypeId, Description, QCC_Combined_Case_Description__c,
                                       Origin, Override_Hotword__c, Status, Category__c, Sub_Category__c, Type, Group__c, Is_Response_Requested__c
                                       FROM Case 
                                       WHERE OwnerId=:UserInfo.getUserId() AND IsClosed = false];
                if(listCase.size() == 0){
                    return;
                }
                        
                List<Case> blankLastQueue = new List<Case>();        
                        
                        
                for(integer i= 0; i < listCase.size(); i++){
                    Case c = listCase[i];
                    if(!String.isBlank(c.Last_Queue_Owner__c)){

                        String queueName = mapQueueName.get(c.Last_Queue_Owner__c);
                        if(queueName.endsWithIgnoreCase('SLA') || queueName.endsWithIgnoreCase('Low') 
                            || queueName.endsWithIgnoreCase('Medium')){
                            queueName = queueName.removeEndIgnoreCase('SLA').removeEndIgnoreCase('Low').removeEndIgnoreCase('Medium') + 'High';
                        }
                        c.OwnerId = mapQueue.get(queueName);

                        //the flag only turn on if the Case is assigned back to the Last Queue
                        c.Returned_to_Queue__c = true;
                    }else{
                        c.QCC_enableAssignmentRules__c = false;
                        c.Assign_using_active_assignment_rules__c = true;

                        blankLastQueue.add(c);
                        listCase.remove(i);
                        i--;
                    }
                }

                listCase.addAll(assignCaseOwner(blankLastQueue));
                
                Database.SaveResult[] srList = Database.update(listCase, false);

                for(Database.SaveResult sr : srList){

                    if(sr.isSuccess()){
                        //
                    }else{
                        
                    }
                }
            }
        }Catch(exception ex){
            System.debug(ex.getMessage());
            System.debug(ex.getStackTraceString());
        }
        
    }
    
    public static List<Case> assignCaseOwner(List<Case> lstCase){
        try{
            Map<String, QCCRoutingTable__mdt> mapQCCRouting = new Map<String, QCCRoutingTable__mdt>();
           
            for(QCCRoutingTable__mdt  qccRouting: [Select Id, CaseType__c, Category__c, Group__c, Order__c,
                                                   QUEUE_Name__c, Sub_Category__c from QCCRoutingTable__mdt])
            {
                string category = qccRouting.Category__c == 'none'?'':qccRouting.Category__c;
                string subCategory = qccRouting.Sub_Category__c == 'none'?'':qccRouting.Sub_Category__c;
                String key = qccRouting.CaseType__c+''+qccRouting.Group__c+''+category+''+subCategory;
                mapQCCRouting.put(key, qccRouting);
               
            }
            
			Set<Id> setQCCRectype = qccCaseRecordTypeIds();

            
            system.debug('mapQueue###########'+mapQueue);
            system.debug('ZZZ mapQCCRouting: '+mapQCCRouting);

            for(Case objCase: lstCase){
                if(setQCCRectype.contains(objCase.RecordTypeId)){
                   
                    string description = objCase.Description;
                    if(!String.isBlank(objCase.QCC_Combined_Case_Description__c)){
                       description = objCase.QCC_Combined_Case_Description__c;
                        
                    }

                    caseDescriptionCheck(description);
                    Boolean isHotWord = isDesHasHotword;
                    String queueName = hotWordQueueName;              

                    Case oldCase = Null;
                    system.debug('objCase############'+objCase);

                    if(objCase.Assign_using_active_assignment_rules__c && objCase.Override_Hotword__c != true )
                    {
                        String queueSuffix = 'High';
                        
                        
                        String category = (objCase.Category__c == Null || objCase.Category__c == '')?'':objCase.Category__c;
                        String subCategory = (objCase.Sub_Category__c == Null || objCase.Sub_Category__c == '')?'':objCase.Sub_Category__c;
                        String Prioritykey = objCase.Type+''+objCase.Group__c+''+category+''+subCategory;
                        String key = objCase.Type+''+objCase.Group__c;
                        system.debug('Prioritykey###########'+Prioritykey);
                        system.debug('key###########'+key);
                        QCCRoutingTable__mdt routingInfo;
                        
                        if(mapQCCRouting.containsKey(Prioritykey)){
                            routingInfo = mapQCCRouting.get(Prioritykey);
                        }else if(mapQCCRouting.containsKey(key)){
                            routingInfo = mapQCCRouting.get(key);
                        }
                        system.debug('routingInfo###########'+routingInfo);
                        if(routingInfo != Null){
                                                      
                            if(!isHotWord){
                                if(objCase.Type.equalsIgnoreCase(CustomSettingsUtilities.getConfigDataMap('Case Type Complaint')) && String.isNotBlank(objCase.Type)
                                   && objCase.Group__c.equalsIgnoreCase(CustomSettingsUtilities.getConfigDataMap('Case Group Lounges')) && String.isNotBlank(objCase.Group__c))
                                {
                                    queueName = CustomSettingsUtilities.getConfigDataMap('QCC Lounges Queue');
                                }else if(objCase.Type.equalsIgnoreCase(CustomSettingsUtilities.getConfigDataMap('Case Insurance Letter Type')) && String.isNotBlank(objCase.Type)){
                                    queueName =CustomSettingsUtilities.getConfigDataMap('QCC Insurance Queue');
                                }else {
                                    queueName = routingInfo.QUEUE_Name__c;
                                }
                            }

                            if(isHotWord){                                
                                if(oldCase == Null && objCase.Status == CustomSettingsUtilities.getConfigDataMap('CaseStatusClosed') && 
                                    objCase.Is_Response_Requested__c == CustomSettingsUtilities.getConfigDataMap('QCC_ResponseRequired'))
                                {
                                    objCase.Status = CustomSettingsUtilities.getConfigDataMap('QCC_CaseStatusReceived');
                                }
                            }
                            
                            
                            queueName = queueName+'_'+queueSuffix;
                            String slaQueueName = queueName+'_SLA';
                            system.debug('ZZZqueueName###########'+queueName);
                            if(mapQueue.containsKey(queueName)){
                                objCase.OwnerId = mapQueue.get(queueName);
                            }
                        }
                        objCase.Assign_using_active_assignment_rules__c = false;
                        objCase.QCC_enableAssignmentRules__c = true;
                    }else if(objCase.Assign_using_active_assignment_rules__c && objCase.Override_Hotword__c ){
                        objCase.Override_Hotword__c = false;
                        //objCase.addError(CustomSettingsUtilities.getConfigDataMap('QCC_RoutingConflictMSG'));
                    }
                    
                    String ownerVal = objCase.OwnerId;
                    if(ownerVal.startsWith('00G')){
                        objCase.QCC_Routing_Invoked__c = true;
                        String slaQueueName = mapQueueName.get(objCase.OwnerId);
                        slaQueueName = slaQueueName.substring(0, slaQueueName.lastIndexOf('_'))+'_SLA';
                        objCase.SLA_Queue__c = mapQueue.get(slaQueueName);
                    }
                }
                
            }

        }catch(Exception ex){
            system.debug('ex###############'+ex);
        }

        return lstCase;
    }
    
    public static Map<String, Id> mapQueue;// = new Map<String, Id>();
    public static Map<Id, String> mapQueueName;// = new Map<Id, String>();
    public static Map<String, String> mapHotWordQueue;// = new Map<Id, String>(); 
    public static String hotWordQueueName;
    public static Boolean isDesHasHotword = false;
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        caseQueueInfo
    Description:        Method to Create a map of Queue to avoid SOQL
    Parameter:          
    --------------------------------------------------------------------------------------*/
    public static void caseQueueInfo(){
        if(mapQueue == Null || mapQueueName == Null){
            mapQueue = new Map<String, Id>();
            mapQueueName = new Map<Id, String>();
            for(QueueSobject  queueObj: [Select SobjectType, Id, QueueId, Queue.DeveloperName from QueueSobject where
                                             SobjectType ='Case']){
                mapQueue.put(queueObj.Queue.DeveloperName, queueObj.QueueId);
                mapQueueName.put(queueObj.QueueId, queueObj.Queue.DeveloperName);
            }
        }
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        caseHotWords
    Description:        Method to Create a map of Queue to avoid SOQL
    Parameter:          
    --------------------------------------------------------------------------------------*/
    public static void caseHotWords(){
        if(mapHotWordQueue == Null ){
            mapHotWordQueue = new Map<String, String>();
            for(QCC_HotWord__mdt qccHotWord: [Select DeveloperName, Hot_Word__c, Order__c from QCC_HotWord__mdt order by Order__c]){
                mapHotWordQueue.put(qccHotWord.DeveloperName, qccHotWord.Hot_Word__c);
                String allHotWord = mapHotWordQueue != Null && mapHotWordQueue.containsKey('All')? mapHotWordQueue.get('All')+','+qccHotWord.Hot_Word__c:qccHotWord.Hot_Word__c;
                mapHotWordQueue.put('All', allHotWord);
           }
        }
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        caseDescriptionCheck
    Description:        Method to get the hot word queue name
    Parameter:          
    --------------------------------------------------------------------------------------*/
    public static void caseDescriptionCheck(String caseDescription){
        Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
        Matcher matcher = nonAlphanumeric.matcher(caseDescription);
        String description = ' '+matcher.replaceAll(' ')+' ';
        isDesHasHotword = false;
        hotWordQueueName = '';
        for(String hotWordQueue: mapHotWordQueue.keyset()){
            if(hotWordQueue != 'All'){
                List<String> lstHotword = mapHotWordQueue.get(hotWordQueue).split(',');
                for(String hotWord: lstHotword){
                    String spaceHot = ' '+hotWord.trim()+' ';
                    system.debug('description###########'+description);
                    system.debug('(spaceHot).replace()###########'+(spaceHot).replace('"',''));
                    if(description.containsIgnoreCase((spaceHot).replace('"',''))){
                        isDesHasHotword = true;
                        hotWordQueueName = hotWordQueue;
                        break;
                    }
                }
                if(isDesHasHotword) {
                    break;
                }
            }
        }
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        qccCaseRecordTypeIds
    Description:        Fetch QCC Case RecordTypes
    Parameter:          
    --------------------------------------------------------------------------------------*/  
    public static Set<Id> qccCaseRecordTypeIds(){
        Set<Id> setQCCRectype = new Set<Id>();
        Id recTypeQCCCompliment = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case QCC Compliment RecType'));
        setQCCRectype.add(recTypeQCCCompliment);
        Id recTypeCCCBaggage = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case Baggage RecType'));
        setQCCRectype.add(recTypeCCCBaggage);
        Id recTypeCCCComplaint = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case QCC Complaint RecType'));
        setQCCRectype.add(recTypeCCCComplaint);
        Id recTypeCCCInsurance = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case QCC Insurance Letter RecType'));
        setQCCRectype.add(recTypeCCCInsurance);
        Id recTypeCCCMedical = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case QCC Medical RecType'));
        setQCCRectype.add(recTypeCCCMedical);
        Id recTypeCCCQuery = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case QCC Query RecType'));
        setQCCRectype.add(recTypeCCCQuery);
        Id recTypeCCCFAR = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case QCC Further Action RecType'));
        setQCCRectype.add(recTypeCCCFAR);
        return setQCCRectype;
    }
    
    public class infoWrapper{
        public String baseURL{get;set;}
        public User currentUser{get;set;}
    }
}