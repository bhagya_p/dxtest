/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath/Benazir Amir
Company:       Capgemini
Description:   Wrapper Class Simulates  Scheme Active Member Count received from QCP
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
25-AUG-2017    Praveen Sampath               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
global class SchemeMemberCountWrapper {

    public String schemeId;
    public String salesforceId;
    public Integer nomineeCount;
    
}