/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ChatSyncJob implements Database.AllowsCallouts, Database.Batchable<LiveEngage__ChatSync_Status__c>, Database.Stateful, System.Schedulable {
    global void execute(System.SchedulableContext SC) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> sObjectList) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global List<LiveEngage__ChatSync_Status__c> start(Database.BatchableContext bc) {
        return null;
    }
}
