/***********************************************************************************************************************************

Description: Apex class for retrieving the account information and TMC Information. 

History:
======================================================================================================================
Name                          Description                                                 Tag
======================================================================================================================
Ajay Bharathan			      Secondary Agency Details Parsing Class	                  T01

Created Date    : 19/06/2018 (DD/MM/YYYY)

**********************************************************************************************************************************/

public class QAC_AgencySecondaryParsing 
{
    public String primaryAgencyBusiness;	
    public String agencyAddressLine1;	
    public String city;			
    public String stateCode;		
    public String countryCode;	
    public String postalCode;			
    public String phoneNumber;		
    public String employees;	
    public String accountEmail;		
    public String atasNumber;
    public String agencyWebSite;
    public string dialCode;
    public string phoneCountryCode;
    
    public QAC_AgencySecondaryParsing(Account theAccount)
    {
        primaryAgencyBusiness = theAccount.Business_Type__c;
        agencyAddressLine1 = theAccount.BillingStreet;
        city = theAccount.BillingCity;
        stateCode = theAccount.BillingState;
        countryCode = theAccount.BillingCountryCode;
        postalCode = theAccount.BillingPostalCode;
        
         	String regExp = '[\\-\\+\\.\\^:\\(\\),]';
        	String replaceExp = '';
            String dialingCode;
            string phone;
             
             if(string.isNotBlank(theAccount.Phone_Country_Code__c))
             {
                dialingCode = theAccount.Phone_Country_Code__c.replaceall(regExp,replaceExp);
                dialingCode = dialingCode.replace(' ', replaceExp); 
             }
             
             if(string.isNotBlank(theAccount.Phone_Number__c))
             {
                 phone = theAccount.Phone_Number__c.replaceall(regExp,replaceExp);
                 phone = phone.replace(' ', replaceExp); 
             }
             
            
 			//Uncomment the below code in order to revert            
        	/**if((string.isNotBlank(dialingCode) || string.isNotEmpty(dialingCode)) && (string.isNotBlank(phone) || string.isNotEmpty(phone)))
            {
                phoneNumber = '+' + dialingCode + phone;
            }
             else if(string.isEmpty(dialingCode) && string.isNotEmpty(phone))
             {
                 phoneNumber = phone;
             }**/
        
        
        	if(string.isNotBlank(dialingCode) && string.isNotEmpty(dialingCode))
            {
                dialCode = '+' + dialingCode;
            }
            if(string.isNotBlank(phone) && string.isNotEmpty(phone))
            {
                phoneNumber = phone;
            }
        employees = String.valueOf(theAccount.NumberOfEmployees);
        accountEmail = theAccount.Email__c;
        atasNumber = theAccount.ATAS__c;
        agencyWebSite = theAccount.Website;
    }
    
    public static QAC_AgencySecondaryParsing parse(String json){
		return (QAC_AgencySecondaryParsing) System.JSON.deserialize(json, QAC_AgencySecondaryParsing.class);
	}
}