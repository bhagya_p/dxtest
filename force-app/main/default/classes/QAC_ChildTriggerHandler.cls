/* Created By : Ajay Bharathan | TCS | Cloud Developer */
public with sharing class QAC_ChildTriggerHandler
{
    public static map<Id,Case> caseMap = new map<Id,Case>();
    public static String caseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
    public static void doCaseUpdateonFlightDelete(list<Flight__c> existingFlights)
    {
        system.debug('old Flights to be deleted**'+existingFlights);
        String flightRTId = Schema.SObjectType.Flight__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        for(Flight__c eachFlight : existingFlights){
            if(eachFlight.RecordTypeId == flightRTId && eachFlight.Case__c != null)
            {
                Case myCase = new Case(Id = eachFlight.Case__c,FlightPax_DeleteTimestamp__c = System.now());
                caseMap.put(myCase.Id,myCase);
            }
        }
        if(!caseMap.isEmpty())
            update caseMap.values();
    }
    public static void doCaseUpdateonPaxDelete(list<Affected_Passenger__c> existingPax)
    {
        system.debug('old Passengers to be deleted**'+existingPax);
        String passengerRTId = Schema.SObjectType.Affected_Passenger__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();        
        for(Affected_Passenger__c eachPax : existingPax){
            if(eachPax.RecordTypeId == passengerRTId && eachPax.Case__c != null)
            {
                Case myCase = new Case(Id = eachPax.Case__c,FlightPax_DeleteTimestamp__c = System.now());
                caseMap.put(myCase.Id,myCase);
            }
        }
        if(!caseMap.isEmpty())
            update caseMap.values();
    }
	public static void doCaseUpdateonWODelete(list<WorkOrder> existingWO)
    {
        system.debug('old Work Orders to be deleted**'+existingWO);
        for(WorkOrder eachWO : existingWO){
            if(eachWO.CaseId != null)
            {
                Case myCase = new Case(Id = eachWO.CaseId,FlightPax_DeleteTimestamp__c = System.now());
                caseMap.put(myCase.Id,myCase);
            }
        }
        if(!caseMap.isEmpty())
            update caseMap.values();
    }    
    public static void doCaseUpdateonSPayDelete(list<Service_Payment__c> existingSPay)
    {
        system.debug('old Service Payments to be deleted**'+existingSPay);
        for(Service_Payment__c eachPay : existingSPay){
            if(eachPay.Case__c != null)
            {
                Case myCase = new Case(Id = eachPay.Case__c,FlightPax_DeleteTimestamp__c = System.now());
                caseMap.put(myCase.Id,myCase);
            }
        }
        if(!caseMap.isEmpty())
            update caseMap.values();
    }
    public static void doCaseUpdateonCaseCommentDelete(list<CaseComment> existingCC)
    {
        system.debug('old Case Comments to be deleted**'+existingCC);
        for(CaseComment eachCC : existingCC){
            if(eachCC != null && eachCC.ParentId != null)
            {
                Case myCase = new Case(Id = eachCC.ParentId,FlightPax_DeleteTimestamp__c = System.now());
                caseMap.put(myCase.Id,myCase);
            }
        }
        if(!caseMap.isEmpty())
            update caseMap.values();
    }
    
}