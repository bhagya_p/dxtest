/*==================================================================================================================================
Author:         Abhieet P
Company:        TCS
Purpose:        Freight - Class will be used for parsing the response recieved by iCargo system in JSON format.
Date:           03/08/2017         
TestClass:		Freight_AWBRequestJsonTest

==================================================================================================================================

==================================================================================================================================*/

public class Freight_AWBResponseParsing {

    public class LatestStatus {
        public String station;
        public String milestone;
        public String shipmentdate;
        public String weight;
        public String pieces;
        public string flightDetails;
        public String unit;
    }

    public String xmlnssoapenv;
    public ShipmentMilestones shipmentMilestones;


    public class ShipmentMilestones {
        public ShipmentsTracked shipmentsTracked;
    }

    public class ShipmentsTracked {
        public String shipmentPrefix;
        public String masterDocumentNumber;
        public String origin;
        public String destination;
        public String consigneeName;
        public String shipperName;
        public String shipmentDescription;
        public String handlingCode;
        public String pieces;
        public String weight;
        public String weightUnit;
        public string units;
        public LatestStatus latestStatus;
        public List<LatestStatus> history;
    }

    
    public static Freight_AWBResponseParsing parse(String json) {
        return (Freight_AWBResponseParsing) System.JSON.deserialize(json, Freight_AWBResponseParsing.class);
    }
}