/**
 * This class contains unit tests for validating the behavior of QuoteTriggerHelper class.
 */
@isTest
public class QuoteTriggerHelperTest {
    
    public static testMethod void test() {
        Id LoyaltyRecordTypeId= GenericUtils.getObjectRecordTypeId('Quote', 'Loyalty Commercial');
        Id oppRecordTypeId= GenericUtils.getObjectRecordTypeId('Opportunity', 'Loyalty Commercial');
        List<Account> accList = new List<Account>{};
        List<Opportunity> oppList = new List<Opportunity>{};
        List<Quote> quoteList = new List<Quote>{};
        for(integer i = 0; i < 5; i++)
        {
            Account acc = new Account(Name = 'Test1');
            accList.add(acc);
        }
        insert accList;
        for(integer i = 0; i < 5; i++)
        {
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = oppRecordTypeId;
            opp.AccountId = accList[i].Id;
            opp.Name = 'Opp'+i;
            opp.Customer_Base__c = 1000;
            opp.Points_Per__c = 10;
            opp.Price_Per_Point__c = 0.0180;
            opp.Assumed_Tag_Rate_at_Maturity__c = 1.45;
            opp.Average_Spend_Per_Customer__c = 1000;
            opp.StageName = 'Prospect';
            opp.CloseDate = System.today();
            oppList.add(opp);
        }
        
        insert oppList;
        
        for(integer i = 0; i < 5; i++) {
            Quote q = new Quote(RecordTypeId=LoyaltyRecordTypeId,Name='Opp Quote'+i, OpportunityId = oppList[i].Id, Active__c = true);
            quoteList.add(q);
        }
        insert quoteList;
        Quote q = new Quote(RecordTypeId=LoyaltyRecordTypeId,Name='Opp Quote', OpportunityId = oppList[0].Id, Active__c = true);
        insert q;
        Contract__c con;
        try{
            con = [Select Id, Name FROM Contract__c WHERE Quote__c = :q.id];
        } catch(Exception e) {
            System.assertEquals(null, con);
        }
        List<Quote> qts = [SELECT Id, Active__c, Name, Price_Per_Point__c, OpportunityId FROM Quote WHERE OpportunityId = :oppList[0].Id order By Name Desc];
        System.assertEquals(2, qts.size());
        System.assertEquals(true, qts[1].Active__c);
        System.assertEquals(null, qts[1].Price_Per_Point__c);
        qts[0].Price_Per_Point__c = 0.0185;
        qts[1].Price_Per_Point__c = 0.0185;
        qts[1].Status = 'Accepted';
        update qts;
        Opportunity op = [SELECT Id, Price_Per_Point__c FROM Opportunity where Id = :qts[0].OpportunityId];
        System.assertEquals(0.0185, op.Price_Per_Point__c);
        
        Contract__c con1 = [Select Id, Name FROM Contract__c WHERE Quote__c = :qts[1].id];
        System.assertEquals('Opp Quote', con1.Name);
               

    }   
    
    public static testMethod void futureMethodsClass()
    {
       List<Account> Acclist = TestUtilityDataClassQantas.getAccounts();
       List<Opportunity> OppList = TestUtilityDataClassQantas.getOpportunities(Acclist);
       Test.StartTest();
       futureMethodsClass.closeOpp(OppList[0].id, OppList[0].CloseDate);
       Test.StopTest();
    }
    
}