public class ContentDocumentTriggerHelper {

	public static void eligiblityCheckOnContentDocDel(Map<Id, ContentDocument> mapContentDocument){
		try{
			Boolean isPreventDelete = false;
			Set<Id> setContDocID = new Set<Id>();
			//avoid consultant to delete
			String prefix = Schema.getGlobalDescribe().get('Event__c').getDescribe().getKeyPrefix();
			User curUser = [select id, profileId, profile.Name from user where id = :userInfo.getUserId() limit 1];
			
            Boolean isPreventConsultantDelete = (curUser.profile.Name == 'Qantas CC Consultant');
			Boolean isPreventConsultantDeleteforpretravel = (curUser.profile.Name == 'Qantas Pre Journey CC Consultant');

			
           if(EnableValidationRule__c.getInstance(userInfo.getUserId()) != Null ){
				isPreventDelete = EnableValidationRule__c.getInstance(userInfo.getUserId()).PreventAttachmentDelete__c;
			}


			if(isPreventDelete || isPreventConsultantDelete || isPreventConsultantDeleteforpretravel){
				for(Id contDocID: mapContentDocument.keySet()){
					setContDocID.add(mapContentDocument.get(contDocID).Id);
				}

				for(ContentDocumentLink objContLink: [Select Id, LinkedEntityId, ContentDocumentId from ContentDocumentLink where
													  ContentDocumentId IN: setContDocID ])
				{
					if(isPreventDelete && String.valueOf(objContLink.LinkedEntityId).startsWith('500')){
						mapContentDocument.get(objContLink.ContentDocumentId).addError(CustomSettingsUtilities.getConfigDataMap('QCC_PreventAttDelete'));
					}

					if((isPreventConsultantDelete ||isPreventConsultantDeleteforpretravel) && String.valueOf(objContLink.LinkedEntityId).startsWith(prefix)){
						mapContentDocument.get(objContLink.ContentDocumentId).addError('Consultant can\'t delete file from Flight Event');
					}
				}
			
			}
		}
		catch(Exception ex){
			System.debug(LoggingLevel.ERROR, ex.getMessage());
            System.debug(LoggingLevel.ERROR, ex.getStackTraceString());
	        CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - Content Doc Trigger', 'ContentDocumentTriggerHelper', 
	                                'eligiblityCheckOnContentDocDel', String.valueOf(''), String.valueOf(''),false,'', ex);
	    }
	}
}