/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   APex Class for Customer Detail display on the Service console panel
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
30-OCT-2017    Praveen Sampath               Initial Design
23-Nov-2017	   Purushotham					 Added saveRecovery method
24-Nov-2017	   Anupam						 Added escalateCase method
25-Nov-2017	   Purushotham					 Added isConsultant method
26-Nov-2017    Anupam						 Updated fetchCustomerDetail method
27-Nov-2017	   Purushotham					 Added isCaseClosed method
19-Dec-2017    Anupam                        Updated fetchRecordTypeValues for Radio buttons
19-Jan-2018    Felix                         Update fetchCustomerDetail to perform DML in Queueable Apex so the panel load regardless of DML errors
14-Feb-2018    Praveen Sampath               Removed Queueable and made the update synchronous                  
-----------------------------------------------------------------------------------------------------------------------*/
public class QCC_CustomerDetailController {
    
	/*--------------------------------------------------------------------------------------      
    Method Name:        fetchCustomerDetail
    Description:        Fetches the QCC Customer information to display
    Parameter:          RecordId
    --------------------------------------------------------------------------------------*/    
    @AuraEnabled
    public static BannerWrapper fetchCustomerDetail(String recId, Boolean isPageLoad){ 
         BannerWrapper bannerWrap = new BannerWrapper();
        try{
            system.debug('CaseID####'+recId);
            Id conId;
            
            Contact objCon = fetchContact(recId);
            
            if(objCon == Null){
                //Added by Anupam to handle empty contactid from Case
                return null;
            }
            
            Boolean isChanged = false;
            List<OtherProducts> lstOtherProducts = new List<OtherProducts>();
            if(objCon != Null && objCon.Frequent_Flyer_Number__c != Null && isPageLoad){
                QCC_LoyaltyResponseWrapper loyaltyResponse = QCC_InvokeCAPAPI.invokeLoyaltyAPI(objCon.Frequent_Flyer_Number__c);
                system.debug('loyaltyResponse$$$$$'+loyaltyResponse);
                if(loyaltyResponse != Null){
                    if(objCon.Points_Balance__c != loyaltyResponse.pointBalance){
                        objCon.Points_Balance__c = loyaltyResponse.pointBalance;
                        isChanged = true;
                    }

                    if(loyaltyResponse.dateOfJoining != Null && objCon.Year_of_Joining__c != Date.valueOf(loyaltyResponse.dateOfJoining) || 
                        objCon.QCC_Frequent_Flyer_Anniversary_Date__c != Date.valueOf(loyaltyResponse.dateOfJoining))
                    {
                        objCon.Year_of_Joining__c = Date.valueOf(loyaltyResponse.dateOfJoining);
                        objCon.QCC_Frequent_Flyer_Anniversary_Date__c = Date.valueOf(loyaltyResponse.dateOfJoining);
                        isChanged = true;
                    }

                    if(loyaltyResponse.statusCredits != Null && objCon.Status_Credits__c != loyaltyResponse.statusCredits.lifetime){
                        objCon.Status_Credits__c = loyaltyResponse.statusCredits.lifetime;
                        isChanged = true;
                    }

                    /** Removed by BAU (CRM-3112)
                    if(objCon.Birthdate != Null && objCon.Birthdate !=  Date.valueOf(loyaltyResponse.dateOfBirth)){
                        objCon.Birthdate = Date.valueOf(loyaltyResponse.dateOfBirth);
                        isChanged = true;
                    }*/

                    if(loyaltyResponse.programDetails != Null && loyaltyResponse.programDetails.size()>0){
                        for(QCC_LoyaltyResponseWrapper.programDetails program: loyaltyResponse.programDetails){
                            if(program.programCode != 'QFF'){
                                OtherProducts otherProdInfo = new OtherProducts();
                                otherProdInfo.programName = program.programName;
                                otherProdInfo.programCode = program.programCode;
                                lstOtherProducts.add(otherProdInfo);
                            }
                            if(program.programCode == 'QFF' && 
                                objCon.Frequent_Flyer_tier__c != CustomSettingsUtilities.getConfigDataMap(program.tierCode)
                                )
                            {
                                objCon.Frequent_Flyer_tier__c = CustomSettingsUtilities.getConfigDataMap(program.tierCode);
                                isChanged = true;
                            }
                        } 
                    }
                    system.debug('aaaaaaaaaaaaa'+objCon.Frequent_Flyer_tier__c);
                    String setting = 'QCCFF_SC-'+objCon.Frequent_Flyer_tier__c;
                    system.debug('ssssssssssssss'+setting);
                    Integer statusCredit = loyaltyResponse.statusCredits != Null && loyaltyResponse.statusCredits.lifetime != Null? loyaltyResponse.statusCredits.lifetime: 0;
                    Integer nextLevel = String.isBlank(CustomSettingsUtilities.getConfigDataMap(setting))?statusCredit:Integer.valueOf(CustomSettingsUtilities.getConfigDataMap(setting));
                    Integer remaining = nextLevel-statusCredit;
                    if(objCon.QCC_Status_credits_till_next_Level__c != remaining){
                        objCon.QCC_Status_credits_till_next_Level__c = remaining;
                        isChanged = true;
                    }

                    // Execute Queueable Apex to update the Contact and LoyaltyProgram in the background
                    //System.enqueueJob(new QCC_UpdateLoyaltyAndContactFFQueueable( objCon));
                }
            }

            bannerWrap.objContact = objCon;
            bannerWrap.lstOtherProducts = lstOtherProducts; 
            system.debug('bannerWrap##########'+bannerWrap);
            if(isChanged && objCon.LastName != 'Null'){
                try{
                    update objCon;
                }catch(Exception ex){
                    CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                            'fetchCustomerDetail', String.valueOf(''), String.valueOf(''),false,'', ex);
                }
            }
            //QDCUSCON-4985 - Start
            if(recId.startsWith('500')){
                Case objCase = [select Id, ContactId, First_Name__c, Last_Name__c, Contact_Email__c, Contact_Phone__c,
                                    Frequent_Flyer_Tier__c, Frequent_Flyer_Number__c from Case where Id =: recId];
                bannerWrap.objContact.LastName = objCase.Last_Name__c;
                bannerWrap.objContact.FirstName = objCase.First_Name__c;
                bannerWrap.objContact.Preferred_Email__c = objCase.Contact_Email__c;
                bannerWrap.objContact.Preferred_Phone_Number__c = objCase.Contact_Phone__c;
            }
            //QDCUSCON-4985 - End
            //return bannerWrap;
        }catch(Exception ex){
            system.debug('ex######'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                    'fetchCustomerDetail', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
        return bannerWrap;
    }

   /*--------------------------------------------------------------------------------------      
    Method Name:        fetchContact
    Description:        Fetch all the Contact Info
    Parameter:          
    --------------------------------------------------------------------------------------*/    
    public static Contact fetchContact(String recId){
        Id conId;
        Contact objCon;
        
        if(recId.startsWith('003')){
            conId = recId;
        }
        else if(recId.startsWith('500')){
            Case objCase = [select Id, ContactId from Case where Id =: recId];
            conId = objCase.ContactId;
        }

        if(conId != null){
            objCon = [select Id, Name, FirstName, LastName, Frequent_Flyer_Number__c, Frequent_Flyer_Tier__c, 
                              Points_Balance__c, Status_Credits__c, MobilePhone, Preferred_Phone_Number__c, 
                              Preferred_Email__c, QCC_Status_credits_till_next_Level__c, Birthdate,
                              QCC_Frequent_Flyer_Anniversary_Date__c,Year_of_Joining__c,
                              MailingCity,MailingStreet,MailingState,MailingPostalCode ,CountryName__c, CAP_ID__c 
                              from Contact where Id =: conId];                                   
                              
        }
        return objCon;
    }


    /*--------------------------------------------------------------------------------------      
    Method Name:        fetchCaseSummary
    Description:        Fetches the All recordtype of case
    Parameter:          
    --------------------------------------------------------------------------------------*/    
    @AuraEnabled        
    public static BannerWrapper fetchCaseSummary(String recId){
        try{
            Contact objCon = fetchContact(recId);
            Integer disruptionsCount = 0;

           /* QCC_CAPTransactionWrapper.CAPTransactionRequest requestInfo = new QCC_CAPTransactionWrapper.CAPTransactionRequest();
            requestInfo.system_x = 'CAPCRE_CC';
            requestInfo.firstName = objCon.FirstName;
            requestInfo.surName = objCon.LastName;
            requestInfo.qantasFFNo = objCon.Frequent_Flyer_Number__c;
            requestInfo.version = '1.0';
            //requestInfo.qantasUniqueID = objCon.CAP_ID__c;

            QCC_CAPTransactionWrapper.CAPTransactionResponse disruptionResponse = QCC_InvokeCAPAPI.invokeTransactionCAPAPI(requestInfo);
            if(disruptionResponse != null){
                //disruptionsCount = disruptionResponse.flightDelayDomesticCount == null?0:disruptionResponse.flightDelayDomesticCount +disruptionResponse.flightDelayInternationalCount== null?0: disruptionResponse.flightDelayInternationalCount;
                disruptionsCount = disruptionResponse.flightDelayDomesticCount+disruptionResponse.flightDelayInternationalCount;
            }*/

            Date lastyr = system.today().addMonths(-12);
            Integer caseCount = [ Select count() from case where contactId =: objCon.Id and 
                                 CreatedDate >: lastyr];

            BannerWrapper bannerWrap = new BannerWrapper();
            //QDCUSCON-4985 - Start
            if(recId.startsWith('500')){
                bannerWrap.objContact = objCon;
            }
            //QDCUSCON-4985 - End
            bannerWrap.caseCount = caseCount;
            bannerWrap.disruptionsCount = disruptionsCount;
            return bannerWrap;
        }catch(Exception ex){
            system.debug('ex######'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                    'fetchCaseSummary', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
        return null;
    }
        

      
    /*--------------------------------------------------------------------------------------      
    Class Name:        BannerWrapper
    Description:        Wrapper to display on the page
    Parameter:          
    --------------------------------------------------------------------------------------*/    
    public class BannerWrapper{
        @AuraEnabled public Contact objContact;
        @AuraEnabled public Integer caseCount;
        @AuraEnabled public Integer disruptionsCount;
        @AuraEnabled public List<OtherProducts> lstOtherProducts;
    }

    public class OtherProducts{
        @AuraEnabled public String programName;
        @AuraEnabled public String programCode;

    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        fetchRecordTypeValues
    Description:        Fetches the All recordtype of case
    Parameter:          
    --------------------------------------------------------------------------------------*/    
    @AuraEnabled        
    public static List<RecordType> fetchRecordTypeValues(){
        List<RecordType> rtNames = new List<RecordType>(); 
        List<Schema.RecordTypeInfo> recordtypes = Case.SObjectType.getDescribe().getRecordTypeInfos(); 
        for(RecordTypeInfo rt : recordtypes){
            if(!rt.isMaster() && rt.isAvailable())
             rtNames.add(new RecordType(Id = rt.getRecordTypeId(),Name = rt.getName()));
        }        
        return  rtNames;
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        caseRecordType
    Description:        Fetches the recordtype of case
    Parameter:          
    --------------------------------------------------------------------------------------*/    
    @AuraEnabled        
    public static String caseRecordType(String recId){
        String rid = recId;
        try {
            List<Schema.RecordTypeInfo> recordtypes = Case.SObjectType.getDescribe().getRecordTypeInfos();
            if(rid.startsWith('500')){
                Case cs = [SELECT Id, Status, RecordTypeId, RecordType.Name FROM Case WHERE Id = :recId];
                return cs.RecordType.Name;
            }  
        } catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                     'caseRecordType', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
  
        return  '';
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        saveRecovery
    Description:        Creates a Recovery record by pre-populating some vales from Case
    Parameter:          Id of Case
    --------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static String saveRecovery(Id recId) {
        try {
			User user = [SELECT Id, Profile_Name__c FROM User WHERE Id = :userinfo.getUserId()];
            if(user.Profile_Name__c != 'Qantas Pre Journey CC Consultant') {
            System.debug('Recovery ' + recId);
            Case cs = [SELECT Id,Group__c,Type,EmergencyExpenseStage__c,Recommended_Emergency_Expense__c, 
                       Actual_Emergency_Expense__c, ContactId FROM Case WHERE Id = :recId];
            Recovery__c rec = new Recovery__c();
            rec.Case_Number__c = cs.Id;
            rec.Contact__c = cs.ContactId;
            rec.Status__c = 'Open';
            rec.Investigated_By__c = UserInfo.getUserId();

            if( cs.Group__c == 'Delayed Baggage' && cs.Type == 'Baggage' ){
               rec.Stage__c = cs.EmergencyExpenseStage__c;
                rec.Monetary_Value__c = cs.Recommended_Emergency_Expense__c;
                if(cs.Actual_Emergency_Expense__c != Null){
                    rec.Monetary_Value__c = cs.Actual_Emergency_Expense__c;
                }   
            }

            insert rec;
            return rec.Id;
	} 
    }catch(Exception ex){
            System.debug('Exception: '+ex.getMessage());
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                     'saveRecovery', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
        return null;
	}
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        isConsultant
    Description:        Checks whether the current user is Consultant or not
    Parameter:          Id of User
    --------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static Boolean isConsultant(Id userId) {
        try {
            User user = [SELECT Id, Profile_Name__c FROM User WHERE Id = :userId];
            if(user.Profile_Name__c == 'Qantas CC Consultant' || user.Profile_Name__c == 'PT1') {
                return true;
            }
        } catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                     'isConsultant', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
        return false;
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        isCaseClosed
    Description:        Checks whether the Case is Closed or not
    Parameter:          Id of User
    --------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static Boolean isCaseClosed(Id recId) {
        String rid = recId;
        try {
            if(rid.startsWith('500')){
                Case cs = [SELECT Id, Status FROM Case WHERE Id = :recId];
                if(cs.Status == 'Closed') {
                    return true;
                }
            }
        } catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                     'isCaseClosed', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
        return false;
    }

    /*--------------------------------------------------------------------------------------      
    Method Name:        isCaseInQueue
    Description:        Checks whether the Case is Closed or not
    Parameter:          Id of User
    --------------------------------------------------------------------------------------*/
    @AuraEnabled
    public static Boolean isCaseInQueue(Id recId) {
        String rid = recId;
        try {
            if(rid.startsWith('500')){
                Case cs = [SELECT Id, Status , OwnerId FROM Case WHERE Id = :recId];
                if(string.valueOf(cs.OwnerId).startsWith('00G')) {
                    return true;
                }
            }
        } catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                     'isCaseInQueue', String.valueOf(''), String.valueOf(''),false,'', ex);
            throw new AuraHandledException(ex.getMessage());
        }
        return false;
    }

    
    /*--------------------------------------------------------------------------------------      
    Method Name:        escalateCase
    Description:        Escalates a case and updates the Chatter feed.
    Parameter:          Record Id of Case
	Future:				To add logic to assign the case to Team Lead of the Component.- Done 26-11-2017
    --------------------------------------------------------------------------------------*/
	@AuraEnabled
	public static String escalatingCase(Id recId) {
        try {
            System.debug('Case Id '+recId);
            User user = [SELECT Id, ManagerId, Location__c, Manager.Name FROM User where id = :UserInfo.getUserId()];
            Case cs = [SELECT Id, ContactId, IsEscalated, Owner.Name, OwnerId  FROM Case WHERE Id = :recId];
            
            System.debug('ID of Case + value of Escalated'+ cs.Id + cs.IsEscalated);
            /********************************************************************************************************************/
            if(!Test.isRunningTest()) {
                ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, cs.Id, ConnectApi.FeedElementType.FeedItem, 'Case Escalated By '+ cs.Owner.Name);            
            }
            /********************************************************************************************************************/
            //Added by Purushotham
            cs.Escalated_From__c = cs.Owner.Name;
            cs.Escalated_From_Location__c = user.Location__c;
            //cs.Escalated_To__c = user.Manager.Name;
            //cs.IsEscalated = true; // Commented by Anupam
            cs.OwnerId = user.ManagerId;
            update cs;
            return cs.Id +','+cs.IsEscalated;
        } catch(Exception ex){
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC_CustomerDetailController', 'QCC_CustomerDetailController', 
                                     'saveRecovery', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
        return null;
    }
    
     @AuraEnabled
    public static Boolean getProfileToHideForContact(){
        Boolean check = false;
        Id pflId = UserInfo.getProfileId();
        Profile partnerProfile = [Select Id, Name From Profile Where id =:pflId Limit 1];        
        System.debug(System.Label.QCC_ProfileHide);
        if(partnerProfile != null){
           String pfls = System.Label.QCC_ProfileHide; 
           List<String> prflsNameList = pfls.split(',');                        
           check = prflsNameList.contains(partnerProfile.name); 
        }                 
        return check;
    }
}