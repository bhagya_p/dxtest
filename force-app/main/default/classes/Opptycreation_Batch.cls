/**********************************************************************************************************************************
 
    Created Date    : 31/08/16 (DD/MM/YYYY)
    Added By        : Vinothkumar Balasubramanian
    Description     : Batch job for Auto Oppty Creation.
    JIRA            : CRM - 2053, CRM - 2217 and CRM - 2238
    Version         : V1.0 - 31/08/16 - Initial version
    
    Modified Date   : 20/07/17 (DD/MM/YYYY)
    Added By        : Karpagam Swaminathan
    Description     : Batch job for Auto Oppty Creation.
    JIRA            : CRM - 2653
    Version         : V2.0 - Added Exception Log object
 
**********************************************************************************************************************************/




global class Opptycreation_Batch implements Database.Batchable<sObject> {
  Database.SaveResult[] lsterroResult = new Database.SaveResult[0]; 
  global Database.QueryLocator start(Database.BatchableContext BC){
      Map<string, Opportunity_main__c> categoryCY = Opportunity_main__c.getAll();

      List<String> category = new List<String>();
      List<String> categoryDate = new List<String>();
      if(categoryCY.containsKey('Corporate Airfares')){
        category.add(categoryCY.get('Corporate Airfares').Category__c);
      }
      if(categoryCY.containsKey('Qantas Business Savings')){
      category.add(categoryCY.get('Qantas Business Savings').Category__c);
      }
      
 
    
      return Database.getQueryLocator([SELECT Id, Contract_Start_Date__c, Contracted__c, Contract_End_Date__c, Account__c, Account__r.Name, Account__r.OwnerId,Opportunity__r.StageName,Opportunity__r.RecordType.Name,Opportunity__r.CloseDate,Opportunity__r.Sub_Type_Level_2__c,CreatedDate,
                   Account__r.QF_Revenue__c, Account__r.Estimated_Total_Annual_Revenue__c, Account__r.QF_Domestic_Market_Share__c, Account__r.Estimated_QF_Dom_Market_Share__c, Account__r.QF_International_Market_Share__c,Account__r.Estimated_Int_Market_Share__c, 
                  Domestic_Annual_Share__c, International_Annual_Share__c, Type__c,Qantas_Annual_Expenditure__c,MCA_Routes_Annual_Share__c,Contract_Expiry_year__c, Status__c, Farthest_Signed_Contract_End_Date_check__c, 
                   Account__r.Active__c, Account__r.RecordType.Name, Account__r.BillingCountry, Account__r.Count_of_open_opportunities__c,Account__r.Opportunity_Maximum_Close_Date__c, Account__r.Proposed_Revenue_Amount__c
                   FROM Contract__c where (Account__r.Active__c = TRUE AND 
                   Account__r.RecordType.Name = 'Customer Account' AND 
                   Farthest_Signed_Contract_End_Date_check__c = TRUE AND Account__r.BillingCountry = 'AU' AND Account__r.Count_of_open_opportunities__c = 0 AND
                   Type__c IN:category)]);
    }
    
    global void execute(Database.BatchableContext ctx, List<Contract__c> activecontracts){
      
      
      List<Opportunity> OpportunityList = new List<Opportunity>();
      Map<string, Opportunity_main__c> categoryCY = Opportunity_main__c.getAll();
      List<Id> lstRecId = new List<Id>();
      for(Contract__c theContracts: activecontracts){
          
          
      
           Date d = theContracts.Contract_End_Date__c;
           
           System.debug('Contract End date: '+d);
           
           Date t = Date.today();

           
           Decimal decimalvalue1 =  categoryCY.get('Corporate Airfares').Create_an_Opportunity_before_in_months__c;
           Integer integerValue1 = decimalvalue1.intValue();
           
           System.debug('Corp Create an opportunity before in months : '+integerValue1);
           
           Decimal decimalvalues1 =  categoryCY.get('Qantas Business Savings').Create_an_Opportunity_before_in_months__c;
           Integer integerValues1 = decimalValues1.intValue();
           
           System.debug('QBS Create an opportunity before in months : '+integerValues1);
           
           
        if(theContracts.Account__r.Opportunity_Maximum_Close_Date__c <> null) {
             

if((theContracts.Type__c == categoryCY.get('Corporate Airfares').Category__c && ((t.addMonths(integerValue1)) - 1) == d && categoryCY.get('Corporate Airfares').Active__c == true &&
  ((theContracts.Account__r.Opportunity_Maximum_Close_Date__c < (categoryCY.get('Corporate Airfares').Close_Date__c)))) || 
                        (theContracts.Type__c == categoryCY.get('Qantas Business Savings').Category__c && ((t.addMonths(integerValues1)) - 1) == d  && categoryCY.get('Qantas Business Savings').Active__c == true &&
       ((theContracts.Account__r.Opportunity_Maximum_Close_Date__c < (categoryCY.get('Qantas Business Savings').Close_Date__c))))){
                          Opportunity opp = new Opportunity();
                opp.name      = theContracts.Account__r.Name+' '+theContracts.Contract_Expiry_year__c+' Renewal';
                opp.AccountId = theContracts.Account__c;
                opp.OwnerId   = theContracts.Account__r.OwnerId;
                opp.RecordTypeId = '01290000001IINX';
                opp.StageName = 'Qualify';
                opp.Category__c= theContracts.Type__c;
                opp.Type= 'Business and Government';
                

                opp.Amount =  theContracts.Account__r.Proposed_Revenue_Amount__c;
                opp.Sub_Type_Level_1__c = 'Currently In Contract';
                opp.Sub_Type_Level_2__c = 'Renewal';
                opp.Sub_Type_Level_3__c = 'Single POS';

                opp.Auto_Create_Oppty_for_Con_renewal__c = TRUE;
                opp.CloseDate = theContracts.Contract_End_Date__c;
                opp.QDMEstimated_QF_Revenue__c = (theContracts.Account__r.QF_Revenue__c == null || theContracts.Account__r.QF_Revenue__c == 0.00)? theContracts.Account__r.Estimated_Total_Annual_Revenue__c : theContracts.Account__r.QF_Revenue__c;
                opp.QDMEstimated_DOM_QF_Market_Share__c = (theContracts.Account__r.QF_Domestic_Market_Share__c == null || theContracts.Account__r.QF_Domestic_Market_Share__c == 0.0) ? theContracts.Account__r.Estimated_QF_Dom_Market_Share__c : theContracts.Account__r.QF_Domestic_Market_Share__c;
                opp.QDMEstimated_INT_QF_Market_Share__c = (theContracts.Account__r.QF_International_Market_Share__c == null || theContracts.Account__r.QF_International_Market_Share__c == 0.0) ? theContracts.Account__r.Estimated_Int_Market_Share__c: theContracts.Account__r.QF_International_Market_Share__c;
                
                opp.Proposed_Market_Share__c = 90.0;
                opp.Proposed_International_Market_Share__c = 70.0;
                opp.Proposed_MCA_Routes_Market_Share__c = 70.0;
                

                
                System.debug('CA Opportunity Close Date: '+theContracts.Opportunity__r.CloseDate);
                System.debug('Custom Settings CA Close Date'+categoryCY.get('Corporate Airfares').Close_Date__c);
                System.debug('QBS Opportunity Close Date: '+theContracts.Opportunity__r.CloseDate);
                System.debug('Custom Settings QBS Close Date'+categoryCY.get('Qantas Business Savings').Close_Date__c);
                 System.debug('Account Contract Expiry Year: '+theContracts.Contract_Expiry_year__c);
                 System.debug('Opportunity Name: '+opp.name);
                 System.debug('Opportunity AccountId: '+opp.AccountId);
                 System.debug('Opportunity OwnerId: '+opp.OwnerId);
                 System.debug('Opportunity Stagename: '+opp.StageName);
                 System.debug('Opportunity Category: '+opp.Category__c);
                 System.debug('Opportunity Type: '+opp.Type);
                 System.debug('Opportunity Proposed Revenue Amount : '+opp.Amount);
                 System.debug('Contract Forecast QF spend:' +theContracts.Qantas_Annual_Expenditure__c);
                 System.debug('Opportunity Sub Type (Level 1): '+opp.Sub_Type_Level_1__c);
                 System.debug('Opportunity Sub Type (Level 2): '+opp.Sub_Type_Level_2__c);
                 System.debug('Opportunity Sub Type (Level 3): '+opp.Sub_Type_Level_3__c);
                 System.debug('Opportunity Actual/Estimated QF Revenue: '+opp.QDMEstimated_QF_Revenue__c);
                 System.debug('Opportunity Actual/Estimated Dom QF Market Share: '+opp.QDMEstimated_DOM_QF_Market_Share__c);
                 System.debug('Opportunity Actual/Estimated INT QF Market Share: '+opp.QDMEstimated_INT_QF_Market_Share__c);
                 System.debug('System Created Opportunity: '+opp.Auto_Create_Oppty_for_Con_renewal__c);
                 System.debug('Opportunity Proposed Dom Market Share Target: '+opp.Proposed_Market_Share__c);
                 System.debug('Opportunity Proposed Int Market Share Target: '+opp.Proposed_International_Market_Share__c);
                 System.debug('Opportunity Proposed MCA Market Share Target: '+opp.Proposed_MCA_Routes_Market_Share__c);
                 System.debug('Opportunity Expected Closure Date: '+opp.CloseDate);
                                            
                OpportunityList.add(opp);
                
                         }
      }
      
       if(theContracts.Account__r.Opportunity_Maximum_Close_Date__c == null){
      
      if((theContracts.Type__c == categoryCY.get('Corporate Airfares').Category__c && ((t.addMonths(integerValue1)) - 1) == d && categoryCY.get('Corporate Airfares').Active__c == true) || 
                        (theContracts.Type__c == categoryCY.get('Qantas Business Savings').Category__c && ((t.addMonths(integerValues1)) - 1) == d  && categoryCY.get('Qantas Business Savings').Active__c == true ))
                        
    {
                          Opportunity opp = new Opportunity();
                opp.name      = theContracts.Account__r.Name+' '+theContracts.Contract_Expiry_year__c+' Renewal';
                opp.AccountId = theContracts.Account__c;
                opp.OwnerId   = theContracts.Account__r.OwnerId;
                opp.RecordTypeId = '01290000001IINX';
                opp.StageName = 'Qualify';
                opp.Category__c= theContracts.Type__c;
                opp.Type= 'Business and Government';
                

                opp.Amount =  theContracts.Account__r.Proposed_Revenue_Amount__c;
                opp.Sub_Type_Level_1__c = 'Currently In Contract';
                opp.Sub_Type_Level_2__c = 'Renewal';
                opp.Sub_Type_Level_3__c = 'Single POS';

                opp.Auto_Create_Oppty_for_Con_renewal__c = TRUE;
                opp.CloseDate = theContracts.Contract_End_Date__c;
                opp.QDMEstimated_QF_Revenue__c = (theContracts.Account__r.QF_Revenue__c == null || theContracts.Account__r.QF_Revenue__c == 0.00)? theContracts.Account__r.Estimated_Total_Annual_Revenue__c : theContracts.Account__r.QF_Revenue__c;
                opp.QDMEstimated_DOM_QF_Market_Share__c = (theContracts.Account__r.QF_Domestic_Market_Share__c == null || theContracts.Account__r.QF_Domestic_Market_Share__c == 0.0) ? theContracts.Account__r.Estimated_QF_Dom_Market_Share__c : theContracts.Account__r.QF_Domestic_Market_Share__c;
                opp.QDMEstimated_INT_QF_Market_Share__c = (theContracts.Account__r.QF_International_Market_Share__c == null || theContracts.Account__r.QF_International_Market_Share__c == 0.0) ? theContracts.Account__r.Estimated_Int_Market_Share__c: theContracts.Account__r.QF_International_Market_Share__c;
                
                opp.Proposed_Market_Share__c = 90.0;
                opp.Proposed_International_Market_Share__c = 70.0;
                opp.Proposed_MCA_Routes_Market_Share__c = 70.0;

                System.debug('CA Opportunity Close Date: '+theContracts.Opportunity__r.CloseDate);
                System.debug('Custom Settings CA Close Date'+categoryCY.get('Corporate Airfares').Close_Date__c);
                System.debug('QBS Opportunity Close Date: '+theContracts.Opportunity__r.CloseDate);
                System.debug('Custom Settings QBS Close Date'+categoryCY.get('Qantas Business Savings').Close_Date__c);
                 System.debug('Account Contract Expiry Year: '+theContracts.Contract_Expiry_year__c);
                 System.debug('Opportunity Name: '+opp.name);
                 System.debug('Opportunity AccountId: '+opp.AccountId);
                 System.debug('Opportunity OwnerId: '+opp.OwnerId);
                 System.debug('Opportunity Stagename: '+opp.StageName);
                 System.debug('Opportunity Category: '+opp.Category__c);
                 System.debug('Opportunity Type: '+opp.Type);
                 System.debug('Opportunity Proposed Revenue Amount : '+opp.Amount);
                 System.debug('Contract Forecast QF spend:' +theContracts.Qantas_Annual_Expenditure__c);
                 System.debug('Opportunity Sub Type (Level 1): '+opp.Sub_Type_Level_1__c);
                 System.debug('Opportunity Sub Type (Level 2): '+opp.Sub_Type_Level_2__c);
                 System.debug('Opportunity Sub Type (Level 3): '+opp.Sub_Type_Level_3__c);
                 System.debug('Opportunity Actual/Estimated QF Revenue: '+opp.QDMEstimated_QF_Revenue__c);
                 System.debug('Opportunity Actual/Estimated Dom QF Market Share: '+opp.QDMEstimated_DOM_QF_Market_Share__c);
                 System.debug('Opportunity Actual/Estimated INT QF Market Share: '+opp.QDMEstimated_INT_QF_Market_Share__c);
                 System.debug('System Created Opportunity: '+opp.Auto_Create_Oppty_for_Con_renewal__c);
                 System.debug('Opportunity Proposed Dom Market Share Target: '+opp.Proposed_Market_Share__c);
                 System.debug('Opportunity Proposed Int Market Share Target: '+opp.Proposed_International_Market_Share__c);
                 System.debug('Opportunity Proposed MCA Market Share Target: '+opp.Proposed_MCA_Routes_Market_Share__c);
                 System.debug('Opportunity Expected Closure Date: '+opp.CloseDate);
                                            
                OpportunityList.add(opp);
                
                         }                
            
      
                      }
      }            
       
                try {
                      
                 Database.SaveResult[] srListInsert = Database.Insert(OpportunityList, false);
                 for (Integer i=0;i<OpportunityList.size();i++){
                    Database.SaveResult s = srListInsert[i];
                    Opportunity origRecord = OpportunityList[i];
                    if (!s.isSuccess()) {
                        lstRecId.add(origRecord.Id);
                        lsterroResult.add(s);
                        system.debug('origRecord.Id**********'+origRecord.Id); 
                    } 
                }
                for (Database.SaveResult sr : srListInsert ) {
                    if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully Inserted account contact relationship:' + sr.getId());
                    }
                    else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('account contact relationship fields that affected this error: ' + err.getFields());
                    }
                    }
                }
                }
           
                catch(Exception e){
                //Including logs to capture the exception                 
                    Integer i = 0;
                    for(Database.SaveResult sr : lsterroResult){
                    system.debug('sr############'+sr);
                    CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'Opptycreation_Batch', 'Opptycreation_Batch', 
                                     'Opptycreation_Batch',String.valueOf(''), String.valueOf(''),false, lstRecId[i], e);
                    i++;
                    }

                        System.debug('Exception Occured: '+e.getMessage());   
                }
    }
    
    global void finish(Database.BatchableContext ctx){
      
    }
}