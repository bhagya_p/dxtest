/***********************************************************************************************************************************

Description: Lightning component handler for the convert contract functionality

History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Karpagam                 CRM-2797    LEx-Contract Convert                                           T01
                                    
Created Date    : 25/09/17 (DD/MM/YYYY)

**********************************************************************************************************************************/
public with sharing class QL_ConvertContractController {
    
    @AuraEnabled public Boolean isSuccess {get;set;}
    @AuraEnabled public String msg {get;set;}
    @AuraEnabled public String recID {get;set;}
    
    @AuraEnabled
    public static QL_ConvertContractController getConvertedContract(Id propId)
    {
        
        QL_ConvertContractController obj = new QL_ConvertContractController();
        
        try{
            String rtype = '';
            Id RId;
            list<Contract__c> existingContractList = [SELECT Id,Status__c FROM Contract__c where Proposal__c =: propId];
            system.debug('Existing Cont***'+existingContractList);
            //Validate the contract exists scenario
            if(!existingContractList.isEmpty()){
                for(Contract__c eachContract : existingContractList){
                    if(eachContract.Status__c != 'Rejected – Legal' && eachContract.Status__c != 'Rejected - Customer'){
                        obj.isSuccess = false;
                        obj.msg = 'Contract exist for this Proposal';
                        return obj;
                    }
                }    
            }
            
            Proposal__c currentProposal;
            for (Proposal__c thisProposal : [SELECT Id,Name,Account__c,Commercial_TC__c,Domestic_Annual_Share__c,International_Annual_Share__c,Legal_TC__c,Opportunity__c,Contact__c,
                                             Qantas_Annual_Expenditure__c,Status__c,Valid_From_Date__c,Valid_To_Date__c,Standard__c,Additional_Change_Details__c,Deal_Type__c,MCA_Routes_Annual_Share__c,
                                             Agent_Name__c,Agent_Fax__c,Agent_Email__c,Agent_Address__c,Frequent_Flyer_Status_Upgrade__c,Frequent_Flyer_Status_Upgrade_Offer__c,Qantas_Club_Discount__c,Forecast_Group_Booking_Expenditure__c,Group_Travel_Discount__c,Group_Travel_Offer__c,
                                             Qantas_Club_Join_Discount_Offer__c,Qantas_Club_Discount_Offer__c,TMC_Code__c,Travel_Fund__c,Travel_Fund_Amount__c,Type__c,Proposal_Start_Date__c,Proposal_End_Date__c, Opportunity__r.Type,Opportunity__r.Sub_Type_Level_1__c,Opportunity__r.Sub_Type_Level_2__c,Opportunity__r.Category__c,
                                             (SELECT Id, FareStructure__c, Approval_Comment_NAM__c,Approval_Comment_Pricing__c, Comment__c, Discount__c, Discount_Threshold_AM__c, Discount_Threshold_NAM__c FROM Fare_Structure_Lists__r),
                                             (SELECT Id, Contract_Agreement__c FROM Charter_Flight_Information__r),
                                             (SELECT Id, Amount__c,Paid_As__c,Payment_Criteria__c,Payment_Frequency__c,Type__c,Proposal__c FROM Contract_Payments__r)
                                             FROM Proposal__c where Active__c =true AND Status__c = 'Accepted by Customer' AND Id =: propId])
            {
                currentProposal = (thisProposal != null) ? thisProposal : null;
            }
         // Handle the Contract variation scenario   
         if(currentProposal.Opportunity__r.Type == 'Business and Government' && currentProposal . Opportunity__r.Sub_Type_Level_1__c == 'Currently In Contract' && currentProposal . Opportunity__r.Sub_Type_Level_2__c == 'Variation' && (currentProposal . Opportunity__r.Category__c == 'Corporate Airfares' || currentProposal . Opportunity__r.Category__c == 'Qantas Business Savings'))
          
            {
            obj.isSuccess = false;
            obj.msg = 'For Variation Type Proposals, you do not have to convert this Proposal to a Contract. Please raise your Deal Support Case on the Renewal Contract';
            return obj;
            }

         // Create a contract if already there is no such contract exist   
            if(currentProposal != null)
            {
                Contract__c createContract = new Contract__c();
                createContract.Name = currentProposal.Name;
                createContract.Account__c = currentProposal.Account__c;
                createContract.Commercial_TC__c = currentProposal.Commercial_TC__c;
                createContract.Contact__c = currentProposal.Contact__c;
                createContract.Domestic_Annual_Share__c = currentProposal.Domestic_Annual_Share__c;
                createContract.International_Annual_Share__c = currentProposal.International_Annual_Share__c;
                createContract.Legal_TC__c = currentProposal.Legal_TC__c;
                createContract.Opportunity__c = currentProposal.Opportunity__c;
                createContract.Qantas_Annual_Expenditure__c = currentProposal.Qantas_Annual_Expenditure__c;
                createContract.Proposal__c = currentProposal.Id;
                createContract.Type__c = currentProposal.Type__c;
                //createContract.Valid_From_Date__c = currentProposal.Valid_From_Date__c;
                //createContract.Valid_To_Date__c = currentProposal.Valid_To_Date__c;
                createContract.Travel_Fund__c = currentProposal.Travel_Fund__c;
                createContract.Travel_Fund_Amount__c = currentProposal.Travel_Fund_Amount__c;
                createContract.Standard__c = currentProposal.Standard__c;
                createContract.Additional_Change_Details__c = currentProposal.Additional_Change_Details__c;
                createContract.Status__c = 'Signature Required by Customer';
                createContract.Deal_Type__c = currentProposal.Deal_Type__c;
                createContract.MCA_Routes_Annual_Share__c = currentProposal.MCA_Routes_Annual_Share__c;
                createContract.Agent_Name__c = currentProposal.Agent_Name__c;
                createContract.Agent_Fax__c = currentProposal.Agent_Fax__c;
                createContract.Agent_Email__c = currentProposal.Agent_Email__c;
                createContract.Agent_Address__c = currentProposal.Agent_Address__c;
                createContract.Qantas_Club_Discount__c = currentProposal.Qantas_Club_Discount__c;
                createContract.Frequent_Flyer_Status_Upgrade__c = currentProposal.Frequent_Flyer_Status_Upgrade__c;
                createContract.Frequent_Flyer_Status_Upgrade_Offer__c = currentProposal.Frequent_Flyer_Status_Upgrade_Offer__c;
                createContract.Qantas_Club_Join_Discount_Offer__c = currentProposal.Qantas_Club_Join_Discount_Offer__c;
                createContract.Qantas_Club_Discount_Offer__c = currentProposal.Qantas_Club_Discount_Offer__c;
                createContract.TMC_Code__c = currentProposal.TMC_Code__c;
                createContract.contract_Start_Date__c = currentProposal.Proposal_Start_Date__c;
                createContract.contract_End_Date__c = currentProposal.Proposal_End_Date__c; 
                createContract.Forecast_Group_Booking_Expenditure__c = currentProposal.Forecast_Group_Booking_Expenditure__c;
                createContract.Group_Travel_Discount__c = currentProposal.Group_Travel_Discount__c;
                createContract.Group_Travel_Offer__c = currentProposal.Group_Travel_Offer__c;           
                
                if(currentProposal.Type__c == 'Qantas Business Savings'){
                    rtype = 'QBS - Legal/Customer Pending';
                }else if(currentProposal.Type__c == 'Corporate Airfares'){
                    rtype = 'CA - Legal/Customer Pending';
                }else if(currentProposal.Type__c == 'Adhoc'){
                    rtype = 'Charters Contract - Legal/Customer Pending';
                }else if(currentProposal.Type__c == 'Scheduled'){
                    rtype = 'Charters Contract - Legal/Customer Pending';
                }
                
                if(String.isNotBlank(rtype))
                    RId = Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get(rtype).getRecordTypeId();
                
                createContract.RecordTypeId = RId;
                insert createContract;
                if(createContract.id!=null ||createContract.id<>'')
                    obj.recID=createContract.id;
                
                // Contract Fare Discount creation done here
                list<Contract_Discount_List__c> createCFDiscountList = new list<Contract_Discount_List__c>();
                if(currentProposal.Fare_Structure_Lists__r.size() > 0){
                  list<Discount_List__c> existingPFDiscountList = currentProposal.Fare_Structure_Lists__r;
                    for(Discount_List__c eachPFD : existingPFDiscountList){
                        Contract_Discount_List__c contFareDisc = new Contract_Discount_List__c();
                        contFareDisc.Approval_Comment_NAM__c = eachPFD.Approval_Comment_NAM__c;
                        contFareDisc.Approval_Comment_Pricing__c = eachPFD.Approval_Comment_Pricing__c;
                        contFareDisc.FareStructure__c = eachPFD.FareStructure__c;
                        contFareDisc.Comment__c = eachPFD.Comment__c;
                        contFareDisc.Contract__c = createContract.id;
                        contFareDisc.Discount__c = eachPFD.Discount__c;
                        contFareDisc.Discount_Threshold_AM__c = eachPFD.Discount_Threshold_AM__c;
                        contFareDisc.Discount_Threshold_NAM__c = eachPFD.Discount_Threshold_NAM__c; 
                        createCFDiscountList.add(contFareDisc);
                    }
                }

                if(!createCFDiscountList.isEmpty())        
                    insert createCFDiscountList;            
                
                // Contract Payments Update done here
                //list<Contract_Payments__c> existingCPList = new list<Contract_Payments__c>();
                if(currentProposal.Contract_Payments__r.size() > 0){
                    list<Contract_Payments__c> existingCPList = currentProposal.Contract_Payments__r;
                    for(Contract_Payments__c eachCP : existingCPList){
                        eachCP.Contract_Agreement__c = createContract.Id;
                        eachCP.Proposal__c = currentProposal.Id;
                    }
                    update existingCPList;                    
                }
                
                // Charter Flight Information Update done here
                //list<Charter_Flight_Information__c> existingCFIList = new list<Charter_Flight_Information__c>();
                if(currentProposal.Charter_Flight_Information__r.size() > 0){
                    list<Charter_Flight_Information__c> existingCFIList = currentProposal.Charter_Flight_Information__r;
                    for(Charter_Flight_Information__c eachCFI : existingCFIList){
                        eachCFI.Contract_Agreement__c = createContract.Id;
                        //eachCP.Proposal__c = currentProposal.Id;
                    }
                    update existingCFIList; 
                }
                
                // Contract Attachment insert done here
                Attachment existingPropAtt;
                for(Attachment thisAttachment : [SELECT Id, Name, Body, ParentId FROM Attachment WHERE ParentId =: currentProposal.Id ORDER BY CreatedDate DESC LIMIT 1]){
                  existingPropAtt = (thisAttachment != null) ? thisAttachment : null;      
                }
                
                if(existingPropAtt != null)
                {
                    Attachment createContrAtt = new Attachment();
                    createContrAtt.ParentId = createContract.Id;
                    createContrAtt.Body = existingPropAtt.Body;
                    createContrAtt.Name = existingPropAtt.Name;
                    insert createContrAtt; 
                }
                
                obj.isSuccess = true;
                obj.msg = 'The current Proposal is converted to Contract : '+ createContract.Name;
            }
            else{
                obj.isSuccess = false;
                obj.msg = Label.Convert_Contract_Error;
            }
            
            return obj;
        }
        
        catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());    
        }
    }
    
}