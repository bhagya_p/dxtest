/*----------------------------------------------------------------------------------------------------------------------
Author:        Bharathkumar
Company:       Tata Consultancy services
Description:   QCC CreditCardNoticeHelper
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
20-July-2018      Bharathkumar               Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public with sharing  class QCC_CreditCradNoticeHelper {

    @AuraEnabled
    public static Boolean hasCreditCard(Id caseId){
        Boolean hasCardInfo;      
        Case c = [select id,HasCreditCard__c,type,Description,Origin from Case where id = :caseId Limit 1];
        Boolean isUpdate = false;
        System.debug('c#####'+c);
        if(c != null){
          if(c.Origin!=null && c.Origin!='Auto Email' && (c.type=='Baggage' || c.type=='Insurance Letter' || c.type=='Complaint' || c.type=='Compliment' || c.type=='Further Action Required'|| c.type=='Medical-Patient' || c.type=='Medical-Assistance Provided' || c.type=='On the Spot Recovery' || c.type=='Query')){
              if(c.HasCreditCard__c){
                  return true;
              }
               String str = c.Description;
              str= str.replace('\n',' ');
                List<String> spiltStr = str.split(' ');
                for(String s : spiltStr){
                    String s1 = s;
                    System.debug('s####'+s);
                    s= s.replace('\n','');
                    System.debug('s####'+s);
                    s=s.replace('\r','');
                    System.debug('s####'+s);
                    s =  s.trim(); 
                    System.debug('s####'+s);
                    Boolean isAusPhoneNumberWithplus = s.startsWith('+61')||s.startsWith('0')||s.startsWith('61')||s.startsWith('+');                    
                    Boolean isPhoneNumber;
                    System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                    if(isAusPhoneNumberWithplus){
                        System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                        isPhoneNumber =  true;
                        continue;
                    } 
                    System.debug('isAusPhoneNumberWithplus"""""""::::::"""""""'+isAusPhoneNumberWithplus);
                    
                    s = s.replaceAll('\\D','');
                    Pattern p=Pattern.compile('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35*\\d{3})*\\d{11})$'); 
                    Matcher m = p.matcher(s);
                    System.debug('m.matches():'+m.matches());
                    if(m.matches()){                        
                        
                        //String mcard1='5555555555554444';
                        System.debug('mcard1'+s);
                        System.debug('mcard1 length()'+s.length());
                        integer i =s.length();
                        String s2 = s.substring(2, i);
                        System.debug('ss'+s2);
                        String s3 = s.replace(s2, '-XXXX-XXXX-XXXX');
                        System.debug('ss2'+s3);
                        String strChanged = str.replace(s1, s3);
                        c.Description = strChanged;
                        System.debug('c.Description###'+c.Description);
                        isUpdate= true;
                        if(isUpdate){
                              c.HasCreditCard__c = true;
                              update c;
                          }
                        return true;
                        
                    }                    
                }               
               // hasCardInfo = c.HasCreditCard__c;

            }           
        }        
        return false;
    }
    
    //for masking
    public static void maskCreditCard(List<Case> clist){
        Boolean hasCardInfo;      
       // Case c = [select id,HasCreditCard__c,Description,Origin from Case where id = :caseId Limit 1];
        Boolean isUpdate = false;
        for(Case c :clist){
            System.debug('c#####'+c);
        if(c != null){
          if(c.Origin!=null && c.Description != null && c.Origin!='Auto Email' && (c.type=='Baggage' || c.type=='Insurance Letter' || c.type=='Complaint' || c.type=='Compliment' || c.type=='Further Action Required'|| c.type=='Medical-Patient' || c.type=='Medical-Assistance Provided' || c.type=='On the Spot Recovery' || c.type=='Query' )){
               String str = c.Description;
              str= str.replace('\n',' ');
                List<String> spiltStr = str.split(' ');
                for(String s : spiltStr){
                    String s1 = s;
                    System.debug('s####'+s);
                    s= s.replace('\n','');
                    System.debug('s####'+s);
                    s=s.replace('\r','');
                    System.debug('s####'+s);
                    s =  s.trim(); 
                    System.debug('s####'+s);
                    Boolean isAusPhoneNumberWithplus = s.startsWith('+61')||s.startsWith('0')||s.startsWith('61')||s.startsWith('+');                    
                    Boolean isPhoneNumber;
                    System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                    if(isAusPhoneNumberWithplus){
                        System.debug('isAusPhoneNumberWithplus#######'+isAusPhoneNumberWithplus);
                        isPhoneNumber =  true;
                        continue;
                    } 
                    System.debug('isAusPhoneNumberWithplus"""""""::::::"""""""'+isAusPhoneNumberWithplus);                    
                    s = s.replaceAll('\\D','');
                    Pattern p=Pattern.compile('^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35*\\d{3})*\\d{11})$');
                    Matcher m = p.matcher(s);
                    System.debug('m.matches():'+m.matches());
                    if(m.matches()){                        
                        
                        //String mcard1='5555555555554444';
                        System.debug('mcard1'+s);
                        System.debug('mcard1 length()'+s.length());
                        integer i =s.length();
                        String s2 = s.substring(2, i);
                        System.debug('ss'+s2);
                        String s3 = s.replace(s2, '-XXXX-XXXX-XXXX');
                        System.debug('ss2'+s3);
                        String strChanged = str.replace(s1, s3);
                        c.Description = strChanged;
                        System.debug('c.Description###'+c.Description);
                        isUpdate= true;
                        if(isUpdate){
                              c.HasCreditCard__c = true;                              
                          }                        
                    }                    
                }               
               // hasCardInfo = c.HasCreditCard__c;

            }           
        } 
        }
               
    }
}