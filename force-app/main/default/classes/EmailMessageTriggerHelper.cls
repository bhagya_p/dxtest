public class EmailMessageTriggerHelper {
    
    public static void UpdateCaseStatus(List<EmailMessage> emailObject){
        try {
            List<Case> lstCase = new List<Case>();
            Map<String, Id> mapQueue = new Map<String, Id>();
            Map<Id, String> mapQueueName = new Map<Id, String>();
            Map<Id, Case> mapCase = new Map<Id, Case>();
            Set<Id> setCaseId = new Set<Id>();
            Set<String> emails = new Set<String>();
            List<Task> taskList = new List<Task>();
            Id recTypeCCCBaggage = GenericUtils.getObjectRecordTypeId('Case', CustomSettingsUtilities.getConfigDataMap('Case Baggage RecType'));

            Map<String, QCCRoutingPriority__mdt> mapQCCPriority = new Map<String, QCCRoutingPriority__mdt>();

            for(EmailMessage eMessage: emailObject){
                setCaseId.add(eMessage.parentId);
            }
            Set<Id> setCaseRecType = CaseTriggerHelper.qccCaseRecordTypeIds();
            for(Case objCase: [SELECT Id , Group__c, ClosedBylocation__c, Last_Queue_Owner__c, RecordTypeId, 
                                Cabin_Class__c, Frequent_Flyer_Tier__c, OwnerId ,Reopened_Date__c , 
                                Reason, Closed_By__c, Status , CaseNumber , Priority, Contact.Name, 
                                Assign_using_active_assignment_rules__c , Type, Email_Template__c , Auto_Close_on_Email_Sent__c,QCC_enableAssignmentRules__c, Recovery_Inprogress__c 
                                FROM Case WHERE Id =: setCaseId and RecordTypeId IN: setCaseRecType]){
                if(objCase.Email_Template__c != CustomSettingsUtilities.getConfigDataMap('EE Payment Template')) {
                    mapCase.put(objCase.Id, objCase);
                }
            }

            for(QCCRoutingPriority__mdt  qccPriority: [Select Id, Cabin__c, Priority__c,  Tier__c from QCCRoutingPriority__mdt])
            {
                String key = qccPriority.Cabin__c+''+qccPriority.Tier__c;
                mapQCCPriority.put(key, qccPriority);
            }
            
            
            for(QueueSobject  queueObj: [Select SobjectType, Id, QueueId, Queue.DeveloperName from QueueSobject where 
                                     SobjectType ='Case'])
            {
                mapQueue.put(queueObj.Queue.DeveloperName, queueObj.QueueId);
                mapQueueName.put(queueObj.QueueId, queueObj.Queue.DeveloperName);
            }

            for(Contact c : [Select Id,Name,Email From Contact Where Account.Name ='QantasCorporateEmailAccount']){
                emails.add(c.email);
            }

            User currentUser =  [Select Location__c from User where Id = : UserInfo.getUserId()];
            
            system.debug('mapCase$$$$$$$$'+mapCase);
            for(EmailMessage message : emailObject){         
                system.debug('message$$$$$$$$'+message);
                Case objCase = mapCase.get(message.parentId);
                System.debug('message.ToAddress ' + message.ToAddress);
                if(objCase != Null){
                    if(message.Incoming){
                        Boolean emailFound = false; 
                        if(message.ToAddress!=null)
                        {
                            for(String toAdd : message.ToAddress.split(';'))
                            {
                                toAdd = (toAdd!=null)?toAdd.trim():'';
                                
                                if(toAdd !=null && toAdd.equalsIgnoreCase(CustomSettingsUtilities.getConfigDataMap('Qantas Customer Care Email')))
                                {
                                    emailFound = true;
                                }
                            }
                        }
                        if(emailFound)
                        {
                            if(objCase.Status == CustomSettingsUtilities.getConfigDataMap('CaseStatusClosed'))
                            {
                                objCase.Status = CustomSettingsUtilities.getConfigDataMap('CaseStatusReopened');
                                
                                objCase.Reopened_Date__c = system.now();
                                
                                String queueSuffix;
                                if(objCase.Cabin_Class__c != Null  && objCase.Frequent_Flyer_Tier__c != Null )
                                {
                                    string key = objCase.Cabin_Class__c+''+objCase.Frequent_Flyer_Tier__c;
                                    queueSuffix = mapQCCPriority.get(key).Priority__c;
                                }else{
                                    queueSuffix = 'Low';
                                }
                                        
                                String content = objCase.Type + '\nPriority: ' + objCase.Priority + '\nStatus: ' + objCase.Status;
                                if(objCase.Contact != null){
                                    content +=  '\nCustomer Name: ' + objCase.Contact.Name + '\nDate Received: ' + Date.today();
                                }

                                if(string.valueOf(objCase.OwnerId).startsWith('005')){
                                    QCC_GenericUtils.postToCaseWithMention(objCase , ' ' + objCase.CaseNumber + ' Case reopened!\n Case Type: ' + content);
                                }
                                
                                if(objCase.ClosedBylocation__c == 'Sydney'){
                                    //assign case to user
                                    objCase.OwnerId = objCase.Closed_By__c; 
                                }
                                else if(objCase.RecordTypeId == recTypeCCCBaggage && objCase.Group__c == CustomSettingsUtilities.getConfigDataMap('QCC_DelayBaggage')){
                                    String delayQueuName = CustomSettingsUtilities.getConfigDataMap('QCC_DelayBaggageHighQueue');
                                    objCase.OwnerId =mapQueue.get(delayQueuName);
                                }                            
                                else{
                                    String queueName = 'Bounce_Back_Queue_'+queueSuffix;
                                    if(String.isNotBlank(objCase.Type) && String.isNotBlank(objCase.Group__c)){
                                        if(objCase.Type.equalsIgnoreCase('Baggage') && objCase.Group__c.equalsIgnoreCase('Damaged Baggage')){
                                            queueName = 'Baggage_Damaged_Queue_High';
                                        }
                                    }
                                    system.debug('queueName#######3'+queueName);
                                    objCase.OwnerId = mapQueue.get(queueName);
                                    system.debug('queueName#######3'+queueName);

                                }

                                if(objCase.Contact != null){
                                    content +=  '\nCustomer Name: ' + objCase.Contact.Name + '\nDate Received: ' + Date.today();
                                }

                                if(string.valueOf(objCase.OwnerId).startsWith('005')){
                                    QCC_GenericUtils.postToCaseWithMention(objCase , ' ' + objCase.CaseNumber + ' has been assigned to you!\n Case Type: ' + content);
                                }
                            }
                        }
                    }
                    else if(!message.TECH_FromChatter__c && !message.Incoming){
                        system.debug('1111111111'+objCase);
                        System.debug('Auto_Close_on_Email_Sent__c ' + objCase.Auto_Close_on_Email_Sent__c);
                        if(objCase.Auto_Close_on_Email_Sent__c)
                        {
                            objCase.Auto_Close_on_Email_Sent__c = false;    
                            System.debug('Closing this cases..');
                            objCase.Status = CustomSettingsUtilities.getConfigDataMap('CaseStatusClosed');
                            objCase.Resolution_Method__c = CustomSettingsUtilities.getConfigDataMap('CaseResolutionMethod_AutoClosed');
                            objCase.Reason = CustomSettingsUtilities.getConfigDataMap('CaseCloseReason');
                            objCase.Closed_By__c = UserInfo.getUserId();
                            
                            if(String.isnotBlank(currentUser.Location__c)){
                                objCase.ClosedByLocation__c = currentUser.Location__c;
                            }
                            
                            //For Defect --> JIRA-2437 added below condition....
                            if(objCase.Type == CustomSettingsUtilities.getConfigDataMap('Case Insurance Letter Type') || objCase.Type == CustomSettingsUtilities.getConfigDataMap('Case QCC Insurance Letter RecType')){
                                 ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, objCase.id, ConnectApi.FeedElementType.FeedItem, 'Insurance letter sent to customer and case closed');
                            }
                        }
                        //QDCUSCON 3340 post feed to chatter when recovery in progress can't automatically close case
                        if(objCase.Recovery_Inprogress__c){
                            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, objCase.id, ConnectApi.FeedElementType.FeedItem, 'Email sent and case is not auto-closed since a recovery was still in progress');
                            //ConnectApi.Comment comment = ConnectApi.ChatterFeeds.postCommentToFeedElement(null, objCase.id, 'Email sent and case is not auto-closed since a recovery was still in progress' );
                            objCase.Recovery_Inprogress__c = false;
                        }
                        //end QDCUSCON 3340 
                    }

                    system.debug('message#####'+message);

                    //Combined in one method
                    if(message.Incoming == false && message.ToAddress != '' && emails.contains(message.ToAddress.removeEnd(','))){
                        Datetime dt     = message.CreatedDate != Null? message.CreatedDate: system.now();
                        Datetime dt2    = dt.addDays(2);
                        Task t          = new Task();
                        t.Subject       = 'Reminder for other departments to respond';
                        t.Status        = 'In Progress';
                        t.WhatId        = message.parentId;
                        t.ActivityDate  = dt2.date();
                        t.Email__c      = message.ToAddress;
                        t.Task_From__c  = 'QantasCorporateEmailAccount';
                        if(objCase != null){
                            t.WhoId                  = objCase.ContactId;
                            t.Case_Status__c         = objCase.Status;
                            objCase.Email_Content__c = message.HtmlBody;
                        }
                        taskList.add(t);
                    }
                    System.debug('Case contain ' + lstCase);
                    lstCase.add(objCase);   
                    
                }

                if(taskList != null && taskList.size() > 0){
                    insert taskList;
                }

                if(lstCase != null && lstCase.size() > 0){
                    update lstCase;
                }
            }
        }catch(Exception ex){
            system.debug('ex###############'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - EmailObject Trigger', 'EmailMessageTriggerHelper', 
                                    'UpdateCaseStatus', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }
    
    public static void PreventdeleteEmailMessage(List<EmailMessage> emailObject){
        try {
            Id profileId=userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            if(profileName =='Qantas CC Consultant'||
               profileName =='Qantas CC Team Lead'|| 
               profileName =='Qantas CC Operational Manager'){
            
                for(EmailMessage eMessage: emailObject){
                    eMessage.adderror('You do not have access to delete the EmailMessage');
                }
            }
        }
        catch(Exception ex){
            system.debug('ex###############'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - EmailObject Trigger', 'EmailMessageTriggerHelper', 
                                    'PreventdeleteEmailMessage', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }

    //This method is not needed because there is no Contact for Non FF as part of Remediation
    /*----------------------------------------------------------------------------------------------      
    Method Name:        UpdateRecoveryStatusForNonFrequentFlyerMember
    Description:        Handle non frequent flight member for lounge pass.
    Parameter:          the list of the email
    Return:             void
    ----------------------------------------------------------------------------------------------*/
    /*public static void UpdateRecoveryStatusForNonFrequentFlyerMember(List<EmailMessage> emailObject){
        try {                       
            Set<Id> setCaseRecType = CaseTriggerHelper.qccCaseRecordTypeIds();
            Set<Id> setCaseId = new Set<Id>();

            for(EmailMessage eMessage: emailObject){
                if(eMessage.Incoming && GenericUtils.checkBlankorNull(eMessage.parentId)){
                    setCaseId.add(eMessage.parentId);
                }               
            }
           
            if(setCaseId.size() > 0){
                Database.executeBatch(new QCC_EmailMsgLoungePassNonFFMBatchable(setCaseId, setCaseRecType), 50);
            }
        }catch(Exception ex){
            system.debug('ex###############'+ex);
            CreateLogs.insertLogRec( 'Log Exception Logs Rec Type', 'QCC - EmailObject Trigger', 'EmailMessageTriggerHelper', 
                                    'UpdateRecoveryStatusForNonFrequentFlyerMember', String.valueOf(''), String.valueOf(''),false,'', ex);
        }
    }*/
}