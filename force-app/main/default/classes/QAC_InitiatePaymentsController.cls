/*
* Created By : Ajay Bharathan | TCS | Cloud Developer 
* Purpose : Manual Payment Processing Class
* Referred : QAC_PaymentsProcess, QAC_InitiatePayments (Lightning Bundle)
*/

public with sharing class QAC_InitiatePaymentsController {
    
    @AuraEnabled
    public String isSuccess { get; set; }
    
    @AuraEnabled
    public String msg { get; set; }
    
    @AuraEnabled
    public static Service_Payment__c getPayRecord(String caseId){
        list<Service_Payment__c> mySPRecords = new list<Service_Payment__c>();
        for(Service_Payment__c eachSP : [Select Id, Name, Payment_Currency__c, Case__c, Work_Order__c, Total_Amount__c,Payment_Status__c,
                                        Payment_Type__c, EMD_Number__c, Quantity_UOM__c, Work_Order__r.WorkOrderNumber, Payment_Due_Date__c 
                                        from Service_Payment__c where Case__c =: caseId])
        {
            if(eachSP != null)    
                mySPRecords.add(eachSP);
        }
        if(!mySPRecords.isEmpty())
        	return mySPRecords[0];
        return null;
    }
    
    @AuraEnabled
    public static QAC_InitiatePaymentsController ValidatePayment(String caseId)
    {
        QAC_InitiatePaymentsController obj = new QAC_InitiatePaymentsController(); 
        Case myCase;
        Boolean myPaxSeq, myWorkOrder;
        String waivCombo;
        
        if(String.isNotBlank(caseId))
        {
            myCase = getCurrentCase(caseId);
            myPaxSeq = getPaxRecord(caseId);
            myWorkOrder = getWORecord(caseId);
            waivCombo = myCase.Problem_Type__c + '_' + myCase.Waiver_Sub_Type__c;
            
            if(myCase.Paid_Service__c == null || !myCase.Paid_Service__c){
                obj.msg = 'You cannot create payments for Non-Paid Service';
                obj.isSuccess = 'error';
                return obj;
            }
            else if(String.isBlank(myCase.QAC_Request_Country__c) || String.isBlank(myCase.Travel_Type__c)){
                obj.msg = 'Request Country (or) Travel type is incorrect.You cannot create payments';
                obj.isSuccess = 'error';
                return obj;
            }
            else if(myWorkOrder){
                obj.msg = 'Payment already initiated. Please modify the existing Payments';
                obj.isSuccess = 'error';
                return obj;
            }
            else if(!myPaxSeq){
                obj.msg = 'No Passenger records. Please add Passengers and retry';
                obj.isSuccess = 'error';
                return obj;
            }
            else{
                // Proceed with Payments Creation
                QAC_PaymentsProcess.doPaymentsCreation(waivCombo,myCase.CreatedDate,myCase,myPaxSeq);
                obj.msg = 'Payment Records created successfully';
                obj.isSuccess = 'success';
                return obj;
            }
        }
        system.debug('obj**'+obj);
        return obj;
    }
    
    public static Boolean getPaxRecord(String caseId){
        String pRTID = Schema.SObjectType.Affected_Passenger__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();                    
        for(Affected_Passenger__c eachPassenger : [Select Id,Name,Passenger_Sequence__c,Case__c,Salutation__c,Passenger_Type__c,New_Salutation__c,
                                                   QAC_Passenger_Id__c,FirstName__c,LastName__c,New_First_Name__c,New_Last_Name__c,CreatedDate,
                                                   FrequentFlyerNumber__c,QAC_Flight_Number__c,Ticket_Number__c,Date_of_Birth__c,LastModifiedDate 
                                                   FROM Affected_Passenger__c WHERE Case__c =: caseId AND RecordTypeId =: pRTID])
        {
            if(eachPassenger != null)
                return TRUE;
        }
        
        return FALSE;
    }
    
    public static Boolean getWORecord(String caseId){

        for(WorkOrder eachOrder : [Select Id, WorkOrderNumber, StartDate, EndDate, Travel_Type__c, Pricebook2Id, 
                                   Pricebook2.Name, CurrencyIsoCode, LineItemCount, Sub_Total__c, GST__c, TotalPrice, 
                                   CreatedDate, LastModifiedDate, LastModifiedBy.Name, CaseId,
                                   (SELECT Id, WorkOrderId, WorkOrder.WorkOrderNumber, Product2Id, Product2.ProductCode, 
                                    Product2.Name, Quantity, UnitPrice, Subtotal, TotalPrice FROM WorkOrderLineItems)
                                   FROM WorkOrder WHERE CaseId =: caseId])
        {
            if(eachOrder != null && eachOrder.WorkOrderLineItems != null)
                return true;
        }
        return false;                                                                          
    }

    public static Case getCurrentCase(String caseId)
    {
        Case existingCase;
        String rtypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        
        for(Case eachCase : [SELECT Id,Paid_Service__c, QAC_Request_Country__c,CaseNumber,External_System_Reference_Id__c,ParentId,Parent.CaseNumber,Origin,Type,Problem_Type__c,
                             Subject,Description,Reason_for_Waiver__c,Request_Reason_Sub_Type__c,Request_Reason_Others__c,Justification__c,
                             AccountId,Consultants_Name__c,Consultant_Email__c,Consultant_Phone__c,Account.Name,Account.BillingCountryCode,
                             Owner.Name,CreatedDate,LastModifiedDate,Expected_Closure_Date__c,Authority_Number__c,ABN_Tax_Reference__c,
                             PNR_number__c,Old_PNR_Number__c,Passenger_Name__c,Ticket_Number__c,IATA__c,PCC_DI__c,GDS_DI__c,Additional_Requirements__c,
                             Qantas_Corporate_Identifier__c,Number_of_Passengers__c,Travel_Type__c,First_Flight_Date__c,Waiver_Sub_Type__c,
                             Frequent_Flyer_Number__c,Fare_basis_value__c,Fare_Type__c,Private_Fare_Basis__c,Region_Code__c,
                             Account.Qantas_Industry_Centre_ID__c,Status,Point_Of_Sale__c,Customer_Tier__c,Ticketing_Time_Limit__c
                             FROM Case WHERE Id =: caseId AND RecordTypeId =: rtypeId])
        {
            existingCase = eachCase;
        }
        return existingCase;            
    }
}