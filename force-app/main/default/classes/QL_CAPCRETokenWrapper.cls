/*----------------------------------------------------------------------------------------------------------------------
Author:        Priyadharshini Vellingiri
Company:       TCS
Description:   Wrapper for getting CAP System Token
Inputs:
Test Class:    QL_CAPCRETokenWrapperTest 
************************************************************************************************
History
************************************************************************************************
27-Apr-2018           Priyadharshini Vellingiri      Initial Design

-----------------------------------------------------------------------------------------------------------------------*/
public class QL_CAPCRETokenWrapper {
    public String token_type;   
    public Integer expires_in;  
    public String access_token; 
    
}