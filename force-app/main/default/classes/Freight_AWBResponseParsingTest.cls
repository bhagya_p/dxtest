@isTest
private class Freight_AWBResponseParsingTest {
    
	static testMethod void testParse() {
        String json = '{'+
		'  \"Envelope\": {'+
		'    \"Body\": {'+
		'      \"shipmentMilestones\": {'+
		'        \"shipmentsTracked\": {'+
		'          \"shipmentPrefix\": \"081\",'+
		'          \"masterDocumentNumber\": \"85606625\",'+
		'          \"origin\": \"SYDNEY\",'+
		'          \"destination\": \"SINGAPORE\",'+
		'          \"shipmentDescription\": \"CHILLED MEAT\",'+
		'          \"handlingCode\": \"ECC,ICE,PEM\",'+
		'          \"pieces\": \"1\",'+
		'          \"weight\": \"1450.0\",'+
		'          \"weightUnit\": \"Kg\",'+
		'          \"units\": \"AKE\",'+
		'          \"latestStatus\": {'+
		'            \"station\": \"SYD\",'+
		'            \"milestone\": \"Booked\",'+
		'            \"shipmentdate\": \"09-Jun-2017 13:06:41\",'+
		'            \"weight\": \"1450.0\",'+
		'            \"pieces\": \"1\",'+
		'            \"flightDetails\": \"QF0005,DEP ; SYD.22-Jun-2017 15:50(S) ARR ; SIN. 22-Jun-2017 22:15(S)\",'+
		'            \"unit\": \"AKE-1\"'+
		'          },'+
		'          \"history\": {'+
		'            \"station\": \"SYD\",'+
		'            \"milestone\": \"Booked\",'+
		'            \"shipmentdate\": \"09-Jun-2017 13:06:41\",'+
		'            \"weight\": \"1450.0\",'+
		'            \"pieces\": \"1\",'+
		'            \"flightDetails\": \"QF0005,DEP ; SYD.22-Jun-2017 15:50(S) ARR ; SIN. 22-Jun-2017 22:15(S)\",'+
		'            \"unit\": \"AKE-1\"'+
		'          }'+
		'        }'+
		'      }'+
		'    }'+
		'  }'+
		'}';
		Freight_AWBResponseParsing obj = Freight_AWBResponseParsing.parse(json);
		System.assert(obj != null);
	}
}