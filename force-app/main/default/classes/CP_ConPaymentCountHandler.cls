/**********************************************************************************************************************************
 
    Created Date: 26/02/18
 
    Description: Roll up of Contract Payments of Proposal.
  
    Versión:
    V1.0 - 26/02/18 - Initial version [FO]
 
**********************************************************************************************************************************/
Public class CP_ConPaymentCountHandler { 
    public static void CountNumofConPayments(List <Contract_Payments__c> newList) {
    Set<Id> setProposalIds = new Set<Id>();
    
     for(Contract_Payments__c con : newList)
       {
        setProposalIds.add(con.Proposal__c);
        system.debug('setProposalIds$$$$'+setProposalIds);
       }
  
     List<Proposal__c> listProptoUpdate = [Select id,name,No_of_Cps__c ,(Select id from Contract_Payments__r) from Proposal__c where Id in : setProposalIds ];               
     system.debug('query$$$$'+listProptoUpdate );
     
     for(Proposal__c Prop: listProptoUpdate)  {
     
     List<Contract_Payments__c> conPmt = new list<Contract_Payments__c>();
     conPmt = Prop.Contract_Payments__r;
        if (Prop.No_of_Cps__c != conPmt.size())
            Prop.No_of_Cps__c = conPmt.size();
    }
      update listProptoUpdate;
  }
}