trigger AffectedPassengerTrigger on Affected_Passenger__c (before insert, before update, after insert, after update, after delete) {
 
    if(CheckRecursive.runOnce()){
        if(Trigger.isBefore){
            if(Trigger_Status__c.getValues('isAffectedPassenger').Active__c && (Trigger.isInsert || Trigger.isUpdate)) {
                AffectedPassengerTriggerHelper.isAffectedPassenger(Trigger.New, Trigger.oldMap);
            }
            if(Trigger.isUpdate){
                AffectedPassengerTriggerHelper.updateFullEmailBody(Trigger.New, Trigger.oldMap);
            }
            //parse airport code to airpost name
            if(Trigger_Status__c.getValues('PassengerUpdateAirportName').Active__c && (Trigger.isInsert)){
                AffectedPassengerTriggerHelper.updateAirportName(Trigger.New);
            }
        }
        
        if(Trigger.isAfter){
            if(Trigger_Status__c.getValues('RollupPassengerBasedonTier').Active__c && (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete)){
                AffectedPassengerTriggerHelper.rollupPassengerBasedonTier(Trigger.New, Trigger.oldMap);
            }
            if(Trigger.isDelete){
                QAC_ChildTriggerHandler.doCaseUpdateonPaxDelete(Trigger.Old);
            }
        }

    }
    
}