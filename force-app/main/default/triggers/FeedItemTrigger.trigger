trigger FeedItemTrigger on FeedItem (before delete) {
    if(Trigger_Status__c.getValues('FeedItemTrgPreventChatterDeletion').Active__c && Trigger.isDelete && Trigger.isBefore ){
        FeedItemTriggerHelper.preventChatterDeletion(trigger.old);
    }
}