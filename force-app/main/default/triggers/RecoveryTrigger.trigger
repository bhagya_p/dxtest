/*----------------------------------------------------------------------------------------------------------------------
Author:        Cyrille
Company:       Capgemini
Description:   Triggers on the recovery object
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
22-Nov-2017      Cyrille               Initial Design
27-Nov-2017      Praveen Sampath       RecoveryTriggerHelper.invokeLoyaltyPostService
-----------------------------------------------------------------------------------------------------------------------*/
trigger RecoveryTrigger on Recovery__c (before insert,before update, after update , after insert) {
    if(!RecoveryTriggerHelper.hasRun){
        if(Trigger_Status__c.getValues('isNotFrequentFlyerMember').Active__c && Trigger.isBefore && Trigger.isUpdate) {
            RecoveryTriggerHelper.isNotFrequentFlyerMember(Trigger.New, Trigger.oldMap);
        }

        if(Trigger_Status__c.getValues('isFrequentFlyerMember').Active__c && Trigger.isBefore && Trigger.isUpdate) {
            RecoveryTriggerHelper.isFrequentFlyerMember(Trigger.New, Trigger.oldMap);
        }
        
        if(Trigger_Status__c.getValues('getSuggestedRange').Active__c && Trigger.isBefore && Trigger.isInsert) {
            RecoveryTriggerHelper.getSuggestedRange(Trigger.New);
        }

        if(Trigger_Status__c.getValues('updateRecoveryType').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
            RecoveryTriggerHelper.updateRecoveryType(Trigger.New, Trigger.oldMap);
        }

        if(Trigger_Status__c.getValues('setFulfilledBy').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
            RecoveryTriggerHelper.setFulfilledBy(Trigger.New, Trigger.oldMap);
        }

        if(Trigger_Status__c.getValues('handleRecoveryTrigger').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
            RecoveryTriggerHelper.handleRecoveryTrigger(Trigger.New, Trigger.oldMap);
        }

        if(Trigger_Status__c.getValues('RecoveryInvokeLoyaltyPostService').Active__c && Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)){
            RecoveryTriggerHelper.invokeLoyaltyPostService(Trigger.New, Trigger.oldMap);
        }
        
        if(Trigger_Status__c.getValues('RecoveryTriggerUpdateCaseRecoveryValue').Active__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
            RecoveryTriggerHelper.updateCaseRecoveryValue(Trigger.New);
        }
        
        if(Trigger_Status__c.getValues('RecoveryTriggerPostToChatter').Active__c && Trigger.isBefore && Trigger.isUpdate ){
            RecoveryTriggerHelper.postToChatter(Trigger.New , Trigger.oldMap);
        }

        if(Trigger_Status__c.getValues('routeCaseOnRecoveryPaymentUpdate').Active__c && Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
            RecoveryTriggerHelper.routeCaseOnRecoveryPaymentUpdate(Trigger.New, Trigger.oldMap);
        }

        if(Trigger_Status__c.getValues('RecoveryTriggerUpdateCaseRecoveryEvent').Active__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
            RecoveryTriggerHelper.updateCaseStatusRecoveryEvent(Trigger.New);
        }

        

        //RecoveryTriggerHelper.hasRun = true;
    }
    if(Trigger_Status__c.getValues('RecoveryTriggerSubmitForFinalisation').Active__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        RecoveryTriggerHelper.invokeOPLAAPIForLoungePass(Trigger.New, Trigger.oldMap);
    }
    
}