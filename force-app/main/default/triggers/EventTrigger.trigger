/*----------------------------------------------------------------------------------------------------------------------
Author:        Judy
Company:       Capgemini
Description:   QCC Case Processing
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
02-May-2018     Judy Tran          	      Initial Design
02-May-2018     Cyrille Jeufo             Method - getEventPassengers
-----------------------------------------------------------------------------------------------------------------------*/
trigger EventTrigger on Event__c (before insert,before update,after insert,after update) {
    // Moved this to top -- QDCUSCON-4481 
    //QDCUSCON-3250
    if(Trigger_Status__c.getValues('EventTriggerGetFlightInfo').Active__c && Trigger.isAfter && (Trigger.isUpdate|| Trigger.isInsert) ){
        EventTriggerHelper.updateEventFightInfo(Trigger.New, Trigger.oldMap);
    }
    //end QDCUSCON-3250

    if(Trigger_Status__c.getValues('EventTriggerGetPassengers').Active__c && Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)) {
        EventTriggerHelper.invokePaggengerAPI(Trigger.New, Trigger.oldMap);
    }
    if(Trigger_Status__c.getValues('EventTriggerUpdateEventFields').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
		
    	EventTriggerHelper.updateEventFields(Trigger.New, Trigger.oldMap);
    }

    //QDCUSCON-3394
    if(Trigger_Status__c.getValues('EventTriggerArrivingAtDestination').Active__c && Trigger.isBefore && Trigger.isUpdate){
        EventTriggerHelper.updateEventArrivingAtDestination(Trigger.New, Trigger.oldMap);
    }
    //end QDCUSCON-3394

    if(Trigger_Status__c.getValues('EventTriggerCreateRecovery').Active__c && Trigger.isBefore && Trigger.isUpdate){
        EventTriggerHelper.createRecovery(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('EventTriggerUpdateAffectedPassenger').Active__c && Trigger.isAfter && Trigger.isUpdate) {
        //EventTriggerHelper.updateAffectedPassenger(Trigger.New, Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('EventTriggerCreateTaskforEvent').Active__c && Trigger.isAfter &&(Trigger.isInsert || Trigger.isUpdate)){
        EventTriggerHelper.createTaskforEvent(Trigger.New, Trigger.oldMap);
    }

    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        //EventTriggerHelper.updateOverNightFlight(Trigger.oldMap, Trigger.new);
    }

    if(Trigger_Status__c.getValues('EventTriggerCreateEventComms').Active__c && Trigger.isAfter && Trigger.isUpdate){
        EventTriggerHelper.createEventComms(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('EventTriggerSendCommsOnEventClosure').Active__c && Trigger.isAfter && Trigger.isUpdate){
        EventTriggerHelper.sendCommsOnEventClosure(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('EventTriggerAutoCloseEvent').Active__c && Trigger.isBefore && Trigger.isUpdate){
        EventTriggerHelper.autoCloseEvent(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('EventTriggerAutoCloseEvent').Active__c && Trigger.isAfter && Trigger.isInsert){
        EventTriggerHelper.createChildRecord(Trigger.New);
    }

    //process for closed Events
    if(Trigger_Status__c.getValues('EventTriggerPostEventClosedAction').Active__c && Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)){
        EventTriggerHelper.postEventClosedAction(Trigger.newMap, Trigger.oldMap);
    }

    //update OwnerId to QCC_EventCostQueue if DelayCostsCompleted__c == true
    if(Trigger_Status__c.getValues('EventTriggerDelayCostsCompletedAction').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
        //EventTriggerHelper.delayCostsCompletedAction(Trigger.New, Trigger.oldMap);
    }

    //calculate cost if RecoveryIsCreated == true
    if(Trigger_Status__c.getValues('EventTriggerUpdateEventCost').Active__c && Trigger.isAfter && Trigger.isUpdate){
        EventTriggerHelper.updateEventCost(Trigger.New, trigger.oldMap);
    }
    
    //Global Event Trigger
    // update audit fields if trigger status is active
    //if(Trigger_Status__c.getValues('GlobalEventTriggerUpdateAuditFields').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
       // EventTriggerHelper.updateAuditFields(Trigger.New, Trigger.oldMap);
    //}


}