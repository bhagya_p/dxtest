trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert) {
	if(trigger.isBefore && trigger.isInsert){
		ContentDocumentLinkTriggerHelper.avoidingUploadFileToEvent(trigger.new);
	}
}