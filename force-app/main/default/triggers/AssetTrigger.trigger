/*----------------------------------------------------------------------------------------------------------------------
Author:        Praveen Sampath
Company:       Capgemini
Description:   Trigger on Asset for Insert/Update Events
Test Class:    
*********************************************************************************************************************************
*********************************************************************************************************************************
History
*********************************************************************************************************************************

**********************************************************************************************************************************/
trigger AssetTrigger on Asset (before update, before insert) {
    if(Trigger_Status__c.getValues('AssetCalculateFeesandDiscounts').Active__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate) ){
        AssetTriggerHelper.calculateAssetFeesandDiscounts(trigger.new, trigger.oldMap);
    }
    if(Trigger_Status__c.getValues('AssetConvertingAssetType').Active__c && Trigger.isBefore && Trigger.isUpdate ){
        AssetTriggerHelper.convertingAssetType(trigger.newMap, trigger.oldMap);
    }
}