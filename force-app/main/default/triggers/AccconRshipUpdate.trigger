/**********************************************************************************************************************************

Created Date: 27/09/2016

Description: Process Contact trigger for:
1. Populate the details at the Account Relationship level automatically upon Contact creation/update

Versión:
V1.0 - 27/09/2016 - Initial version [FO]

Modified Date: 23/05/2017
Decription: Roll up of CL contacts of an account

Change History:
======================================================================================================================
Name                          Jira           Description                                             Tag
======================================================================================================================
Priyadharshini Vellingiri    2306    Populate the details at the Account Relationship level          T01
automatically upon Contact creation/update  
Karpagam Swaminathan         2563    Roll up of CL contacts of an account                            T01
**********************************************************************************************************************************/   
trigger AccconRshipUpdate on Contact (after insert, after Update,after delete,after undelete, before insert, before update){
    
    try{
        Trigger_Status__c ts = Trigger_Status__c.getValues('AccconRshipUpdate');
        if(ts.Active__c) { 
            if ((trigger.isafter && trigger.isInsert) || (trigger.isafter && trigger.isUpdate)) {
                accConRshipUpdateonContacts.AccContactInsUpdate(trigger.new);
            }
            if ((trigger.isafter && trigger.isInsert) || (trigger.isafter && trigger.isUpdate) || (trigger.isafter && trigger.isdelete)|| (trigger.isafter && trigger.isUndelete))      
            {
                Contact[] cons;
                if (Trigger.isDelete)
                    cons = Trigger.old;
                else
                    cons = Trigger.new;
                RollupofCLcontactsHandler.CountNumofContacts(cons);    
            } 
        }
        
        if( Trigger_Status__c.getValues('ContactCeateAccount').Active__c && trigger.isBefore && Trigger.isInsert){
            accConRshipUpdateonContacts.createAccount(Trigger.New);  
        }


        if( Trigger_Status__c.getValues('ContactUpdateContact').Active__c && trigger.isBefore){
            accConRshipUpdateonContacts.updateContact(Trigger.New, Trigger.oldMap);            
        }


       /* if( Trigger_Status__c.getValues('ContactCeateAccount').Active__c && trigger.isAfter && trigger.isInsert){
            accConRshipUpdateonContacts.createACRforPrimaryBusinessAcc(Trigger.New);            
        } */
        if( Trigger_Status__c.getValues('ContactCeateAccount').Active__c && trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
            accConRshipUpdateonContacts.updateStandardEmailandPhone(Trigger.New);   
            accConRshipUpdateonContacts.UpdateFrequentFlyerNumber(Trigger.New, Trigger.oldMap);
        }
        if(Trigger_Status__c.getValues('ContactAttachCase').Active__c && Trigger.isBefore && Trigger.isUpdate ){
            accConRshipUpdateonContacts.linkCaseToContact(Trigger.New, Trigger.oldMap);
        }
        
    }catch(Exception e){
        System.debug('Error Occured From AccconRshipUpdate  Trigger: '+e.getMessage());
    }
    
    
}