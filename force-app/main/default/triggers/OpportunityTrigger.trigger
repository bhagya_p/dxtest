// Creates a Quote when Opportunity stage is changed to 'Negotiate' for 'Loyalty Commercial' record type

trigger OpportunityTrigger on Opportunity (after update,before update) {
    System.debug('Reached Trigger');    
    //Check the trigger executing first time:
     if(IsRecursive.runOnce()) {
         if(Trigger.isBefore){
           if(Trigger.isUpdate){
              System.debug('TriggerInst**'+Trigger.New);
              OpportunityTriggerHandler.oppPusher(Trigger.New,Trigger.oldMap);
            }
         }
         if(Trigger.isAfter){
             if(Trigger.isUpdate){
               OpportunityTriggerHandler.getNewQuoteOppotunities(Trigger.New,Trigger.oldMap);
             }
         }
   
     }
    
}