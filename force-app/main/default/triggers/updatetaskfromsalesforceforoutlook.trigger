/***********************************************************************************************************************************

Description: Trigger which calls the handler class Task trigger handler  

History:
======================================================================================================================
Name                    Jira        Description                                                 Tag
======================================================================================================================
Bharathkumar Narayanan  CRM-2707    Log a call id capture on campaign member                T01
                                    
Created Date    : 23/09/17 (DD/MM/YYYY)

**********************************************************************************************************************************/




trigger updatetaskfromsalesforceforoutlook on Task (before insert,after insert)
{

try{
     if(Trigger.isBefore){
     if(Trigger.isInsert){


map<Id, UserRole> roleIdVsUserRoleMap = new map<id, UserRole>([SELECT Id, Name FROM UserRole]) ;
map<Id, String> roleIdVsRoleNameMap = new map<Id, String>();

for(UserRole eachRole : [SELECT Id, Name FROM UserRole]){
    roleIdVsRoleNameMap.put(eachRole.Id, eachRole.Name);
}

set<Id> ownerIdsSet = new set<Id>();
set<Id> freightOwnerIdsSet = new set<Id>();
string freightRoleName = 'Freight';

for(Task eachTask : Trigger.New){
    ownerIdsSet.add(eachTask.OwnerId);
}

system.debug('@@ map @@ ' +  roleIdVsRoleNameMap);

for(User eachUser : [SELECT Id, UserRoleId FROM User WHERE Id IN: ownerIdsSet]){
    if(roleIdVsRoleNameMap.containsKey(eachUser.UserRoleId)){
        if(roleIdVsRoleNameMap.get(eachUser.UserRoleId).contains(freightRoleName)){
            freightOwnerIdsSet.add(eachUser.Id);
        }
    }
}


for(Task t:trigger.new)

{

    //checking if subject contains email:

    if (t.Subject.contains('Email:'))

    {
        if(freightOwnerIdsSet.contains(t.OwnerId)){
            t.Task_type__c = 'Freight'; 
            t.Subtype__c = 'Email/Phone';
        }

        else{
            t.Task_type__c = 'Strategic Account Management'; 
            t.Subtype__c = 'Email/Phone';
        }
        
    }
    
    
    }
    }
    }
    
    //Logic begins for Task log a call id populate on campaign member//
    /* if(Trigger.isAfter){
     if(Trigger.isInsert){
     
    TaskTriggerHandler.ValidateTasks(Trigger.new);
           
           }
           }*/
           
    
    }catch(Exception e){
        System.debug('Error Occured From Task Trigger: '+e.getMessage());
        }
}