/*----------------------------------------------------------------------------------------------------------------------
Author:        Purushotham
Company:       Capgemini
Description:   QCC Case Processing
Inputs:
Test Class:     
************************************************************************************************
History
************************************************************************************************
07-Nov-2017      Purushotham               Initial Design
27-Nov-2017      Praveen Sampath           Method - CaseTriggerUpdateCaseSLA
27-Nov-2017      Praveen Sampath           Method - CaseTriggerCloseCase
-----------------------------------------------------------------------------------------------------------------------*/
trigger CaseTrigger on Case (before insert,before update,after insert,after update) {
    if(Trigger_Status__c.getValues('trgTradeSiteResponses').Active__c){
        //Freight Changes
        Freight_CaseTriggerHandler triggerHandler = new Freight_CaseTriggerHandler();
        if(Trigger.isBefore){
            if(Trigger.isInsert || Trigger.isUpdate){
                triggerHandler.assignEntitlement(Trigger.New, Trigger.OldMap);
            }
            if(Trigger.isInsert){
                triggerHandler.assignEntitlement1(Trigger.New, null);
                triggerHandler.assignRecordTypeWebToCase(Trigger.New);// Assign Freight Recordtypes
                 /* QAC Release 2 Authority Number Generation logic removed from Status field */
                //clsTradesiteHandler.processTradesiteAuthorityNumber(trigger.new);
                //Depricated old name change request coming from old tradeite via QSOA
                //clsTradesiteHandler.processTradesiteAuthorityNumberNew(trigger.new);
            }
            if(Trigger.isUpdate){
                triggerHandler.assignEntitlement1(Trigger.New, Trigger.oldMap);
                triggerHandler.autoChangeStatus(Trigger.New, Trigger.OldMap);
                triggerHandler.autoChangeStatus1(Trigger.New, Trigger.OldMap);
            }
            
        }
        if(Trigger.isAfter){
            if(Trigger.isUpdate){
                triggerHandler.autoMilestonCompletion(Trigger.New, Trigger.OldMap);
                triggerHandler.autoMilestonCompletionforservicerequest(Trigger.New, Trigger.OldMap);
                clsCaseFieldUpdatesHelper.updateAccountRequestPending(Trigger.New, Trigger.oldMap);
            }
            if(Trigger.isInsert){
                //Depricated old name change classes
                //clsTradesiteHandler.processNameChangeRequest(trigger.new);
                
                // to Create Fulfillment record from Custom Meta Data types
                // For Tradesite - Service Request Case Creation by Digital Team
                QAC_caseFulfilmentHandler.autoCreateFulfillmentRecords(trigger.new);
            }
        }
    }
    if(Trigger_Status__c.getValues('trgCaseFieldUpdates').Active__c){
        /* QAC Release 2 Authority Number Generation logic removed from Status field */
            
        //clsCaseFieldUpdatesHelper.generateAuthorityNumber(trigger.new); 
        clsCaseFieldUpdatesHelper.generateAuthorityNumberNew(trigger.new);
    }
    if(Trigger_Status__c.getValues('CaseTriggerPopulateBasicAutoCaseValues').Active__c && Trigger.isBefore && Trigger.isInsert){
        if(Trigger.New[0].Origin == CustomSettingsUtilities.getConfigDataMap('QCC_Red App') || Trigger.New[0].Origin == CustomSettingsUtilities.getConfigDataMap('QCC Qantas')) {
            CaseTriggerHelper.populateBasicAutoCaseValues(Trigger.New[0]);
        }
    }
    
    if(Trigger_Status__c.getValues('updateCaseComplimentTemplateValue').Active__c && Trigger.isBefore && Trigger.isInsert) {
        CaseTriggerHelper.updateCaseComplimentTemplateValue(Trigger.New);
    }
    
    if(Trigger_Status__c.getValues('updateCaseFields').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)) {
        CaseTriggerHelper.updateCaseCalculatedFields(Trigger.New);
    }
    
    if(Trigger_Status__c.getValues('fetchCongaEmailTemplateId').Active__c && Trigger.isBefore){
        System.debug('Inside fetchCongaEmailTemplateId');
        CaseTriggerHelper.fetchCongaEmailTemplateId(Trigger.new,Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('autoCaseProcess').Active__c && Trigger.isAfter && Trigger.isInsert){
        if(Trigger.New[0].Origin == CustomSettingsUtilities.getConfigDataMap('QCC_Red App') || Trigger.New[0].Origin == CustomSettingsUtilities.getConfigDataMap('QCC Qantas')){
            ID jobID = System.enqueueJob(new QCC_CaseCreator(Trigger.new[0], UserInfo.getSessionId()));   
            //QCC_CaseCreator.autoCaseProcess(Trigger.New[0].id);    
        }
    }    
    
    
    
    if(Trigger_Status__c.getValues('assignCaseOwner').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
        CaseTriggerHelper.assignCaseOwner(Trigger.New , Trigger.oldMap);
    }

    
    
    if(Trigger_Status__c.getValues('postToChatterFeed').Active__c &&Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)){
        //CaseTriggerHelper.postToChatterFeed(Trigger.New , Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('CaseTriggerUpdateCaseSLA').Active__c && Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        CaseTriggerHelper.updateCaseSLA(Trigger.New, Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('CaseTriggerUpdateCaseEscalatedTo').Active__c && Trigger.isBefore && Trigger.isUpdate) {
        CaseTriggerHelper.updateCaseEscalatedTo(Trigger.New, Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('SubmitForFinalisation').Active__c && Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        CaseTriggerHelper.submitForFinalisation(Trigger.New, Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('CaseTriggerCalculateEmergencyExpense').Active__c && Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        CaseTriggerHelper.calculateEmergencyExpense(Trigger.New, Trigger.oldMap);
    }
    
    if(Trigger_Status__c.getValues('CaseTriggerupdateContactBlankEmail').Active__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        CaseTriggerHelper.updateContactBlankEmail(Trigger.new);
    }
    
    if(Trigger_Status__c.getValues('CaseTriggerpopulateonCaseCreate').Active__c && Trigger.isBefore && Trigger.isInsert){
        CaseTriggerHelper.populateonCaseCreate(Trigger.new);
    }

    /*if(Trigger_Status__c.getValues('CaseTriggerCloseCaseOnEmailSent').Active__c && Trigger.isBefore && Trigger.isUpdate){
        CaseTriggerHelper.closeCaseOnEmailSent(Trigger.new);
    }*/
    
    if(Trigger_Status__c.getValues('CaseTriggerUpdateCaseOnClose').Active__c && Trigger.isBefore && (Trigger.isInsert ||Trigger.isUpdate)){
        CaseTriggerHelper.updateCaseOnClose(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('caseStatusUpdated').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
        CaseTriggerHelper.caseStatusUpdated(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('CaseTriggerDeleteAffectedPassengers').Active__c && Trigger.isAfter && (Trigger.isUpdate)){
        CaseTriggerHelper.deleteAffectedPassengers(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('SetAuditFieldsOnCase').Active__c && Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)){
        CaseTriggerHelper.setAuditFields(Trigger.new , Trigger.oldMap); 
    }

    if(Trigger_Status__c.getValues('CaseTriggerUpdateTECHFields').Active__c && Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        System.debug('OldMap ^^^^' + Trigger.oldMap);
        CaseTriggerHelper.updateTECHFields(Trigger.New, Trigger.oldMap);
    }

    if(Trigger_Status__c.getValues('CaseTriggerPostNotification').Active__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
        System.debug('OldMap ^^^^' + Trigger.oldMap);
        CaseTriggerHelper.caseNotification(Trigger.newMap, Trigger.oldMap);
    }
    
    if( Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        QCC_CreditCradNoticeHelper.maskCreditCard(Trigger.New);
        QCC_casedeparturedatecalculation.datecalculation(Trigger.new);
    }
    
    if(Trigger_Status__c.getValues('CaseTriggerUpdateRecContact').Active__c && Trigger.isAfter && Trigger.isUpdate) {
        CaseTriggerHelper.updateRecoveryContact(Trigger.new,Trigger.oldMap);
    }

}