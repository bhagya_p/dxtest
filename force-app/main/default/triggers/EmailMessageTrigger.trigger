trigger EmailMessageTrigger on EmailMessage ( before insert,before update,after update,before delete) { 
    try{
        Trigger_Status__c ts = Trigger_Status__c.getValues('EmailMessageHelper');
        if(ts.Active__c) { 
            if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
                //logic for sending to email functionality starts
                for(EmailMessage em :Trigger.new ){
                    if(em.To_Address__c!=null){
                        em.ToAddress =em.To_Address__c;
                        //em.BccAddress=em.To_Address__c;
                        system.debug('to address'+em.ToAddress);
                    }
                    else if(em.To_Addresss__c!=null){                     
                        em.ToAddress =em.To_Addresss__c;
                    }                 
                    system.debug('to address12'+em.ToAddress);                                 
                }
                
                EmailMessageTriggerHelper.UpdateCaseStatus(Trigger.new);
            }
            
            if(Trigger.isDelete && Trigger.isBefore)  {
                EmailMessageTriggerHelper.PreventdeleteEmailMessage(Trigger.old);
            }
        }

        if(Trigger_Status__c.getValues('EmailMessageTriggerLoungePassNonFFM').Active__c && Trigger.isBefore && Trigger.isInsert) {
            //This call is not needed because there is no Contact for Non FF as part of Remediation
            //EmailMessageTriggerHelper.UpdateRecoveryStatusForNonFrequentFlyerMember(Trigger.New);
        }
        
        //credit card masking functionality
        if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
            QCC_EmailMessageCreditCradNoticeHelper.maskCreditCard(trigger.new);
        }
        
    }catch(Exception e){
        System.debug('Error Occured From EmailMessageTrigger: '+e.getMessage());
    }       
}