<apex:page controller="LiveEngage.ChatSyncController" docType="html-5.0" action="{!init}">
	<style>
		.first-input{
			width:0;
			height:0;
			position:absolute;
			right:0;
			top:0;
			border: 0 none !important;
		}
		.first-input:focus {
			outline: none; 
		}
		.bPageBlock {
			margin-bottom: 30px !important;
		}
	</style>
	<apex:sectionHeader title="Setup Sync Jobs" />
	<c:AuthSetup isAuthSetup="{!isAuthSetup}"/>

	<apex:form id="fullForm" rendered="{!isAuthSetup}">
		<apex:pageMessages id="message-area"/>
		<!--
			This is included to prevent the date time boxes from getting the initial focus.  
			That would cause the date time pickers to pop.  It's just a bit on the ugly side.
		-->
		<apex:inputText styleClass="first-input"/>
		<apex:actionFunction name="refreshStatus" action="{!refreshStatus}" />

		<apex:pageBlock title="Manual Sync Job" mode="inlineEdit">
			<apex:pageBlockButtons location="top" >
				<apex:commandButton value="Schedule Manual Job" action="{!scheduleManualJob}"/>
			</apex:pageBlockButtons>
			<apex:pageBlockSection columns="1">
					Run a job that will retreive chats from LiveEngage between any two dates.
			</apex:pageBlockSection>
			<apex:pageBlockSection columns="2">
				<apex:inputField label="Start Time" value="{!manualJobInput.LiveEngage__Chat_Start_Time__c}"/>
				<apex:inputField label="End Time" value="{!manualJobInput.LiveEngage__Chat_End_Time__c}"/>
			</apex:pageBlockSection>
			<apex:pageBlockSection id="chatManualJobStatus" title="Chat Job Run Status" rendered="{!apiStatusMap['Chat'].manualJob!=null}" columns="2" collapsible="false">
				<apex:actionPoller rendered="{!manualJobStatus!='Completed'}" interval="10" action="{!refreshStatus}" 
					rerender="chatManualJobStatus" />
				<apex:outputField rendered="{!apiStatusMap['Chat'].manualJob.Status!='Completed'}" value="{!apiStatusMap['Chat'].manualJob.Status}" />
				<apex:outputField rendered="{!apiStatusMap['Chat'].manualJob.Status=='Completed' && apiStatusMap['Chat'].manualStatus!=null}" 
					value="{!apiStatusMap['Chat'].manualStatus.Status__c}"/>
				<apex:outputField rendered="{!apiStatusMap['Chat'].manualJob.Status=='Completed' && apiStatusMap['Chat'].manualStatus!=null}" 
					value="{!apiStatusMap['Chat'].manualStatus.Last_Complete_Run__c}"/>
			</apex:pageBlockSection>
			<apex:pageBlockSection id="msgManualJobStatus" title="Msg Job Run Status" rendered="{!apiStatusMap['Msg'].manualJob!=null}" columns="2" collapsible="false">
				<apex:actionPoller rendered="{!manualJobStatus!='Completed'}" interval="10" action="{!refreshStatus}" 
					rerender="msgManualJobStatus" />
				<apex:outputField rendered="{!apiStatusMap['Msg'].manualJob.Status!='Completed'}" value="{!apiStatusMap['Msg'].manualJob.Status}" />
				<apex:outputField rendered="{!apiStatusMap['Msg'].manualJob.Status=='Completed' && apiStatusMap['Msg'].manualStatus!=null}" 
					value="{!apiStatusMap['Msg'].manualStatus.Status__c}"/>
				<apex:outputField rendered="{!apiStatusMap['Msg'].manualJob.Status=='Completed' && apiStatusMap['Msg'].manualStatus!=null}" 
					value="{!apiStatusMap['Msg'].manualStatus.Last_Complete_Run__c}"/>
			</apex:pageBlockSection>
		</apex:pageBlock>

		<apex:pageBlock title="Daily Sync Job" mode="inlineEdit">
			<apex:pageBlockButtons location="top" >
				<apex:commandButton rendered="{!dailyTrigger==null}" value="Schedule Daily Job" action="{!scheduleDailyJob}" />
				<apex:commandButton rendered="{!dailyTrigger!=null}" value="Cancel Daily Job" action="{!cancelDailyJob}" />
			</apex:pageBlockButtons>
			<apex:pageBlockSection columns="1">
					Schedule a job that will retreive chats from LiveEngage daily.
			</apex:pageBlockSection>
			<apex:pageBlockSection columns="2">
				<apex:selectList label="Select Hour" multiselect="false" size="1" value="{!dailyHour}">
					<apex:selectOption itemLabel="12 AM" itemValue="0"/>
					<apex:selectOption itemLabel=" 1 AM" itemValue="1"/>
					<apex:selectOption itemLabel=" 2 AM" itemValue="2"/>
					<apex:selectOption itemLabel=" 3 AM" itemValue="3"/>
					<apex:selectOption itemLabel=" 4 AM" itemValue="4"/>
					<apex:selectOption itemLabel=" 5 AM" itemValue="5"/>
					<apex:selectOption itemLabel=" 6 AM" itemValue="6"/>
					<apex:selectOption itemLabel=" 7 AM" itemValue="7"/>
					<apex:selectOption itemLabel=" 8 AM" itemValue="8"/>
					<apex:selectOption itemLabel=" 9 AM" itemValue="9"/>
					<apex:selectOption itemLabel="10 AM" itemValue="10"/>
					<apex:selectOption itemLabel="11 AM" itemValue="11"/>
					<apex:selectOption itemLabel="12 PM" itemValue="12"/>
					<apex:selectOption itemLabel=" 1 PM" itemValue="13"/>
					<apex:selectOption itemLabel=" 2 PM" itemValue="14"/>
					<apex:selectOption itemLabel=" 3 PM" itemValue="15"/>
					<apex:selectOption itemLabel=" 4 PM" itemValue="16"/>
					<apex:selectOption itemLabel=" 5 PM" itemValue="17"/>
					<apex:selectOption itemLabel=" 6 PM" itemValue="18"/>
					<apex:selectOption itemLabel=" 7 PM" itemValue="19"/>
					<apex:selectOption itemLabel=" 8 PM" itemValue="20"/>
					<apex:selectOption itemLabel=" 9 PM" itemValue="21"/>
					<apex:selectOption itemLabel="10 PM" itemValue="22"/>
					<apex:selectOption itemLabel="11 PM" itemValue="23"/>
				</apex:selectList>
			</apex:pageBlockSection>
			<apex:pageBlockSection id="dailyScheduleStatus" rendered="{!dailyTrigger!=null}" title="Job Schedule Status" columns="2" collapsible="false">
				<apex:outputField value="{!dailyTrigger.CronJobDetail.Name}" />
				<apex:outputField value="{!dailyTrigger.State}" />
				<apex:outputField value="{!dailyTrigger.PreviousFireTime}" />
				<apex:outputField value="{!dailyTrigger.NextFireTime}" />
			</apex:pageBlockSection>
			<apex:pageBlockSection id="chatDailyJobStatus" title="Chat Job Run Status" rendered="{!apiStatusMap['Chat'].dailyJob!=null}" columns="2" collapsible="false">
				<apex:actionPoller rendered="{!dailyJobStatus!='Completed'}" interval="10" action="{!refreshStatus}" 
					rerender="chatDailyJobStatus" />
				<apex:outputField rendered="{!apiStatusMap['Chat'].dailyJob.Status!='Completed'}" value="{!apiStatusMap['Chat'].dailyJob.Status}" />
				<apex:outputField rendered="{!apiStatusMap['Chat'].dailyJob.Status=='Completed' && apiStatusMap['Chat'].dailyStatus!=null}" 
					value="{!apiStatusMap['Chat'].dailyStatus.Status__c}"/>
				<apex:outputField rendered="{!apiStatusMap['Chat'].dailyJob.Status=='Completed' && apiStatusMap['Chat'].dailyStatus!=null}" 
					value="{!apiStatusMap['Chat'].dailyStatus.Last_Complete_Run__c}"/>
			</apex:pageBlockSection>

			<apex:pageBlockSection id="msgDailyJobStatus" title="Msg Job Run Status" rendered="{!apiStatusMap['Msg'].dailyJob!=null}" columns="2" collapsible="false">
				<apex:actionPoller rendered="{!dailyJobStatus!='Completed'}" interval="10" action="{!refreshStatus}" 
					rerender="msgDailyJobStatus" />
				<apex:outputField rendered="{!apiStatusMap['Msg'].dailyJob.Status!='Completed'}" value="{!apiStatusMap['Msg'].dailyJob.Status}" />
				<apex:outputField rendered="{!apiStatusMap['Msg'].dailyJob.Status=='Completed' && apiStatusMap['Msg'].dailyStatus!=null}" 
					value="{!apiStatusMap['Msg'].dailyStatus.Status__c}"/>
				<apex:outputField rendered="{!apiStatusMap['Msg'].dailyJob.Status=='Completed' && apiStatusMap['Msg'].dailyStatus!=null}" 
					value="{!apiStatusMap['Msg'].dailyStatus.Last_Complete_Run__c}"/>
			</apex:pageBlockSection>
		</apex:pageBlock>
		<apex:pageBlock title="Periodic Sync Job" mode="inlineEdit">
			<apex:pageBlockButtons location="top" >
				<apex:commandButton rendered="{!periodicTrigger==null}" value="Schedule Periodic Job" action="{!schedulePeriodicJob}" />
				<apex:commandButton rendered="{!periodicTrigger!=null}" value="Cancel Periodic Job" action="{!cancelPeriodicJob}" />
			</apex:pageBlockButtons>
			<apex:pageBlockSection columns="1">
					Schedule a job that will retreive chats from LiveEngage periodically.
			</apex:pageBlockSection>
			<apex:pageBlockSection id="periodicScheduleStatus" rendered="{!periodicTrigger!=null}" title="Job Schedule Status" columns="2" collapsible="false">
				<apex:outputField value="{!periodicTrigger.CronJobDetail.Name}" />
				<apex:outputField Value="{!periodicTrigger.State}" />
				<apex:outputField value="{!periodicTrigger.PreviousFireTime}" />
				<apex:outputField value="{!periodicTrigger.NextFireTime}" />
			</apex:pageBlockSection>
			<apex:pageBlockSection id="chatPeriodicJobStatus" title="Chat Job Run Status" rendered="{!apiStatusMap['Chat'].periodicJob!=null}" columns="2" collapsible="false">
				<apex:actionPoller rendered="{!periodicJobStatus!='Completed'}" interval="10" action="{!refreshStatus}" 
					rerender="chatPeriodicJobStatus" />
				<apex:outputField rendered="{!apiStatusMap['Chat'].periodicJob.Status!='Completed'}" value="{!apiStatusMap['Chat'].periodicJob.Status}" />
				<apex:outputField rendered="{!apiStatusMap['Chat'].periodicJob.Status=='Completed' && apiStatusMap['Chat'].periodicStatus!=null}" 
					value="{!apiStatusMap['Chat'].periodicStatus.Status__c}"/>
				<apex:outputField rendered="{!apiStatusMap['Chat'].periodicJob.Status=='Completed' && apiStatusMap['Chat'].periodicStatus!=null}" 
					value="{!apiStatusMap['Chat'].periodicStatus.Last_Complete_Run__c}"/>
			</apex:pageBlockSection>
			<apex:pageBlockSection id="msgPeriodicJobStatus" title="Msg Job Run Status" rendered="{!apiStatusMap['Msg'].periodicJob!=null}" columns="2" collapsible="false">
				<apex:actionPoller rendered="{!periodicJobStatus!='Completed'}" interval="10" action="{!refreshStatus}" 
					rerender="msgPeriodicJobStatus" />
				<apex:outputField rendered="{!apiStatusMap['Msg'].periodicJob.Status!='Completed'}" value="{!apiStatusMap['Msg'].periodicJob.Status}" />
				<apex:outputField rendered="{!apiStatusMap['Msg'].periodicJob.Status=='Completed' && apiStatusMap['Msg'].periodicStatus!=null}" 
					value="{!apiStatusMap['Msg'].periodicStatus.Status__c}"/>
				<apex:outputField rendered="{!apiStatusMap['Msg'].periodicJob.Status=='Completed' && apiStatusMap['Msg'].periodicStatus!=null}" 
					value="{!apiStatusMap['Msg'].periodicStatus.Last_Complete_Run__c}"/>
			</apex:pageBlockSection>
		</apex:pageBlock>
	</apex:form>
</apex:page>